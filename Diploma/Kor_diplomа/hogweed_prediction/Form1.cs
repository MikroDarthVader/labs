﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace hogweed_prediction
{
    public partial class Form1 : Form
    {
        string dataset_path = @"\Assets\Dataset";
        string predict_path = @"\Assets\Predict";
        string model_checkpoints_path = @"\Assets\Model";
        string python_log_dir = @"\Log";

        string python_path = @"C:\Users\asus\AppData\Local\Programs\Python\Python39\python.exe";
        string train_script = @"D:\Downloads\stud\cbl\progalet\labs\Diploma\Kor_diplomа\hogweed_prediction\Train.py";
        string prediction_script = @"D:\Downloads\stud\cbl\progalet\labs\Diploma\Kor_diplomа\hogweed_prediction\Prediction.py";

        public Form1()
        {
            InitializeComponent();
            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;

            var currDir = Directory.GetCurrentDirectory();
            dataset_path = currDir + dataset_path;
            model_checkpoints_path = currDir + model_checkpoints_path;
            python_log_dir = currDir + python_log_dir;
            predict_path = currDir + predict_path;
        }

        private void button_loadPic_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Filter = "PNG|*.png|JPEG|*.jpg";
            openFileDialog.FilterIndex = 2;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() != DialogResult.OK)
                return;

            pictureBox.Image = new Bitmap(openFileDialog.FileName);
            InitMarkup();
        }

        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            if (pictureBox.Image == null)
                return;

            Graphics g = e.Graphics;
            DrawMarkup(g);
            DrawGrid(g);
        }

        private RectangleF getPicBounds()
        {
            RectangleF bounds;

            float clientWidth = pictureBox.ClientRectangle.Width - 1;
            float clientHeight = pictureBox.ClientRectangle.Height - 1;

            float im_ratio = pictureBox.Image.Width / (float)pictureBox.Image.Height;
            float client_ratio = clientWidth / clientHeight;

            if (im_ratio > client_ratio)
            {
                float scaledHeight = clientWidth / im_ratio;
                bounds = new RectangleF(0, (clientHeight - scaledHeight) / 2,
                    clientWidth, scaledHeight);
            }
            else
            {
                float scaledWidth = clientHeight * im_ratio;
                bounds = new RectangleF((clientWidth - scaledWidth) / 2, 0,
                   scaledWidth, clientHeight);
            }

            return bounds;
        }

        private void DrawGrid(Graphics g)
        {
            if (pictureBox.Image == null)
                return;

            var bounds = getPicBounds();
            float cellSize = bounds.Width / getCellsInWidth();
            Pen p = new Pen(Color.Black);
            float width = bounds.Width + 1;
            float height = bounds.Height + 1;

            for (float y = bounds.Y; y <= bounds.Y + height; y += cellSize)
                g.DrawLine(p, bounds.X, y, bounds.X + (int)(width / cellSize) * cellSize, y);

            for (float x = bounds.X; x <= bounds.X + width; x += cellSize)
                g.DrawLine(p, x, bounds.Y, x, bounds.Y + (int)(height / cellSize) * cellSize);
        }

        float lastCellsInWidth;
        private float getCellsInWidth()
        {
            if (float.TryParse(textBox_picScale.Text, out float tmp))
                lastCellsInWidth = tmp;

            return lastCellsInWidth;
        }

        private void textBox_picScale_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
                return;

            InitMarkup();
            pictureBox.Refresh();
        }

        private void InitMarkup()
        {
            if (pictureBox.Image == null)
                return;

            var bounds = getPicBounds();
            float cellSize = bounds.Width / getCellsInWidth();

            markup = new CellMark[(int)getCellsInWidth()][];
            for (int i = 0; i < markup.Length; i++)
                markup[i] = new CellMark[(int)(bounds.Height / cellSize)];
        }

        private Rectangle GetCellBounds(int x, int y, bool onScreen = true)
        {
            var picBounds = onScreen ? getPicBounds() : new RectangleF(PointF.Empty, pictureBox.Image.Size);
            float cellSize = picBounds.Width / getCellsInWidth();

            return new Rectangle((int)(picBounds.X + x * cellSize), (int)(picBounds.Y + y * cellSize), (int)cellSize, (int)cellSize);
        }

        private Point getCellId(Point innerPoint)
        {
            var picBounds = getPicBounds();
            float cellSize = picBounds.Width / getCellsInWidth();

            innerPoint.X = (int)((innerPoint.X - (int)picBounds.X) / cellSize);
            innerPoint.Y = (int)((innerPoint.Y - (int)picBounds.Y) / cellSize);
            return innerPoint;
        }

        private enum CellMark
        {
            available,
            hogweed,
            unavailable
        }

        private CellMark[][] markup;

        private void DrawMarkup(Graphics g)
        {
            for (int x = 0; x < markup.Length; x++)
            {
                for (int y = 0; y < markup[x].Length; y++)
                {
                    Color markupCol = Color.Black;//will never be black
                    switch (markup[x][y])
                    {
                        case CellMark.available:
                            markupCol = Color.FromArgb(60, Color.GreenYellow);
                            break;
                        case CellMark.unavailable:
                            markupCol = Color.FromArgb(60, Color.Black);
                            break;
                        case CellMark.hogweed:
                            markupCol = Color.FromArgb(60, Color.Red);
                            break;
                    }

                    g.FillRectangle(new SolidBrush(markupCol), GetCellBounds(x, y));
                }
            }
        }

        private void pictureBox_mouseListener(MouseEventArgs e)
        {
            if (pictureBox.Image == null || e.Button != MouseButtons.Left)
                return;

            var markupPoint = getCellId(e.Location);

            if (markupPoint.X >= markup.Length || markupPoint.Y >= markup[0].Length || markupPoint.X < 0 || markupPoint.Y < 0)
                return;

            markup[markupPoint.X][markupPoint.Y] = ParseCellMarkRus(comboBox_markingColor.Text);
            pictureBox.Refresh();
        }

        private void pictureBox_MouseDown(object sender, MouseEventArgs e) { pictureBox_mouseListener(e); }
        private void pictureBox_MouseMove(object sender, MouseEventArgs e) { pictureBox_mouseListener(e); }

        private CellMark ParseCellMarkRus(string str)
        {
            switch (str)
            {
                case "Борщевик": return CellMark.hogweed;
                case "Недоступен": return CellMark.unavailable;
                default: return CellMark.available;
            }
        }

        private void button_savePic_Click(object sender, EventArgs e)
        {
            if (pictureBox.Image == null)
                return;

            var bmp = new Bitmap(pictureBox.Image);
            int pic_hash = getImageHash(bmp);

            RemovePicFromDataset(bmp);

            for (int x = 0; x < markup.Length; x++)
            {
                for (int y = 0; y < markup[x].Length; y++)
                {
                    var cellBounds = GetCellBounds(x, y, false);
                    var cellBmp = bmp.Clone(cellBounds, bmp.PixelFormat);

                    string filepath = dataset_path + "\\" + markup[x][y] + "\\";
                    string filename = pic_hash + "_" + x + "_" + y + ".png";

                    if (!Directory.Exists(filepath))
                        Directory.CreateDirectory(filepath);
                    cellBmp.Save(filepath + filename);
                }
            }
        }

        private int getImageHash(Bitmap img)
        {
            byte[] SS = new byte[1];
            SS = (byte[])new ImageConverter().ConvertTo(img, SS.GetType());

            SHA256Managed hash = new SHA256Managed();
            return BitConverter.ToInt32(hash.ComputeHash(SS), 0);
        }

        private void RemovePicFromDataset(Bitmap bmp)
        {
            if (!Directory.Exists(dataset_path))
                return;

            var filereg = getImageHash(bmp) + "*.png";
            string[] filenames = new string[0];
            foreach (var dir in Directory.GetDirectories(dataset_path))
                filenames = filenames.Concat(Directory.GetFiles(dir, filereg)).ToArray();
            foreach (var fn in filenames)
                File.Delete(fn);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (pictureBox.Image == null)
                return;

            RemovePicFromDataset(new Bitmap(pictureBox.Image));
        }

        private string ExecutePythonProcess(string scriptPath, bool hideWindow, params string[] args)
        {
            Process proc = new Process();
            proc.StartInfo.FileName = python_path;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.CreateNoWindow = hideWindow;
            proc.StartInfo.UseShellExecute = false;

            proc.StartInfo.Arguments = scriptPath;
            foreach (var arg in args)
                proc.StartInfo.Arguments += " " + arg;
            proc.Start();

            StreamReader sReader = proc.StandardOutput;
            var output = sReader.ReadToEnd();

            proc.WaitForExit();
            return output;
        }

        private void SaveLog(string process_name, string log)
        {
            string fileName = @"\" + process_name + " (" + DateTime.Now.ToString().Replace(':', '_').Replace('.', '_') + ").log";

            if (!Directory.Exists(python_log_dir))
                Directory.CreateDirectory(python_log_dir);

            File.WriteAllText(python_log_dir + fileName, log);
        }

        private void button_predict_Click(object sender, EventArgs e)
        {
            if (pictureBox.Image == null)
                return;

            if (!Directory.Exists(predict_path))
                Directory.CreateDirectory(predict_path);

            var bmp = new Bitmap(pictureBox.Image);
            int pic_hash = getImageHash(bmp);

            for (int x = 0; x < markup.Length; x++)
            {
                for (int y = 0; y < markup[x].Length; y++)
                {
                    var cellBmp = bmp.Clone(GetCellBounds(x, y, false), bmp.PixelFormat);

                    string filename = @"\" + string.Format("{0:00000}", x * markup[0].Length + y) + ".png";

                    cellBmp.Save(predict_path + filename);
                }
            }

            var log = ExecutePythonProcess(prediction_script, false, predict_path, model_checkpoints_path);
            SaveLog("Prediction", log);

            var result = log.Substring(log.Length - markup.Length * markup[0].Length);

            for (int i = 0; i < result.Length; i++)
                markup[i / markup[0].Length][i % markup[0].Length] = (CellMark)(result[i] - 48);

            foreach (var fn in Directory.GetFiles(predict_path))
                File.Delete(fn);

            pictureBox.Refresh();
        }

        private void button_train_Click(object sender, EventArgs e)
        {
            SaveLog("Train", ExecutePythonProcess(train_script, false, dataset_path, model_checkpoints_path));
        }
    }
}
