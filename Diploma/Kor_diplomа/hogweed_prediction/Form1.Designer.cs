﻿
namespace hogweed_prediction
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_predict = new System.Windows.Forms.Button();
            this.button_savePic = new System.Windows.Forms.Button();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.button_loadPic = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_picScale = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox_markingColor = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_predictionRange = new System.Windows.Forms.TextBox();
            this.button_train = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox_reproduction = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_distance = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button_spread = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_predict
            // 
            this.button_predict.Location = new System.Drawing.Point(7, 19);
            this.button_predict.Name = "button_predict";
            this.button_predict.Size = new System.Drawing.Size(180, 23);
            this.button_predict.TabIndex = 7;
            this.button_predict.Text = "Распознать";
            this.button_predict.UseVisualStyleBackColor = true;
            this.button_predict.Click += new System.EventHandler(this.button_predict_Click);
            // 
            // button_savePic
            // 
            this.button_savePic.Location = new System.Drawing.Point(6, 72);
            this.button_savePic.Name = "button_savePic";
            this.button_savePic.Size = new System.Drawing.Size(181, 23);
            this.button_savePic.TabIndex = 6;
            this.button_savePic.Text = "Сохранить в выборке";
            this.button_savePic.UseVisualStyleBackColor = true;
            this.button_savePic.Click += new System.EventHandler(this.button_savePic_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox.Location = new System.Drawing.Point(211, 12);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(577, 426);
            this.pictureBox.TabIndex = 5;
            this.pictureBox.TabStop = false;
            this.pictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_Paint);
            this.pictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseDown);
            this.pictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseMove);
            // 
            // button_loadPic
            // 
            this.button_loadPic.Location = new System.Drawing.Point(6, 19);
            this.button_loadPic.Name = "button_loadPic";
            this.button_loadPic.Size = new System.Drawing.Size(181, 23);
            this.button_loadPic.TabIndex = 4;
            this.button_loadPic.Text = "Загрузить изображение";
            this.button_loadPic.UseVisualStyleBackColor = true;
            this.button_loadPic.Click += new System.EventHandler(this.button_loadPic_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button_loadPic);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(193, 47);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Загрузить изображение";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.textBox_picScale);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.comboBox_markingColor);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.button_savePic);
            this.groupBox2.Location = new System.Drawing.Point(12, 65);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(193, 129);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Управление выборкой";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Плотность клеток";
            // 
            // textBox_picScale
            // 
            this.textBox_picScale.Location = new System.Drawing.Point(108, 19);
            this.textBox_picScale.Name = "textBox_picScale";
            this.textBox_picScale.Size = new System.Drawing.Size(79, 20);
            this.textBox_picScale.TabIndex = 8;
            this.textBox_picScale.Text = "30";
            this.textBox_picScale.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_picScale_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Цвет маркера";
            // 
            // comboBox_markingColor
            // 
            this.comboBox_markingColor.FormattingEnabled = true;
            this.comboBox_markingColor.Items.AddRange(new object[] {
            "Доступен",
            "Недоступен",
            "Борщевик"});
            this.comboBox_markingColor.Location = new System.Drawing.Point(108, 45);
            this.comboBox_markingColor.Name = "comboBox_markingColor";
            this.comboBox_markingColor.Size = new System.Drawing.Size(79, 21);
            this.comboBox_markingColor.TabIndex = 0;
            this.comboBox_markingColor.Text = "Доступен";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 101);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(181, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Удалить из выборки";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button_train);
            this.groupBox3.Controls.Add(this.button_predict);
            this.groupBox3.Location = new System.Drawing.Point(12, 200);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(193, 76);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Распознавание";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Срок прогноза (лет)";
            // 
            // textBox_predictionRange
            // 
            this.textBox_predictionRange.Location = new System.Drawing.Point(128, 19);
            this.textBox_predictionRange.Name = "textBox_predictionRange";
            this.textBox_predictionRange.Size = new System.Drawing.Size(58, 20);
            this.textBox_predictionRange.TabIndex = 12;
            this.textBox_predictionRange.Text = "0";
            // 
            // button_train
            // 
            this.button_train.Location = new System.Drawing.Point(6, 48);
            this.button_train.Name = "button_train";
            this.button_train.Size = new System.Drawing.Size(181, 23);
            this.button_train.TabIndex = 7;
            this.button_train.Text = "Обучить распознавание";
            this.button_train.UseVisualStyleBackColor = true;
            this.button_train.Click += new System.EventHandler(this.button_train_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button_spread);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.textBox_distance);
            this.groupBox4.Controls.Add(this.textBox_reproduction);
            this.groupBox4.Controls.Add(this.textBox_predictionRange);
            this.groupBox4.Location = new System.Drawing.Point(12, 282);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(193, 126);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Прогнозирование";
            // 
            // textBox_reproduction
            // 
            this.textBox_reproduction.Location = new System.Drawing.Point(128, 45);
            this.textBox_reproduction.Name = "textBox_reproduction";
            this.textBox_reproduction.Size = new System.Drawing.Size(58, 20);
            this.textBox_reproduction.TabIndex = 12;
            this.textBox_reproduction.Text = "3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Распростряняемость";
            // 
            // textBox_distance
            // 
            this.textBox_distance.Location = new System.Drawing.Point(128, 71);
            this.textBox_distance.Name = "textBox_distance";
            this.textBox_distance.Size = new System.Drawing.Size(58, 20);
            this.textBox_distance.TabIndex = 12;
            this.textBox_distance.Text = "2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Расстояние";
            // 
            // button_spread
            // 
            this.button_spread.Location = new System.Drawing.Point(7, 97);
            this.button_spread.Name = "button_spread";
            this.button_spread.Size = new System.Drawing.Size(180, 23);
            this.button_spread.TabIndex = 14;
            this.button_spread.Text = "Прогнозировать";
            this.button_spread.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox);
            this.MinimumSize = new System.Drawing.Size(816, 489);
            this.Name = "Form1";
            this.Text = "Распознавание и прогнозирование борщевика Сосновского";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_predict;
        private System.Windows.Forms.Button button_savePic;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button button_loadPic;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBox_markingColor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_picScale;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_predictionRange;
        private System.Windows.Forms.Button button_train;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_distance;
        private System.Windows.Forms.TextBox textBox_reproduction;
        private System.Windows.Forms.Button button_spread;
    }
}

