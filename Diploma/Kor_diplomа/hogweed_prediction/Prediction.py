import os
import sys

import numpy as np
import tensorflow as tf

import pathlib
pics_path = sys.argv[1]
checkpoint_path = sys.argv[2] + "/model_checkpoint.ckpt"
pics_dir = pathlib.Path(pics_path)

batch_size = 32
img_height = 50
img_width = 50

import model_creator
model = model_creator.CreateModel()

model.load_weights(checkpoint_path)

print('model assembled')

image_count = len(list(pics_dir.glob('*')))
print('image_count: ', image_count)
print(pics_dir)

list_ds = tf.data.Dataset.list_files(str(pics_dir / '*'), shuffle=False)

def decode_img(file_path):
    file_data = tf.io.read_file(file_path)
    img = tf.image.decode_png(file_data, channels=3)
    img = tf.image.resize(img, [img_height, img_width])
    return img, file_path

list_ds = list_ds.map(decode_img, num_parallel_calls=tf.data.AUTOTUNE)

def configure_for_performance(ds):
  ds = ds.cache()
  ds = ds.prefetch(buffer_size=tf.data.AUTOTUNE)
  return ds

list_ds = configure_for_performance(list_ds)

print('Prediction:')

for img, filepath in list_ds:
    print(np.argmax(model.predict(np.expand_dims(img, axis=0))), end = '')
