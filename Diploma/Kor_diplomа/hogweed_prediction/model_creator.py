import tensorflow as tf
from tensorflow.keras import layers

def CreateModel():
    model = tf.keras.Sequential([
      layers.experimental.preprocessing.Rescaling(1. / 255),
      layers.Conv2D(32, 3, activation='relu'),
      layers.MaxPooling2D(),
      layers.Conv2D(64, 3, activation='relu'),
      layers.MaxPooling2D(),
      layers.Flatten(),
      layers.Dense(128, activation='relu'),
      layers.Dense(3)])

    model.compile(optimizer='adam',
      loss=tf.losses.SparseCategoricalCrossentropy(from_logits=True),
      metrics=['accuracy'])
    return model