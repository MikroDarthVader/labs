os = 10;
damping_ratio = (-log(os/100))/(sqrt(pi^2 + log(os/100)^2));
display(damping_ratio);

dr = 0.848;
overshoot = exp(-(dr*pi/(sqrt(1-dr^2)))) * 100;
display(overshoot);