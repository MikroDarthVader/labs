clear
clc
close all;

fid = fopen('D:\folder\fucking_learning\labs\Diploma\Nik_diploma\Data\tf_performance.txt', 'r');
h=strings([8,1]);
t=strings([8,1]);
e=strings([8,1]);
tline = fgetl(fid);

i = 3;
while ischar(tline)
    if (mod(i, 3) == 0)
        h(floor(i/3)) = tline;
    elseif (mod(i, 3) == 1)
        t(floor(i/3)) = tline;
    else
        e(floor(i/3)) = tline;
    end
    
    tline = fgetl(fid);
    i  = i + 1;
end

fclose(fid);

h = split(h);
t = split(t);
e = split(e);

h = str2double(h);
t = str2double(t);
e = str2double(e);

display(h);
display(t);
display(e);

figure;
loglog(e(1,:), t(1,:));hold on
loglog(e(2,:), t(2,:));
loglog(e(3,:), t(3,:));
loglog(e(4,:), t(4,:));
loglog(e(5,:), t(5,:));
loglog(e(6,:), t(6,:));
loglog(e(7,:), t(7,:));
loglog(e(8,:), t(8,:),'-+');
grid on
grid minor
legend('Explicit euler','Implicit euler', 'Trapezoidal', 'Explicit middle point', 'D', 'CD', 'Semi-implicit middle point', 'Semi-implicit euler');
xlabel('Truncation error');
ylabel('Elapsed time');

figure;
loglog(h(1,:), e(1,:));hold on
loglog(h(2,:), e(2,:));
loglog(h(3,:), e(3,:));
loglog(h(4,:), e(4,:));
loglog(h(5,:), e(5,:));
loglog(h(6,:), e(6,:));
loglog(h(7,:), e(7,:));
loglog(h(8,:), e(8,:),'-+');
grid on
grid minor
legend('Explicit euler','Implicit euler', 'Trapezoidal', 'Explicit middle point', 'D', 'CD', 'Semi-implicit middle point', 'Semi-implicit euler');
xlabel('h');
ylabel('Truncation error');

