clear
clc
close all

% mu = [0.0000558208, -0.0000747752, -0.0000444781, -0.0001116422];
% sigma = [0.0003425355, 0.0003778956, 0.0005559408, 0.0005890183];
% 
% x = -0.22:0.001:0.22;
% 
% figure;
% for i = 1:4
%     plot(x, normpdf(x, abs(mu(i)*100), sigma(i)*100)); hold on
% end
% xlabel('Error');
% ylabel('Probability');
% grid on
% grid minor
% legend('direct programming', 'parallel programming', 'sequential programming', 'joint integration');

%%%%%%%%%%%%%%%%%%
mu = [-0.0000543479, -0.0000546892, -0.0000582496]*100;
sigma = [0.0004073832, 0.0004075058, 0.0004367303]*100;

x = -0.2:0.001:0.2;

figure;
    plot(x, normpdf(x, mu(1), sigma(1))); hold on
    plot(x, normpdf(x, mu(2), sigma(2)),'--'); hold on
    plot(x, normpdf(x, mu(3), sigma(3))); hold on
xlabel('Error');
ylabel('Probability');
grid on
grid minor
legend('direct programming', 'sequential programming', 'joint integration');