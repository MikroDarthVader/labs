clear
clc
close all;

fid = fopen('D:\folder\fucking_learning\labs\Diploma\Nik_diploma\Data\compensator_performance.txt', 'r');
h=strings([4,1]);
t=strings([4,1]);
e=strings([4,1]);
tline = fgetl(fid);

i = 3;
while ischar(tline)
    if (mod(i, 3) == 0)
        h(floor(i/3)) = tline;
    elseif (mod(i, 3) == 1)
        t(floor(i/3)) = tline;
    else
        e(floor(i/3)) = tline;
    end
    
    tline = fgetl(fid);
    i  = i + 1;
end

fclose(fid);

h = split(h);
t = split(t);
e = split(e);

h = str2double(h);
t = str2double(t);
e = str2double(e);

display(h);
display(t);
display(e);

figure;
loglog(e(1,:), t(1,:));hold on
loglog(e(2,:), t(2,:), '-+');
loglog(e(3,:), t(3,:));
loglog(e(4,:), t(4,:));
grid on
grid minor
legend('Explicit euler','Implicit euler', 'Trapezoidal', 'Explicit middle point');
xlabel('Truncation error');
ylabel('Elapsed time');

figure;
loglog(h(1,:), e(1,:));hold on
loglog(h(2,:), e(2,:));
loglog(h(3,:), e(3,:));
loglog(h(4,:), e(4,:));
grid on
grid minor
legend('Explicit euler','Implicit euler', 'Trapezoidal', 'Explicit middle point');
xlabel('h');
ylabel('Truncation error');
