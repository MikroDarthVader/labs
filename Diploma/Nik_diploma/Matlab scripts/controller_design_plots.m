clear
clc
close all


s = tf('s');
plant = (160*s^2+448*s+235.2)/(s^4+5.03*s^3+40.21*s^2+1.5*s+2.39999);
compensator = (s+5.35567)/(s+40.7539);

sys1 = feedback(3.9538*compensator*plant, 1);
step(sys1);

%%%%%%%%%%%%%%
% k = 0.01:0.01:1;
% rt = zeros(1, 100);
% os = zeros(1, 100);
% for i = 1:100
%     sys = feedback(k(i)*plant, 1);
%     si = stepinfo(sys);
%     rt(i) = si.RiseTime;
%     os(i) = si.Overshoot;
% end

% figure;
% plot(k, rt); hold on
% plot([0, 1], [1, 1]);
% grid on
% grid minor
% legend('Rise time','Required rise time');
% xlabel('K');
% ylabel('Rise time');
% 
% figure;
% plot(k, os); hold on
% plot([0 1], [10 10]);
% grid on
% grid minor
% ylim([0 70]);
% legend('Overshoot','Required overshoot');
% xlabel('K');
% ylabel('Overshoot');

%%%%%%%%%%%%%%%%%
% figure;
% bode(plant); hold on
% bode(compensator); hold on
% bode(compensator*plant); hold on
% legend('Plant','Compensator','Applied compensator');

%%%%%%%%%%%%%
% k = 0.01:0.01:10;
% rt = zeros(1, 1000);
% os = zeros(1, 1000);
% for i = 1:1000
%     sys = feedback(k(i)*compensator*plant, 1);
%     si = stepinfo(sys);
%     rt(i) = si.RiseTime;
%     os(i) = si.Overshoot;
% end
% 
% figure;
% plot(k, rt); hold on
% plot([0, 10], [1, 1]);
% grid on
% grid minor
% legend('Rise time','Required rise time');
% xlabel('K');
% ylabel('Rise time');
% 
% figure;
% plot(k, os); hold on
% plot([0 10], [10 10]);
% grid on
% grid minor
% ylim([0 70]);
% legend('Overshoot','Required overshoot');
% xlabel('K');
% ylabel('Overshoot');