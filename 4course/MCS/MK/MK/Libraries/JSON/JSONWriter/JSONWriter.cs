﻿using System.Collections.Generic;
using System.IO;

namespace Libraries.JSON
{
    public static class JSONWriter
    {
        public static JSONDataSetToWrite GetDataStoreRoot(bool isArray = false)
        {
            return new JSONDataSetToWrite(isArray);
        }

        public static void WriteToFile(string fileName, JSONDataSetToWrite root)
        {
            StringWriter fileData = new StringWriter();
            LinkedList<LinkedListNode<JSONDataToWrite>> wayToCurr = new LinkedList<LinkedListNode<JSONDataToWrite>>();
            wayToCurr.AddLast(new LinkedListNode<JSONDataToWrite>(JSONDataToWrite.Create(null, root)));

            while (wayToCurr.Count != 0)
            {
                var data = wayToCurr.Last.Value?.Value;

                fileData.Write(Indent(wayToCurr.Count - 1) + (data != null ? DataStr(data) : ""));

                if (data != null && data.containDataSet)
                {
                    wayToCurr.AddLast(data.dataSet.listOfData.First);
                    fileData.WriteLine();
                    continue;
                }
                else if (wayToCurr.Last.Value?.Next == null)
                {
                    fileData.WriteLine();
                    wayToCurr.RemoveLast();

                    while (wayToCurr.Count != 0 && wayToCurr.Last.Value.Next == null)
                    {
                        fileData.WriteLine(Indent(wayToCurr.Count - 1) + CloseBracket(wayToCurr.Last.Value.Value.dataSet));
                        wayToCurr.RemoveLast();
                    }

                    if (wayToCurr.Count != 0)
                    {
                        fileData.WriteLine(Indent(wayToCurr.Count - 1) + CloseBracket(wayToCurr.Last.Value.Value.dataSet) + ",\n");
                        wayToCurr.Last.Value = wayToCurr.Last.Value.Next;
                    }
                    continue;
                }
                else
                {
                    wayToCurr.Last.Value = wayToCurr.Last.Value.Next;
                    fileData.WriteLine(',');
                    continue;
                }
            }

            StreamWriter writer = new StreamWriter(fileName);
            writer.Write(fileData);
            writer.Close();

            fileData.Close();
        }

        private static string DataStr(JSONDataToWrite data)
        {
            if (data.containDataSet)
                return (data.name != null ? "\"" + data.name + "\": " : "") + OpenBracket(data.dataSet);
            else
                return (data.name != null ? "\"" + data.name + "\": " : "") + data.content;
        }

        private static char OpenBracket(JSONDataSetToWrite dataSet)
        {
            return dataSet.isArray ? '[' : '{';
        }

        private static char CloseBracket(JSONDataSetToWrite dataSet)
        {
            return dataSet.isArray ? ']' : '}';
        }

        private static string Indent(int depth)
        {
            string result = "";
            for (int i = 0; i < depth; i++)
                result += '\t';
            return result;
        }
    }
}
