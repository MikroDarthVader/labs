﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libraries.JSON
{
    public class JSONDataToWrite
    {
        public readonly string name;
        public readonly string content;

        public readonly bool containDataSet;
        public readonly JSONDataSetToWrite dataSet;

        private JSONDataToWrite(string name, JSONDataSetToWrite dataSet)
        {
            containDataSet = true;
            this.name = name;
            this.dataSet = dataSet;
        }

        private JSONDataToWrite(string name, string content)
        {
            containDataSet = false;
            this.name = name;
            this.content = content;
        }

        public static JSONDataToWrite Create<T>(string name, T content)
        {
            JSONDataToWrite result;

            if (content is JSONDataSetToWrite)
                result = new JSONDataToWrite(name, content as JSONDataSetToWrite);
            else
                result = new JSONDataToWrite(name, ContentToJSONString(content));

            return result;
        }

        private static string ContentToJSONString<T>(T content)
        {
            if (typeof(T) == typeof(bool))
                return content.ToString().ToLower();
            if (typeof(T) == typeof(string))
                return "\"" + content.ToString() + "\"";
            if (typeof(T) == typeof(char))
                return "\"" + content.ToString() + "\"";

            return content.ToString();
        }
    }
}
