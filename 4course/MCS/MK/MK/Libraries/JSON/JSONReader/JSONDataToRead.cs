﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libraries.JSON
{
    public class JSONDataToReadInvalidCastException : JSONException
    {
        public JSONDataToReadInvalidCastException(JSONDataToRead data, Type requiredType) :
            base("Can't cast data '" + data.content + "' to '" + requiredType + "'")
        { }
    }

    public class JSONDataToRead : IEnumerable
    {
        public string name;
        public string content;

        private object cache;

        public JSONDataToRead(string name, string content)
        {
            this.name = name;
            this.content = content;

            cache = null;
        }

        public JSONDataToRead this[string Key]
        {
            get => GetValue<JSONDataSetToRead>()[Key];
        }

        public JSONDataToRead this[int index]
        {
            get => GetValue<JSONDataSetToRead>()[index];
        }

        public bool TryGetData<T>(string Key, out T data)
        {
            bool success = GetValue<JSONDataSetToRead>().TryGetData(Key, out var jsonData);
            if (success)
                data = jsonData.GetValue<T>();
            else
                data = default;

            return success;
        }

        public IEnumerator GetEnumerator()
        {
            return GetValue<JSONDataSetToRead>().GetEnumerator();
        }

        public T GetValue<T>()
        {
            T result;

            if (cache != null && cache is T)
                return (T)cache;

            string contentData = content;
            if (typeof(T) == typeof(float) || typeof(T) == typeof(double))
            {
                int index = contentData.IndexOf('.');
                if (index != -1)
                {
                    contentData = contentData.Remove(index, 1);
                    contentData = contentData.Insert(index, ",");
                }
            }

            if (typeof(T) == typeof(int) && int.TryParse(contentData, out var tmp1))
                result = (T)(object)tmp1;
            else if (typeof(T) == typeof(uint) && uint.TryParse(contentData, out var tmp2))
                result = (T)(object)tmp2;
            else if (typeof(T) == typeof(float) && float.TryParse(contentData, out var tmp3))
                result = (T)(object)tmp3;
            else if (typeof(T) == typeof(double) && double.TryParse(contentData, out var tmp4))
                result = (T)(object)tmp4;
            else if (typeof(T) == typeof(bool) && bool.TryParse(contentData, out var tmp5))
                result = (T)(object)tmp5;
            else if (typeof(T) == typeof(JSONDataSetToRead))
                result = (T)(object)JSONReader.Read(contentData);
            else throw new JSONDataToReadInvalidCastException(this, typeof(T));

            cache = result;
            return result;
        }
    }
}
