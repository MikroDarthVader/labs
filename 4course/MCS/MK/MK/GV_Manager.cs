﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Libraries;

namespace MK
{
    public static class GV_Manager
    {
        public readonly static _GV GV = new _GV();
        public readonly static _GV GV_Buffer = new _GV();

        private static bool Writing = false;

        public static void StartWriting()
        {
            if (Writing)
                throw new Exception("Writing already started");

            Writing = true;
            GV_Buffer.Clear();
        }

        public static void AbortWriting()
        {
            if (!Writing)
                throw new Exception("Writing not started");

            Writing = false;
        }

        public static void EndWriting()
        {
            if (!Writing)
                throw new Exception("Writing not started");

            Helpers.Swap(ref GV.CD, ref GV_Buffer.CD);
            Helpers.Swap(ref GV.R_Data, ref GV_Buffer.R_Data);
            Helpers.Swap(ref GV.C_Data, ref GV_Buffer.C_Data);
            Helpers.Swap(ref GV.L_Data, ref GV_Buffer.L_Data);

            Writing = false;
        }
    }
}
