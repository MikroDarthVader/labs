﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK
{
    public class _GV
    {
        public class _CD
        {
            public uint NV;
            public uint NR;
            public uint NC;
            public uint NL;
        }

        public class _Calc
        {
            public uint Node_in_p;
            public uint Node_in_m;
            public uint Node_out_p;
            public uint Node_out_m;

            public List<float> Freq;

            public _Calc()
            {
                Freq = new List<float>();
            }
        }

        public _CD CD;
        public _Calc Calc;

        public List<_R_Data> R_Data;
        public List<_C_Data> C_Data;
        public List<_L_Data> L_Data;

        public _GV()
        {
            CD = new _CD();
            Calc = new _Calc();

            R_Data = new List<_R_Data>();
            C_Data = new List<_C_Data>();
            L_Data = new List<_L_Data>();
        }

        public void Clear()
        {
            R_Data.Clear();
            C_Data.Clear();
            L_Data.Clear();

            Calc.Freq.Clear();
        }
    }

    public class _R_Data
    {
        public  uint[] In;
        public float Z;

        public _R_Data()
        {
            In = new uint[2];
        }

        public _R_Data(uint in_0, uint in_1, float z) : this()
        {
            In[0] = in_0;
            In[1] = in_1;
            Z = z;
        }
    }

    public class _C_Data
    {
        public uint[] In;
        public float Z;

        public _C_Data()
        {
            In = new uint[2];
        }

        public _C_Data(uint in_0, uint in_1, float z) : this()
        {
            In[0] = in_0;
            In[1] = in_1;
            Z = z;
        }
    }

    public class _L_Data
    {
        public uint[] In;
        public float Z;

        public _L_Data()
        {
            In = new uint[2];
        }

        public _L_Data(uint in_0, uint in_1, float z) : this()
        {
            In[0] = in_0;
            In[1] = in_1;
            Z = z;
        }
    }
}
