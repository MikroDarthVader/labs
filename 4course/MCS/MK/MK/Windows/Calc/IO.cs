﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MK.Windows.Calc
{
    public partial class IO : Form
    {
        public IO()
        {
            InitializeComponent();
        }

        private uint CanParseAllVars;

        private void IDC_IOOK_BUTTON_Click(object sender, EventArgs e)
        {
            if (CanParseAllVars != 0)
                return;

            var GV = GV_Manager.GV;

            GV.Calc.Node_in_p = uint.Parse(m_lp.Text);
            GV.Calc.Node_in_m = uint.Parse(m_lm.Text);
            GV.Calc.Node_out_p = uint.Parse(m_kp.Text);
            GV.Calc.Node_out_m = uint.Parse(m_km.Text);
            Close();
        }

        private void UIntCheck(TextBox textBox, int index)
        {
            if (!uint.TryParse(textBox.Text, out _))
            {
                CanParseAllVars |= (1u << index);
                textBox.ForeColor = Color.FromArgb(210, 15, 0);
            }
            else
            {
                CanParseAllVars &= ~(1u << index);
                textBox.ForeColor = SystemColors.WindowText;
            }
        }

        private void m_lp_TextChanged(object sender, EventArgs e)
        {
            UIntCheck(m_lp, 0);
        }

        private void m_lm_TextChanged(object sender, EventArgs e)
        {
            UIntCheck(m_lm, 1);
        }

        private void m_kp_TextChanged(object sender, EventArgs e)
        {
            UIntCheck(m_kp, 2);
        }

        private void m_km_TextChanged(object sender, EventArgs e)
        {
            UIntCheck(m_km, 3);
        }
    }
}
