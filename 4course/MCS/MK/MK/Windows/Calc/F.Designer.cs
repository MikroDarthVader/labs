﻿namespace MK.Windows.Calc
{
    partial class F
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.IDC_T3 = new System.Windows.Forms.Label();
            this.IDC_F3 = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.IDC_T2 = new System.Windows.Forms.Label();
            this.IDC_F2 = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.IDC_T1 = new System.Windows.Forms.Label();
            this.IDC_F1 = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.IDC_KF = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.IDC_DF = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.IDC_F = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.IDC_FOK_BUTTON = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 6);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(39, 9);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(30, 0, 30, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(303, 295);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.IDC_T3);
            this.panel6.Controls.Add(this.IDC_F3);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 210);
            this.panel6.Margin = new System.Windows.Forms.Padding(0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(303, 42);
            this.panel6.TabIndex = 5;
            // 
            // IDC_T3
            // 
            this.IDC_T3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IDC_T3.AutoSize = true;
            this.IDC_T3.Location = new System.Drawing.Point(3, 22);
            this.IDC_T3.Name = "IDC_T3";
            this.IDC_T3.Size = new System.Drawing.Size(0, 13);
            this.IDC_T3.TabIndex = 5;
            // 
            // IDC_F3
            // 
            this.IDC_F3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDC_F3.Location = new System.Drawing.Point(200, 19);
            this.IDC_F3.Name = "IDC_F3";
            this.IDC_F3.Size = new System.Drawing.Size(100, 20);
            this.IDC_F3.TabIndex = 3;
            this.IDC_F3.Text = "0";
            this.IDC_F3.Visible = false;
            this.IDC_F3.TextChanged += new System.EventHandler(this.IDC_F3_TextChanged);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.IDC_T2);
            this.panel5.Controls.Add(this.IDC_F2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 168);
            this.panel5.Margin = new System.Windows.Forms.Padding(0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(303, 42);
            this.panel5.TabIndex = 4;
            // 
            // IDC_T2
            // 
            this.IDC_T2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IDC_T2.AutoSize = true;
            this.IDC_T2.Location = new System.Drawing.Point(3, 22);
            this.IDC_T2.Name = "IDC_T2";
            this.IDC_T2.Size = new System.Drawing.Size(0, 13);
            this.IDC_T2.TabIndex = 4;
            // 
            // IDC_F2
            // 
            this.IDC_F2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDC_F2.Location = new System.Drawing.Point(200, 19);
            this.IDC_F2.Name = "IDC_F2";
            this.IDC_F2.Size = new System.Drawing.Size(100, 20);
            this.IDC_F2.TabIndex = 3;
            this.IDC_F2.Text = "0";
            this.IDC_F2.Visible = false;
            this.IDC_F2.TextChanged += new System.EventHandler(this.IDC_F2_TextChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.IDC_T1);
            this.panel4.Controls.Add(this.IDC_F1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 126);
            this.panel4.Margin = new System.Windows.Forms.Padding(0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(303, 42);
            this.panel4.TabIndex = 3;
            // 
            // IDC_T1
            // 
            this.IDC_T1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IDC_T1.AutoSize = true;
            this.IDC_T1.Location = new System.Drawing.Point(3, 22);
            this.IDC_T1.Name = "IDC_T1";
            this.IDC_T1.Size = new System.Drawing.Size(125, 13);
            this.IDC_T1.TabIndex = 2;
            this.IDC_T1.Text = "Значение частоты (кгц)";
            // 
            // IDC_F1
            // 
            this.IDC_F1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDC_F1.Location = new System.Drawing.Point(200, 19);
            this.IDC_F1.Name = "IDC_F1";
            this.IDC_F1.Size = new System.Drawing.Size(100, 20);
            this.IDC_F1.TabIndex = 1;
            this.IDC_F1.Text = "0";
            this.IDC_F1.TextChanged += new System.EventHandler(this.IDC_F1_TextChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.IDC_KF);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 84);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(303, 42);
            this.panel3.TabIndex = 2;
            // 
            // IDC_KF
            // 
            this.IDC_KF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.IDC_KF.AutoSize = true;
            this.IDC_KF.Location = new System.Drawing.Point(3, 22);
            this.IDC_KF.Name = "IDC_KF";
            this.IDC_KF.Size = new System.Drawing.Size(255, 17);
            this.IDC_KF.TabIndex = 1;
            this.IDC_KF.TabStop = true;
            this.IDC_KF.Text = "Логарифмический закон изменения частоты";
            this.IDC_KF.UseVisualStyleBackColor = true;
            this.IDC_KF.CheckedChanged += new System.EventHandler(this.IDC_KF_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.IDC_DF);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 42);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(303, 42);
            this.panel2.TabIndex = 1;
            // 
            // IDC_DF
            // 
            this.IDC_DF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.IDC_DF.AutoSize = true;
            this.IDC_DF.Location = new System.Drawing.Point(3, 22);
            this.IDC_DF.Name = "IDC_DF";
            this.IDC_DF.Size = new System.Drawing.Size(213, 17);
            this.IDC_DF.TabIndex = 1;
            this.IDC_DF.TabStop = true;
            this.IDC_DF.Text = "Линейный закон изменения частоты";
            this.IDC_DF.UseVisualStyleBackColor = true;
            this.IDC_DF.CheckedChanged += new System.EventHandler(this.IDC_DF_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.IDC_F);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(303, 42);
            this.panel1.TabIndex = 0;
            // 
            // IDC_F
            // 
            this.IDC_F.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.IDC_F.AutoSize = true;
            this.IDC_F.Checked = true;
            this.IDC_F.Location = new System.Drawing.Point(3, 22);
            this.IDC_F.Name = "IDC_F";
            this.IDC_F.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.IDC_F.Size = new System.Drawing.Size(182, 17);
            this.IDC_F.TabIndex = 0;
            this.IDC_F.TabStop = true;
            this.IDC_F.Text = "Единственная частотная точка";
            this.IDC_F.UseVisualStyleBackColor = true;
            this.IDC_F.CheckedChanged += new System.EventHandler(this.IDC_F_CheckedChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.00001F));
            this.tableLayoutPanel2.Controls.Add(this.panel7, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 252);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(303, 43);
            this.tableLayoutPanel2.TabIndex = 6;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.IDC_FOK_BUTTON);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(101, 0);
            this.panel7.Margin = new System.Windows.Forms.Padding(0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(100, 43);
            this.panel7.TabIndex = 0;
            // 
            // IDC_FOK_BUTTON
            // 
            this.IDC_FOK_BUTTON.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IDC_FOK_BUTTON.Location = new System.Drawing.Point(3, 17);
            this.IDC_FOK_BUTTON.Name = "IDC_FOK_BUTTON";
            this.IDC_FOK_BUTTON.Size = new System.Drawing.Size(94, 23);
            this.IDC_FOK_BUTTON.TabIndex = 0;
            this.IDC_FOK_BUTTON.Text = "Ок";
            this.IDC_FOK_BUTTON.UseVisualStyleBackColor = true;
            this.IDC_FOK_BUTTON.Click += new System.EventHandler(this.IDC_FOK_BUTTON_Click);
            // 
            // F
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 313);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximumSize = new System.Drawing.Size(397, 352);
            this.MinimumSize = new System.Drawing.Size(397, 352);
            this.Name = "F";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Вид частотной характеристики";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton IDC_F;
        private System.Windows.Forms.RadioButton IDC_KF;
        private System.Windows.Forms.RadioButton IDC_DF;
        private System.Windows.Forms.TextBox IDC_F3;
        private System.Windows.Forms.TextBox IDC_F2;
        private System.Windows.Forms.TextBox IDC_F1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button IDC_FOK_BUTTON;
        private System.Windows.Forms.Label IDC_T2;
        private System.Windows.Forms.Label IDC_T1;
        private System.Windows.Forms.Label IDC_T3;
    }
}