﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MK.Windows.Calc
{
    public partial class F : Form
    {
        public F()
        {
            InitializeComponent();
        }

        private uint CanParseAllVars;

        private void IDC_F_CheckedChanged(object sender, EventArgs e)
        {
            if (!IDC_F.Checked)
                return;

            IDC_T1.Text = "Значение частоты (кгц)";
            IDC_F1.Text = "0";

            IDC_T2.Text = "";
            IDC_F2.Text = "0";
            IDC_F2.Visible = false;

            IDC_T3.Text = "";
            IDC_F3.Text = "0";
            IDC_F3.Visible = false;

            IDC_F.Checked = true;
            IDC_DF.Checked = false;
            IDC_KF.Checked = false;
        }

        private void IDC_DF_CheckedChanged(object sender, EventArgs e)
        {
            if (!IDC_DF.Checked)
                return;

            IDC_T1.Text = "Минимальная частота Fmin(кгц)";
            if (IDC_F.Checked)
                IDC_F1.Text = "0";

            IDC_T2.Text = "Максимальная частота Fmax(кгц)";
            if (IDC_F.Checked)
                IDC_F2.Text = "0";
            IDC_F2.Visible = true;

            IDC_T3.Text = "Шаг изменения частоты dF(кгц)";
            IDC_F3.Text = "0";
            IDC_F3.Visible = true;

            IDC_F.Checked = false;
            IDC_DF.Checked = true;
            IDC_KF.Checked = false;
        }

        private void IDC_KF_CheckedChanged(object sender, EventArgs e)
        {
            if (!IDC_KF.Checked)
                return;

            IDC_T1.Text = "Минимальная частота Fmin(кгц)";
            if (IDC_F.Checked)
                IDC_F1.Text = "0";

            IDC_T2.Text = "Максимальная частота Fmax(кгц)";
            if (IDC_F.Checked)
                IDC_F2.Text = "0";
            IDC_F2.Visible = true;

            IDC_T3.Text = "Отношение соседних частот K";
            IDC_F3.Text = "0";
            IDC_F3.Visible = true;

            IDC_F.Checked = false;
            IDC_DF.Checked = false;
            IDC_KF.Checked = true;
        }

        private void FloatPozitiveCheck(TextBox textBox, int index)
        {
            if (!float.TryParse(textBox.Text, out float val) || val < 0)
            {
                CanParseAllVars |= (1u << index);
                textBox.ForeColor = Color.FromArgb(210, 15, 0);
            }
            else
            {
                CanParseAllVars &= ~(1u << index);
                textBox.ForeColor = SystemColors.WindowText;
            }
        }

        private void IDC_F1_TextChanged(object sender, EventArgs e)
        {
            FloatPozitiveCheck(IDC_F1, 0);
        }

        private void IDC_F2_TextChanged(object sender, EventArgs e)
        {
            FloatPozitiveCheck(IDC_F2, 1);
        }

        private void IDC_F3_TextChanged(object sender, EventArgs e)
        {
            FloatPozitiveCheck(IDC_F3, 2);
        }

        private void IDC_FOK_BUTTON_Click(object sender, EventArgs e)
        {
            if (CanParseAllVars != 0)
                return;

            var Freq = GV_Manager.GV.Calc.Freq;
            Freq.Clear();

            float fmax, df;
            ;
            if (IDC_F.Checked)
                Freq.Add(float.Parse(IDC_F1.Text));
            else if (IDC_DF.Checked)
            {
                Freq.Add(float.Parse(IDC_F1.Text));
                fmax = float.Parse(IDC_F2.Text);
                df = float.Parse(IDC_F3.Text);

                while (Freq.Last() + df <= fmax)
                    Freq.Add(Freq.Last() + df);
            }
            else if (IDC_KF.Checked)
            {
                Freq.Add(float.Parse(IDC_F1.Text));
                fmax = float.Parse(IDC_F2.Text);
                df = float.Parse(IDC_F3.Text);

                while (Freq.Last() * df <= fmax)
                    Freq.Add(Freq.Last() * df);
            }
            Close();
        }
    }
}
