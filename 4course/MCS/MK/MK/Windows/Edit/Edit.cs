﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MK.Windows.Edit
{
    public partial class Edit : Form
    {
        public Edit()
        {
            InitializeComponent();
        }

        private uint CanParseAllVars;

        private void Edit_Load(object sender, EventArgs e)
        {
            var GV = GV_Manager.GV;

            m_editTree.BeginUpdate();
            for (int i = 1; i <= GV.R_Data.Count; i++)
                m_editTree.Nodes["R"].Nodes.Add(i.ToString());
            for (int i = 1; i <= GV.C_Data.Count; i++)
                m_editTree.Nodes["C"].Nodes.Add(i.ToString());
            for (int i = 1; i <= GV.L_Data.Count; i++)
                m_editTree.Nodes["L"].Nodes.Add(i.ToString());
            m_editTree.EndUpdate();

            index = -1;
        }

        private string typeKey;
        private int index;

        private void m_editTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (m_editTree.SelectedNode == null || m_editTree.SelectedNode.Parent == null)
                return;

            np1_box.Hide();
            nm1_box.Hide();
            np2_box.Hide();
            nm2_box.Hide();
            z1_box.Hide();
            z2_box.Hide();
            z3_box.Hide();
            z4_box.Hide();
            z5_box.Hide();
            z6_box.Hide();

            typeKey = m_editTree.SelectedNode.Parent.Name;
            index = m_editTree.SelectedNode.Index;

            var GV = GV_Manager.GV;

            switch (typeKey)
            {
                case "R":
                    IDC_NP2_STATIC.Text = "n+";
                    IDC_NM2_STATIC.Text = "n-";
                    IDC_Z1_STATIC.Text = "Сопротивление (кОм)";
                    m_np2.Text = GV.R_Data[index].In[0].ToString();
                    m_nm2.Text = GV.R_Data[index].In[1].ToString();
                    m_z1.Text = GV.R_Data[index].Z.ToString();

                    np2_box.Show();
                    nm2_box.Show();
                    z1_box.Show();
                    break;
                case "C":
                    IDC_NP2_STATIC.Text = "n+";
                    IDC_NM2_STATIC.Text = "n-";
                    IDC_Z1_STATIC.Text = "Емкость (пФ)";
                    m_np2.Text = GV.C_Data[index].In[0].ToString();
                    m_nm2.Text = GV.C_Data[index].In[1].ToString();
                    m_z1.Text = GV.C_Data[index].Z.ToString();

                    np2_box.Show();
                    nm2_box.Show();
                    z1_box.Show();
                    break;
                case "L":
                    IDC_NP2_STATIC.Text = "n+";
                    IDC_NM2_STATIC.Text = "n-";
                    IDC_Z1_STATIC.Text = "Индуктивность (мкГн)";
                    m_np2.Text = GV.L_Data[index].In[0].ToString();
                    m_nm2.Text = GV.L_Data[index].In[1].ToString();
                    m_z1.Text = GV.L_Data[index].Z.ToString();

                    np2_box.Show();
                    nm2_box.Show();
                    z1_box.Show();
                    break;
            }

            CanParseAllVars = 0;
        }

        private void IDC_IN_BUTTON_Click(object sender, EventArgs e)
        {
            if (index == -1 || CanParseAllVars != 0)
                return;

            var GV = GV_Manager.GV;

            switch (typeKey)
            {
                case "R":
                    GV.R_Data[index].In[0] = uint.Parse(m_np2.Text);
                    GV.R_Data[index].In[1] = uint.Parse(m_nm2.Text);
                    GV.R_Data[index].Z = float.Parse(m_z1.Text);
                    break;
                case "C":
                    GV.C_Data[index].In[0] = uint.Parse(m_np2.Text);
                    GV.C_Data[index].In[1] = uint.Parse(m_nm2.Text);
                    GV.C_Data[index].Z = float.Parse(m_z1.Text);
                    break;
                case "L":
                    GV.L_Data[index].In[0] = uint.Parse(m_np2.Text);
                    GV.L_Data[index].In[1] = uint.Parse(m_nm2.Text);
                    GV.L_Data[index].Z = float.Parse(m_z1.Text);
                    break;
            }
        }

        private void OK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void UIntCheck(TextBox textBox, int index)
        {
            if (!uint.TryParse(textBox.Text, out _))
            {
                CanParseAllVars |= (1u << index);
                textBox.ForeColor = Color.FromArgb(210, 15, 0);
            }
            else
            {
                CanParseAllVars &= ~(1u << index);
                textBox.ForeColor = SystemColors.WindowText;
            }
        }

        private void FloatPozitiveCheck(TextBox textBox, int index)
        {
            if (!float.TryParse(textBox.Text, out float val) || val < 0)
            {
                CanParseAllVars |= (1u << index);
                textBox.ForeColor = Color.FromArgb(210, 15, 0);
            }
            else
            {
                CanParseAllVars &= ~(1u << index);
                textBox.ForeColor = SystemColors.WindowText;
            }
        }

        private void m_np1_TextChanged(object sender, EventArgs e)
        {
            UIntCheck(m_np1, 0);
        }

        private void m_nm1_TextChanged(object sender, EventArgs e)
        {
            UIntCheck(m_nm1, 1);
        }

        private void m_np2_TextChanged(object sender, EventArgs e)
        {
            UIntCheck(m_np2, 2);
        }

        private void m_nm2_TextChanged(object sender, EventArgs e)
        {
            UIntCheck(m_nm2, 3);
        }

        private void m_z1_TextChanged(object sender, EventArgs e)
        {
            FloatPozitiveCheck(m_z1, 4);
        }

        private void m_z2_TextChanged(object sender, EventArgs e)
        {
            FloatPozitiveCheck(m_z2, 5);
        }

        private void m_z3_TextChanged(object sender, EventArgs e)
        {
            FloatPozitiveCheck(m_z3, 6);
        }

        private void m_z4_TextChanged(object sender, EventArgs e)
        {
            FloatPozitiveCheck(m_z4, 7);
        }

        private void m_z5_TextChanged(object sender, EventArgs e)
        {
            FloatPozitiveCheck(m_z5, 8);
        }

        private void m_z6_TextChanged(object sender, EventArgs e)
        {
            FloatPozitiveCheck(m_z6, 9);
        }
    }
}
