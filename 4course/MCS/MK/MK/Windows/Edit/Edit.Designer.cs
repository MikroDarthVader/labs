﻿namespace MK.Windows.Edit
{
    partial class Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Резисторы");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Конденсаторы");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Катушки");
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.IDC_IN_BUTTON = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.z6_box = new System.Windows.Forms.Panel();
            this.m_z6 = new System.Windows.Forms.TextBox();
            this.IDC_Z6_STATIC = new System.Windows.Forms.Label();
            this.z5_box = new System.Windows.Forms.Panel();
            this.m_z5 = new System.Windows.Forms.TextBox();
            this.IDC_Z5_STATIC = new System.Windows.Forms.Label();
            this.z4_box = new System.Windows.Forms.Panel();
            this.m_z4 = new System.Windows.Forms.TextBox();
            this.IDC_Z4_STATIC = new System.Windows.Forms.Label();
            this.z3_box = new System.Windows.Forms.Panel();
            this.m_z3 = new System.Windows.Forms.TextBox();
            this.IDC_Z3_STATIC = new System.Windows.Forms.Label();
            this.z2_box = new System.Windows.Forms.Panel();
            this.m_z2 = new System.Windows.Forms.TextBox();
            this.IDC_Z2_STATIC = new System.Windows.Forms.Label();
            this.z1_box = new System.Windows.Forms.Panel();
            this.m_z1 = new System.Windows.Forms.TextBox();
            this.IDC_Z1_STATIC = new System.Windows.Forms.Label();
            this.nm1_box = new System.Windows.Forms.Panel();
            this.m_nm1 = new System.Windows.Forms.TextBox();
            this.IDC_NM1_STATIC = new System.Windows.Forms.Label();
            this.nm2_box = new System.Windows.Forms.Panel();
            this.m_nm2 = new System.Windows.Forms.TextBox();
            this.IDC_NM2_STATIC = new System.Windows.Forms.Label();
            this.np2_box = new System.Windows.Forms.Panel();
            this.m_np2 = new System.Windows.Forms.TextBox();
            this.IDC_NP2_STATIC = new System.Windows.Forms.Label();
            this.np1_box = new System.Windows.Forms.Panel();
            this.m_np1 = new System.Windows.Forms.TextBox();
            this.IDC_NP1_STATIC = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.OK = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.m_editTree = new System.Windows.Forms.TreeView();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.z6_box.SuspendLayout();
            this.z5_box.SuspendLayout();
            this.z4_box.SuspendLayout();
            this.z3_box.SuspendLayout();
            this.z2_box.SuspendLayout();
            this.z1_box.SuspendLayout();
            this.nm1_box.SuspendLayout();
            this.nm2_box.SuspendLayout();
            this.np2_box.SuspendLayout();
            this.np1_box.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(15, 5, 15, 5);
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(684, 404);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.panel14, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(190, 5);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 87.68082F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.31918F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(479, 394);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // panel14
            // 
            this.panel14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel14.Controls.Add(this.IDC_IN_BUTTON);
            this.panel14.Location = new System.Drawing.Point(0, 349);
            this.panel14.Margin = new System.Windows.Forms.Padding(0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(479, 45);
            this.panel14.TabIndex = 3;
            // 
            // IDC_IN_BUTTON
            // 
            this.IDC_IN_BUTTON.AutoSize = true;
            this.IDC_IN_BUTTON.Location = new System.Drawing.Point(194, 10);
            this.IDC_IN_BUTTON.Margin = new System.Windows.Forms.Padding(10);
            this.IDC_IN_BUTTON.Name = "IDC_IN_BUTTON";
            this.IDC_IN_BUTTON.Size = new System.Drawing.Size(93, 23);
            this.IDC_IN_BUTTON.TabIndex = 0;
            this.IDC_IN_BUTTON.Text = "Ввод описания";
            this.IDC_IN_BUTTON.UseVisualStyleBackColor = true;
            this.IDC_IN_BUTTON.Click += new System.EventHandler(this.IDC_IN_BUTTON_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(479, 33);
            this.panel2.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(479, 33);
            this.label2.TabIndex = 0;
            this.label2.Text = "Параметры компонента";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.z6_box, 1, 7);
            this.tableLayoutPanel4.Controls.Add(this.z5_box, 1, 6);
            this.tableLayoutPanel4.Controls.Add(this.z4_box, 1, 5);
            this.tableLayoutPanel4.Controls.Add(this.z3_box, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.z2_box, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.z1_box, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.nm1_box, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.nm2_box, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.np2_box, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.np1_box, 0, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 33);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 8;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(479, 316);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // z6_box
            // 
            this.z6_box.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.z6_box.Controls.Add(this.m_z6);
            this.z6_box.Controls.Add(this.IDC_Z6_STATIC);
            this.z6_box.Location = new System.Drawing.Point(240, 275);
            this.z6_box.Margin = new System.Windows.Forms.Padding(0);
            this.z6_box.Name = "z6_box";
            this.z6_box.Size = new System.Drawing.Size(237, 39);
            this.z6_box.TabIndex = 5;
            this.z6_box.Visible = false;
            // 
            // m_z6
            // 
            this.m_z6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_z6.Location = new System.Drawing.Point(154, 13);
            this.m_z6.Name = "m_z6";
            this.m_z6.Size = new System.Drawing.Size(80, 20);
            this.m_z6.TabIndex = 1;
            this.m_z6.TextChanged += new System.EventHandler(this.m_z6_TextChanged);
            // 
            // IDC_Z6_STATIC
            // 
            this.IDC_Z6_STATIC.AutoSize = true;
            this.IDC_Z6_STATIC.Location = new System.Drawing.Point(5, 16);
            this.IDC_Z6_STATIC.Margin = new System.Windows.Forms.Padding(5);
            this.IDC_Z6_STATIC.Name = "IDC_Z6_STATIC";
            this.IDC_Z6_STATIC.Size = new System.Drawing.Size(55, 13);
            this.IDC_Z6_STATIC.TabIndex = 0;
            this.IDC_Z6_STATIC.Text = "Значение";
            // 
            // z5_box
            // 
            this.z5_box.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.z5_box.Controls.Add(this.m_z5);
            this.z5_box.Controls.Add(this.IDC_Z5_STATIC);
            this.z5_box.Location = new System.Drawing.Point(240, 236);
            this.z5_box.Margin = new System.Windows.Forms.Padding(0);
            this.z5_box.Name = "z5_box";
            this.z5_box.Size = new System.Drawing.Size(237, 37);
            this.z5_box.TabIndex = 5;
            this.z5_box.Visible = false;
            // 
            // m_z5
            // 
            this.m_z5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_z5.Location = new System.Drawing.Point(154, 13);
            this.m_z5.Name = "m_z5";
            this.m_z5.Size = new System.Drawing.Size(80, 20);
            this.m_z5.TabIndex = 1;
            this.m_z5.TextChanged += new System.EventHandler(this.m_z5_TextChanged);
            // 
            // IDC_Z5_STATIC
            // 
            this.IDC_Z5_STATIC.AutoSize = true;
            this.IDC_Z5_STATIC.Location = new System.Drawing.Point(5, 16);
            this.IDC_Z5_STATIC.Margin = new System.Windows.Forms.Padding(5);
            this.IDC_Z5_STATIC.Name = "IDC_Z5_STATIC";
            this.IDC_Z5_STATIC.Size = new System.Drawing.Size(55, 13);
            this.IDC_Z5_STATIC.TabIndex = 0;
            this.IDC_Z5_STATIC.Text = "Значение";
            // 
            // z4_box
            // 
            this.z4_box.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.z4_box.Controls.Add(this.m_z4);
            this.z4_box.Controls.Add(this.IDC_Z4_STATIC);
            this.z4_box.Location = new System.Drawing.Point(240, 197);
            this.z4_box.Margin = new System.Windows.Forms.Padding(0);
            this.z4_box.Name = "z4_box";
            this.z4_box.Size = new System.Drawing.Size(237, 37);
            this.z4_box.TabIndex = 5;
            this.z4_box.Visible = false;
            // 
            // m_z4
            // 
            this.m_z4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_z4.Location = new System.Drawing.Point(154, 13);
            this.m_z4.Name = "m_z4";
            this.m_z4.Size = new System.Drawing.Size(80, 20);
            this.m_z4.TabIndex = 1;
            this.m_z4.TextChanged += new System.EventHandler(this.m_z4_TextChanged);
            // 
            // IDC_Z4_STATIC
            // 
            this.IDC_Z4_STATIC.AutoSize = true;
            this.IDC_Z4_STATIC.Location = new System.Drawing.Point(5, 16);
            this.IDC_Z4_STATIC.Margin = new System.Windows.Forms.Padding(5);
            this.IDC_Z4_STATIC.Name = "IDC_Z4_STATIC";
            this.IDC_Z4_STATIC.Size = new System.Drawing.Size(55, 13);
            this.IDC_Z4_STATIC.TabIndex = 0;
            this.IDC_Z4_STATIC.Text = "Значение";
            // 
            // z3_box
            // 
            this.z3_box.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.z3_box.Controls.Add(this.m_z3);
            this.z3_box.Controls.Add(this.IDC_Z3_STATIC);
            this.z3_box.Location = new System.Drawing.Point(240, 158);
            this.z3_box.Margin = new System.Windows.Forms.Padding(0);
            this.z3_box.Name = "z3_box";
            this.z3_box.Size = new System.Drawing.Size(237, 37);
            this.z3_box.TabIndex = 5;
            this.z3_box.Visible = false;
            // 
            // m_z3
            // 
            this.m_z3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_z3.Location = new System.Drawing.Point(154, 13);
            this.m_z3.Name = "m_z3";
            this.m_z3.Size = new System.Drawing.Size(80, 20);
            this.m_z3.TabIndex = 1;
            this.m_z3.TextChanged += new System.EventHandler(this.m_z3_TextChanged);
            // 
            // IDC_Z3_STATIC
            // 
            this.IDC_Z3_STATIC.AutoSize = true;
            this.IDC_Z3_STATIC.Location = new System.Drawing.Point(5, 16);
            this.IDC_Z3_STATIC.Margin = new System.Windows.Forms.Padding(5);
            this.IDC_Z3_STATIC.Name = "IDC_Z3_STATIC";
            this.IDC_Z3_STATIC.Size = new System.Drawing.Size(55, 13);
            this.IDC_Z3_STATIC.TabIndex = 0;
            this.IDC_Z3_STATIC.Text = "Значение";
            // 
            // z2_box
            // 
            this.z2_box.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.z2_box.Controls.Add(this.m_z2);
            this.z2_box.Controls.Add(this.IDC_Z2_STATIC);
            this.z2_box.Location = new System.Drawing.Point(240, 119);
            this.z2_box.Margin = new System.Windows.Forms.Padding(0);
            this.z2_box.Name = "z2_box";
            this.z2_box.Size = new System.Drawing.Size(237, 37);
            this.z2_box.TabIndex = 5;
            this.z2_box.Visible = false;
            // 
            // m_z2
            // 
            this.m_z2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_z2.Location = new System.Drawing.Point(154, 13);
            this.m_z2.Name = "m_z2";
            this.m_z2.Size = new System.Drawing.Size(80, 20);
            this.m_z2.TabIndex = 1;
            this.m_z2.TextChanged += new System.EventHandler(this.m_z2_TextChanged);
            // 
            // IDC_Z2_STATIC
            // 
            this.IDC_Z2_STATIC.AutoSize = true;
            this.IDC_Z2_STATIC.Location = new System.Drawing.Point(5, 16);
            this.IDC_Z2_STATIC.Margin = new System.Windows.Forms.Padding(5);
            this.IDC_Z2_STATIC.Name = "IDC_Z2_STATIC";
            this.IDC_Z2_STATIC.Size = new System.Drawing.Size(55, 13);
            this.IDC_Z2_STATIC.TabIndex = 0;
            this.IDC_Z2_STATIC.Text = "Значение";
            // 
            // z1_box
            // 
            this.z1_box.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.z1_box.Controls.Add(this.m_z1);
            this.z1_box.Controls.Add(this.IDC_Z1_STATIC);
            this.z1_box.Location = new System.Drawing.Point(240, 80);
            this.z1_box.Margin = new System.Windows.Forms.Padding(0);
            this.z1_box.Name = "z1_box";
            this.z1_box.Size = new System.Drawing.Size(237, 37);
            this.z1_box.TabIndex = 4;
            this.z1_box.Visible = false;
            // 
            // m_z1
            // 
            this.m_z1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_z1.Location = new System.Drawing.Point(154, 13);
            this.m_z1.Name = "m_z1";
            this.m_z1.Size = new System.Drawing.Size(80, 20);
            this.m_z1.TabIndex = 1;
            this.m_z1.TextChanged += new System.EventHandler(this.m_z1_TextChanged);
            // 
            // IDC_Z1_STATIC
            // 
            this.IDC_Z1_STATIC.AutoSize = true;
            this.IDC_Z1_STATIC.Location = new System.Drawing.Point(5, 16);
            this.IDC_Z1_STATIC.Margin = new System.Windows.Forms.Padding(5);
            this.IDC_Z1_STATIC.Name = "IDC_Z1_STATIC";
            this.IDC_Z1_STATIC.Size = new System.Drawing.Size(55, 13);
            this.IDC_Z1_STATIC.TabIndex = 0;
            this.IDC_Z1_STATIC.Text = "Значение";
            // 
            // nm1_box
            // 
            this.nm1_box.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nm1_box.Controls.Add(this.m_nm1);
            this.nm1_box.Controls.Add(this.IDC_NM1_STATIC);
            this.nm1_box.Location = new System.Drawing.Point(2, 41);
            this.nm1_box.Margin = new System.Windows.Forms.Padding(0);
            this.nm1_box.Name = "nm1_box";
            this.nm1_box.Size = new System.Drawing.Size(236, 37);
            this.nm1_box.TabIndex = 4;
            this.nm1_box.Visible = false;
            // 
            // m_nm1
            // 
            this.m_nm1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_nm1.Location = new System.Drawing.Point(154, 13);
            this.m_nm1.Name = "m_nm1";
            this.m_nm1.Size = new System.Drawing.Size(80, 20);
            this.m_nm1.TabIndex = 1;
            this.m_nm1.TextChanged += new System.EventHandler(this.m_nm1_TextChanged);
            // 
            // IDC_NM1_STATIC
            // 
            this.IDC_NM1_STATIC.AutoSize = true;
            this.IDC_NM1_STATIC.Location = new System.Drawing.Point(5, 16);
            this.IDC_NM1_STATIC.Margin = new System.Windows.Forms.Padding(5);
            this.IDC_NM1_STATIC.Name = "IDC_NM1_STATIC";
            this.IDC_NM1_STATIC.Size = new System.Drawing.Size(55, 13);
            this.IDC_NM1_STATIC.TabIndex = 0;
            this.IDC_NM1_STATIC.Text = "Значение";
            // 
            // nm2_box
            // 
            this.nm2_box.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nm2_box.Controls.Add(this.m_nm2);
            this.nm2_box.Controls.Add(this.IDC_NM2_STATIC);
            this.nm2_box.Location = new System.Drawing.Point(240, 41);
            this.nm2_box.Margin = new System.Windows.Forms.Padding(0);
            this.nm2_box.Name = "nm2_box";
            this.nm2_box.Size = new System.Drawing.Size(237, 37);
            this.nm2_box.TabIndex = 3;
            this.nm2_box.Visible = false;
            // 
            // m_nm2
            // 
            this.m_nm2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_nm2.Location = new System.Drawing.Point(154, 13);
            this.m_nm2.Name = "m_nm2";
            this.m_nm2.Size = new System.Drawing.Size(80, 20);
            this.m_nm2.TabIndex = 1;
            this.m_nm2.TextChanged += new System.EventHandler(this.m_nm2_TextChanged);
            // 
            // IDC_NM2_STATIC
            // 
            this.IDC_NM2_STATIC.AutoSize = true;
            this.IDC_NM2_STATIC.Location = new System.Drawing.Point(5, 16);
            this.IDC_NM2_STATIC.Margin = new System.Windows.Forms.Padding(5);
            this.IDC_NM2_STATIC.Name = "IDC_NM2_STATIC";
            this.IDC_NM2_STATIC.Size = new System.Drawing.Size(55, 13);
            this.IDC_NM2_STATIC.TabIndex = 0;
            this.IDC_NM2_STATIC.Text = "Значение";
            // 
            // np2_box
            // 
            this.np2_box.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.np2_box.Controls.Add(this.m_np2);
            this.np2_box.Controls.Add(this.IDC_NP2_STATIC);
            this.np2_box.Location = new System.Drawing.Point(240, 2);
            this.np2_box.Margin = new System.Windows.Forms.Padding(0);
            this.np2_box.Name = "np2_box";
            this.np2_box.Size = new System.Drawing.Size(237, 37);
            this.np2_box.TabIndex = 2;
            this.np2_box.Visible = false;
            // 
            // m_np2
            // 
            this.m_np2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_np2.Location = new System.Drawing.Point(154, 13);
            this.m_np2.Name = "m_np2";
            this.m_np2.Size = new System.Drawing.Size(80, 20);
            this.m_np2.TabIndex = 1;
            this.m_np2.TextChanged += new System.EventHandler(this.m_np2_TextChanged);
            // 
            // IDC_NP2_STATIC
            // 
            this.IDC_NP2_STATIC.AutoSize = true;
            this.IDC_NP2_STATIC.Location = new System.Drawing.Point(5, 16);
            this.IDC_NP2_STATIC.Margin = new System.Windows.Forms.Padding(5);
            this.IDC_NP2_STATIC.Name = "IDC_NP2_STATIC";
            this.IDC_NP2_STATIC.Size = new System.Drawing.Size(55, 13);
            this.IDC_NP2_STATIC.TabIndex = 0;
            this.IDC_NP2_STATIC.Text = "Значение";
            // 
            // np1_box
            // 
            this.np1_box.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.np1_box.Controls.Add(this.m_np1);
            this.np1_box.Controls.Add(this.IDC_NP1_STATIC);
            this.np1_box.Location = new System.Drawing.Point(2, 2);
            this.np1_box.Margin = new System.Windows.Forms.Padding(0);
            this.np1_box.Name = "np1_box";
            this.np1_box.Size = new System.Drawing.Size(236, 37);
            this.np1_box.TabIndex = 1;
            this.np1_box.Visible = false;
            // 
            // m_np1
            // 
            this.m_np1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_np1.Location = new System.Drawing.Point(154, 13);
            this.m_np1.Name = "m_np1";
            this.m_np1.Size = new System.Drawing.Size(80, 20);
            this.m_np1.TabIndex = 1;
            this.m_np1.TextChanged += new System.EventHandler(this.m_np1_TextChanged);
            // 
            // IDC_NP1_STATIC
            // 
            this.IDC_NP1_STATIC.AutoSize = true;
            this.IDC_NP1_STATIC.Location = new System.Drawing.Point(5, 16);
            this.IDC_NP1_STATIC.Margin = new System.Windows.Forms.Padding(5);
            this.IDC_NP1_STATIC.Name = "IDC_NP1_STATIC";
            this.IDC_NP1_STATIC.Size = new System.Drawing.Size(55, 13);
            this.IDC_NP1_STATIC.TabIndex = 0;
            this.IDC_NP1_STATIC.Text = "Значение";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.panel3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.m_editTree, 0, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(15, 5);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 87.68328F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.31672F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(170, 394);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.OK);
            this.panel3.Location = new System.Drawing.Point(0, 349);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(170, 45);
            this.panel3.TabIndex = 2;
            // 
            // OK
            // 
            this.OK.AutoSize = true;
            this.OK.Location = new System.Drawing.Point(48, 10);
            this.OK.Margin = new System.Windows.Forms.Padding(10);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(75, 23);
            this.OK.TabIndex = 0;
            this.OK.Text = "Ок";
            this.OK.UseVisualStyleBackColor = true;
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(170, 33);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "Компоненты";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // m_editTree
            // 
            this.m_editTree.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m_editTree.Location = new System.Drawing.Point(10, 33);
            this.m_editTree.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.m_editTree.Name = "m_editTree";
            treeNode1.Name = "R";
            treeNode1.Text = "Резисторы";
            treeNode2.Name = "C";
            treeNode2.Text = "Конденсаторы";
            treeNode3.Name = "L";
            treeNode3.Text = "Катушки";
            this.m_editTree.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3});
            this.m_editTree.Size = new System.Drawing.Size(150, 316);
            this.m_editTree.TabIndex = 3;
            this.m_editTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.m_editTree_AfterSelect);
            // 
            // Edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 404);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximumSize = new System.Drawing.Size(700, 443);
            this.MinimumSize = new System.Drawing.Size(700, 443);
            this.Name = "Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Редактирование компонентов";
            this.Load += new System.EventHandler(this.Edit_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.z6_box.ResumeLayout(false);
            this.z6_box.PerformLayout();
            this.z5_box.ResumeLayout(false);
            this.z5_box.PerformLayout();
            this.z4_box.ResumeLayout(false);
            this.z4_box.PerformLayout();
            this.z3_box.ResumeLayout(false);
            this.z3_box.PerformLayout();
            this.z2_box.ResumeLayout(false);
            this.z2_box.PerformLayout();
            this.z1_box.ResumeLayout(false);
            this.z1_box.PerformLayout();
            this.nm1_box.ResumeLayout(false);
            this.nm1_box.PerformLayout();
            this.nm2_box.ResumeLayout(false);
            this.nm2_box.PerformLayout();
            this.np2_box.ResumeLayout(false);
            this.np2_box.PerformLayout();
            this.np1_box.ResumeLayout(false);
            this.np1_box.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel np1_box;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TreeView m_editTree;
        private System.Windows.Forms.TextBox m_np1;
        private System.Windows.Forms.Label IDC_NP1_STATIC;
        private System.Windows.Forms.Button OK;
        private System.Windows.Forms.Panel z1_box;
        private System.Windows.Forms.TextBox m_z1;
        private System.Windows.Forms.Label IDC_Z1_STATIC;
        private System.Windows.Forms.Panel nm1_box;
        private System.Windows.Forms.TextBox m_nm1;
        private System.Windows.Forms.Label IDC_NM1_STATIC;
        private System.Windows.Forms.Panel nm2_box;
        private System.Windows.Forms.TextBox m_nm2;
        private System.Windows.Forms.Label IDC_NM2_STATIC;
        private System.Windows.Forms.Panel np2_box;
        private System.Windows.Forms.TextBox m_np2;
        private System.Windows.Forms.Label IDC_NP2_STATIC;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Button IDC_IN_BUTTON;
        private System.Windows.Forms.Panel z5_box;
        private System.Windows.Forms.TextBox m_z5;
        private System.Windows.Forms.Label IDC_Z5_STATIC;
        private System.Windows.Forms.Panel z4_box;
        private System.Windows.Forms.TextBox m_z4;
        private System.Windows.Forms.Label IDC_Z4_STATIC;
        private System.Windows.Forms.Panel z3_box;
        private System.Windows.Forms.TextBox m_z3;
        private System.Windows.Forms.Label IDC_Z3_STATIC;
        private System.Windows.Forms.Panel z2_box;
        private System.Windows.Forms.TextBox m_z2;
        private System.Windows.Forms.Label IDC_Z2_STATIC;
        private System.Windows.Forms.Panel z6_box;
        private System.Windows.Forms.TextBox m_z6;
        private System.Windows.Forms.Label IDC_Z6_STATIC;
    }
}