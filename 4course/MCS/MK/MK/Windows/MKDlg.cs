﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MK;
using MK.Windows.AddWithKeyboard;
using MK.Windows.Edit;
using MK.Windows.Calc;
using MK.Windows.Internet;

using Libraries.JSON;
using System.IO;

namespace МК.Windows
{
    public partial class MKDlg : Form
    {
        public MKDlg()
        {
            InitializeComponent();
        }

        private bool Sys_browser;

        private void MKDlg_Load(object sender, EventArgs e)
        {
            Sys_browser = false;
        }

        private void ID_EXIT_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ID_CONS_Click(object sender, EventArgs e)
        {
            GV_Manager.StartWriting();

            CircuitDimension cd = new CircuitDimension();
            var dialogRes = cd.ShowDialog(this);
            cd.Dispose();

            if (dialogRes != DialogResult.OK)
            {
                GV_Manager.AbortWriting();
                return;
            }

            if (GV_Manager.GV_Buffer.CD.NR > 0)
            {
                R r = new R();
                dialogRes = r.ShowDialog(this);
                r.Dispose();

                if (dialogRes != DialogResult.OK)
                {
                    GV_Manager.AbortWriting();
                    return;
                }
            }

            if (GV_Manager.GV_Buffer.CD.NC > 0)
            {
                C c = new C();
                dialogRes = c.ShowDialog(this);
                c.Dispose();

                if (dialogRes != DialogResult.OK)
                {
                    GV_Manager.AbortWriting();
                    return;
                }
            }

            if (GV_Manager.GV_Buffer.CD.NL > 0)
            {
                L l = new L();
                dialogRes = l.ShowDialog(this);
                l.Dispose();

                if (dialogRes != DialogResult.OK)
                {
                    GV_Manager.AbortWriting();
                    return;
                }
            }

            GV_Manager.EndWriting();
        }

        private void ID_RED_Click(object sender, EventArgs e)
        {
            Edit edit = new Edit();
            edit.ShowDialog(this);
            edit.Dispose();
        }

        private void ID_FILE_READ_Click(object sender, EventArgs e)
        {
            var fileDlg = new OpenFileDialog();

            string curDir = Directory.GetCurrentDirectory();
            curDir = curDir.Remove(curDir.LastIndexOf('\\'));
            curDir = curDir.Remove(curDir.LastIndexOf('\\'));
            fileDlg.InitialDirectory = curDir + "\\Saves\\";
            fileDlg.RestoreDirectory = true;

            fileDlg.Filter = "Text files (*.json)|*json";
            fileDlg.DefaultExt = "json";
            fileDlg.AddExtension = true;

            fileDlg.CheckFileExists = true;
            fileDlg.CheckPathExists = true;

            if (fileDlg.ShowDialog() == DialogResult.OK)
                ReadCDFromFile(fileDlg.FileName);
        }

        private void ReadCDFromFile(string filePath)
        {
            GV_Manager.StartWriting();

            var data = JSONReader.ReadFile(filePath);

            var cd_data = data["CD"];
            GV_Manager.GV_Buffer.CD.NV = cd_data["NV"].GetValue<uint>();
            GV_Manager.GV_Buffer.CD.NR = cd_data["NR"].GetValue<uint>();
            GV_Manager.GV_Buffer.CD.NC = cd_data["NC"].GetValue<uint>();
            GV_Manager.GV_Buffer.CD.NL = cd_data["NL"].GetValue<uint>();

            var rDatas_array = data["R_Data"];
            foreach (JSONDataToRead element in rDatas_array)
                GV_Manager.GV_Buffer.R_Data.Add(new _R_Data(
                                                            element["In_0"].GetValue<uint>(),
                                                            element["In_1"].GetValue<uint>(),
                                                            element["Z"].GetValue<float>()));

            var cDatas_array = data["C_Data"];
            foreach (JSONDataToRead element in cDatas_array)
                GV_Manager.GV_Buffer.C_Data.Add(new _C_Data(
                                                            element["In_0"].GetValue<uint>(),
                                                            element["In_1"].GetValue<uint>(),
                                                            element["Z"].GetValue<float>()));

            var lDatas_array = data["L_Data"];
            foreach (JSONDataToRead element in lDatas_array)
                GV_Manager.GV_Buffer.L_Data.Add(new _L_Data(
                                                            element["In_0"].GetValue<uint>(),
                                                            element["In_1"].GetValue<uint>(),
                                                            element["Z"].GetValue<float>()));

            GV_Manager.EndWriting();
        }

        private void ID_FILE_WRITE_Click(object sender, EventArgs e)
        {
            var fileDlg = new SaveFileDialog();

            string curDir = Directory.GetCurrentDirectory();
            curDir = curDir.Remove(curDir.LastIndexOf('\\'));
            curDir = curDir.Remove(curDir.LastIndexOf('\\'));
            fileDlg.InitialDirectory = curDir + "\\Saves\\";
            fileDlg.RestoreDirectory = true;

            fileDlg.Filter = "Text files (*.json)|*json";
            fileDlg.DefaultExt = "json";
            fileDlg.AddExtension = true;

            fileDlg.OverwritePrompt = true;

            if (fileDlg.ShowDialog() == DialogResult.OK)
                WriteCDToFile(fileDlg.FileName);
        }

        private void WriteCDToFile(string filePath)
        {
            var container = JSONWriter.GetDataStoreRoot();

            var cd_container = container.AddDataSet("CD");
            cd_container.AddData("NV", GV_Manager.GV.CD.NV);
            cd_container.AddData("NR", GV_Manager.GV.CD.NR);
            cd_container.AddData("NC", GV_Manager.GV.CD.NC);
            cd_container.AddData("NL", GV_Manager.GV.CD.NL);

            var rDatas_container = container.AddArray("R_Data");
            for (int i = 0; i < GV_Manager.GV.R_Data.Count; i++)
            {
                var element_container = rDatas_container.AddDataSet();
                element_container.AddData("In_0", GV_Manager.GV.R_Data[i].In[0]);
                element_container.AddData("In_1", GV_Manager.GV.R_Data[i].In[1]);
                element_container.AddData("Z", GV_Manager.GV.R_Data[i].Z);
            }

            var cDatas_container = container.AddArray("C_Data");
            for (int i = 0; i < GV_Manager.GV.C_Data.Count; i++)
            {
                var element_container = cDatas_container.AddDataSet();
                element_container.AddData("In_0", GV_Manager.GV.C_Data[i].In[0]);
                element_container.AddData("In_1", GV_Manager.GV.C_Data[i].In[1]);
                element_container.AddData("Z", GV_Manager.GV.C_Data[i].Z);
            }

            var lDatas_container = container.AddArray("L_Data");
            for (int i = 0; i < GV_Manager.GV.L_Data.Count; i++)
            {
                var element_container = lDatas_container.AddDataSet();
                element_container.AddData("In_0", GV_Manager.GV.L_Data[i].In[0]);
                element_container.AddData("In_1", GV_Manager.GV.L_Data[i].In[1]);
                element_container.AddData("Z", GV_Manager.GV.L_Data[i].Z);
            }

            JSONWriter.WriteToFile(filePath, container);
        }

        private void ID_F_Click(object sender, EventArgs e)
        {
            F f = new F();
            f.ShowDialog(this);
            f.Dispose();
        }

        private void ID_IO_Click(object sender, EventArgs e)
        {
            IO io = new IO();
            io.ShowDialog(this);
            io.Dispose();
        }

        private void ID_PRIV_Click(object sender, EventArgs e)
        {
            Sys_browser = false;
        }

        private void ID_SYS_Click(object sender, EventArgs e)
        {
            Sys_browser = true;
        }

        private void ID_INTERNET_Click(object sender, EventArgs e)
        {
            if (!Sys_browser)
            {
                INT cint = new INT();
                cint.Show(this);
            }
            else
                System.Diagnostics.Process.Start("http://127.0.0.1/MF/ParamComp.html");
        }
    }
}
