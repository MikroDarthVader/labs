﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MK.Windows.AddWithKeyboard
{
    public partial class C : Form
    {
        public C()
        {
            InitializeComponent();
        }

        private uint CanParseAllVars;
        private uint iter;

        private void C_Load(object sender, EventArgs e)
        {
            CanParseAllVars = 0;

            iter = uint.Parse(C_NEXT.Text);
        }

        private void C_Shown(object sender, EventArgs e)
        {
            C_NP.Focus();
        }

        private void IDC_NEXTC_BUTTON_Click(object sender, EventArgs e)
        {
            if (CanParseAllVars > 0)
                return;

            var GV = GV_Manager.GV_Buffer;

            GV.C_Data.Add(new _C_Data(uint.Parse(C_NP.Text), 
                                      uint.Parse(C_NM.Text), 
                                      float.Parse(C_Z.Text)));

            if (iter < GV.CD.NC)
            {
                C_NEXT.Text = (++iter).ToString();
                C_NP.Text = "0";
                C_NM.Text = "0";
                C_Z.Text = "0";
                C_NP.Focus();
            }
            else
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void UIntCheck(TextBox textBox, int index)
        {
            if (!uint.TryParse(textBox.Text, out _))
            {
                CanParseAllVars |= (1u << index);
                textBox.ForeColor = Color.FromArgb(210, 15, 0);
            }
            else
            {
                CanParseAllVars &= ~(1u << index);
                textBox.ForeColor = SystemColors.WindowText;
            }
        }

        private void FloatPozitiveCheck(TextBox textBox, int index)
        {
            if (!float.TryParse(textBox.Text, out float val) || val < 0)
            {
                CanParseAllVars |= (1u << index);
                textBox.ForeColor = Color.FromArgb(210, 15, 0);
            }
            else
            {
                CanParseAllVars &= ~(1u << index);
                textBox.ForeColor = SystemColors.WindowText;
            }
        }

        private void C_NP_TextChanged(object sender, EventArgs e)
        {
            UIntCheck(C_NP, 0);
        }

        private void C_NM_TextChanged(object sender, EventArgs e)
        {
            UIntCheck(C_NM, 1);
        }

        private void C_Z_TextChanged(object sender, EventArgs e)
        {
            FloatPozitiveCheck(C_Z, 2);
        }
    }
}
