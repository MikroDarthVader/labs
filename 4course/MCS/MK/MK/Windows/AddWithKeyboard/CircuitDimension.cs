﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MK.Windows.AddWithKeyboard
{
    public partial class CircuitDimension : Form
    {
        public CircuitDimension()
        {
            InitializeComponent();
        }

        private uint CanParseAllVars;

        private void CircuitDimension_Load(object sender, EventArgs e)
        {
            CanParseAllVars = 0;
        }

        private void OK_Click(object sender, EventArgs e)
        {
            if (CanParseAllVars > 0)
                return;

            GV_Manager.GV_Buffer.CD.NV = uint.Parse(CD_NV.Text);
            GV_Manager.GV_Buffer.CD.NR = uint.Parse(CD_NR.Text);
            GV_Manager.GV_Buffer.CD.NC = uint.Parse(CD_NC.Text);
            GV_Manager.GV_Buffer.CD.NL = uint.Parse(CD_NL.Text);

            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void UIntCheck(TextBox textBox, int index)
        {
            if (!uint.TryParse(textBox.Text, out _))
            {
                CanParseAllVars |= (1u << index);
                textBox.ForeColor = Color.FromArgb(210, 15, 0);
            }
            else
            {
                CanParseAllVars &= ~(1u << index);
                textBox.ForeColor = SystemColors.WindowText;
            }
        }

        private void CD_NV_TextChanged(object sender, EventArgs e)
        {
            UIntCheck(CD_NV, 0);
        }

        private void CD_NR_TextChanged(object sender, EventArgs e)
        {
            UIntCheck(CD_NR, 1);
        }

        private void CD_NC_TextChanged(object sender, EventArgs e)
        {
            UIntCheck(CD_NC, 2);
        }

        private void CD_NL_TextChanged(object sender, EventArgs e)
        {
            UIntCheck(CD_NL, 3);
        }
    }
}
