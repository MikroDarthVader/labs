﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MK.Windows.AddWithKeyboard
{
    public partial class R : Form
    {
        public R()
        {
            InitializeComponent();
        }

        private uint CanParseAllVars;
        private uint iter;

        private void R_Load(object sender, EventArgs e)
        {
            CanParseAllVars = 0;

            iter = uint.Parse(R_NEXT.Text);
        }

        private void R_Shown(object sender, EventArgs e)
        {
            R_NP.Focus();
        }

        private void IDC_NEXTR_BUTTON_Click(object sender, EventArgs e)
        {
            if (CanParseAllVars > 0)
                return;

            var GV = GV_Manager.GV_Buffer;

            GV.R_Data.Add(new _R_Data(uint.Parse(R_NP.Text), 
                                      uint.Parse(R_NM.Text), 
                                      float.Parse(R_Z.Text)));

            if (iter < GV.CD.NR)
            {
                R_NEXT.Text = (++iter).ToString();
                R_NP.Text = "0";
                R_NM.Text = "0";
                R_Z.Text = "0";
                R_NP.Focus();
            }
            else
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void UIntCheck(TextBox textBox, int index)
        {
            if (!uint.TryParse(textBox.Text, out _))
            {
                CanParseAllVars |= (1u << index);
                textBox.ForeColor = Color.FromArgb(210, 15, 0);
            }
            else
            {
                CanParseAllVars &= ~(1u << index);
                textBox.ForeColor = SystemColors.WindowText;
            }
        }

        private void FloatPozitiveCheck(TextBox textBox, int index)
        {
            if (!float.TryParse(textBox.Text, out float val) || val < 0)
            {
                CanParseAllVars |= (1u << index);
                textBox.ForeColor = Color.FromArgb(210, 15, 0);
            }
            else
            {
                CanParseAllVars &= ~(1u << index);
                textBox.ForeColor = SystemColors.WindowText;
            }
        }

        private void R_NP_TextChanged(object sender, EventArgs e)
        {
            UIntCheck(R_NP, 0);
        }

        private void R_NM_TextChanged(object sender, EventArgs e)
        {
            UIntCheck(R_NM, 1);
        }

        private void R_Z_TextChanged(object sender, EventArgs e)
        {
            FloatPozitiveCheck(R_Z, 2);
        }
    }
}
