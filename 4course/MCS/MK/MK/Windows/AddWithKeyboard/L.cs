﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MK.Windows.AddWithKeyboard
{
    public partial class L : Form
    {
        public L()
        {
            InitializeComponent();
        }

        private uint CanParseAllVars;
        private uint iter;

        private void L_Load(object sender, EventArgs e)
        {
            CanParseAllVars = 0;

            iter = uint.Parse(L_NEXT.Text);
        }

        private void L_Shown(object sender, EventArgs e)
        {
            L_NP.Focus();
        }

        private void IDL_NEXTL_BUTTON_Click(object sender, EventArgs e)
        {
            if (CanParseAllVars > 0)
                return;

            var GV = GV_Manager.GV_Buffer;

            GV.L_Data.Add(new _L_Data(uint.Parse(L_NP.Text), 
                                      uint.Parse(L_NM.Text), 
                                      float.Parse(L_Z.Text)));

            if (iter < GV.CD.NL)
            {
                L_NEXT.Text = (++iter).ToString();
                L_NP.Text = "0";
                L_NM.Text = "0";
                L_Z.Text = "0";
                L_NP.Focus();
            }
            else
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void UIntCheck(TextBox textBox, int index)
        {
            if (!uint.TryParse(textBox.Text, out _))
            {
                CanParseAllVars |= (1u << index);
                textBox.ForeColor = Color.FromArgb(210, 15, 0);
            }
            else
            {
                CanParseAllVars &= ~(1u << index);
                textBox.ForeColor = SystemColors.WindowText;
            }
        }

        private void FloatPozitiveCheck(TextBox textBox, int index)
        {
            if (!float.TryParse(textBox.Text, out float val) || val < 0)
            {
                CanParseAllVars |= (1u << index);
                textBox.ForeColor = Color.FromArgb(210, 15, 0);
            }
            else
            {
                CanParseAllVars &= ~(1u << index);
                textBox.ForeColor = SystemColors.WindowText;
            }
        }

        private void L_NP_TextChanged(object sender, EventArgs e)
        {
            UIntCheck(L_NP, 0);
        }

        private void L_NM_TextChanged(object sender, EventArgs e)
        {
            UIntCheck(L_NM, 1);
        }

        private void L_Z_TextChanged(object sender, EventArgs e)
        {
            FloatPozitiveCheck(L_Z, 2);
        }
    }
}
