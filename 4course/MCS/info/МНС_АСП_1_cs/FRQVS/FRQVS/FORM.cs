﻿
namespace FRQVS
{
    public partial class FrqvsDlg
    {
        void form1_d(ref int[,] in_d, ref float[] z_d, int nd, char td)
        {
            int i, j, l, m, g;
            if (td != 'L')
                for (int kd = 1; kd <= nd; kd++)
                    for (l = 0; l <= 1; l++)
                    {
                        i = in_d[kd, l];
                        if (i == 0) continue;
                        for (m = 0; m <= 1; m++)
                        {
                            j = in_d[kd, m];
                            if (j == 0) continue;
                            g = (1 - 2 * l) * (1 - 2 * m);
                            switch (td)
                            {
                                case 'R':
                                    GV.a[i, j] += g / z_d[kd];
                                    break;
                                case 'C':
                                    GV.b[i, j] += g * z_d[kd];
                                    break;
                            }
                        }
                    }
            else
            {
                for (int kd = 1; kd <= nd; kd++)
                {
                    i = GV.n + kd;
                    GV.b[i, i] = z_d[kd];
                    for (m = 0; m <= 1; m++)
                    {
                        j = in_d[kd, m];
                        if (j == 0) continue;
                        g = 1 - 2 * m;
                        GV.a[i, j] -= g;
                        GV.a[j, i] += g;
                    }
                }
                GV.n += nd;
            }
        }

        private void form_w()
        {
            int i, j;
            double t;
            for (i = 1; i <= GV.n; i++)
                for (j = 1; j <= GV.n; j++)
                {
                    t = GV.b[i, j];
                    if (t != 0)
                        t *= GV.om;
                    GV.w[i, j] = new Complex(GV.a[i, j], t);
                }
        }


        private void form_s()
        {
            for (int i = 1; i <= GV.n; i++)
                GV.w[i, 0] = new Complex(0, 0);
            if (GV.lp != 0)
                GV.w[GV.lp, 0] = new Complex(-1, 0);
            if (GV.lm != 0)
                GV.w[GV.lm, 0] = new Complex(1, 0);
        }
    }
}