

module segmentation
#(
parameter
	IMAGE_WIDTH,
	IMAGE_HEIGHT,
	IGNORED_MAX
)
(
	input clk,
	input reset,
	
	input [10:0] data_stream_in,
	input data_stream_in_valid,
	
	output [31:0] data_stream_out,
	output data_stream_out_valid
);

	reg [10:0] grad [IMAGE_HEIGHT-1:0][IMAGE_WIDTH-1:0];
	reg [31:0] sect [IMAGE_HEIGHT-1:0][IMAGE_WIDTH-1:0];

	reg [1:0] stage;
	
	reg [31:0] crs_grad_row;
	reg [31:0] crs_grad_col;
	
	reg [31:0] crs_sect_row;
	reg [31:0] crs_sect_col;
	
	reg [31:0] valid_sect_ind;
	
	reg [31:0] op_num;
	reg [31:0] op_num_prev;

	initial begin
		stage <= 0;
		
		crs_grad_row <= 0;
		crs_grad_col <= 0;
		
		crs_sect_row <= 0;
		crs_sect_col <= 0;
		
		valid_sect_ind <= 1;
		
		op_num_prev <= 0;
		op_num <= 1;
	end
	
	always@(posedge reset) begin
		stage <= 0;
		
		crs_grad_row <= 0;
		crs_grad_col <= 0;
		
		crs_sect_row <= 0;
		crs_sect_col <= 0;
		
		valid_sect_ind <= 1;
		
		op_num_prev <= 0;
		op_num <= 1;
	end

/*---First stage---*/
	
	wire [11:0] g0_m00 = (crs_sect_row == 0 || crs_sect_col == 0) ? -1 : (grad[crs_sect_row-1][crs_sect_col-1] + IGNORED_MAX);
	wire [11:0] g0_m01 = (crs_sect_row == 0) ? -1 : (grad[crs_sect_row-1][crs_sect_col] + IGNORED_MAX);
	wire [11:0] g0_m02 = (crs_sect_row == 0 || crs_sect_col == IMAGE_WIDTH-1) ? -1 : (grad[crs_sect_row-1][crs_sect_col+1] + IGNORED_MAX);
	
	wire [11:0] g0_m10 = (crs_sect_col == 0) ? -1 : (grad[crs_sect_row][crs_sect_col-1] + IGNORED_MAX);
	wire [11:0] g0_m11 = grad[crs_sect_row][crs_sect_col];
	wire [11:0] g0_m12 = (crs_sect_col == IMAGE_WIDTH-1) ? -1 : (grad[crs_sect_row][crs_sect_col+1] + IGNORED_MAX);
	
	wire [11:0] g0_m20 = (crs_sect_row == IMAGE_HEIGHT-1 || crs_sect_col == 0) ? -1 : (grad[crs_sect_row+1][crs_sect_col-1] + IGNORED_MAX);
	wire [11:0] g0_m21 = (crs_sect_row == IMAGE_HEIGHT-1) ? -1 : (grad[crs_sect_row+1][crs_sect_col] + IGNORED_MAX);
	wire [11:0] g0_m22 = (crs_sect_row == IMAGE_HEIGHT-1 || crs_sect_col == IMAGE_WIDTH-1) ? -1 : (grad[crs_sect_row+1][crs_sect_col+1] + IGNORED_MAX);
	
	wire is_min = g0_m11 <= g0_m00 && g0_m11 <= g0_m01 && g0_m11 <= g0_m02 && g0_m11 <= g0_m10 && g0_m11 <= g0_m12 && g0_m11 <= g0_m20 && g0_m11 <= g0_m21 && g0_m11 <= g0_m22;
	
	// Sect min setup
	always@(posedge clk) begin
		if (stage == 0 && (crs_sect_row + 1 < crs_grad_row || crs_grad_row == IMAGE_HEIGHT))
			if (is_min) begin
				sect[crs_sect_row][crs_sect_col] <= valid_sect_ind;
				valid_sect_ind <= valid_sect_ind+1;
			end
			else
				sect[crs_sect_row][crs_sect_col] <= 0;
	end
	
	// Sect mem counting
	always@(posedge clk) begin
		if (stage == 0 && (crs_sect_row + 1 < crs_grad_row || crs_grad_row == IMAGE_HEIGHT)) begin
			if (crs_sect_col == IMAGE_WIDTH-1) begin
				if (crs_sect_row == IMAGE_HEIGHT - 1)
					stage <= 1;
				else begin
					crs_sect_row <= crs_sect_row+1;
					crs_sect_col <= 0;	
				end
			end
			else
				crs_sect_col <= crs_sect_col+1;
		end
	end
	
	// In counting
	always@(posedge clk) begin
		if (stage == 0 && data_stream_in_valid && crs_grad_row < IMAGE_HEIGHT) begin
			if (crs_grad_col == IMAGE_WIDTH-1) begin
				crs_grad_row <= crs_grad_row+1;
				crs_grad_col <= 0;
			end
			else
				crs_grad_col <= crs_grad_col+1;
		end
	end
	
	// In reading
	always@(posedge clk) begin
		if (stage == 0 && data_stream_in_valid && crs_grad_row < IMAGE_HEIGHT)
			grad[crs_grad_row][crs_grad_col] <= data_stream_in;
	end

/*---First stage---*/

/*---Second stage---*/
	reg [31:0] s0_m01;
	reg [31:0] s0_m10;
	reg [31:0] s0_m12;
	reg [31:0] s0_m21;

	always@(posedge clk) begin
		if (stage == 1) begin
			op_num_prev <= op_num;
			if (op_num_prev != op_num) begin
				for (crs_sect_row = 0; crs_sect_row < IMAGE_HEIGHT; crs_sect_row = crs_sect_row + 1)
					for (crs_sect_col = 0; crs_sect_col < IMAGE_WIDTH; crs_sect_col = crs_sect_col + 1) begin
						if (sect[crs_sect_row][crs_sect_col] > 0) begin
							s0_m01 = (crs_sect_row == 0) ? 0 : sect[crs_sect_row-1][crs_sect_col];
							s0_m10 = (crs_sect_col == 0) ? 0 : sect[crs_sect_row][crs_sect_col-1];
							s0_m12 = (crs_sect_col == IMAGE_WIDTH-1) ? 0 : sect[crs_sect_row][crs_sect_col+1];
							s0_m21 = (crs_sect_row == IMAGE_HEIGHT-1) ? 0 : sect[crs_sect_row+1][crs_sect_col];
							
							if (s0_m01 != 0 && s0_m01 < sect[crs_sect_row][crs_sect_col] || 
								 s0_m10 != 0 && s0_m10 < sect[crs_sect_row][crs_sect_col] || 
								 s0_m12 != 0 && s0_m12 < sect[crs_sect_row][crs_sect_col] || 
								 s0_m21 != 0 && s0_m21 < sect[crs_sect_row][crs_sect_col])
								op_num <= op_num + 1;
							
							if (s0_m01 != 0 && s0_m01 < sect[crs_sect_row][crs_sect_col])
								sect[crs_sect_row][crs_sect_col] <= s0_m01;
							if (s0_m10 != 0 && s0_m10 < sect[crs_sect_row][crs_sect_col])
								sect[crs_sect_row][crs_sect_col] <= s0_m10;
							if (s0_m12 != 0 && s0_m12 < sect[crs_sect_row][crs_sect_col])
								sect[crs_sect_row][crs_sect_col] <= s0_m12;
							if (s0_m21 != 0 && s0_m21 < sect[crs_sect_row][crs_sect_col])
								sect[crs_sect_row][crs_sect_col] <= s0_m21;
						end
					end
			end
			else begin
				stage <= 2;
				op_num_prev <= 0;
				op_num <= 1;
			end
		end
	end

/*---Second stage---*/

/*---Third stage---*/
	reg [10:0] g1_m00;
	reg [10:0] g1_m01;
	reg [10:0] g1_m02;
	reg [10:0] g1_m10;
	reg [10:0] g1_m11;
	reg [10:0] g1_m12;
	reg [10:0] g1_m20;
	reg [10:0] g1_m21;
	reg [10:0] g1_m22;
	
	reg [31:0] s1_m00;
	reg [31:0] s1_m01;
	reg [31:0] s1_m02;
	reg [31:0] s1_m10;
	reg [31:0] s1_m12;
	reg [31:0] s1_m20;
	reg [31:0] s1_m21;
	reg [31:0] s1_m22;

	always@(posedge clk) begin
		if (stage == 2) begin
			op_num_prev <= op_num;
			if (op_num_prev != op_num) begin
				for (crs_sect_row = 0; crs_sect_row < IMAGE_HEIGHT; crs_sect_row = crs_sect_row + 1)
					for (crs_sect_col = 0; crs_sect_col < IMAGE_WIDTH; crs_sect_col = crs_sect_col + 1) begin
						if (sect[crs_sect_row][crs_sect_col] == 0) begin
							op_num <= op_num + 1;
							
							g1_m00 = (crs_sect_row == 0 || crs_sect_col == 0) ? -1 : grad[crs_sect_row-1][crs_sect_col-1];
							g1_m01 = (crs_sect_row == 0) ? -1 : grad[crs_sect_row-1][crs_sect_col];
							g1_m02 = (crs_sect_row == 0 || crs_sect_col == IMAGE_WIDTH-1) ? -1 : grad[crs_sect_row-1][crs_sect_col+1];
							
							g1_m10 = (crs_sect_col == 0) ? -1 : grad[crs_sect_row][crs_sect_col-1];
							g1_m11 = grad[crs_sect_row][crs_sect_col];
							g1_m12 = (crs_sect_col == IMAGE_WIDTH-1) ? -1 : grad[crs_sect_row][crs_sect_col+1];
							
							g1_m20 = (crs_sect_row == IMAGE_HEIGHT-1 || crs_sect_col == 0) ? -1 : grad[crs_sect_row+1][crs_sect_col-1];
							g1_m21 = (crs_sect_row == IMAGE_HEIGHT-1) ? -1 : grad[crs_sect_row+1][crs_sect_col];
							g1_m22 = (crs_sect_row == IMAGE_HEIGHT-1 || crs_sect_col == IMAGE_WIDTH-1) ? -1 : grad[crs_sect_row+1][crs_sect_col+1];
							
							s1_m00 = (crs_sect_row == 0 || crs_sect_col == 0) ? 0 : sect[crs_sect_row-1][crs_sect_col-1];
							s1_m01 = (crs_sect_row == 0) ? 0 : sect[crs_sect_row-1][crs_sect_col];
							s1_m02 = (crs_sect_row == 0 || crs_sect_col == IMAGE_WIDTH-1) ? 0 : sect[crs_sect_row-1][crs_sect_col+1];
							
							s1_m10 = (crs_sect_col == 0) ? 0 : sect[crs_sect_row][crs_sect_col-1];
							s1_m12 = (crs_sect_col == IMAGE_WIDTH-1) ? 0 : sect[crs_sect_row][crs_sect_col+1];
							
							s1_m20 = (crs_sect_row == IMAGE_HEIGHT-1 || crs_sect_col == 0) ? 0 : sect[crs_sect_row+1][crs_sect_col-1];
							s1_m21 = (crs_sect_row == IMAGE_HEIGHT-1) ? 0 : sect[crs_sect_row+1][crs_sect_col];
							s1_m22 = (crs_sect_row == IMAGE_HEIGHT-1 || crs_sect_col == IMAGE_WIDTH-1) ? 0 : sect[crs_sect_row+1][crs_sect_col+1];
							
							if (g1_m00 < g1_m11 && s1_m00 != 0)
								sect[crs_sect_row][crs_sect_col] <= s1_m00;
							else if (g1_m01 < g1_m11 && s1_m01 != 0)
								sect[crs_sect_row][crs_sect_col] <= s1_m01;
							else if (g1_m02 < g1_m11 && s1_m02 != 0)
								sect[crs_sect_row][crs_sect_col] <= s1_m02;
							else if (g1_m10 < g1_m11 && s1_m10 != 0)
								sect[crs_sect_row][crs_sect_col] <= s1_m10;
							else if (g1_m12 < g1_m11 && s1_m12 != 0)
								sect[crs_sect_row][crs_sect_col] <= s1_m12;
							else if (g1_m20 < g1_m11 && s1_m20 != 0)
								sect[crs_sect_row][crs_sect_col] <= s1_m20;
							else if (g1_m21 < g1_m11 && s1_m21 != 0)
								sect[crs_sect_row][crs_sect_col] <= s1_m21;
							else if (g1_m22 < g1_m11 && s1_m22 != 0)
								sect[crs_sect_row][crs_sect_col] <= s1_m22;
						end
					end
			end
			else begin
				stage <= 3;
				crs_sect_row <= 0;
				crs_sect_col <= 0;
			end
		end
	end

/*---Third stage---*/

/*---Output stage---*/
	
	assign data_stream_out_valid = stage == 3 && crs_sect_row < IMAGE_HEIGHT;
	assign data_stream_out = sect[crs_sect_row][crs_sect_col];
	
	// Out counting
	always@(posedge clk) begin
		if (stage == 3 && crs_sect_row < IMAGE_HEIGHT) begin
			if (crs_sect_col == IMAGE_WIDTH-1) begin
				crs_sect_row <= crs_sect_row+1;
				crs_sect_col <= 0;	
			end
			else
				crs_sect_col <= crs_sect_col+1;
		end
	end

/*---Output stage---*/

endmodule
