

module convert_image_stream_24_to_8
(
	input clk,
	input reset,
	
	input [7:0] data_stream_in,
	input data_stream_in_valid,
	
	output [7:0] data_stream_out,
	output reg data_stream_out_valid
);

	reg [1:0] pos;
	reg [7:0] values [2:0];

	assign data_stream_out = (values[0] + values[1] + values[2]) / 3;

	initial begin
		pos <= 2'b00;
	end
	//
	// Perform reset
	always@(posedge reset) begin
		pos <= 2'b00;
	end
	//
	// Counting data from input stream
	always@ (posedge clk) begin
		if (data_stream_in_valid) begin
			if (pos == 2'b10) begin
				pos <= 2'b00;
				data_stream_out_valid <= 1;
			end
			else
				pos <= pos + 1'b1;
		end
	end

	always@(posedge clk) begin
		if (data_stream_out_valid)
			data_stream_out_valid <= 0;
	end
	//
	// Reading data from input stream
	always@ (posedge clk) begin
		if (data_stream_in_valid) begin
			values[pos] = data_stream_in;
		end
	end

endmodule
