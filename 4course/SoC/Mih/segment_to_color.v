

module segment_to_color
(
	input [31:0] data_stream_in,
	input data_stream_in_valid,
	
	output [23:0] data_stream_out,
	output data_stream_out_valid
);

assign data_stream_out_valid = data_stream_in_valid;

wire [11:0] cutted = data_stream_in;

wire [7:0] r = cutted[0] * 8'b11000000 + cutted[3] * 8'b00110000 + cutted[6] * 8'b00001100 + cutted[9] * 8'b00000011;
wire [7:0] g = cutted[1] * 8'b11000000 + cutted[4] * 8'b00110000 + cutted[7] * 8'b00001100 + cutted[10] * 8'b00000011;
wire [7:0] b = cutted[ 2] * 8'b11000000 + cutted[5] * 8'b00110000 + cutted[8] * 8'b00001100 + cutted[11] * 8'b00000011;

assign data_stream_out = {r, g, b};

endmodule
