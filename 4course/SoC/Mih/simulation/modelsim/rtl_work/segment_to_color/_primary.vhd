library verilog;
use verilog.vl_types.all;
entity segment_to_color is
    port(
        data_stream_in  : in     vl_logic_vector(31 downto 0);
        data_stream_in_valid: in     vl_logic;
        data_stream_out : out    vl_logic_vector(23 downto 0);
        data_stream_out_valid: out    vl_logic
    );
end segment_to_color;
