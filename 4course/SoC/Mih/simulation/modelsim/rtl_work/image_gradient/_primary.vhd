library verilog;
use verilog.vl_types.all;
entity image_gradient is
    generic(
        IMAGE_WIDTH     : vl_notype;
        IMAGE_HEIGHT    : vl_notype
    );
    port(
        clk             : in     vl_logic;
        reset           : in     vl_logic;
        data_stream_in  : in     vl_logic_vector(7 downto 0);
        data_stream_in_valid: in     vl_logic;
        data_stream_out : out    vl_logic_vector(10 downto 0);
        data_stream_out_valid: out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of IMAGE_WIDTH : constant is 5;
    attribute mti_svvh_generic_type of IMAGE_HEIGHT : constant is 5;
end image_gradient;
