

`timescale 1ns/1ps

module CourseWork_vlg_tst();

	localparam INPUTFILENAME = "../../img/input/lena.hex"; // Input file name
	localparam IMAGE_WIDTH = 512;
	localparam IMAGE_HEIGHT = 512;
	localparam IGNORED_MAX = 20;

	localparam OUTPUTFILENAME = "../../img/output.bmp"; // Output file name
	localparam LOGFILENAME = "../../img/log.txt"; // log file name

	integer file;
	
	reg clk, reset;
	
	reg [7:0] data_stream_out;
	reg data_stream_out_valid;
	
	convert_image_stream_24_to_8
		convert_24_to_8
	(
		.clk(clk),
		.reset(reset),
		
		.data_stream_in(data_stream_out),
		.data_stream_in_valid(data_stream_out_valid),
		
		.data_stream_out(),
		.data_stream_out_valid()
	);
	
	image_gradient
	#(
		.IMAGE_WIDTH(IMAGE_WIDTH),
		.IMAGE_HEIGHT(IMAGE_HEIGHT)
	)
		image_gradient_0
	(
		.clk(clk),
		.reset(reset),
		
		.data_stream_in(convert_24_to_8.data_stream_out),
		.data_stream_in_valid(convert_24_to_8.data_stream_out_valid),
		
		.data_stream_out(),
		.data_stream_out_valid()
	);
	
	segmentation
	#(
		.IMAGE_WIDTH(IMAGE_WIDTH),
		.IMAGE_HEIGHT(IMAGE_HEIGHT),
		.IGNORED_MAX(IGNORED_MAX)
	)
		segmentation_0
	(
		.clk(clk),
		.reset(reset),
		
		.data_stream_in(image_gradient_0.data_stream_out),
		.data_stream_in_valid(image_gradient_0.data_stream_out_valid),
		
		.data_stream_out(),
		.data_stream_out_valid()
	);
	
	segment_to_color
		segment_to_color_0
	(
		.data_stream_in(segmentation_0.data_stream_out),
		.data_stream_in_valid(segmentation_0.data_stream_out_valid),
		
		.data_stream_out(),
		.data_stream_out_valid()
	);

	create_bmp
	#(
	.OUTPUTFILE(OUTPUTFILENAME),
	.IMAGE_WIDTH(IMAGE_WIDTH),
	.IMAGE_HEIGHT(IMAGE_HEIGHT)
	)
		create_bmp
	(
		.clk(clk),
		.reset(reset),
		
		.data_stream_in(segment_to_color_0.data_stream_out),
		.data_stream_in_valid(segment_to_color_0.data_stream_out_valid)
	);

	initial begin
		clk <= 0;
		forever #10 clk <= ~clk;
	end

	initial begin
		reset <= 0;
		//#150000 reset <= 1;
	end

	initial begin
		file <= $fopen(INPUTFILENAME, "r");
	end

	always@(posedge reset) begin
		$fclose(file);
		file <= $fopen(INPUTFILENAME, "r");
		data_stream_out_valid <= 0;
	end

	always@(posedge clk) begin
		if (reset)
			reset <= 0;
	end

	always@(posedge clk) begin
		if ($fscanf(file, "%x", data_stream_out) == 1)
			data_stream_out_valid <= 1;
		else if (data_stream_out_valid)
			data_stream_out_valid <= 0;
	end
endmodule
