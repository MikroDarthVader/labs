

module image_gradient
#(
parameter
	IMAGE_WIDTH,
	IMAGE_HEIGHT
)
(
	input clk,
	input reset,
	
	input [7:0] data_stream_in,
	input data_stream_in_valid,
	
	output [10:0] data_stream_out,
	output data_stream_out_valid
);

	function [15:0] sqrt;
		 input [31:0] num;  //declare input
		 //intermediate signals.
		 reg [31:0] a;
		 reg [15:0] q;
		 reg [17:0] left,right,r;    
		 integer i;
	begin
		 //initialize all the variables.
		 a = num;
		 q = 0;
		 i = 0;
		 left = 0;   //input to adder/sub
		 right = 0;  //input to adder/sub
		 r = 0;  //remainder
		 //run the calculations for 16 iterations.
		 for(i=0;i<16;i=i+1) begin 
			  right = {q,r[17],1'b1};
			  left = {r[15:0],a[31:30]};
			  a = {a[29:0],2'b00};    //left shift by 2 bits.
			  if (r[17] == 1) //add if r is negative
					r = left + right;
			  else    //subtract if r is positive
					r = left - right;
			  q = {q[14:0],!r[17]};       
		 end
		 sqrt = q;   //final assignment of output.
	end
	endfunction

	reg [7:0] mem [3:0] [IMAGE_WIDTH-1:0];

	reg [31:0] crs_in_img_row;
	reg [31:0] crs_in_img_col;

	wire [1:0] crs_in_mem_row = crs_in_img_row;

	reg [31:0] crs_out_img_row;
	reg [31:0] crs_out_img_col;

	wire [1:0] crs_out_mem_row = crs_out_img_row;
	wire [1:0] crs_out_mem_row_prev = crs_out_mem_row-1;
	wire [1:0] crs_out_mem_row_next = crs_out_mem_row+1;

	wire [7:0] m01 = (crs_out_img_row == 0) ? mem[0][crs_out_img_col] : mem[crs_out_mem_row_prev][crs_out_img_col];
	wire [7:0] m00 = (crs_out_img_col == 0) ? m01 : ((crs_out_img_row == 0) ? mem[crs_out_mem_row][crs_out_img_col-1] : mem[crs_out_mem_row_prev][crs_out_img_col-1]);
	wire [7:0] m02 = (crs_out_img_col == IMAGE_WIDTH-1) ? m01 : ((crs_out_img_row == 0) ? mem[crs_out_mem_row][crs_out_img_col+1] : mem[crs_out_mem_row_prev][crs_out_img_col+1]);

	wire [7:0] m10 = (crs_out_img_col == 0) ? mem[crs_out_mem_row][0] : mem[crs_out_mem_row][crs_out_img_col-1];
	wire [7:0] m11 = mem[crs_out_mem_row][crs_out_img_col];
	wire [7:0] m12 = (crs_out_img_col == IMAGE_WIDTH-1) ? mem[crs_out_mem_row][IMAGE_WIDTH-1] : mem[crs_out_mem_row][crs_out_img_col+1];

	wire [7:0] m21 = (crs_out_img_row == IMAGE_HEIGHT-1) ? mem[crs_out_mem_row][crs_out_img_col] : mem[crs_out_mem_row_next][crs_out_img_col];
	wire [7:0] m20 = (crs_out_img_col == 0) ? m21 : ((crs_out_img_row == IMAGE_HEIGHT-1) ? mem[crs_out_mem_row][crs_out_img_col-1] : mem[crs_out_mem_row_next][crs_out_img_col-1]);
	wire [7:0] m22 = (crs_out_img_col == IMAGE_WIDTH-1) ? m21 : ((crs_out_img_row == IMAGE_HEIGHT-1) ? mem[crs_out_mem_row][crs_out_img_col+1] : mem[crs_out_mem_row_next][crs_out_img_col+1]);

	assign data_stream_out_valid = (crs_in_img_row > crs_out_img_row+1 || crs_in_img_row == IMAGE_HEIGHT) && crs_out_img_row < IMAGE_HEIGHT;
	wire signed [11:0] temp_x = m00 - m02 + m10*2 - m12*2 + m20 - m22;
	wire signed [11:0] temp_y = m00 + m01*2 + m02 - m20 - m21*2 - m22;
	wire signed [11:0] temp_x_0 = (temp_x[11]) ? (~temp_x + 1) : temp_x;
	wire signed [11:0] temp_y_0 = (temp_y[11]) ? (~temp_y + 1) : temp_y;
	assign data_stream_out = sqrt(temp_x_0*temp_x_0 + temp_y_0*temp_y_0);

	initial begin
		crs_in_img_row <= 0;
		crs_in_img_col <= 0;
		
		crs_out_img_row <= 0;
		crs_out_img_col <= 0;
	end
	
	// Reset
	always@(posedge reset) begin
		crs_in_img_row <= 0;
		crs_in_img_col <= 0;
		
		crs_out_img_row <= 0;
		crs_out_img_col <= 0;
	end

	// Out counting
	always@(posedge clk) begin
		if (data_stream_out_valid) begin
			if (crs_out_img_col == IMAGE_WIDTH-1) begin
				crs_out_img_row <= crs_out_img_row+1;
				crs_out_img_col <= 0;
			end
			else
				crs_out_img_col <= crs_out_img_col+1;
		end
	end

	// In counting
	always@(posedge clk) begin
		if (data_stream_in_valid && crs_in_img_row < IMAGE_HEIGHT) begin
			if (crs_in_img_col == IMAGE_WIDTH-1) begin
				crs_in_img_row <= crs_in_img_row+1;
				crs_in_img_col <= 0;
			end
			else
				crs_in_img_col <= crs_in_img_col+1;
		end
	end

	// In processing
	always@(posedge clk) begin
		if (data_stream_in_valid && crs_in_img_row < IMAGE_HEIGHT)
			mem[crs_in_mem_row][crs_in_img_col] <= data_stream_in;
	end

endmodule
