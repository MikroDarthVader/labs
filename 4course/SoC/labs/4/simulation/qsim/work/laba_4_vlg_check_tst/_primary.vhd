library verilog;
use verilog.vl_types.all;
entity laba_4_vlg_check_tst is
    port(
        accum_out       : in     vl_logic_vector(7 downto 0);
        CO              : in     vl_logic;
        overflow        : in     vl_logic;
        SUM             : in     vl_logic_vector(7 downto 0);
        sampler_rx      : in     vl_logic
    );
end laba_4_vlg_check_tst;
