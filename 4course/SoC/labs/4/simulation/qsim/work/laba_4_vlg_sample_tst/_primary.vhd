library verilog;
use verilog.vl_types.all;
entity laba_4_vlg_sample_tst is
    port(
        A               : in     vl_logic_vector(7 downto 0);
        accumulate      : in     vl_logic;
        B               : in     vl_logic_vector(7 downto 0);
        clear           : in     vl_logic;
        clock           : in     vl_logic;
        in1             : in     vl_logic_vector(7 downto 0);
        sampler_tx      : out    vl_logic
    );
end laba_4_vlg_sample_tst;
