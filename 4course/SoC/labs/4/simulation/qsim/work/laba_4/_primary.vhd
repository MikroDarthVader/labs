library verilog;
use verilog.vl_types.all;
entity laba_4 is
    port(
        overflow        : out    vl_logic;
        clock           : in     vl_logic;
        accumulate      : in     vl_logic;
        clear           : in     vl_logic;
        in1             : in     vl_logic_vector(7 downto 0);
        CO              : out    vl_logic;
        A               : in     vl_logic_vector(7 downto 0);
        B               : in     vl_logic_vector(7 downto 0);
        accum_out       : out    vl_logic_vector(7 downto 0);
        SUM             : out    vl_logic_vector(7 downto 0)
    );
end laba_4;
