module _DECODER(input [2:0] a, output reg [4:0] y);
integer N;
always @(a)
	begin
		for (N = 0; N <= 4; N = N + 1)
			if (a == N)
				y[N] = 1;
			else
				y[N] = 0;
	end
endmodule