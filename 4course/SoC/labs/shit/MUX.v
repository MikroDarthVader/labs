module _MUX(a, b, sel, q);
input a, b;
input  [1:0] sel; 
output reg q; 

always @* 
begin 
	case(sel)
		2'b00: q = a; 
		2'b01: q = b;
	endcase 
end 
endmodule