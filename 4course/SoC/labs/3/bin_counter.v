module _BIN_DEC_COUNTER (out, cout, clk, reset);

output wire cout;
output reg [3:0]out;
input clk, reset;

always @(posedge clk or posedge reset)
begin
	if(reset)
		out <= 4'd0;
	else
	if(out==4'd9)
		out <= 4'd0;
	else
		out <= out + 1'd1;
end

assign cout = (out == 4'd9);

endmodule