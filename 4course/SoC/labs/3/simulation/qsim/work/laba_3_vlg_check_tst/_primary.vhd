library verilog;
use verilog.vl_types.all;
entity laba_3_vlg_check_tst is
    port(
        bin_dec_out     : in     vl_logic_vector(3 downto 0);
        bin_out         : in     vl_logic_vector(2 downto 0);
        cout            : in     vl_logic;
        up_down_out     : in     vl_logic_vector(7 downto 0);
        sampler_rx      : in     vl_logic
    );
end laba_3_vlg_check_tst;
