library verilog;
use verilog.vl_types.all;
entity laba_3 is
    port(
        cout            : out    vl_logic;
        clk             : in     vl_logic;
        reset           : in     vl_logic;
        bin_dec_out     : out    vl_logic_vector(3 downto 0);
        bin_out         : out    vl_logic_vector(2 downto 0);
        up_down_out     : out    vl_logic_vector(7 downto 0);
        up_down         : in     vl_logic
    );
end laba_3;
