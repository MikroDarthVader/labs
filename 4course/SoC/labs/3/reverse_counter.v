module _UP_DOWN_COUNTER (out, up_down, clk, reset);

output [7:0] out;
input up_down, clk, reset;
reg [7:0] out;

always @(posedge clk)
if (reset)
	out <= 8'b0;
else if (up_down != 8'b0)
	out <= out + 1;
else 
	out <= out - 1;
endmodule 