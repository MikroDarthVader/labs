module _BIN_COUNTER (out, clk, reset);

output reg [2:0]out;
input clk, reset;

always @(posedge clk or posedge reset)
begin
	if(reset)
		out <= 3'd0;
	else
		out <= out + 1'd1;
end

endmodule