onerror {quit -f}
vlib work
vlog -work work laba_5.vo
vlog -work work laba_5.vt
vsim -novopt -c -t 1ps -L cycloneiv_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.laba_5_vlg_vec_tst
vcd file -direction laba_5.msim.vcd
vcd add -internal laba_5_vlg_vec_tst/*
vcd add -internal laba_5_vlg_vec_tst/i1/*
add wave /*
run -all
