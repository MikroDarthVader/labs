library verilog;
use verilog.vl_types.all;
entity laba_5 is
    port(
        equal           : out    vl_logic;
        a               : in     vl_logic_vector(3 downto 0);
        b               : in     vl_logic_vector(3 downto 0);
        lower           : out    vl_logic;
        greater         : out    vl_logic
    );
end laba_5;
