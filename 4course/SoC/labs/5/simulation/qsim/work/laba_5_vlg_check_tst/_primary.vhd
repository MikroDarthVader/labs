library verilog;
use verilog.vl_types.all;
entity laba_5_vlg_check_tst is
    port(
        equal           : in     vl_logic;
        greater         : in     vl_logic;
        lower           : in     vl_logic;
        sampler_rx      : in     vl_logic
    );
end laba_5_vlg_check_tst;
