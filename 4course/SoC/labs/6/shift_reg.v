module SHIFT_REG (clk, si, dir, so); 
input clk, si, dir; 
output reg[7:0] so; 
reg [7:0] tmp; 
 
	always @(posedge clk)
	begin 
		if (dir) 
		begin
			tmp = tmp << 1;
			tmp[0] = si;
		end
		else
		begin
			tmp = tmp >> 1;
			tmp[7] = si;
		end
		  
		so = tmp;
	end
endmodule 