library verilog;
use verilog.vl_types.all;
entity laba_6 is
    port(
        so              : out    vl_logic_vector(7 downto 0);
        clk             : in     vl_logic;
        si              : in     vl_logic;
        dir             : in     vl_logic
    );
end laba_6;
