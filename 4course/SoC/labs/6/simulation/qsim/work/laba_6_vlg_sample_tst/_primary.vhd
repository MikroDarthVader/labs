library verilog;
use verilog.vl_types.all;
entity laba_6_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        dir             : in     vl_logic;
        si              : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end laba_6_vlg_sample_tst;
