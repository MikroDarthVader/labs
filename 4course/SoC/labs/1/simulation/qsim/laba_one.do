onerror {quit -f}
vlib work
vlog -work work laba_one.vo
vlog -work work laba_one.vt
vsim -novopt -c -t 1ps -L cycloneiv_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.laba_one_vlg_vec_tst
vcd file -direction laba_one.msim.vcd
vcd add -internal laba_one_vlg_vec_tst/*
vcd add -internal laba_one_vlg_vec_tst/i1/*
add wave /*
run -all
