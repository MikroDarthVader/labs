library verilog;
use verilog.vl_types.all;
entity laba_one_vlg_sample_tst is
    port(
        a               : in     vl_logic;
        b               : in     vl_logic;
        selector        : in     vl_logic;
        selector1       : in     vl_logic;
        \signal\        : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end laba_one_vlg_sample_tst;
