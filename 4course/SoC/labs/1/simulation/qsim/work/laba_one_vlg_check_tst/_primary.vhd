library verilog;
use verilog.vl_types.all;
entity laba_one_vlg_check_tst is
    port(
        \out\           : in     vl_logic_vector(1 downto 0);
        out_0           : in     vl_logic;
        sampler_rx      : in     vl_logic
    );
end laba_one_vlg_check_tst;
