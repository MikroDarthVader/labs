library verilog;
use verilog.vl_types.all;
entity laba_one is
    port(
        out_0           : out    vl_logic;
        a               : in     vl_logic;
        b               : in     vl_logic;
        selector        : in     vl_logic;
        \out\           : out    vl_logic_vector(1 downto 0);
        \signal\        : in     vl_logic;
        selector1       : in     vl_logic
    );
end laba_one;
