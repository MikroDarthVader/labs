module _DMUX( signal, selector, out ); 

input signal;
input selector;
output reg [1:0] out;

always @* 
begin 
	case(selector) 
		2'b00: out = { 1'b0, signal };
		2'b01: out = { signal, 1'b0 };
	endcase 
end 
endmodule 