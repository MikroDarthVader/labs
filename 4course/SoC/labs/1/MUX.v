module _MUX(a, b, selector, out);
input a, b;
input selector; 
output reg out; 

always @* 
begin 
	case(selector)
		2'b00: out = a; 
		2'b01: out = b;
	endcase 
end 
endmodule