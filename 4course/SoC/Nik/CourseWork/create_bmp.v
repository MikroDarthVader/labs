

module create_bmp
#(
parameter
	OUTPUTFILE,
	IMAGE_WIDTH,
	IMAGE_HEIGHT
)
(
	input clk,
	input reset,
	
	input [7:0] data_stream_in,
	input data_stream_in_valid
);

localparam MAX_DATA_COUNT = IMAGE_WIDTH * IMAGE_HEIGHT;
localparam FILE_SIZE = MAX_DATA_COUNT + 54;

reg[31:0] data_counter; // counting the number of received data

reg[7:0] bmp_header [53 : 0];
reg[5:0] i;
integer file;

// Windows BMP files begin with a 54-byte header: 
// Check the website to see the value of this header: http://www.fastgraph.com/help/bmp_header_format.html
initial begin
	bmp_header[ 0] = 66;bmp_header[28] = 24;
	bmp_header[ 1] = 77;bmp_header[29] = 0;
	bmp_header[ 2] = FILE_SIZE;bmp_header[30] = 0;
	bmp_header[ 3] = FILE_SIZE >> 8;bmp_header[31] = 0;
	bmp_header[ 4] = FILE_SIZE >> 16;bmp_header[32] = 0;
	bmp_header[ 5] = FILE_SIZE >> 24;bmp_header[33] = 0;
	bmp_header[ 6] =  0;bmp_header[34] = 0;
	bmp_header[ 7] =  0;bmp_header[35] = 0;
	bmp_header[ 8] =  0;bmp_header[36] = 0;
	bmp_header[ 9] =  0;bmp_header[37] = 0;
	bmp_header[10] = 54;bmp_header[38] = 0;
	bmp_header[11] =  0;bmp_header[39] = 0;
	bmp_header[12] =  0;bmp_header[40] = 0;
	bmp_header[13] =  0;bmp_header[41] = 0;
	bmp_header[14] = 40;bmp_header[42] = 0;
	bmp_header[15] =  0;bmp_header[43] = 0;
	bmp_header[16] =  0;bmp_header[44] = 0;
	bmp_header[17] =  0;bmp_header[45] = 0;
	bmp_header[18] =  IMAGE_WIDTH;bmp_header[46] = 0;
	bmp_header[19] =  IMAGE_WIDTH >> 8;bmp_header[47] = 0;
	bmp_header[20] =  IMAGE_WIDTH >> 16;bmp_header[48] = 0;
	bmp_header[21] =  IMAGE_WIDTH >> 24;bmp_header[49] = 0;
	bmp_header[22] =  IMAGE_HEIGHT;bmp_header[50] = 0;
	bmp_header[23] =  IMAGE_HEIGHT >> 8;bmp_header[51] = 0;	
	bmp_header[24] =  IMAGE_HEIGHT >> 16;bmp_header[52] = 0;
	bmp_header[25] =  IMAGE_HEIGHT >> 24;bmp_header[53] = 0;
	bmp_header[26] =  1;
	bmp_header[27] =  0;
end

initial begin
	data_counter <= 0;
	
	file = $fopen(OUTPUTFILE, "wb+");
	for(i=0; i<54; i=i+1)
		$fwrite(file, "%c", bmp_header[i][7:0]); // write the header
end
//
// Perform reset
always@(posedge reset) begin
	data_counter <= 0;
		
	$fclose(file);
	file = $fopen(OUTPUTFILE, "wb+");
	for(i=0; i<54; i=i+1)
		$fwrite(file, "%c", bmp_header[i][7:0]); // write the header
end
//
// Close .bmp file when done writing
always@(posedge clk) begin
	if (data_counter == MAX_DATA_COUNT) begin
		$fclose(file);
		file <= 0;
	end
end
//
// Write .bmp file
always@(posedge clk) begin
	if (data_stream_in_valid && data_counter < MAX_DATA_COUNT) begin
		$fwrite(file, "%c", data_stream_in);
		$fwrite(file, "%c", data_stream_in);
		$fwrite(file, "%c", data_stream_in);
		
		data_counter <= data_counter + 1;
	end
end
endmodule