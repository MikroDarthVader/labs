

module convert_image_stream_from_sectors_8x8
#(
parameter
	IMAGE_WIDTH
)
(
	input clk,
	input reset,
	
	input [7:0] data_stream_in,
	input data_stream_in_valid,
	
	output [7:0] data_stream_out,
	output data_stream_out_valid
);

localparam MEM_LENGTH = IMAGE_WIDTH << 4;
localparam MEM_HALF_LENGTH = IMAGE_WIDTH << 3;
localparam SECT_WIDTH_NUM = IMAGE_WIDTH >> 3;

reg [7:0] mem [MEM_LENGTH-1:0];

reg [31:0] crs_out;

reg [31:0] crs_in_sector;
reg [2:0] crs_in_row;
reg [2:0] crs_in_col;

assign data_stream_out_valid = (crs_out >= MEM_HALF_LENGTH && crs_in_sector < SECT_WIDTH_NUM || crs_out < MEM_HALF_LENGTH && crs_in_sector >= SECT_WIDTH_NUM) ? 1 : 0;
assign data_stream_out = mem[crs_out];

initial begin
	crs_out <= 0;
	
	crs_in_sector <= 0;
	crs_in_row <= 0;
	crs_in_col <= 0;
end
//
// Perform reset
always@(posedge reset) begin
	crs_out <= 0;
	
	crs_in_sector <= 0;
	crs_in_row <= 0;
	crs_in_col <= 0;
end
//
// Reading data from input stream sector by sector
always@(posedge clk) begin
	if (data_stream_in_valid) begin
		if (crs_in_sector < SECT_WIDTH_NUM)
			mem[crs_in_sector*8 + crs_in_row*IMAGE_WIDTH + crs_in_col] <= data_stream_in;
		else
			mem[IMAGE_WIDTH*8 + (crs_in_sector-SECT_WIDTH_NUM)*8 + crs_in_row*IMAGE_WIDTH + crs_in_col] <= data_stream_in;
		
		if (crs_in_col == 7) begin
			if (crs_in_row == 7) begin
				if (crs_in_sector < SECT_WIDTH_NUM*2 - 1)
					crs_in_sector <= crs_in_sector + 1;
				else
					crs_in_sector <= 0;
			end
			
			crs_in_row <= crs_in_row + 1;
		end
					
		crs_in_col <= crs_in_col + 1;
	end
end
//
// Writing data to output stream in standard order
always@(posedge clk) begin
	if (crs_out >= MEM_HALF_LENGTH && crs_in_sector < SECT_WIDTH_NUM || crs_out < MEM_HALF_LENGTH && crs_in_sector >= SECT_WIDTH_NUM)
		if (crs_out < MEM_LENGTH-1)
			crs_out <= crs_out + 1;
		else
			crs_out <= 0;
end

endmodule