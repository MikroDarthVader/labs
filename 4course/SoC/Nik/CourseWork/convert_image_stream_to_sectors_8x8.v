

module convert_image_stream_to_sectors_8x8
#(
parameter
	IMAGE_WIDTH
)
(
	input clk,
	input reset,
	
	input [7:0] data_stream_in,
	input data_stream_in_valid,
	
	output [7:0] data_stream_out,
	output data_stream_out_valid
);

localparam MEM_LENGTH = IMAGE_WIDTH << 4;
localparam MEM_HALF_LENGTH = IMAGE_WIDTH << 3;
localparam SECT_WIDTH_NUM = IMAGE_WIDTH >> 3;

reg [7:0] mem [MEM_LENGTH-1:0];

reg [31:0] crs_in;

reg [31:0] crs_out_sector;
reg [2:0] crs_out_row;
reg [2:0] crs_out_col;

wire [31:0] sector_coord = (crs_out_sector < SECT_WIDTH_NUM) ? crs_out_sector*8 : IMAGE_WIDTH*8 + (crs_out_sector-SECT_WIDTH_NUM)*8;
assign data_stream_out = mem[sector_coord + crs_out_row*IMAGE_WIDTH + crs_out_col];

assign data_stream_out_valid = (crs_in >= MEM_HALF_LENGTH && crs_out_sector < SECT_WIDTH_NUM || crs_in < MEM_HALF_LENGTH && crs_out_sector >= SECT_WIDTH_NUM) ? 1 : 0;

initial begin
	crs_in <= 0;
	
	crs_out_sector <= 0;
	crs_out_row <= 0;
	crs_out_col <= 0;
end
//
// Perform reset
always@(posedge reset) begin
	crs_in <= 0;
	
	crs_out_sector <= 0;
	crs_out_row <= 0;
	crs_out_col <= 0;
end
//
// Writing data to output stream sector by sector
always@(posedge clk) begin
	if (crs_in >= MEM_HALF_LENGTH && crs_out_sector < SECT_WIDTH_NUM || crs_in < MEM_HALF_LENGTH && crs_out_sector >= SECT_WIDTH_NUM) begin
		if (crs_out_col == 7) begin
			if (crs_out_row == 7) begin
				if (crs_out_sector < SECT_WIDTH_NUM*2 - 1)
					crs_out_sector <= crs_out_sector + 1;
				else
					crs_out_sector <= 0;
			end
			
			crs_out_row <= crs_out_row + 1;
		end
					
		crs_out_col <= crs_out_col + 1;
	end
end
//
// Reading data from input stream to mem
always@(posedge clk) begin
	if (data_stream_in_valid) begin
		mem[crs_in] <= data_stream_in;
		
		if (crs_in < MEM_LENGTH-1)
			crs_in <= crs_in + 1;
		else
			crs_in <= 0;
	end
end

endmodule