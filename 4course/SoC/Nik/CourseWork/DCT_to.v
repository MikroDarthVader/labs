

module DCT_to
(
	input clk,
	input reset,
	
	input [7:0] data_stream_in,
	input data_stream_in_valid,
	
	output signed [31:0] data_stream_out,
	output data_stream_out_valid
);

reg in_module;
reg out_module;

wire input_state [1:0];
reg [5:0] in_pos;
reg [5:0] out_pos;
wire signed [31:0] data_out [1:0];

assign input_state[0] = (data_stream_in_valid && ~in_module) ? 1 : 0;
assign input_state[1] = (data_stream_in_valid &&  in_module) ? 1 : 0;

assign data_stream_out_valid = (in_module != out_module) ? 1 : 0;
assign data_stream_out = data_out[out_module];

DCT_8x8_to DCT_8x8[1:0]
(
	.clk(clk),
	
	.input_state({input_state[0], input_state[1]}),
	.in_pos(in_pos),
	.out_pos(out_pos),
	
	.data_stream_in(data_stream_in),
	.data_stream_out({data_out[0], data_out[1]})
);

initial begin
	in_module <= 0;
	out_module <= 0;
	in_pos <= 0;
	out_pos <= 0;
end
//
// Perform reset
always@(posedge reset) begin
	in_module <= 0;
	out_module <= 0;
	in_pos <= 0;
	out_pos <= 0;
end
//
// Counting data from input stream
always@(posedge clk) begin
	if (data_stream_in_valid && ~reset) begin
		in_pos <= in_pos + 1;
		if (in_pos == 63)
			in_module <= ~in_module;
	end
end
//
// Counting data to output stream
always@(posedge clk) begin
	if (data_stream_out_valid && ~reset) begin
		out_pos <= out_pos + 1;
		if (out_pos == 63)
			out_module <= ~out_module;
	end
end

endmodule