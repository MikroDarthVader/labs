

module round
#(
parameter
	FACTOR = 5 // power of two
)
(
	input signed [31:0] data_stream_in,
	input data_stream_in_valid,
	
	output [31:0] data_stream_out,
	output data_stream_out_valid
);

assign data_stream_out_valid = data_stream_in_valid;
wire signed [31:0] poz_in = (data_stream_in[31]) ? (~data_stream_in + 1) : data_stream_in;
wire signed [31:0] calc = ((poz_in >> FACTOR) + poz_in[FACTOR - 1]) << FACTOR;
assign data_stream_out = (data_stream_in[31]) ? (~calc + 1) : calc;

endmodule