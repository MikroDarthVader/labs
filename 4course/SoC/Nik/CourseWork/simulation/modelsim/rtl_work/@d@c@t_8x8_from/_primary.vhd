library verilog;
use verilog.vl_types.all;
entity DCT_8x8_from is
    port(
        clk             : in     vl_logic;
        input_state     : in     vl_logic;
        in_pos          : in     vl_logic_vector(5 downto 0);
        out_pos         : in     vl_logic_vector(5 downto 0);
        data_stream_in  : in     vl_logic_vector(31 downto 0);
        data_stream_out : out    vl_logic_vector(7 downto 0)
    );
end DCT_8x8_from;
