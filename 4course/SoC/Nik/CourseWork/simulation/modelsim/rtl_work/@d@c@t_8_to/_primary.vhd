library verilog;
use verilog.vl_types.all;
entity DCT_8_to is
    port(
        data_in_0       : in     vl_logic_vector(31 downto 0);
        data_in_1       : in     vl_logic_vector(31 downto 0);
        data_in_2       : in     vl_logic_vector(31 downto 0);
        data_in_3       : in     vl_logic_vector(31 downto 0);
        data_in_4       : in     vl_logic_vector(31 downto 0);
        data_in_5       : in     vl_logic_vector(31 downto 0);
        data_in_6       : in     vl_logic_vector(31 downto 0);
        data_in_7       : in     vl_logic_vector(31 downto 0);
        data_out_0      : out    vl_logic_vector(31 downto 0);
        data_out_1      : out    vl_logic_vector(31 downto 0);
        data_out_2      : out    vl_logic_vector(31 downto 0);
        data_out_3      : out    vl_logic_vector(31 downto 0);
        data_out_4      : out    vl_logic_vector(31 downto 0);
        data_out_5      : out    vl_logic_vector(31 downto 0);
        data_out_6      : out    vl_logic_vector(31 downto 0);
        data_out_7      : out    vl_logic_vector(31 downto 0)
    );
end DCT_8_to;
