library verilog;
use verilog.vl_types.all;
entity mult is
    port(
        i               : in     vl_logic_vector(31 downto 0);
        f               : in     vl_logic_vector(11 downto 0);
        r               : out    vl_logic_vector(31 downto 0)
    );
end mult;
