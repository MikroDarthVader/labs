library verilog;
use verilog.vl_types.all;
entity DCT_to is
    port(
        clk             : in     vl_logic;
        reset           : in     vl_logic;
        data_stream_in  : in     vl_logic_vector(7 downto 0);
        data_stream_in_valid: in     vl_logic;
        data_stream_out : out    vl_logic_vector(31 downto 0);
        data_stream_out_valid: out    vl_logic
    );
end DCT_to;
