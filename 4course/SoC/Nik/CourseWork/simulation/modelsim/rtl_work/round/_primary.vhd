library verilog;
use verilog.vl_types.all;
entity round is
    generic(
        FACTOR          : integer := 5
    );
    port(
        data_stream_in  : in     vl_logic_vector(31 downto 0);
        data_stream_in_valid: in     vl_logic;
        data_stream_out : out    vl_logic_vector(31 downto 0);
        data_stream_out_valid: out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of FACTOR : constant is 1;
end round;
