transcript on
if {[file exists gate_work]} {
	vdel -lib gate_work -all
}
vlib gate_work
vmap work gate_work

vlog -vlog01compat -work work +incdir+. {CourseWork_6_1200mv_85c_slow.vo}

vlog -vlog01compat -work work +incdir+D:/folder/fucking_learning/labs/4course/SoC/Nik/CourseWork/simulation/modelsim {D:/folder/fucking_learning/labs/4course/SoC/Nik/CourseWork/simulation/modelsim/CourseWork.vt}

vsim -t 1ps +transport_int_delays +transport_path_delays -L altera_mf_ver -L altera_ver -L lpm_ver -L sgate_ver -L cycloneiv_hssi_ver -L cycloneiv_pcie_hip_ver -L cycloneiv_ver -L gate_work -L work -voptargs="+acc"  CourseWork_vlg_tst

add wave *
view structure
view signals
run -all
