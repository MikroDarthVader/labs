

module DCT_8_to
(
	input signed [31:0] data_in_0,
	input signed [31:0] data_in_1,
	input signed [31:0] data_in_2,
	input signed [31:0] data_in_3,
	input signed [31:0] data_in_4,
	input signed [31:0] data_in_5,
	input signed [31:0] data_in_6,
	input signed [31:0] data_in_7,
	
	output signed [31:0] data_out_0,
	output signed [31:0] data_out_1,
	output signed [31:0] data_out_2,
	output signed [31:0] data_out_3,
	output signed [31:0] data_out_4,
	output signed [31:0] data_out_5,
	output signed [31:0] data_out_6,
	output signed [31:0] data_out_7
);

reg [11:0] c09 = 12'b000110001111; // 0.097545
reg [11:0] c19 = 12'b001100001111; // 0.191342
reg [11:0] c27 = 12'b010001110001; // 0.277785
reg [11:0] c35 = 12'b010110101000; // 0.353553
reg [11:0] c41 = 12'b011010100110; // 0.415735
reg [11:0] c46 = 12'b011101100100; // 0.461940
reg [11:0] c49 = 12'b011111011000; // 0.490393

wire signed [31:0] t0p4 = data_in_0 + data_in_4;
wire signed [31:0] t0m4 = data_in_0 - data_in_4;

// negative values
wire signed [31:0] tm1 = ~data_in_1 + 1;
wire signed [31:0] tm2 = ~data_in_2 + 1;
wire signed [31:0] tm3 = ~data_in_3 + 1;
wire signed [31:0] tm5 = ~data_in_5 + 1;
wire signed [31:0] tm6 = ~data_in_6 + 1;
wire signed [31:0] tm7 = ~data_in_7 + 1;

// multiply results
wire [31:0] t0p4x35, t0m4x35;
wire [31:0] tp1x09, tm1x09, tp1x27, tm1x27, tp1x41, tm1x41, tp1x49, tm1x49;
wire [31:0] tp2x19, tm2x19, tp2x46, tm2x46;
wire [31:0] tp3x09, tm3x09, tp3x27, tm3x27, tp3x41, tm3x41, tp3x49, tm3x49;
wire [31:0] tp5x09, tm5x09, tp5x27, tm5x27, tp5x41, tm5x41, tp5x49, tm5x49;
wire [31:0] tp6x19, tm6x19, tp6x46, tm6x46;
wire [31:0] tp7x09, tm7x09, tp7x27, tm7x27, tp7x41, tm7x41, tp7x49, tm7x49;

// perform multiplies

// zero-fourth
mult mult_0p4x35(t0p4, c35, t0p4x35);
mult mult_0m4x35(t0m4, c35, t0m4x35);

// first
mult mult_p1x09(data_in_1, c09, tp1x09);
mult mult_m1x09(tm1, c09, tm1x09);
mult mult_p1x27(data_in_1, c27, tp1x27);
mult mult_m1x27(tm1, c27, tm1x27);
mult mult_p1x41(data_in_1, c41, tp1x41);
mult mult_m1x41(tm1, c41, tm1x41);
mult mult_p1x49(data_in_1, c49, tp1x49);
mult mult_m1x49(tm1, c49, tm1x49);

// second
mult mult_p2x19(data_in_2, c19, tp2x19);
mult mult_m2x19(tm2, c19, tm2x19);
mult mult_p2x46(data_in_2, c46, tp2x46);
mult mult_m2x46(tm2, c46, tm2x46);

// third
mult mult_p3x09(data_in_3, c09, tp3x09);
mult mult_m3x09(tm3, c09, tm3x09);
mult mult_p3x27(data_in_3, c27, tp3x27);
mult mult_m3x27(tm3, c27, tm3x27);
mult mult_p3x41(data_in_3, c41, tp3x41);
mult mult_m3x41(tm3, c41, tm3x41);
mult mult_p3x49(data_in_3, c49, tp3x49);
mult mult_m3x49(tm3, c49, tm3x49);

//fifth
mult mult_p5x09(data_in_5, c09, tp5x09);
mult mult_m5x09(tm5, c09, tm5x09);
mult mult_p5x27(data_in_5, c27, tp5x27);
mult mult_m5x27(tm5, c27, tm5x27);
mult mult_p5x41(data_in_5, c41, tp5x41);
mult mult_m5x41(tm5, c41, tm5x41);
mult mult_p5x49(data_in_5, c49, tp5x49);
mult mult_m5x49(tm5, c49, tm5x49);

// sixth
mult mult_p6x19(data_in_6, c19, tp6x19);
mult mult_m6x19(tm6, c19, tm6x19);
mult mult_p6x46(data_in_6, c46, tp6x46);
mult mult_m6x46(tm6, c46, tm6x46);

//seventh
mult mult_p7x09(data_in_7, c09, tp7x09);
mult mult_m7x09(tm7, c09, tm7x09);
mult mult_p7x27(data_in_7, c27, tp7x27);
mult mult_m7x27(tm7, c27, tm7x27);
mult mult_p7x41(data_in_7, c41, tp7x41);
mult mult_m7x41(tm7, c41, tm7x41);
mult mult_p7x49(data_in_7, c49, tp7x49);
mult mult_m7x49(tm7, c49, tm7x49);

// perform output
assign data_out_0 = t0p4x35 + tp1x49 + tp2x46 + tp3x41 + tp5x27 + tp6x19 + tp7x09 + 4;
assign data_out_1 = t0m4x35 + tp1x41 + tp2x19 + tm3x09 + tm5x49 + tm6x46 + tm7x27 + 4;
assign data_out_2 = t0m4x35 + tp1x27 + tm2x19 + tm3x49 + tp5x09 + tp6x46 + tp7x41 + 4;
assign data_out_3 = t0p4x35 + tp1x09 + tm2x46 + tm3x27 + tp5x41 + tm6x19 + tm7x49 + 4;
assign data_out_4 = t0p4x35 + tm1x09 + tm2x46 + tp3x27 + tm5x41 + tm6x19 + tp7x49 + 4;
assign data_out_5 = t0m4x35 + tm1x27 + tm2x19 + tp3x49 + tm5x09 + tp6x46 + tm7x41 + 4;
assign data_out_6 = t0m4x35 + tm1x41 + tp2x19 + tp3x09 + tp5x49 + tm6x46 + tp7x27 + 4;
assign data_out_7 = t0p4x35 + tm1x49 + tp2x46 + tm3x41 + tm5x27 + tp6x19 + tm7x09 + 4;

endmodule