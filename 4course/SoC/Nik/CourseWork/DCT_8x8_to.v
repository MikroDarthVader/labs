

module DCT_8x8_to
(
	input clk,
	
	input input_state,
	input [5:0] in_pos,
	input [5:0] out_pos,
	
	input [7:0] data_stream_in,
	output signed [31:0] data_stream_out
);

reg signed [31:0] mem_0 [63:0];
wire signed [31:0] mem_1 [63:0];
wire signed [31:0] mem_2 [63:0];

assign data_stream_out = mem_2[out_pos];

// rows transformation
DCT_8_to DCT_0(mem_0[0], mem_0[1], mem_0[2], mem_0[3], mem_0[4], mem_0[5], mem_0[6], mem_0[7], 
					mem_1[0], mem_1[1], mem_1[2], mem_1[3], mem_1[4], mem_1[5], mem_1[6], mem_1[7]);
DCT_8_to DCT_1(mem_0[8], mem_0[9], mem_0[10], mem_0[11], mem_0[12], mem_0[13], mem_0[14], mem_0[15],
					mem_1[8], mem_1[9], mem_1[10], mem_1[11], mem_1[12], mem_1[13], mem_1[14], mem_1[15]);
DCT_8_to DCT_2(mem_0[16], mem_0[17], mem_0[18], mem_0[19], mem_0[20], mem_0[21], mem_0[22], mem_0[23],
					mem_1[16], mem_1[17], mem_1[18], mem_1[19], mem_1[20], mem_1[21], mem_1[22], mem_1[23]);
DCT_8_to DCT_3(mem_0[24], mem_0[25], mem_0[26], mem_0[27], mem_0[28], mem_0[29], mem_0[30], mem_0[31],
					mem_1[24], mem_1[25], mem_1[26], mem_1[27], mem_1[28], mem_1[29], mem_1[30], mem_1[31]);
DCT_8_to DCT_4(mem_0[32], mem_0[33], mem_0[34], mem_0[35], mem_0[36], mem_0[37], mem_0[38], mem_0[39],
					mem_1[32], mem_1[33], mem_1[34], mem_1[35], mem_1[36], mem_1[37], mem_1[38], mem_1[39]);
DCT_8_to DCT_5(mem_0[40], mem_0[41], mem_0[42], mem_0[43], mem_0[44], mem_0[45], mem_0[46], mem_0[47],
					mem_1[40], mem_1[41], mem_1[42], mem_1[43], mem_1[44], mem_1[45], mem_1[46], mem_1[47]);
DCT_8_to DCT_6(mem_0[48], mem_0[49], mem_0[50], mem_0[51], mem_0[52], mem_0[53], mem_0[54], mem_0[55],
					mem_1[48], mem_1[49], mem_1[50], mem_1[51], mem_1[52], mem_1[53], mem_1[54], mem_1[55]);
DCT_8_to DCT_7(mem_0[56], mem_0[57], mem_0[58], mem_0[59], mem_0[60], mem_0[61], mem_0[62], mem_0[63],
					mem_1[56], mem_1[57], mem_1[58], mem_1[59], mem_1[60], mem_1[61], mem_1[62], mem_1[63]);

// cols transformation
DCT_8_to DCT_8(mem_1[0], mem_1[8], mem_1[16], mem_1[24], mem_1[32], mem_1[40], mem_1[48], mem_1[56], 
					mem_2[0], mem_2[8], mem_2[16], mem_2[24], mem_2[32], mem_2[40], mem_2[48], mem_2[56]);
DCT_8_to DCT_9(mem_1[1], mem_1[9], mem_1[17], mem_1[25], mem_1[33], mem_1[41], mem_1[49], mem_1[57],
					mem_2[1], mem_2[9], mem_2[17], mem_2[25], mem_2[33], mem_2[41], mem_2[49], mem_2[57]);
DCT_8_to DCT_10(mem_1[2], mem_1[10], mem_1[18], mem_1[26], mem_1[34], mem_1[42], mem_1[50], mem_1[58],
					mem_2[2], mem_2[10], mem_2[18], mem_2[26], mem_2[34], mem_2[42], mem_2[50], mem_2[58]);
DCT_8_to DCT_11(mem_1[3], mem_1[11], mem_1[19], mem_1[27], mem_1[35], mem_1[43], mem_1[51], mem_1[59],
					mem_2[3], mem_2[11], mem_2[19], mem_2[27], mem_2[35], mem_2[43], mem_2[51], mem_2[59]);
DCT_8_to DCT_12(mem_1[4], mem_1[12], mem_1[20], mem_1[28], mem_1[36], mem_1[44], mem_1[52], mem_1[60],
					mem_2[4], mem_2[12], mem_2[20], mem_2[28], mem_2[36], mem_2[44], mem_2[52], mem_2[60]);
DCT_8_to DCT_13(mem_1[5], mem_1[13], mem_1[21], mem_1[29], mem_1[37], mem_1[45], mem_1[53], mem_1[61],
					mem_2[5], mem_2[13], mem_2[21], mem_2[29], mem_2[37], mem_2[45], mem_2[53], mem_2[61]);
DCT_8_to DCT_14(mem_1[6], mem_1[14], mem_1[22], mem_1[30], mem_1[38], mem_1[46], mem_1[54], mem_1[62],
					mem_2[6], mem_2[14], mem_2[22], mem_2[30], mem_2[38], mem_2[46], mem_2[54], mem_2[62]);
DCT_8_to DCT_15(mem_1[7], mem_1[15], mem_1[23], mem_1[31], mem_1[39], mem_1[47], mem_1[55], mem_1[63],
					mem_2[7], mem_2[15], mem_2[23], mem_2[31], mem_2[39], mem_2[47], mem_2[55], mem_2[63]);

always@(posedge clk) begin
	if (input_state)
		mem_0[in_pos] <= data_stream_in;
end

endmodule