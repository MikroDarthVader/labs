

module DCT_8_from
(
	input signed [31:0] data_in_0,
	input signed [31:0] data_in_1,
	input signed [31:0] data_in_2,
	input signed [31:0] data_in_3,
	input signed [31:0] data_in_4,
	input signed [31:0] data_in_5,
	input signed [31:0] data_in_6,
	input signed [31:0] data_in_7,
	
	output signed [31:0] data_out_0,
	output signed [31:0] data_out_1,
	output signed [31:0] data_out_2,
	output signed [31:0] data_out_3,
	output signed [31:0] data_out_4,
	output signed [31:0] data_out_5,
	output signed [31:0] data_out_6,
	output signed [31:0] data_out_7
);

reg [11:0] c09 = 12'b000110001111; // 0.097545
reg [11:0] c19 = 12'b001100001111; // 0.191342
reg [11:0] c27 = 12'b010001110001; // 0.277785
reg [11:0] c35 = 12'b010110101000; // 0.353553
reg [11:0] c41 = 12'b011010100110; // 0.415735
reg [11:0] c46 = 12'b011101100100; // 0.461940
reg [11:0] c49 = 12'b011111011000; // 0.490393

// negative values
wire signed [31:0] tm1 = ~data_in_1 + 1;
wire signed [31:0] tm2 = ~data_in_2 + 1;
wire signed [31:0] tm3 = ~data_in_3 + 1;
wire signed [31:0] tm4 = ~data_in_4 + 1;
wire signed [31:0] tm5 = ~data_in_5 + 1;
wire signed [31:0] tm6 = ~data_in_6 + 1;
wire signed [31:0] tm7 = ~data_in_7 + 1;

// addition results
wire signed [31:0] t0r01234567 = data_in_0 + data_in_1 + data_in_2 + data_in_3 + data_in_4 + data_in_5 + data_in_6 + data_in_7;
wire signed [31:0] t4r01234567 = data_in_0 + tm1 + tm2 + data_in_3 + data_in_4 + tm5 + tm6 + data_in_7;
wire signed [31:0] tp0p7, tp0m7;
wire signed [31:0] tp1p6, tp1m6, tm1p6, tm1m6;
wire signed [31:0] tp2p5, tp2m5, tm2p5, tm2m5;
wire signed [31:0] tp3m4, tm3p4, tm3m4;

// perform additions
assign tp0p7 = data_in_0 + data_in_7;
assign tp0m7 = data_in_0 + tm7;

assign tp1p6 = data_in_1 + data_in_6;
assign tp1m6 = data_in_1 + tm6;
assign tm1p6 = tm1 + data_in_6;
assign tm1m6 = tm1 + tm6;

assign tp2p5 = data_in_2 + data_in_5;
assign tp2m5 = data_in_2 + tm5;
assign tm2p5 = tm2 + data_in_5;
assign tm2m5 = tm2 + tm5;

assign tp3m4 = data_in_3 + tm4;
assign tm3p4 = tm3 + data_in_4;
assign tm3m4 = tm3 + tm4;

// multiply results
wire signed [31:0] t0r01234567x35;
wire signed [31:0] t4r01234567x35;
wire signed [31:0] tp0p7x46, tp0p7x19, tp0m7x49, tp0m7x41, tp0m7x27, tp0m7x09;
wire signed [31:0] tp1p6x19, tp1m6x41, tm1p6x09, tm1p6x49, tm1p6x27, tm1m6x46;
wire signed [31:0] tp2p5x46, tp2m5x27, tp2m5x09, tp2m5x41, tm2p5x49, tm2m5x19;
wire signed [31:0] tp3m4x09, tp3m4x41, tm3p4x27, tm3p4x49, tm3m4x19, tm3m4x46;

// perform multiplies

// zeroth, fourth rows
mult mult_t0r(t0r01234567, c35, t0r01234567x35);
mult mult_t4r(t4r01234567, c35, t4r01234567x35);

// zeroth-seventh cols
mult mult_p0p7x46(tp0p7, c46, tp0p7x46);
mult mult_p0p7x19(tp0p7, c19, tp0p7x19);
mult mult_p0m7x49(tp0m7, c49, tp0m7x49);
mult mult_p0m7x41(tp0m7, c41, tp0m7x41);
mult mult_p0m7x27(tp0m7, c27, tp0m7x27);
mult mult_p0m7x09(tp0m7, c09, tp0m7x09);

// first-sixth cols
mult mult_p1p6x19(tp1p6, c19, tp1p6x19);
mult mult_p1m6x41(tp1m6, c41, tp1m6x41);
mult mult_m1p6x09(tm1p6, c09, tm1p6x09);
mult mult_m1p6x49(tm1p6, c49, tm1p6x49);
mult mult_m1p6x27(tm1p6, c27, tm1p6x27);
mult mult_m1m6x46(tm1m6, c46, tm1m6x46);

// second-fifth cols
mult mult_p2p5x46(tp2p5, c46, tp2p5x46);
mult mult_p2m5x27(tp2m5, c27, tp2m5x27);
mult mult_p2m5x09(tp2m5, c09, tp2m5x09);
mult mult_p2m5x41(tp2m5, c41, tp2m5x41);
mult mult_m2p5x49(tm2p5, c49, tm2p5x49);
mult mult_m2m5x19(tm2m5, c19, tm2m5x19);

// third-fourth cols
mult mult_p3m4x09(tp3m4, c09, tp3m4x09);
mult mult_p3m4x41(tp3m4, c41, tp3m4x41);
mult mult_m3p4x27(tm3p4, c27, tm3p4x27);
mult mult_m3p4x49(tm3p4, c49, tm3p4x49);
mult mult_m3m4x19(tm3m4, c19, tm3m4x19);
mult mult_m3m4x46(tm3m4, c46, tm3m4x46);

// perform output
assign data_out_0 = t0r01234567x35;
assign data_out_1 = tp0m7x49 + tp1m6x41 + tp2m5x27 + tp3m4x09 + 2;
assign data_out_2 = tp0p7x46 + tp1p6x19 + tm2m5x19 + tm3m4x46 + 2;
assign data_out_3 = tp0m7x41 + tm1p6x09 + tm2p5x49 + tm3p4x27 + 2;
assign data_out_4 = t4r01234567x35;
assign data_out_5 = tp0m7x27 + tm1p6x49 + tp2m5x09 + tp3m4x41 + 2;
assign data_out_6 = tp0p7x19 + tm1m6x46 + tp2p5x46 + tm3m4x19 + 2;
assign data_out_7 = tp0m7x09 + tm1p6x27 + tp2m5x41 + tm3p4x49 + 2;

endmodule





