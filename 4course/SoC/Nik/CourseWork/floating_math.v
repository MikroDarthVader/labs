

module mult
(
	input signed [31:0] i, // integer
	input [11:0] f, // float
	output signed [31:0] r // result
);

wire signed [31:0] rev = ~i + 1; // reverse
wire signed [31:0] neg = rev * f >> 12;
assign r = (i[31] == 1'b0) ? (i * f) >> 12 : ~neg;

endmodule