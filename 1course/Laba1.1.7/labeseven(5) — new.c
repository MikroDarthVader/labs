#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

char *scanString();
bool contains(char *str, char c);

int main()
{
    char *seps,*text,*max,*currentWord;
    int wordKitty = 0,maxWordNumber = 0, maxWordSize = 0;

    text = scanString();
    seps = scanString();
    currentWord = text;

    while(*text != '\0')
    {
        if(!contains(seps,*text))
        {
            wordKitty++;
            currentWord = text;

            for(; *text != '\0' && !contains(seps,*text); text++);

            if(maxWordSize <= text - currentWord)
            {
                maxWordNumber = wordKitty;
                maxWordSize = text - currentWord;
                max = currentWord;
            }
        }
        else
        {
            *text++ = '\0';
            for(; *text != '\0' && contains(seps,*text); text++);
        }
    }

    if(maxWordSize!=0)
        printf("Max word is '%s' has number %d\n",max,maxWordNumber);
    else
        printf("There are no words");

    free(seps);
    free(text);
    return 0;
}

bool contains(char *str, char c)
{
    int i;
    for(i = 0; str[i]!='\0';i++)
    {
        if(str[i] == c)
            return true;
    }
    return false;
}

char *scanString()
{
    char* out = (char *)calloc(1,sizeof(char));
    int i;
    for(i=0; out[i-1]!='\n'; i++)
    {
        out = (char *)realloc(out,(i+1)*sizeof(char));
        out[i] = getchar();
    }
    out[i-1] = '\0';

    return out;
}
