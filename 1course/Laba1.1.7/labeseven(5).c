#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

char *scan(char *);
int getWordsQuantity(char *text,char *seps);
char **cutOnWords(char *text,char *seps);
int getLength(char *str);
bool contains(char *str, char c);

int main()
{
    char *seps,*text,**words;
    int i,maxWordSize = 0, maxWordNumber;

    seps = (char *)calloc(1,sizeof(char));
    text = (char *)calloc(1,sizeof(char));

    text = scan(text);
    seps = scan(seps);

    words = cutOnWords(text,seps);


    for(i = 0; i < getWordsQuantity(text,seps); i++)
    {
        if(getLength(words[i]) > maxWordSize)
        {
            maxWordSize = getLength(words[i]);
            maxWordNumber = i;
        }
    }

    printf("Max word is '%s' has number %d\n",words[maxWordNumber],maxWordNumber + 1);

    for(i = 0; i < getWordsQuantity(text,seps); i++)
        free(words[i]);
    free(words);

    free(seps);
    free(text);
    return 0;
}

int getWordsQuantity(char *text,char *seps)
{
    int i,counter = 0;
    for(i = 0; text[i]!='\0'; i++)
    {
        if(contains(seps,text[i]) && i>0 && !contains(seps,text[i - 1]))
            counter++;
    }
    return counter;
}

char **cutOnWords(char *text,char *seps)
{
    char **words = calloc(1,sizeof(char*));
    int counter = 0,j = 0,i;
    for(i = 0; text[i]!='\0'; i++)
    {
        if(contains(seps,text[i]) && i>0 && !contains(seps,text[i - 1]))
        {
            words[counter][j] = '\0';

            counter++;
            words = (char**)realloc(words,(counter+1)*sizeof(char*));

            j = 0;
            words[counter] = calloc(1,sizeof(char));
        }
        else if(!contains(seps,text[i]))
        {
            words[counter] = (char*)realloc(words[counter],(j+1)*sizeof(char));
            words[counter][j] = text[i];
            j++;
        }
    }
    return words;
}

int getLength(char *str)
{
    int i;
    for(i = 0; str[i]!='\0';i++);
    return i;
}

bool contains(char *str, char c)
{
    int i;
    for(i = 0; str[i]!='\0';i++)
    {
        if(str[i] == c)
            return true;
    }
    return false;
}

char *scan(char *str)
{
    int i;
    for(i=0; str[i-1]!='\n'; i++)
    {
        str = (char *)realloc(str,(i+1)*sizeof(char));
        str[i] = getchar();
    }
    str[i-1] = '\0';

    return str;
}
