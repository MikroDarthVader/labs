#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*-----------------Structures------------------*/
typedef struct
{
	char *name;
	char *madeFrom;
	int price;
	unsigned int availability : 1;
	unsigned int standardized : 1;
	unsigned int productOfMonth : 1;
}SHOP;
typedef SHOP Product;
#define ProductFields 6
#define DefaultProduct (Product){0,0,0,0,0,0}

typedef struct
{
	int Length;
	void **Values;
}PointerArray;
#define EmptyPointerArray {0,0}

#define true 1
#define false 0
/*-----------------Structures------------------*/

/*---------Pointer Array/List actions----------*/
void Append(PointerArray *self, void *record)
{
	self->Values = realloc(self->Values, (self->Length + 1) * sizeof(void*));
	self->Values[self->Length] = record;
	self->Length++;
}
/*---------Pointer Array/List actions----------*/

/*------------File and Text actions------------*/
char *ReadAllText(char* fileName)
{
	char *result = malloc(1), i;
	int resLen;
	FILE *file = fopen(fileName, "r");
	for (i = fgetc(file), resLen = 0; i != EOF; i = fgetc(file), resLen++)
	{
		result = realloc(result, resLen + 2);
		result[resLen] = i;
	}
	result[resLen] = i;
	fclose(file);
	return result;
}

PointerArray Split(char *str) /* Parsing readed string by separators without copying*/
{
	PointerArray result = EmptyPointerArray;

	if (*str == EOF)
		return result;

	char *i;
	Append(&result, str);
	for (i = str; *i != EOF; i++)
		if (*i == '\n' || *i == '|')
		{
			*i = '\0';
			Append(&result, i + 1);
		}
	*i = '\0';

	return result;
}
/*------------File and Text actions------------*/

/*---------------Product actions---------------*/
PointerArray getProductArray(PointerArray strArr)
{
	int i;
	PointerArray result = EmptyPointerArray;
	for (i = 0; i < strArr.Length; i += ProductFields)
	{
		Product *current = malloc(sizeof(Product));
		current->name = strArr.Values[i];
		current->madeFrom = strArr.Values[i + 1];
		current->price = atoi(strArr.Values[i + 2]);
		current->availability = atoi(strArr.Values[i + 3]);
		current->standardized = atoi(strArr.Values[i + 4]);
		current->productOfMonth = atoi(strArr.Values[i + 5]);
		Append(&result, current);
	}
	return result;
}

void printProduct(Product self)
{
	printf("| %-7s | %-9s | %-5d | %-8s | %-23s | %-16s |\n",
		self.name,
		self.madeFrom,
		self.price,
		(self.availability ? "true" : "false"),
		(self.standardized ? "true" : "false"),
		(self.productOfMonth ? "true" : "false"));
}

void printProductArray(PointerArray array, _Bool(*PrintCondition)(void*))
{
	int i, printedCount;
	for (i = 0, printedCount = 0; i < array.Length; i++)
		if (!PrintCondition || PrintCondition(array.Values[i]))
		{
			if (printedCount == 0)
			{
				printf("\n| INDEX | NAME    | MADE FROM | PRICE | IN STOCK | CORRESPONDS TO STANDARD | PRODUCT OF MONTH |\n");
				printf("+-------+---------+-----------+-------+----------+-------------------------+------------------+\n");
			}
			printf("| %-5d ", i + 1);
			printProduct(*((Product*)array.Values[i]));
			printf("+-------+---------+-----------+-------+----------+-------------------------+------------------+\n");
			printedCount++;
		}

	if (printedCount == 0)
	{
		if (array.Length == 0)
			printf("Data Base is empty!");
		else
			printf("No such Products!");
	}
	printf("\n");
}

_Bool isNotStandardized(Product* self)
{
	return !self->standardized && self->availability;
}

void freeProductArray(PointerArray array)
{
	int i;
	for (i = 0; i < array.Length; i++)
		free(array.Values[i]);
	free(array.Values);
}
/*---------------Product actions---------------*/

/*---------------------UI----------------------*/
void UIAppendProduct(PointerArray *self)
{
	Product *result = malloc(sizeof(Product));
	*result = DefaultProduct;
	result->name = malloc(7);
	result->madeFrom = malloc(9);
	int UserData;

	printf("Enter name:\n\t");
	fgets(result->name, 9, stdin);
	result->name[strlen(result->name) - 1] = '\0';

	printf("Enter manufacturer country:\n\t");
	fgets(result->madeFrom, 10, stdin);
	result->madeFrom[strlen(result->madeFrom) - 1] = '\0';

	printf("Enter price:\n\t");
	scanf("%d", &result->price);

	printf("Enter availability[0 - No/1 - Yes]:\n\t");
	scanf("%d", &UserData);
	result->availability = UserData;

	printf("Enter standardized[0 - No/1 - Yes]:\n\t");
	scanf("%d", &UserData);
	result->standardized = UserData;

	printf("Enter is product of month[0 - No/1 - Yes]:\n\t");
	scanf("%d", &UserData);
	result->productOfMonth = UserData;

	Append(self, result);
}
/*---------------------UI----------------------*/

int main()
{
	char* baseFileString = ReadAllText("CSV.txt");
	PointerArray baseStrings = Split(baseFileString);
	PointerArray records = getProductArray(baseStrings);

	printProductArray(records, NULL);
	printProductArray(records, isNotStandardized);

	int wantToExit = false;
	while (!wantToExit)
	{
		UIAppendProduct(&records);
		printProductArray(records, NULL);
		printProductArray(records, isNotStandardized);

		printf("Do you want to exit? [0 - No/1 - Yes]\n\t");
		scanf("%d", &wantToExit);
	}

	freeProductArray(records);
	free(baseStrings.Values);
	free(baseFileString);

	return 0;
}
