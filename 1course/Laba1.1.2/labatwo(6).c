#include <stdio.h>
#include <Math.h>

int main()
{
    int k;
    for(k=0;k<6;k++)
    {

    double s,x,res = 0,err,errOld,fact;
    int i,j;

    /*scanning*/
    printf("Enter function argument: ");
    scanf("%lf",&x);
    /*scanning*/

    x=M_PI*x/180;   //conversion of degrees to radians
    s = sin(x);     //calculating sinus of X by math library function
    err = fabs(s);        //error between library sinus and my sinus

    /*calculation sin(x) by Taylor series*/
    for(i=1;err>0.0001;i++)
    {
        fact=1;
        for(j=1;j<2*i;j++)
            fact*=j;

        res+=pow(-1,i-1)*pow(x,2*i-1)/fact;
        err=fabs(s-res);
    }
    /*calculation sin(x) by Taylor series*/

    /*printing results*/
    printf("Iterations: %d   ",i);
    printf("Result: %lf   ",res);
    printf("Sin(x): %lf   ",s);
    printf("Error: %lf\n",err);
    /*printing results*/
    }
    return 0;
}
