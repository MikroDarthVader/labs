#include"Comparers.h"

int CmpByPlace(Anime *left, Anime *right)
{
	return left->placeInRanking - right->placeInRanking;
}

int CmpBySeasons(Anime *left, Anime *right)
{
	return left->seasons - right->seasons;
}

int sign(float val)
{
	if (val > 0) return 1;
	if (val < 0) return -1;
	return 0;
}

int CmpByMyRating(Anime *left, Anime *right)
{
	return sign(left->myRating - right->myRating);
}

int CmpByInternetRaiting(Anime *left, Anime *right)
{
	return sign(left->internetRating - right->internetRating);
}

int CmpByStartDate(Anime *left, Anime *right)
{
	return (left->startDate[0] + left->startDate[1] * 32) -
		(right->startDate[0] + right->startDate[1] * 32) +
		(left->startDate[2] - right->startDate[2]) * 384;
}

int CmpByTitle(Anime *left, Anime *right)
{
	return strcmp(left->title, right->title);
}

int CmpByStudio(Anime *left, Anime *right)
{
	return strcmp(left->studio, right->studio);
}

const struct animeComparers AnimeComparers = {
	.CmpByPlace = CmpByPlace,
	.CmpBySeasons = CmpBySeasons,
	.CmpByMyRating = CmpByMyRating,
	.CmpByInternetRaiting = CmpByInternetRaiting,
	.CmpByStartDate = CmpByStartDate,
	.CmpByTitle = CmpByTitle,
	.CmpByStudio = CmpByStudio
};