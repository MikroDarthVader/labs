#include"DataBase.h"

char *FilePath;
PointerList Records, Studios;

char *readFile()
{
	char *result = NULL, i;
	int resLen = 0;

	FILE * file = fopen(FilePath, "r");

	if (!file)
		Exception("File does not exist");

	for (i = fgetc(file); i != EOF; i = fgetc(file), resLen++)
	{
		result = realloc(result, resLen + 2);
		result[resLen] = i;
	}

	result[resLen] = i;

	fclose(file);
	return result;
}

void printAnime(Anime *self)
{
	printf("| %-25s | %-13s | %-7d | %-16d | %-15.1f | %-9.1f | %02d.%02d.%0004d |\n",
		self->title,
		self->studio,
		self->seasons,
		self->placeInRanking,
		self->internetRating,
		self->myRating,
		self->startDate[0],
		self->startDate[1],
		self->startDate[2]);
}

void freeAnime(void *self)
{
	free(((Anime *)self)->title);
	free(((Anime *)self)->startDate);
	free(self);
}

void FreeDB()
{
	List.Free(Studios, free);
	List.Free(Records, freeAnime);
}

void SetDBOrigin(char *filePath)
{
	FilePath = filePath;
}

void InitDB()
{
	Records = EmptyPointerList;
	Studios = EmptyPointerList;
	FilePath = NULL;
}

void LoadDB()
{
	if (!FilePath)
		Exception("File is not pointed");

	char *fileString = readFile();
	PointerList fileTokens = StrSplit(fileString, "|\n", EOF);
	Anime *current = NULL;

	FreeDB();
	Records = EmptyPointerList;
	Studios = EmptyPointerList;

	if (fileTokens.Count % AnimeFields != 0)
		Exception("File broken!");

	for_each(fileTokens, ind, val)
	{
		switch (ind % AnimeFields)
		{
		case 0:
		{
			if (current)
				List.Add(&Records, current);

			current = malloc(sizeof(Anime));
			current->startDate = malloc(DateFields * sizeof(int));
			int len = strlen(val) + 1;
			current->title = malloc(len);

			memcpy(current->title, val, len);
		}
		break;

		case 1:
		{
			char *studio = List.Find(Studios, val, (void(*)(void *, void *))strcmp);
			if (studio)
				current->studio = studio;
			else
			{
				int len = strlen(val) + 1;
				current->studio = malloc(len);
				memcpy(current->studio, val, len);
				List.Add(&Studios, current->studio);
			}
		}
		break;

		case 2:
			current->placeInRanking = atoi(val);
			break;

		case 3:
			current->seasons = atoi(val);
			break;

		case 4:
			current->internetRating = (float)atof(val);
			break;

		case 5:
			current->myRating = (float)atof(val);
			break;

		default:
			current->startDate[ind % AnimeFields - 6] = atoi(val);
			break;
		}
	}
	if (current)
		List.Add(&Records, current);

	List.Free(fileTokens, NULL);
	free(fileString);
}

void SaveDB()
{
	if (!FilePath)
		Exception("File is not pointed");

	FILE *file = fopen(FilePath, "w");

	if (!file)
		Exception("File does not exist");

	for_each(Records, ind, val)
	{
		Anime *anime = val;
		fprintf(file, "%s|%s|%d|%d|%.1f|%.1f|%d|%d|%d\n",
			anime->title,
			anime->studio,
			anime->placeInRanking,
			anime->seasons,
			anime->internetRating,
			anime->myRating,
			anime->startDate[0],
			anime->startDate[1],
			anime->startDate[2]);
	}
	fclose(file);
}

void PrintDB(int(*PrintCondition)(void*, void*), void *ConditionParams)
{

	bool printedAny = false;
	for_each(Records, ind, val)
	{
		if (PrintCondition && PrintCondition(val, ConditionParams) != 0)
			continue;

		if (!printedAny)
		{
			printf("\n| INDEX | TITLE                     | STUDIO        | SEASONS | PLACE IN RANKING | INTERNET RATING | MY RATING | START DATE |\n");
			printf("+-------+---------------------------+---------------+---------+------------------+-----------------+-----------+------------+\n");
			printedAny = true;
		}

		printf("| %-5d ", ind + 1);
		printAnime(val);
		printf("+-------+---------------------------+---------------+---------+------------------+-----------------+-----------+------------+\n");
	}

	if (!printedAny)
	{
		if (Records.Count > 0)
			printf("\nNo such records!\n");
		else
			printf("\nData Base is empty!\n");
	}
	printf("\n");
}

void AddToDB(Anime *record)
{
	List.Add(&Records, record);
}

void SortDB(Comparer cmp, int direction)
{
	List.Sort(&Records, cmp, direction);
}

bool RemoveFromDB(int ind)
{
	return List.Remove(&Records, ind, freeAnime);
}

void *GetDBRecord(int ind)
{
	return List.GetValue(Records, ind);
}

PointerList *GetDBStudios()
{
	return &Studios;
}

const struct dataBase DataBase = {
	.Init = InitDB,
	.Load = LoadDB,
	.SetOrigin = SetDBOrigin,
	.Print = PrintDB,
	.Free = FreeDB,
	.Sort = SortDB,
	.Save = SaveDB,
	.Remove = RemoveFromDB,
	.Add = AddToDB,
	.GetRecord = GetDBRecord,
	.GetStudios = GetDBStudios
};
