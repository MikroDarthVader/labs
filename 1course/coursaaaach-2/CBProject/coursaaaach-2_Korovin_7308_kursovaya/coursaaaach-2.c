﻿#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include"DataBase.h"
#include"Comparers.h"

Anime *UserAnime;
char *UserText;
int UserInt;

bool UI();
void UIPrint();
void UISort();
void UILoad();
void UISave();
void UIRemove();
void UIAdd();
void UICorrect();
void UIHelp();

bool ReadField(Anime *self, int fieldNumber, bool rewrite);
void Read(char** oldstr, int maxLen);
Comparer GetComparer(int ind);
char *GetField(int ind);



int main()
{
	UserAnime = malloc(sizeof(Anime));
	*UserAnime = DefaultAnime;
	UserText = NULL;
	DataBase.Init();
	DataBase.SetOrigin("CSV.txt");
	while (UI());
	DataBase.Free();
	if (UserAnime)
	{
		if (UserAnime->startDate)free(UserAnime->startDate);
		if (UserAnime->title)free(UserAnime->title);
		if (UserAnime->studio)free(UserAnime->studio);
		free(UserAnime);
	}
	if (UserText) free(UserText);
	return 0;
}

bool UI()
{
	printf("Enter your desired action:\n");
	printf("1 - Show help\n");
	printf("2 - Print records\n");
	printf("3 - Load data\n");
	printf("4 - Save data\n");
	printf("5 - Add new record\n");
	printf("6 - Remove record\n");
	printf("7 - Correct record\n");
	printf("8 - Sort records\n");
	printf("9 - Exit\n\t");

	Read(&UserText, 1);
	switch (atoi(UserText))
	{
	case 1: UIHelp(); break;
	case 2: UIPrint(); break;
	case 3: UILoad(); break;
	case 4: UISave(); break;
	case 5: UIAdd(); break;
	case 6: UIRemove(); break;
	case 7: UICorrect(); break;
	case 8: UISort(); break;
	case 9: return false;
	default: printf("\nWrong input!\n\n");
	}
	return true;
}

void UIHelp()
{
	printf("\nThis is an electronic anime database.\n");
	printf("You can display all records on the screen,\n");
	printf("as well as search for the records you are interested in by the selected criterion in the menu item 'Print records'.\n");
	printf("For synchronization with the data file, the functions 'Save data' (copies the active state of the database to a file)\n");
	printf("and 'Load data' (resets the state of the active database to the state of the file) are intended.\n");
	printf("If you need to adjust the database, use the functions 'Add new record' and 'Remove record',\n");
	printf("as well as 'Correct record' function of the values of the existing record.\n");
	printf("To sort records by a certain parameter, select 'Sort records', and then select the sort parameter itself.\n");
	printf("To exit the program, use the menu item 'Exit'.\n\n");
}

void UIRemove()
{
	printf("\nEnter index of record that you want to remove:\n\t");
	Read(&UserText, 0);//0 means that string with any lenth will be readed
	if (DataBase.Remove(atoi(UserText) - 1))
		printf("\nRecord was removed!\n\n");
	else
		printf("\nCan't remove record with such index!\n\n");
}

void UIAdd()
{
	printf("\n");
	int i;
	for (i = 1; i < 8; i++)
	{
		printf("Enter %s:\n\t", GetField(i));
		if (!ReadField(UserAnime, i, true))
			return;
	}
	DataBase.Add(UserAnime);
	UserAnime = malloc(sizeof(Anime));
	*UserAnime = DefaultAnime;
	printf("\nAdded new record!\n\n");
}

void UICorrect()
{
	printf("\nEnter index of record that you want to correct[natural number]:\n\t");
	Read(&UserText, 0);
	Anime *foundAnime = DataBase.GetRecord(atoi(UserText) - 1);

	if (!foundAnime)
	{
		printf("\nWrong input!\n\n");
		return;
	}

	printf("Enter parameter that you want to correct:\n");
	printf("1 - Correct title\n");
	printf("2 - Correct studio\n");
	printf("3 - Correct place in ranking\n");
	printf("4 - Correct seasons count\n");
	printf("5 - Correct internet raiting\n");
	printf("6 - Correct my raiting\n");
	printf("7 - Correct date of start\n\t");

	Read(&UserText, 1);
	UserInt = atoi(UserText);

	if (UserInt < 1 || UserInt > 7)
	{
		printf("\nWrong input!\n\n");
		return;
	}

	printf("Enter new %s:\n\t", GetField(UserInt));
	ReadField(foundAnime, UserInt, true);

	printf("\nRecord was corrected!\n\n");
}

void UILoad()
{
	DataBase.Load();
	printf("\nDatabase was loaded!\n\n");
}

void UISave()
{
	DataBase.Save();
	printf("\nDatabase was saved!\n\n");
}

void UISort()
{
	printf("\nEnter sort parameter:\n");
	printf("1 - Sort by title\n");
	printf("2 - Sort by studio\n");
	printf("3 - Sort by place in ranking\n");
	printf("4 - Sort by seasons count\n");
	printf("5 - Sort by internet raiting\n");
	printf("6 - Sort by my raiting\n");
	printf("7 - Sort by date of start\n\t");

	Read(&UserText, 1);
	UserInt = atoi(UserText);
	if (UserInt < 1 || UserInt > 7)
	{
		printf("\nWrong input!\n\n");
		return;
	}
	Comparer cmp = GetComparer(UserInt);

	printf("Enter desired order[1 - ascending, 2 - descending]:\n\t");
	Read(&UserText, 1);
	UserInt = atoi(UserText) * 2 - 3;//from [1,2] to [-1,1]
	DataBase.Sort(cmp, UserInt);
	printf("\nDatabase was sorted\n\n");
}

void UIPrint()
{
	printf("\nEnter print properties:\n");
	printf("1 - Print all records\n");
	printf("2 - Print with title\n");
	printf("3 - Print all from studio\n");
	printf("4 - Print with place in ranking\n");
	printf("5 - Print all with seasons count\n");
	printf("6 - Print all with internet raiting\n");
	printf("7 - Print all with my raiting\n");
	printf("8 - Print all with date of start\n\t");

	Read(&UserText, 1);
	UserInt = atoi(UserText);

	if (UserInt == 1)
	{
		DataBase.Print(NULL, NULL);
		return;
	}
	if (UserInt < 2 || UserInt > 8)
	{
		printf("\nWrong input!\n\n");
		return;
	}
	UserInt--;
	if (UserInt != 2)
		printf("Enter desired %s:\n\t", GetField(UserInt));
	ReadField(UserAnime, UserInt, false);
	DataBase.Print(GetComparer(UserInt), UserAnime);
}

Comparer GetComparer(int ind)
{
	switch (ind)
	{
	case 1: return AnimeComparers.CmpByTitle;
	case 2: return AnimeComparers.CmpByStudio;
	case 3: return AnimeComparers.CmpByPlace;
	case 4: return AnimeComparers.CmpBySeasons;
	case 5: return AnimeComparers.CmpByInternetRaiting;
	case 6: return AnimeComparers.CmpByMyRating;
	case 7: return AnimeComparers.CmpByStartDate;
	}
	return NULL;
}

char *GetField(int ind)
{
	switch (ind)
	{
	case 1: return "title[string]";
	case 2: return "[1 - new studio, 2 - select existing studio]";
	case 3: return "place in ranking[natural number]";
	case 4: return "seasons count[natural number]";
	case 5: return "internet raiting[positive rational number]";
	case 6: return "my raiting[positive rational number]";
	case 7: return "date of start[day.month.year]";
	}
	return NULL;
}

bool ReadField(Anime *self, int fieldNumber, bool rewrite)
{
	switch (fieldNumber)
	{
	case 1: Read(&self->title, 25);	break;
	case 2:
	{
		bool needToRead = true;
		if (rewrite)
			Read(&UserText, 1);
		PointerList *studios = DataBase.GetStudios();
		if (!rewrite || atoi(UserText) - 1)
		{
			if (studios->Count)
			{
				printf("Enter index of desired studio[natural number]\n");

				needToRead = false;
				for_each((*studios), ind, val)
					printf("%d - %s\n", ind + 1, (char*)val);
				printf("\t");

				Read(&UserText, 0);
				int UserInt = atoi(UserText);

				if (UserInt < 1 || UserInt > studios->Count)
				{
					printf("wrong input\n\n");
					return false;
				}

				self->studio = List.GetValue(*studios, UserInt - 1);
			}
			else if (rewrite)
				printf("Studios database is empty!\n");
		}
		if (rewrite && needToRead)
		{
			printf("Enter desired studio[string]:\n\t");
			Read(&UserText, 13);

			char *studio = List.Find(*studios, UserText, strcmp);
			if (!studio)
				List.Add(studios, UserText);
			self->studio = studio;
			UserText = NULL;
		}
	}
	break;
	case 3: Read(&UserText, 16);	self->placeInRanking = atoi(UserText); break;
	case 4: Read(&UserText, 7);		self->seasons = atoi(UserText);	break;
	case 5: Read(&UserText, 15);	self->internetRating = atof(UserText); break;
	case 6: Read(&UserText, 9);		self->myRating = atof(UserText); break;
	case 7:
	{
		if (!self->startDate)
			self->startDate = malloc(3 * sizeof(int));

		int i;
		for (i = 0; i < DateFields; i++)
			self->startDate[i] = 0;

		Read(&UserText, 10);
		PointerList dateFields = StrSplit(UserText, ".", '\0');

		for_each(dateFields, ind, val)
			self->startDate[ind] = atoi(val);
		List.Free(dateFields, NULL);
	}
	break;
	}
	return true;
}

void Read(char** oldstr, int maxLen)
{
	int i = 0;
	char readed = getchar();
	for (; readed != '\n'; i++, readed = getchar())
	{
		if (i <= maxLen || !maxLen)
		{
			*oldstr = realloc(*oldstr, i + 2);
			(*oldstr)[i] = readed;
		}
	}
	if (!(*oldstr))
		*oldstr = malloc(1);
	(*oldstr)[(i > maxLen && maxLen > 0) ? maxLen : i] = '\0';
}
