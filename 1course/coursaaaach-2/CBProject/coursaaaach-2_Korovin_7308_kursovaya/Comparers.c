#include"Comparers.h"

int CmpByPlace(void *left, void *right)
{
	return ((Anime *)left)->placeInRanking - ((Anime *)right)->placeInRanking;
}

int CmpBySeasons(void *left, void *right)
{
	return ((Anime *)left)->seasons - ((Anime *)right)->seasons;
}

int sign(float val)
{
	if (val > 0) return 1;
	if (val < 0) return -1;
	return 0;
}

int CmpByMyRating(void *left, void *right)
{
	return sign(((Anime *)left)->myRating - ((Anime *)right)->myRating);
}

int CmpByInternetRaiting(void *left, void *right)
{
	return sign(((Anime *)left)->internetRating - ((Anime *)right)->internetRating);
}

int CmpByStartDate(void *left, void *right)
{
	return (((Anime *)left)->startDate[0] + ((Anime *)left)->startDate[1] * 32) -
		(((Anime *)right)->startDate[0] + ((Anime *)right)->startDate[1] * 32) +
		(((Anime *)left)->startDate[2] - ((Anime *)right)->startDate[2]) * 384;
}

int CmpByTitle(void *left, void *right)
{
	return strcmp(((Anime *)left)->title, ((Anime *)right)->title);
}

int CmpByStudio(void *left, void *right)
{
	return strcmp(((Anime *)left)->studio, ((Anime *)right)->studio);
}

const struct animeComparers AnimeComparers = {
	.CmpByPlace = CmpByPlace,
	.CmpBySeasons = CmpBySeasons,
	.CmpByMyRating = CmpByMyRating,
	.CmpByInternetRaiting = CmpByInternetRaiting,
	.CmpByStartDate = CmpByStartDate,
	.CmpByTitle = CmpByTitle,
	.CmpByStudio = CmpByStudio
};
