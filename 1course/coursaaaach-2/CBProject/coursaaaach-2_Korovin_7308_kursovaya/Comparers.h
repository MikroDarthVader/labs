#pragma once
typedef int(*Comparer)(void* left, void* right);

#include"DataBase.h"

struct animeComparers
{
	Comparer CmpByPlace;
	Comparer CmpBySeasons;
	Comparer CmpByMyRating;
	Comparer CmpByInternetRaiting;
	Comparer CmpByStartDate;
	Comparer CmpByTitle;
	Comparer CmpByStudio;
};

extern const struct animeComparers AnimeComparers;