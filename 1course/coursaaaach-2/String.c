#include"String.h"
bool IsCharInString(char ch, const char str[])
{
	int i;
	for (i = 0; i < strlen(str); i++)
		if (str[i] == ch)
			return true;
	return false;
}

PointerList StrSplit(char *str, const char sep[], char end)
{
	PointerList result = EmptyPointerList;

	if (!str)
		return result;

	char *ch = str;

	if (!IsCharInString(*ch, sep) && *ch != end)
		List.Add(&result, str);

	for (; *ch != end; ch++)
		if (IsCharInString(*ch, sep))
		{
			*ch = '\0';
			if (!IsCharInString(*(ch + 1), sep) && *(ch + 1) != end)
				List.Add(&result, ch + 1);
		}
	*ch = '\0';
	return result;
}
