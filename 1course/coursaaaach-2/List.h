#pragma once
#include <stdbool.h>
#include <stdlib.h>
typedef int(*Comparer)(void* left, void* right);

typedef struct PointerListNode
{
	struct PointerListNode *next;
	struct PointerListNode *prev;
	void *Value;
}PointerListNode;

typedef struct
{
	int Count;
	PointerListNode *first;
	PointerListNode *last;
}PointerList;
#define EmptyPointerList (PointerList){ 0,0,0 }

#define for_each(self, ind, val) \
	PointerListNode *listNode = self.first; \
	void *val = listNode ? listNode->Value : NULL; \
	int ind = 0; \
	for(; ind < self.Count && listNode ; listNode = listNode->next, val = listNode? listNode->Value : NULL, ind++)

struct list
{
	void(*Add)(PointerList *self, void *record);
	void*(*Find)(PointerList self, void *record, Comparer cmp);
	bool(*Remove)(PointerList *self, int index, void(*freeValue)(void *));
	void(*Free)(PointerList self, void(*freeValue)(void *));
	void(*Sort)(PointerList *self, Comparer cmp, int direction);
	void*(*GetValue)(PointerList self, int index);
};

extern const struct list List;
