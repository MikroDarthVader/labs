#pragma once
#include<stdio.h>
#include<string.h>
#include"Exception.h"
#include"List.h"
#include"String.h"

typedef struct
{
	char *title;
	char *studio;
	int placeInRanking;
	int seasons;
	float internetRating;
	float myRating;
	int *startDate;
}Anime;
#define AnimeFields 9
#define DateFields 3
#define DefaultAnime (Anime){0,0,0,0,0,0,0}

struct dataBase
{
	void(*Init)();
	void(*Load)();
	void(*SetOrigin)(char *filePath);
	void(*Print)(int(*PrintCondition)(void*, void*), void *ConditionParams);
	void(*Free)();
	void(*Sort)(Comparer cmp, int direction);
	void(*Save)();
	bool(*Remove)(int ind);
	void(*Add)(Anime *record);
	void*(*GetRecord)(int ind);
	PointerList*(*GetStudios)();
};

extern const struct dataBase DataBase;

