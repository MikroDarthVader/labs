#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*-----------------Structures------------------*/
typedef struct
{
	char *title;
	char *studio;
	int placeInRanking;
	int seasons;
	float internetRating;
	float myRating;
	int *startDate;
}Anime;
#define AnimeFields 9
#define DateFields 3
#define DefaultAnime {0,0,0,0,0,0,0}

typedef struct
{
	int Length;
	void **Values;
}PointerArray;
#define EmptyPointerArray {0,0}

typedef struct
{
	int Count;
	struct PointerListNode *first;
	struct PointerListNode *last;
}PointerList;
#define EmptyPointerList {0,0,0}

typedef struct PointerListNode
{
	struct PointerListNode *next;
	struct PointerListNode *prev;
	void *Value;
}PointerListNode;
#define EmptyPointerListNode {0,0,0}

typedef int(*Comparer)(void*, void*);

#define true 1
#define false 0
/*-----------------Structures------------------*/

/*---------Pointer Array/List actions----------*/
void Append(PointerArray *self, void *record)
{
	self->Values = realloc(self->Values, (self->Length + 1) * sizeof(void*));
	self->Values[self->Length] = record;
	self->Length++;
}

/*----*/

void Add(PointerList *self, void *record)
{
	PointerListNode *node = self->first;
	if (!node)
	{
		node = malloc(sizeof(PointerListNode));
		self->first = node;
		node->prev = NULL;
	}
	else
	{
		PointerListNode *prev;
		for (; node; prev = node, node = node->next);

		node = malloc(sizeof(PointerListNode));
		node->prev = prev;
		prev->next = node;
	}
	node->Value = record;
	node->next = NULL;
	self->last = node;
	self->Count++;
}

void* Find(PointerList self, void *record, Comparer cmp)
{
	PointerListNode *node;
	for (node = self.first; node; node = node->next)
	{
		if (cmp(record, node->Value) == 0)
			return node->Value;
	}
	return NULL;
} 

PointerList Reverse(PointerList self)
{
	PointerList result = EmptyPointerList;
	PointerListNode *node;
	for (node = self.last; node; node = node->prev)
		Add(&result, node->Value);
	return result;
}

_Bool Remove(PointerList *self, int index, void(*freeValue)(void *))
{
	if (index >= self->Count || index < 0)
		return false;

	PointerListNode *node;
	int i;
	for (i = 0, node = self->first; i != index; i++, node = node->next);

	if (node->prev)
		node->prev->next = node->next;
	else
		self->first = node->next;

	if (node->next)
		node->next->prev = node->prev;
	else
		self->last = node->prev;

	if (freeValue)
		freeValue(node->Value);
	free(node);

	return true;
}

void FreeList(PointerList self, void(*freeValue)(void *))
{
	PointerListNode *i = self.first;
	PointerListNode *prev;
	while (i)
	{
		if (freeValue)
			freeValue(i->Value);
		prev = i;
		i = prev->next;
		free(prev);
	}
}
/*---------Pointer Array/List actions----------*/

/*------------File and Text actions------------*/
char *ReadAllText(char* fileName)
{
	char *result = malloc(1), i;
	int resLen;
	FILE *file = fopen(fileName, "r");
	for (i = fgetc(file), resLen = 0; i != EOF; i = fgetc(file), resLen++)
	{
		result = realloc(result, resLen + 1);
		result[resLen] = i;
	}
	result[resLen] = i;
	fclose(file);
	return result;
}

PointerArray Split(char *str) /* Parsing readed string by separators without copying*/
{
	PointerArray result = EmptyPointerArray;

	if (*str == EOF)
		return result;

	char *i;
	Append(&result, str);
	for (i = str; *i != EOF; i++)
		if (*i == '\n' || *i == '|')
		{
			*i = '\0';
			Append(&result, i + 1);
		}
	*i = '\0';

	return result;
}
/*------------File and Text actions------------*/

/*----------------Anime actions----------------*/
PointerList getAnimeList(PointerArray strArr, PointerList *studios)
{
	int i, j;
	PointerList result = EmptyPointerList;
	for (i = 0; i < strArr.Length; i += AnimeFields)
	{
		Anime *current = malloc(sizeof(Anime));
		current->title = strArr.Values[i];

		char* studio = Find(*studios, strArr.Values[i + 1], strcmp);
		if (!studio)
		{
			studio = strArr.Values[i + 1];
			Add(studios, studio);
		}

		current->studio = studio;
		current->placeInRanking = atoi(strArr.Values[i + 2]);
		current->seasons = atoi(strArr.Values[i + 3]);
		current->internetRating = (float)atof(strArr.Values[i + 4]);
		current->myRating = (float)atof(strArr.Values[i + 5]);

		current->startDate = malloc(DateFields * sizeof(int));
		for (j = 0; j < DateFields; j++)
			current->startDate[j] = atoi(strArr.Values[i + 6 + j]);
		Add(&result, current);
	}
	return result;
}

void printAnime(Anime *self)
{
	printf("| %-25s | %-11s | %-7d | %-16d | %-15.1f | %-9.1f | %02d.%02d.%d |\n",
		self->title,
		self->studio,
		self->seasons,
		self->placeInRanking,
		self->internetRating,
		self->myRating,
		self->startDate[0],
		self->startDate[1],
		self->startDate[2]);
}

void printAnimeList(PointerList animList)
{
	if (!animList.first)
		printf("Data Base is empty!");
	else
		printf("\n| TITLE                     | STUDIO      | SEASONS | PLACE IN RANKING | INTERNET RATING | MY RATING | START DATE |\n");

	PointerListNode *i;
	for (i = animList.first; i; i = i->next)
		printAnime(i->Value);
	printf("\n");
}

void freeAnime(Anime *anime)
{
	free(anime->startDate);
	free(anime);
}
/*----------------Anime actions----------------*/

int main()
{
	char* baseFileString = ReadAllText("CSV.txt");
	PointerArray baseAnimeStrings = Split(baseFileString);
	PointerList studios = EmptyPointerList;
	PointerList baseList = getAnimeList(baseAnimeStrings, &studios);
	printAnimeList(baseList);

	int index;
	scanf("%d", &index);
	getchar();

	PointerList result = Reverse(baseList);
	if (!Remove(&result, result.Count - index, NULL))
		printf("Can't remove anime from list by that index");
	else
		printAnimeList(result);

	FreeList(result, NULL);
	FreeList(baseList, freeAnime);
	FreeList(studios, NULL);
	free(baseAnimeStrings.Values);
	free(baseFileString);

	getchar();
	return 0;
}