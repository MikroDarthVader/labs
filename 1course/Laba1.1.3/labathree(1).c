#include<stdio.h>

int main()
{
    int arr[1000],tmp,i=-1,j,length;

    do
    {
        i++;
        scanf("%d",&arr[i]);
    }while((arr[0]>=0)==(arr[i]>=0));

    printf("\n");

    length=i;

    for(i=0;i<length;i++)
    {
        if(i<length/4)
        {
            tmp=arr[i];
            arr[i]=arr[length/2-(i+1)];
            arr[length/2-(i+1)]=tmp;
        }
        else if((i>(length-1)/2) && (arr[i]%2==0))
        {
            for(j=i;j<length-1;j++)
            {
                arr[j] = arr[j+1];
            }

            i--;
            length--;
        }
    }

    printf("{ ");
    for(i=0;i<length;i++)
    {
        if(i!=length-1)
            printf("%d , ",arr[i]);
        else
            printf("%d ",arr[i]);
    }
    printf("}\n");
    return 0;

}
