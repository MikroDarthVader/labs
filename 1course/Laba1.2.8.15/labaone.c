#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct STUDENT
{
	char* Name;
	int Kurs;
	int* SES;
};

struct STUD
{
	struct STUDENT* Array;
	int lenth;
};

struct STUD getStudents(char* fpath)
{
	FILE *dataBase = fopen(fpath, "r");
	int i;
	char carret = 0;
	struct STUD result;
	result.Array = malloc(sizeof(struct STUDENT));
	result.lenth = 0;
	while (carret != EOF)
	{
		result.lenth++;
		result.Array = realloc(result.Array, sizeof(struct STUDENT)*result.lenth);
		result.Array[result.lenth - 1].Name = malloc(1);
		result.Array[result.lenth - 1].SES = malloc(4);

		for (i = 0; carret != ' ' && i < 100; i++)
		{
			carret = fgetc(dataBase);
			result.Array[result.lenth - 1].Name = realloc(result.Array[result.lenth - 1].Name, i + 1);
			result.Array[result.lenth - 1].Name[i] = carret;
		}
		result.Array[result.lenth - 1].Name[i - 1] = '\0';

		result.Array[result.lenth - 1].Kurs = fgetc(dataBase) - 48;
		fgetc(dataBase);

		for (i = 0; (carret != '\n') && (carret != EOF) && i < 100; i++)
		{
			carret = fgetc(dataBase);
			result.Array[result.lenth - 1].SES = realloc(result.Array[result.lenth - 1].SES, sizeof(int)*(i + 1));
			result.Array[result.lenth - 1].SES[i] = carret - 48;
		}

		result.Array[result.lenth - 1].SES[i] = -1;
	}
	fclose(dataBase);
	return result;
}

struct STUD SortByNames(struct STUD st)
{
	struct STUDENT tmp;
	int i, j;
	for (i = 0; i < st.lenth; i++)
		for (j = i + 1; j < st.lenth; j++)
			if (strcmp(st.Array[i].Name, st.Array[j].Name) > 0)
			{
				tmp = st.Array[i];
				st.Array[i] = st.Array[j];
				st.Array[j] = tmp;
			}
	return st;
}

float GetAverage(int* SES)
{
	float result = 0;
	int i;
	for (i = 0; SES[i] > 0; i++)
	{
		result += SES[i];
	}
	return result / i;
}

int main()
{
	struct STUD stud = SortByNames(getStudents("CSV.txt"));
	int i, count = 0, kurs;
	float averageAll = 0;
	short flag = 0;

	scanf("%d", &kurs);

	for (i = 0; i < stud.lenth; i++)
		if (stud.Array[i].Kurs == kurs)
		{
			averageAll += GetAverage(stud.Array[i].SES);
			count++;
		}

	if (averageAll)
		averageAll /= count;
	else
	{
		printf("No records of students of this course\n");
		getch();
		return 0;
	}

	for (i = 0; i < stud.lenth; i++)
		if (stud.Array[i].Kurs == kurs && GetAverage(stud.Array[i].SES) > averageAll)
		{
			printf("%s\n", stud.Array[i].Name);
			flag = 1;
		}

	if (!flag)
		printf("All students of this course has the same average marks\n");

	getch();
	return 0;
}