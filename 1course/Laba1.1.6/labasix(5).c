
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int M,N,minSum,maxSum,**a;

int **scan();
int printArray();
int freeArray();
void removeRow(int num);
void processedArray();

int main()
{
    //srand(time(NULL));
    scan();
    printf("\n\nChecking matrix:\n");
    printArray();
    processedArray();
    printf("\n\nResult matrix:\n");
    printArray();
    freeArray();
    return 0;
}

void processedArray()
{
    int sum,i,j;

    for(j = 0; j < M; j++)
    {
        sum = 0;
        for(i = 0; i < N; i++)
        {
            sum+=a[i][j];
        }
        if(sum<minSum || sum>maxSum)
        {
            removeRow(j);
            j--;
        }
    }
}

void removeRow(int num)
{
    int i,j;

    for(i = 0; i < N; i++)
    {
        for(j = num; j < M - 1; j++)
        {
            a[i][j] = a[i][j+1];
        }
        a[i] = (int*)realloc(a[i],(M-1)*sizeof(int));
    }
    M--;
}

int freeArray()
{
    int i;
    for(i = 0; i < N; i++)
    {
        free(a[i]);
        a[i]=NULL;
    }
    free(a);
    a=NULL;
}

int printArray()
{
    int i,j;
    if(a == NULL)
    {
        printf("Array doesn't exist!");
        return -1;
    }
    else
    {
        for(j = 0; j < M; j++)
        {
            for(i = 0; i < N; i++)
            {
                printf("%5d",a[i][j]);
            }
            printf("\n");
        }
    printf("\n");
    }
}

int **scan()
{
    int i,j;

    printf("Enter matrix size (two parameters) and max & min allowable sum: ");
    scanf("%d %d %d %d", &N, &M, &maxSum, &minSum);

    a = (int**)calloc(N,sizeof(int*));
    for(j = 0; j < N; j++)
        a[j] = (int*)calloc(M,sizeof(int));

    printf("\n\nEnter matrix:\n");

    for(j = 0; j < M; j++)
    {
        for(i = 0; i < N; i++)
            scanf("%d",&a[i][j]);
            //a[i][j] = rand() % 1000;
    }
    return a;
}
