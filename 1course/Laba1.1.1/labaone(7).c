#include <stdio.h>
#include <Math.h>

int main()
{
    float aX,aY,bX,bY,cX,cY,dX,dY,pX,pY; //quadrilateral vertices and point
    float area;
    _Bool triangleCutIsAC;
    _Bool sideFromAB,sideFromBC,sideFromCD,sideFromDA,sideFromAC,sideFromBD;
    _Bool isPointInQuadrilateral;

    isPointInQuadrilateral = 0;

    /*scanning*/
    printf("Enter A coordinates\n");
    scanf("%f",&aX);
    scanf("%f",&aY);
    printf("Enter B coordinates\n");
    scanf("%f",&bX);
    scanf("%f",&bY);
    printf("Enter C coordinates\n");
    scanf("%f",&cX);
    scanf("%f",&cY);
    printf("Enter D coordinates\n");
    scanf("%f",&dX);
    scanf("%f",&dY);
    printf("Enter random point coordinates\n");
    scanf("%f",&pX);
    scanf("%f",&pY);
    /*scanning*/

    /*choosing of diagonals for summing the areas of triangles*/
    triangleCutIsAC=(((cX-aX)*(bY-aY) - (cY-aY)*(bX-aX)) * ((cX-aX)*(dY-aY) - (cY-aY)*(dX-aX))) < 0;
    /*choosing of diagonals for summing the areas of triangles*/

    /*determining the position of the point concerning the sides of a quadrilateral by matrix determinant*/
    sideFromAB=((bX-aX)*(pY-aY) - (bY-aY)*(pX-aX)) > 0;
    sideFromBC=((cX-bX)*(pY-bY) - (cY-bY)*(pX-bX)) > 0;
    sideFromCD=((dX-cX)*(pY-cY) - (dY-cY)*(pX-cX)) > 0;
    sideFromDA=((aX-dX)*(pY-dY) - (aY-dY)*(pX-dX)) > 0;
    sideFromAC=((cX-aX)*(pY-aY) - (cY-aY)*(pX-aX)) > 0;
    sideFromBD=((dX-bX)*(pY-bY) - (dY-bY)*(pX-bX)) > 0;
    /*determining the position of the point concerning the sides of a quadrilateral by matrix determinant*/

    /*is point in quadrilateral?*/
    if(triangleCutIsAC)
        isPointInQuadrilateral = ((sideFromAB == sideFromBC && sideFromBC != sideFromAC) ||      //sideFromAC = !sideFromCA
                                 (sideFromCD == sideFromDA && sideFromDA == sideFromAC));
    else
        isPointInQuadrilateral = ((sideFromBC == sideFromCD && sideFromCD != sideFromBD) ||      //sideFromBD = !sideFromDB
                                 (sideFromDA == sideFromAB && sideFromAB == sideFromBD));
    /*is point in quadrilateral?*/

    /*calculating quadrilateral area using Gauss formula*/
    area = (aX*bY - aY*bX + bX*cY - bY * cX + cX*dY - cY * dX + dX*aY - dY*aX)/2.f;
    /*calculating quadrilateral area using Gauss formula*/

    /*printing results*/
    if((pX==aX && pY==aY) || (pX==aX && bY==bY) || (pX==cX && pY==cY) || (pX==dX && pY==dY))
        printf("Vertex of quadrilateral\n");
    else if(isPointInQuadrilateral)
        printf("Inside the quadrilateral\n");
    else
        printf("Outside the quadrilateral\n");

    printf("%f\n",fabs(area));
    /*printing results*/

    return 0;
}
