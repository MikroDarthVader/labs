
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int i, j, k, tmp, M, N, **m;

    /*scanning*/
    printf("Enter matrix size (two parameters): ");
    scanf("%d %d", &N, &M);
    /*scanning*/

    /*Generating random matrix NxM*/
    printf("\nGenerating random matrix %dx%d...\n", N, M);
    srand(time(NULL));
    m = (int**)calloc(M, sizeof(int*));
    for(i = 0; i < M; i++)
    {
        m[i] = (int*)calloc(N, sizeof(int));
        for(j = 0; j < N; j++)
        {
            m[i][j] = rand() / 1000;
            printf("%5d",m[i][j]);
        }
        printf("\n");
    }
    printf("Random matrix %dx%d generated\n", N, M);
    /*Generating random matrix NxM*/

    /*Sorting columns*/
    printf("\nOperating with columns...\n");
    for(j = 0; j < N; j++)
    {
        if(j%2==0)
        {
            for(i = 0; i < M - 1; i++)
            {
                for(k = 0; k < M - 1; k++)
                {
                    if(m[k][j]>m[k+1][j])
                    {
                            tmp = m[k][j];
                            m[k][j] = m[k+1][j];
                            m[k+1][j] = tmp;
                    }
                }
            }
        }
       else
        {
            for(i = 0; i < M - 1; i++)
            {
                for(k = 0; k < M - 1; k++)
                {
                    if(m[k][j]<m[k+1][j])
                    {
                            tmp = m[k][j];
                            m[k][j] = m[k+1][j];
                            m[k+1][j] = tmp;
                    }
                }
            }
        }
    }
    printf("Matrix was operated\n");
    /*Sorting columns*/

    /*Printing results*/
    printf("\nPrinting results...\n");
    for(i = 0; i < M; i++)
    {
        for(j = 0; j < N; j++)
        {
            printf("%5d",m[i][j]);
        }
        free(m[i]);
        m[i]=NULL;

        printf("\n");
    }
    /*Printing results*/

    free(m);
    m=NULL;
    return 0;
}
