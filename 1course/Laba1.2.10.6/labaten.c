#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*-----------------Structures------------------*/
typedef struct
{
	char *title;
	char *studio;
	int placeInRanking;
	int seasons;
	float internetRating;
	float myRating;
	int *startDate;
}Anime;
#define AnimeFields 9
#define DateFields 3
#define DefaultAnime {0,0,0,0,0,0,0}

typedef struct
{
	int Length;
	void **Values;
}PointerArray;
#define EmptyPointerArray {0,NULL}

typedef struct
{
	struct PointerListNode *head;//buffer element without value
}PointerList;
#define EmptyPointerList {NULL}

typedef struct PointerListNode
{
	struct PointerListNode *next;
	void *Value;
}PointerListNode;
#define EmptyPointerListNode {NULL,NULL}
/*-----------------Structures------------------*/

/*---------Pointer Array/List actions----------*/
void Append(PointerArray *self, void *record)
{
	self->Values = realloc(self->Values, (self->Length + 1) * sizeof(void*));
	self->Values[self->Length] = record;
	self->Length++;
}

PointerList InitPointerList()
{
	PointerList self;
	self.head = malloc(sizeof(PointerListNode));
	*(self.head) = (PointerListNode)EmptyPointerListNode;
	return self;
}

void Add(PointerList *self, void *record)
{
	PointerListNode *node;
	for (node = self->head; node->next != NULL; node = node->next);
	node->next = malloc(sizeof(PointerListNode));
	node->next->Value = record;
	node->next->next = NULL;
}

int RemovePenultimate(PointerList *self)
{
	PointerListNode *first = self->head->next;
	if (first && first->next)
	{
		PointerListNode *node, *penultimate;
		for (node = self->head; node->next->next->next; node = node->next);
		penultimate = node->next;
		node->next = node->next->next;

		free(((Anime*)penultimate->Value)->startDate);
		free(penultimate->Value);
		free(penultimate);
		return 1;
	}
	return 0;
}
/*---------Pointer Array/List actions----------*/

/*------------File and Text actions------------*/
char *ReadAllText(char* fileName)
{
	char *result = malloc(1), i;
	int resLen;
	FILE *file = fopen(fileName, "r");
	for (i = fgetc(file), resLen = 0; i != EOF; i = fgetc(file), resLen++)
	{
		result = realloc(result, resLen + 2);
		result[resLen] = i;
	}
	result[resLen] = i;
	fclose(file);
	return result;
}

PointerArray Split(char *str) /* Parsing readed string by separators without copying*/
{
	PointerArray result = EmptyPointerArray;

	if (*str == EOF)
		return result;

	char *i;
	Append(&result, str);
	for (i = str; *i != EOF; i++)
		if (*i == '\n' || *i == '|')
		{
			*i = '\0';
			Append(&result, i + 1);
		}
	*i = '\0';

	return result;
}
/*------------File and Text actions------------*/

/*----------------Anime actions----------------*/
PointerList getAnimeList(PointerArray strArr)
{
	int i, j;
	PointerList result = InitPointerList();
	for (i = 0; i < strArr.Length; i += AnimeFields)
	{
		Anime *current = malloc(sizeof(Anime));
		current->title = strArr.Values[i];
		current->studio = strArr.Values[i + 1];
		current->placeInRanking = atoi(strArr.Values[i + 2]);
		current->seasons = atoi(strArr.Values[i + 3]);
		current->internetRating = (float)atof(strArr.Values[i + 4]);
		current->myRating = (float)atof(strArr.Values[i + 5]);

		current->startDate = malloc(DateFields * sizeof(int));
		for (j = 0; j < DateFields; j++)
			current->startDate[j] = atoi(strArr.Values[i + 6 + j]);
		Add(&result, current);
	}
	return result;
}

void printAnime(Anime *self)
{
	printf("| %-25s | %-11s | %-7d | %-16d | %-15.1f | %-9.1f | %02d.%02d.%d |\n",
		self->title,
		self->studio,
		self->seasons,
		self->placeInRanking,
		self->internetRating,
		self->myRating,
		self->startDate[0],
		self->startDate[1],
		self->startDate[2]);
	printf("+---------------------------+-------------+---------+------------------+-----------------+-----------+------------+\n");
}

void printAnimeList(PointerList animList)
{
	int printedCount;
	PointerListNode *i;
	for (i = animList.head->next, printedCount = 0; i; i = i->next, printedCount++)
	{
		if (printedCount == 0)
		{
			printf("\n| TITLE                     | STUDIO      | SEASONS | PLACE IN RANKING | INTERNET RATING | MY RATING | START DATE |\n");
			printf("+---------------------------+-------------+---------+------------------+-----------------+-----------+------------+\n");
		}
		printAnime(i->Value);
	}

	if (printedCount == 0)
	{
		printf("Data Base is empty!");
	}
	printf("\n");
}

void freeAnimeList(PointerList animList)
{
	PointerListNode *i = animList.head;
	PointerListNode *prev;
	while (i)
	{
		prev = i;
		i = i->next;
		if (prev->Value)
		{
			free(((Anime*)prev->Value)->startDate);
			free(prev->Value);
		}
		free(prev);
	}
}
/*----------------Anime actions----------------*/

int main()
{
	char* baseFileString = ReadAllText("CSV.txt");
	PointerArray baseAnimeStrings = Split(baseFileString);
	PointerList baseList = getAnimeList(baseAnimeStrings);

	printAnimeList(baseList);

	if (!RemovePenultimate(&baseList))
		printf("It is impossible to remove the penultimate element from the list consisting of fewer than two elements");
	else
		printAnimeList(baseList);

	freeAnimeList(baseList);
	free(baseAnimeStrings.Values);
	free(baseFileString);

	getchar();
	return 0;
}