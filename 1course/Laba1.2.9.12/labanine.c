#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*-----------------Structures------------------*/
typedef struct
{
	char *title;
	char *studio;
	int placeInRanking;
	int seasons;
	float internetRating;
	float myRating;
	int *startDate;
}Anime;
#define AnimeFields 9
#define DateFields 3
#define DefaultAnime {0,0,0,0,0,0,0}

typedef struct
{
	int Length;
	void **Values;
}PointerArray;
#define EmptyPointerArray {0,NULL}

typedef int(*Comparer)(void*, void*);
/*-----------------Structures------------------*/

/*------------Pointer Array actions------------*/
void Append(PointerArray *self, void *record)
{
	self->Values = realloc(self->Values, (self->Length + 1) * sizeof(void*));
	self->Values[self->Length] = record;
	self->Length++;
}

void SortByComparer(PointerArray *self, Comparer cmp)
{
	if (!cmp)
		return;

	void *tmp;
	int i, j;
	for (i = 0; i < self->Length; i++)
		for (j = i + 1; j < self->Length; j++)
			if (cmp(self->Values[i], self->Values[j]) < 0)
			{
				tmp = self->Values[i];
				self->Values[i] = self->Values[j];
				self->Values[j] = tmp;
			}
}
/*------------Pointer Array actions------------*/

/*------------File and Text actions------------*/
char *ReadAllText(char *fileName)
{
	char *result = malloc(1), i;
	int resLen;
	FILE *file = fopen(fileName, "r");
	for (i = fgetc(file), resLen = 0; i != EOF; i = fgetc(file), resLen++)
	{
		result = realloc(result, resLen + 1);
		result[resLen] = i;
	}
	result[resLen] = i;
	fclose(file);
	return result;
}

PointerArray Split(char *str) /* Parsing readed string by separators without copying*/
{
	PointerArray result = EmptyPointerArray;

	if (*str == EOF)
		return result;

	char *i;
	Append(&result, str);
	for (i = str; *i != EOF; i++)
		if (*i == '\n' || *i == '|')
		{
			*i = '\0';
			Append(&result, i + 1);
		}
	*i = '\0';

	return result;
}
/*------------File and Text actions------------*/

/*----------------Anime actions----------------*/
PointerArray getAnimeArray(PointerArray strArr)
{
	int i, j;
	PointerArray result = EmptyPointerArray;
	for (i = 0; i < strArr.Length; i += AnimeFields)
	{
		Anime *current = malloc(sizeof(Anime));
		current->title = strArr.Values[i];
		current->studio = strArr.Values[i + 1];
		current->placeInRanking = atoi(strArr.Values[i + 2]);
		current->seasons = atoi(strArr.Values[i + 3]);
		current->internetRating = atof(strArr.Values[i + 4]);
		current->myRating = atof(strArr.Values[i + 5]);

		current->startDate = malloc(DateFields * sizeof(int));
		for (j = 0; j < DateFields; j++)
			current->startDate[j] = atoi(strArr.Values[i + 6 + j]);
		Append(&result, current);
	}
	return result;
}

void printAnime(Anime *self)
{
	printf("| %-25s | %-11s | %-7d | %-16d | %-15.1f | %-9.1f | %02d.%02d.%d |\n",
		self->title,
		self->studio,
		self->seasons,
		self->placeInRanking,
		self->internetRating,
		self->myRating,
		self->startDate[0],
		self->startDate[1],
		self->startDate[2]);
	printf("+---------------------------+-------------+---------+------------------+-----------------+-----------+------------+\n");
}

void printAnimeArray(PointerArray animArr, int(*PrintCondition)(void*, void*), void *ConditionParams)
{
	int i, printedCount;
	for (i = 0, printedCount = 0; i < animArr.Length; i++)
		if (!PrintCondition || PrintCondition(animArr.Values[i], ConditionParams))
		{
			if (printedCount == 0)
			{
				printf("\n| TITLE                     | STUDIO      | SEASONS | PLACE IN RANKING | INTERNET RATING | MY RATING | START DATE |\n");
				printf("+---------------------------+-------------+---------+------------------+-----------------+-----------+------------+\n");
			}
			printAnime(animArr.Values[i]);
			printedCount++;
		}

	if (printedCount == 0)
	{
		if (animArr.Length == 0)
			printf("Data Base is empty!");
		else
			printf("No such anime!");
	}
	printf("\n");
}

void freeAnimeArray(PointerArray animArr)
{
	int i;
	for (i = 0; i < animArr.Length; i++)
	{
		free(((Anime*)animArr.Values[i])->startDate);
		free(animArr.Values[i]);
	}
	free(animArr.Values);
}
/*----------------Anime actions----------------*/

/*------------------Comparers------------------*/
int cmpByDay(Anime *left, Anime *right)
{
	return left->startDate[0] - right->startDate[0];
}

int cmpByMonth(Anime *left, Anime *right)
{
	return left->startDate[1] - right->startDate[1];
}

int cmpByYear(Anime *left, Anime *right)
{
	return left->startDate[2] - right->startDate[2];
}

int isDateInInterval(Anime *self, int *params)
{
	//params[0] = min
	//params[1] = max
	//params[2] = day/month/year (0,1,2)
	if (self->startDate[params[2]] < params[0] || self->startDate[params[2]] > params[1])
		return 0;
	return 1;
}
/*------------------Comparers------------------*/

/*---------------------UI----------------------*/
void UIAppendAnime(PointerArray *self)
{
	Anime *result = malloc(sizeof(Anime));
	*result = (Anime)DefaultAnime;
	result->title = malloc(25);
	result->studio = malloc(11);
	result->startDate = malloc(sizeof(int)*DateFields);

	printf("Enter title:\n\t");
	getchar();
	fgets(result->title, 25, stdin);
	result->title[strlen(result->title) - 1] = '\0';
	printf("Enter studio:\n\t");
	fgets(result->studio, 11, stdin);
	result->studio[strlen(result->studio) - 1] = '\0';
	printf("Enter seasons count:\n\t");
	scanf("%d", &result->seasons);
	printf("Enter place in internet ranking:\n\t");
	scanf("%d", &result->placeInRanking);
	printf("Enter internet rating:\n\t");
	scanf("%f", &result->internetRating);
	printf("Enter your rating:\n\t");
	scanf("%f", &result->myRating);
	printf("Enter start date:\n\t");
	scanf("%d.%d.%d", &result->startDate[0],
		&result->startDate[1],
		&result->startDate[2]);
	Append(self, result);
}

void UISortAnime(PointerArray *self)
{
	printf("Enter anime sorting parameter:\n");
	printf("1 - sort by Day\n");
	printf("2 - sort by Month\n");
	printf("3 - sort by Year\n\t");

	int userData;
	scanf("%d", &userData);
	Comparer cmp = NULL;

	switch (userData)
	{
	case 1: cmp = cmpByDay; break;
	case 2: cmp = cmpByMonth; break;
	case 3: cmp = cmpByYear; break;
	default: printf("wrong input\n\n"); return; break;
	}

	SortByComparer(self, cmp);
}

void UIPrintAnimeArray(PointerArray *self)
{
	int* userData = malloc(3 * sizeof(int));
	printf("Enter data field for check the entrence to interval:\n1 - day\n2 - month\n3 - year\n4 - Print all records\n\t");
	scanf("%d", &userData[2]);
	if (userData[2] != 4)
	{
		userData[2]--;
		printf("Enter minimal limit\n\t");
		scanf("%d", &userData[0]);
		printf("Enter maximal limit\n\t");
		scanf("%d", &userData[1]);
		UISortAnime(self);
		printAnimeArray(*self, isDateInInterval, userData);
	}
	else
		printAnimeArray(*self, NULL, NULL);
	free(userData);
}

int UI(PointerArray *self)
{
	printf("Enter your desired action:\n");
	printf("1 - exit\n");
	printf("2 - print records\n");
	printf("3 - append new record\n\t");

	int userData;
	scanf("%d", &userData);

	switch (userData)
	{
	case 1: return 0;
	case 2: UIPrintAnimeArray(self); break;
	case 3: UIAppendAnime(self); break;
	default: printf("wrong input\n\n"); break;
	}

	return 1;
}
/*---------------------UI----------------------*/

int main()
{
	char* fileString = ReadAllText("CSV.txt");
	PointerArray animeStrings = Split(fileString);
	PointerArray records = getAnimeArray(animeStrings);

	while (UI(&records));

	freeAnimeArray(records);
	free(animeStrings.Values);
	free(fileString);

	return 0;
}