#include<string.h>
#include<stdio.h>
#include<stdlib.h>

void ProcessingText();                  //Processing entered text by following steps:
                                        //  1)Replace words with minimal length in strings with odd words count by substitutional word(insWord)
                                        //  2)Sort strings by count of replacements
                                        //  3)Remove strings that had not been moved by sorting

void EnterValues();                     //Entering:
                                        //  1)Strings count (stringsQuantity)
                                        //  2)Text (text)
                                        //  3)String of separators (seps)
                                        //  4)Substitutional word (insWord)

void printText(int, char**);            //Printing text as is
int getCommand();                       //Get command from user
char** scanText(int);                   //Scan text as strings array from user's input
int    getWordsQuantity(char*);         //Get count of words from string
char** getWords(char*);                 //Get words from string as strings array
int    getWordLentgth(char*);           //Get length of the word
char*  wordToString(char*);             //Create string by word from text
char*  replaceWord(char*, char*, int);  //Replacement of all words of specific length by substitutional word (insWord)

char *seps = NULL;                      //String of separators
char **text = NULL;                     //Text
char *insWord = NULL;                   //substitutional word
int  stringsQuantity = 0;               //Count of strings in text

int main()
{
    int i;
    printf("Hello!\n");
    while(1)
    {
        printf("\nEnter \"enter\"   if you want to enter text\n");
        printf("Enter \"process\" if you want to process text\n");
        printf("Enter \"print\"   if you want to print text as is\n");
        printf("Enter \"exit\"    if you want to exit the program\n");
        printf("\nYour command: ");
        switch(getCommand())
        {
        case 0:
            printf("Incorrect command. Please, try again.");
            break;
        case 1:
            EnterValues();
            break;
        case 2:
            ProcessingText();
            break;
        case 3:
            printText(stringsQuantity,text);
            break;
        case 4:
            for(i = 0; i < stringsQuantity; i++)
                free(text[i]);
            free(text);
            free(seps);
            free(insWord);
            return 0;
            break;
        }
    }
}

int getCommand()
{
    int i = 0;
    char *command = *scanText(1);

    for(; command[i] == "enter"[i] && i<5; i++)
        if(i == 4)
            return 1;

    for(; command[i] == "process"[i] && i<7; i++)
        if(i == 6)
            return 2;

    for(; command[i] == "print"[i] && i<5; i++)
        if(i == 4)
            return 3;

    for(; command[i] == "exit"[i] && i<4; i++)
        if(i == 3)
            return 4;

    return 0;
}

void ProcessingText()
{
    if(text != NULL)
    {
        int i,j;

        /*Replace words with minimal length in strings with odd words count by substitutional word*/
        printf("\nReplacing words with minimal length in strings with odd words count by substitutional word...\n");
        int* replaces = malloc(stringsQuantity * sizeof(int));
        for(i = 0; i < stringsQuantity; i++)
        {
            if(getWordsQuantity(text[i])%2 != 0)
            {
                char** words = getWords(text[i]);
                int minLength = strlen(wordToString(words[0]));
                replaces[i] = 0;

                for(j = 0; j < getWordsQuantity(text[i]); j++)
                {
                    if(strlen(wordToString(words[j])) < minLength)
                    {
                        minLength = strlen(wordToString(words[j]));
                    }
                }

                for(j = 0; j < getWordsQuantity(text[i]); j++)
                {
                    if(strlen(wordToString(words[j])) == minLength)
                    {
                        replaces[i]++;
                        text[i] = replaceWord(text[i],insWord,(int)(words[j]-text[i]));
                        words = getWords(text[i]);
                    }
                }
            }
            else replaces[i] = 0;
        }

        printText(stringsQuantity,text);
        /*Replace words with minimal length in strings with odd words count by substitutional word*/

        /*Sort strings by count of replacements*/
        printf("\nSorting strings by count of replacements...\n");

        char** textBeforeSort = malloc(stringsQuantity * sizeof(char*));
        for(i = 0; i < stringsQuantity; i++)
            textBeforeSort[i] = text[i];

        for(i = 0; i < stringsQuantity; i++)
        {
            for(j = stringsQuantity - 1; j > i; j--)
            {
                if(replaces[j] < replaces[j-1])
                {
                    int tmp = replaces[j];
                    replaces[j] = replaces[j-1];
                    replaces[j-1] = tmp;

                    char* tmp1 = text[j];
                    text[j] = text[j-1];
                    text[j-1] = tmp1;
                }
            }
        }

        printText(stringsQuantity,text);
        /*Sort strings by count of replacements*/

        /*Remove strings that had not been moved by sorting*/
        printf("\nRemoving strings that had not been moved by sorting...\n");
        for(i = 0, j = 0; i < stringsQuantity; i++)
        {
            if(text[i] != textBeforeSort[i])
            {
                text[j] = text[i];
                j++;
            }
            else
            {
                free(text[i]);
            }
        }
        stringsQuantity = j;
        printText(stringsQuantity,text);
        /*Remove strings that had not been moved by sorting*/
        printf("\nProcessing completed successfully...\n");
    }
    else printf("\nEnter the text at first\n");
}

void EnterValues()
{
    printf("Enter strings number: ");
    scanf("%d",&stringsQuantity);
    printf("Enter text:\n");
    getchar();
    text = scanText(stringsQuantity);
    printf("\nEnter separators: ");
    seps = *scanText(1);
    printf("Enter substitute word: ");
    insWord = *scanText(1);
    printf("\n");
}

void printText(int stringsQuantity, char **text)
{
    printf("\n");
    if(stringsQuantity != 0)
    {
    int i;
    for(i = 0; i < stringsQuantity; i++)
        printf("%s\n",text[i]);
    }
    else
        printf("Text is empty at the moment\n");
}

char **scanText(int stringsQuantity)
{
    int i, j;
    char **str = malloc(stringsQuantity * sizeof(char*));

    for(i = 0; i < stringsQuantity; i++)
    {
        str[i] = malloc(1);
        for(j = 0; (str[i][j] = getchar()) != '\n'; j++)
            str[i] = realloc(str[i],j+2);
        str[i][j] = '\0';
    }
    return str;
}

int getWordsQuantity(char* str)
{
    int wordNumber = 0;
    while (*str != '\0')
    {
        if(strchr(seps,*str) == NULL)
        {
            wordNumber++;
            str+=strlen(wordToString(str));
        }
        else
            for(; *str != '\0' && strchr(seps,*str) != NULL; str++);
    }
    return wordNumber;
}

char **getWords(char *str)
{
    char** wordsPointer = malloc(sizeof(char*));
    int i = 0;
    while(*str != '\0')
    {
        if(strchr(seps,*str) == NULL)
        {
            wordsPointer = realloc(wordsPointer, (i+1) * sizeof(char*));
            wordsPointer[i] = str;
            str+=strlen(wordToString(str));
            i++;
        }
        else
            for(; *str != '\0' && strchr(seps,*str) != NULL; str++);
    }
    return wordsPointer;
}

char* wordToString(char *start)
{
    int length,i;
    for(length = 0; start[length] != '\0' && strchr(seps,start[length]) == NULL; length++);
    char* a = malloc(length+1);
    for(i = 0; i < length; i++)
        a[i] = start[i];
    a[length] = '\0';
    return a;
}

char* replaceWord(char* str,  char* insert, int remove)
{
    int i;
    int diff = strlen(insert) - strlen(wordToString(str+remove));
    if(diff > 0)
    {
        str = realloc(str,strlen(str)+diff+1);
        str[strlen(str)+diff] = '\0';

        for(i = strlen(str); i >= remove; i--)
            str[i + diff] = str[i];
    }
    else
    {
        for(i = remove - diff; i <= strlen(str); i++)
            str[i + diff] = str[i];

        str = realloc(str,strlen(str)+diff+1);
        str[strlen(str)+diff+1] = '\0';
    }

        for(i = remove; i < remove + strlen(insert); i++)
            str[i] = insert[i - remove];

return str;
}
