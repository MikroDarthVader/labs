
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int i, j, min, max, minY, maxY, N, M;
    int a[100][100],tmp[100];

    /*scanning*/
    printf("Enter matrix size (two parameters): ");
    scanf("%d %d", &N, &M);
    /*scanning*/

    /*Generating random 2-dimensional array NxM*/
    printf("\nGenerating random matrix %dx%d...\n", N, M);
    srand(time(NULL));
    for(j = 0; j < M; j++)
    {
        for(i = 0; i < N; i++)
        {
            a[i][j] = rand() / 1000;
            printf("%5d",a[i][j]);
        }
        printf("\n");
    }
    printf("Random matrix %dx%d generated\n\n", N, M);
    /*Generating random matrix NxM*/

    /*finding max & min elements without temporary array*/
    printf("1)Finding max & min elements without temporary array...\n");
    for(i = 0; i < N; i++)
    {
        min=a[i][0];
        max=a[i][0];

        minY=0;
        maxY=0;
        for(j = 0; j < M; j++)
        {
            if(max<=a[i][j])
            {
                max = a[i][j];
                maxY = j;
            }

            if(min>a[i][j])
            {
                min = a[i][j];
                minY = j;
            }
        }

        /*Printing results*/
        printf("in column %d: maximum element %d has coordinates (%d, %d) \n             minimum element %d has coordinates (%d, %d)\n",i,max,i,maxY,min,i,minY);
        /*Printing results*/
    }
    /*finding max & min elements without temporary array*/
        printf("\n\n");

    /*finding max & min elements without temporary array*/
    printf("2)Finding max & min elements with temporary array...\n");
    for(i = 0; i < N; i++)
    {
        min=a[i][0];
        max=a[i][0];

        minY=0;
        maxY=0;

        for(j = 0; j < M; j++)
            tmp[j]=a[i][j];

        for(j = 0; j < M; j++)
        {
            if(max<=tmp[j])
            {
                max = tmp[j];
                maxY = j;
            }

            if(min>tmp[j])
            {
                min = tmp[j];
                minY = j;
            }
        }

        /*Printing results*/
        printf("in column %d: maximum element %d has coordinates (%d, %d) \n             minimum element %d has coordinates (%d, %d)\n",i,max,i,maxY,min,i,minY);
        /*Printing results*/
    }
    /*finding max & min elements without temporary array*/
    return 0;
}
