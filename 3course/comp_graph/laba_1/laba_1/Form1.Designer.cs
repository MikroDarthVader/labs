﻿namespace laba_1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Draw_Window = new System.Windows.Forms.PictureBox();
            this.Draw_button = new System.Windows.Forms.Button();
            this.cam_pos_x = new System.Windows.Forms.TextBox();
            this.cam_pos_y = new System.Windows.Forms.TextBox();
            this.cam_pos_z = new System.Windows.Forms.TextBox();
            this.cam_euler_z = new System.Windows.Forms.TextBox();
            this.cam_euler_y = new System.Windows.Forms.TextBox();
            this.cam_euler_x = new System.Windows.Forms.TextBox();
            this.cameraMode = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.obj_euler_z = new System.Windows.Forms.TextBox();
            this.obj_euler_y = new System.Windows.Forms.TextBox();
            this.obj_euler_x = new System.Windows.Forms.TextBox();
            this.obj_pos_z = new System.Windows.Forms.TextBox();
            this.obj_pos_y = new System.Windows.Forms.TextBox();
            this.obj_pos_x = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Axis = new System.Windows.Forms.CheckBox();
            this.Obj_reflection = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.plane_normal_z = new System.Windows.Forms.TextBox();
            this.plane_normal_y = new System.Windows.Forms.TextBox();
            this.plane_normal_x = new System.Windows.Forms.TextBox();
            this.plane_pos_z = new System.Windows.Forms.TextBox();
            this.plane_pos_y = new System.Windows.Forms.TextBox();
            this.plane_pos_x = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Draw_Window)).BeginInit();
            this.SuspendLayout();
            // 
            // Draw_Window
            // 
            this.Draw_Window.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Draw_Window.Location = new System.Drawing.Point(12, 12);
            this.Draw_Window.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.Draw_Window.Name = "Draw_Window";
            this.Draw_Window.Size = new System.Drawing.Size(1003, 617);
            this.Draw_Window.TabIndex = 0;
            this.Draw_Window.TabStop = false;
            // 
            // Draw_button
            // 
            this.Draw_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Draw_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Draw_button.Location = new System.Drawing.Point(1044, 464);
            this.Draw_button.Name = "Draw_button";
            this.Draw_button.Size = new System.Drawing.Size(161, 30);
            this.Draw_button.TabIndex = 1;
            this.Draw_button.Text = "Render";
            this.Draw_button.UseVisualStyleBackColor = true;
            this.Draw_button.Click += new System.EventHandler(this.Draw_button_Click);
            // 
            // cam_pos_x
            // 
            this.cam_pos_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_pos_x.Location = new System.Drawing.Point(1096, 158);
            this.cam_pos_x.Name = "cam_pos_x";
            this.cam_pos_x.Size = new System.Drawing.Size(30, 20);
            this.cam_pos_x.TabIndex = 2;
            this.cam_pos_x.Text = "-10";
            // 
            // cam_pos_y
            // 
            this.cam_pos_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_pos_y.Location = new System.Drawing.Point(1132, 158);
            this.cam_pos_y.Name = "cam_pos_y";
            this.cam_pos_y.Size = new System.Drawing.Size(30, 20);
            this.cam_pos_y.TabIndex = 3;
            this.cam_pos_y.Text = "0";
            // 
            // cam_pos_z
            // 
            this.cam_pos_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_pos_z.Location = new System.Drawing.Point(1168, 158);
            this.cam_pos_z.Name = "cam_pos_z";
            this.cam_pos_z.Size = new System.Drawing.Size(30, 20);
            this.cam_pos_z.TabIndex = 4;
            this.cam_pos_z.Text = "0";
            // 
            // cam_euler_z
            // 
            this.cam_euler_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_euler_z.Location = new System.Drawing.Point(1168, 184);
            this.cam_euler_z.Name = "cam_euler_z";
            this.cam_euler_z.Size = new System.Drawing.Size(30, 20);
            this.cam_euler_z.TabIndex = 7;
            this.cam_euler_z.Text = "0";
            // 
            // cam_euler_y
            // 
            this.cam_euler_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_euler_y.Location = new System.Drawing.Point(1132, 184);
            this.cam_euler_y.Name = "cam_euler_y";
            this.cam_euler_y.Size = new System.Drawing.Size(30, 20);
            this.cam_euler_y.TabIndex = 6;
            this.cam_euler_y.Text = "0";
            // 
            // cam_euler_x
            // 
            this.cam_euler_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_euler_x.Location = new System.Drawing.Point(1096, 184);
            this.cam_euler_x.Name = "cam_euler_x";
            this.cam_euler_x.Size = new System.Drawing.Size(30, 20);
            this.cam_euler_x.TabIndex = 5;
            this.cam_euler_x.Text = "0";
            // 
            // cameraMode
            // 
            this.cameraMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cameraMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cameraMode.Items.AddRange(new object[] {
            "orthographic",
            "perspective"});
            this.cameraMode.Location = new System.Drawing.Point(1096, 210);
            this.cameraMode.Name = "cameraMode";
            this.cameraMode.Size = new System.Drawing.Size(102, 21);
            this.cameraMode.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(1110, 135);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "Camera";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(1034, 159);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 10;
            this.label2.Text = "Position";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(1051, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 16);
            this.label3.TabIndex = 11;
            this.label3.Text = "Euler";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(1051, 284);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 16);
            this.label4.TabIndex = 20;
            this.label4.Text = "Euler";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(1034, 258);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 16);
            this.label5.TabIndex = 19;
            this.label5.Text = "Position";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(1120, 234);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 20);
            this.label6.TabIndex = 18;
            this.label6.Text = "Object";
            // 
            // obj_euler_z
            // 
            this.obj_euler_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.obj_euler_z.Location = new System.Drawing.Point(1168, 283);
            this.obj_euler_z.Name = "obj_euler_z";
            this.obj_euler_z.Size = new System.Drawing.Size(30, 20);
            this.obj_euler_z.TabIndex = 17;
            this.obj_euler_z.Text = "0";
            // 
            // obj_euler_y
            // 
            this.obj_euler_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.obj_euler_y.Location = new System.Drawing.Point(1132, 283);
            this.obj_euler_y.Name = "obj_euler_y";
            this.obj_euler_y.Size = new System.Drawing.Size(30, 20);
            this.obj_euler_y.TabIndex = 16;
            this.obj_euler_y.Text = "0";
            // 
            // obj_euler_x
            // 
            this.obj_euler_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.obj_euler_x.Location = new System.Drawing.Point(1096, 283);
            this.obj_euler_x.Name = "obj_euler_x";
            this.obj_euler_x.Size = new System.Drawing.Size(30, 20);
            this.obj_euler_x.TabIndex = 15;
            this.obj_euler_x.Text = "0";
            // 
            // obj_pos_z
            // 
            this.obj_pos_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.obj_pos_z.Location = new System.Drawing.Point(1168, 257);
            this.obj_pos_z.Name = "obj_pos_z";
            this.obj_pos_z.Size = new System.Drawing.Size(30, 20);
            this.obj_pos_z.TabIndex = 14;
            this.obj_pos_z.Text = "0";
            // 
            // obj_pos_y
            // 
            this.obj_pos_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.obj_pos_y.Location = new System.Drawing.Point(1132, 257);
            this.obj_pos_y.Name = "obj_pos_y";
            this.obj_pos_y.Size = new System.Drawing.Size(30, 20);
            this.obj_pos_y.TabIndex = 13;
            this.obj_pos_y.Text = "0";
            // 
            // obj_pos_x
            // 
            this.obj_pos_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.obj_pos_x.Location = new System.Drawing.Point(1096, 257);
            this.obj_pos_x.Name = "obj_pos_x";
            this.obj_pos_x.Size = new System.Drawing.Size(30, 20);
            this.obj_pos_x.TabIndex = 12;
            this.obj_pos_x.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(1047, 211);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 16);
            this.label7.TabIndex = 21;
            this.label7.Text = "Mode";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(1110, 306);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 20);
            this.label8.TabIndex = 22;
            this.label8.Text = "Renderer";
            // 
            // Axis
            // 
            this.Axis.AutoSize = true;
            this.Axis.Checked = true;
            this.Axis.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Axis.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Axis.Location = new System.Drawing.Point(1075, 329);
            this.Axis.Name = "Axis";
            this.Axis.Size = new System.Drawing.Size(52, 20);
            this.Axis.TabIndex = 23;
            this.Axis.Text = "Axis";
            this.Axis.UseVisualStyleBackColor = true;
            // 
            // Obj_reflection
            // 
            this.Obj_reflection.AutoSize = true;
            this.Obj_reflection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Obj_reflection.Location = new System.Drawing.Point(1075, 355);
            this.Obj_reflection.Name = "Obj_reflection";
            this.Obj_reflection.Size = new System.Drawing.Size(130, 20);
            this.Obj_reflection.TabIndex = 24;
            this.Obj_reflection.Text = "Objects reflection";
            this.Obj_reflection.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(1038, 428);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 16);
            this.label9.TabIndex = 34;
            this.label9.Text = "Normal";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(1034, 402);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 16);
            this.label10.TabIndex = 33;
            this.label10.Text = "Position";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(1081, 378);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(124, 20);
            this.label11.TabIndex = 32;
            this.label11.Text = "Reflection plane";
            // 
            // plane_normal_z
            // 
            this.plane_normal_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.plane_normal_z.Location = new System.Drawing.Point(1168, 427);
            this.plane_normal_z.Name = "plane_normal_z";
            this.plane_normal_z.Size = new System.Drawing.Size(30, 20);
            this.plane_normal_z.TabIndex = 31;
            this.plane_normal_z.Text = "1";
            // 
            // plane_normal_y
            // 
            this.plane_normal_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.plane_normal_y.Location = new System.Drawing.Point(1132, 427);
            this.plane_normal_y.Name = "plane_normal_y";
            this.plane_normal_y.Size = new System.Drawing.Size(30, 20);
            this.plane_normal_y.TabIndex = 30;
            this.plane_normal_y.Text = "0";
            // 
            // plane_normal_x
            // 
            this.plane_normal_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.plane_normal_x.Location = new System.Drawing.Point(1096, 427);
            this.plane_normal_x.Name = "plane_normal_x";
            this.plane_normal_x.Size = new System.Drawing.Size(30, 20);
            this.plane_normal_x.TabIndex = 29;
            this.plane_normal_x.Text = "0";
            // 
            // plane_pos_z
            // 
            this.plane_pos_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.plane_pos_z.Location = new System.Drawing.Point(1168, 401);
            this.plane_pos_z.Name = "plane_pos_z";
            this.plane_pos_z.Size = new System.Drawing.Size(30, 20);
            this.plane_pos_z.TabIndex = 28;
            this.plane_pos_z.Text = "0";
            // 
            // plane_pos_y
            // 
            this.plane_pos_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.plane_pos_y.Location = new System.Drawing.Point(1132, 401);
            this.plane_pos_y.Name = "plane_pos_y";
            this.plane_pos_y.Size = new System.Drawing.Size(30, 20);
            this.plane_pos_y.TabIndex = 27;
            this.plane_pos_y.Text = "0";
            // 
            // plane_pos_x
            // 
            this.plane_pos_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.plane_pos_x.Location = new System.Drawing.Point(1096, 401);
            this.plane_pos_x.Name = "plane_pos_x";
            this.plane_pos_x.Size = new System.Drawing.Size(30, 20);
            this.plane_pos_x.TabIndex = 26;
            this.plane_pos_x.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1209, 638);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.plane_normal_z);
            this.Controls.Add(this.plane_normal_y);
            this.Controls.Add(this.plane_normal_x);
            this.Controls.Add(this.plane_pos_z);
            this.Controls.Add(this.plane_pos_y);
            this.Controls.Add(this.plane_pos_x);
            this.Controls.Add(this.Obj_reflection);
            this.Controls.Add(this.Axis);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.obj_euler_z);
            this.Controls.Add(this.obj_euler_y);
            this.Controls.Add(this.obj_euler_x);
            this.Controls.Add(this.obj_pos_z);
            this.Controls.Add(this.obj_pos_y);
            this.Controls.Add(this.obj_pos_x);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cameraMode);
            this.Controls.Add(this.cam_euler_z);
            this.Controls.Add(this.cam_euler_y);
            this.Controls.Add(this.cam_euler_x);
            this.Controls.Add(this.cam_pos_z);
            this.Controls.Add(this.cam_pos_y);
            this.Controls.Add(this.cam_pos_x);
            this.Controls.Add(this.Draw_button);
            this.Controls.Add(this.Draw_Window);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Draw_Window)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Draw_Window;
        private System.Windows.Forms.Button Draw_button;
        private System.Windows.Forms.TextBox cam_pos_x;
        private System.Windows.Forms.TextBox cam_pos_y;
        private System.Windows.Forms.TextBox cam_pos_z;
        private System.Windows.Forms.TextBox cam_euler_z;
        private System.Windows.Forms.TextBox cam_euler_y;
        private System.Windows.Forms.TextBox cam_euler_x;
        private System.Windows.Forms.ComboBox cameraMode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox obj_euler_z;
        private System.Windows.Forms.TextBox obj_euler_y;
        private System.Windows.Forms.TextBox obj_euler_x;
        private System.Windows.Forms.TextBox obj_pos_z;
        private System.Windows.Forms.TextBox obj_pos_y;
        private System.Windows.Forms.TextBox obj_pos_x;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox Axis;
        private System.Windows.Forms.CheckBox Obj_reflection;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox plane_normal_z;
        private System.Windows.Forms.TextBox plane_normal_y;
        private System.Windows.Forms.TextBox plane_normal_x;
        private System.Windows.Forms.TextBox plane_pos_z;
        private System.Windows.Forms.TextBox plane_pos_y;
        private System.Windows.Forms.TextBox plane_pos_x;
    }
}

