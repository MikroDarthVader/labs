﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Libraries;

namespace laba_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Renderer renderer;

        private void Draw_button_Click(object sender, EventArgs e)
        {
            if (!TryParceToVector3(cam_pos_x.Text, cam_pos_y.Text, cam_pos_z.Text, out Vector3 cam_pos) ||
                !TryParceToVector3(cam_euler_x.Text, cam_euler_y.Text, cam_euler_z.Text, out Vector3 cam_euler) ||
                !TryParceToVector3(obj_pos_x.Text, obj_pos_y.Text, obj_pos_z.Text, out Vector3 obj_pos) ||
                !TryParceToVector3(obj_euler_x.Text, obj_euler_y.Text, obj_euler_z.Text, out Vector3 obj_euler) ||
                !TryParceToVector3(plane_pos_x.Text, plane_pos_y.Text, plane_pos_z.Text, out Vector3 plane_pos) ||
                !TryParceToVector3(plane_normal_x.Text, plane_normal_y.Text, plane_normal_z.Text, out Vector3 plane_normal) ||
                plane_normal == Vector3.Zero)
                return;

            renderer.scene.cameras[0].position = cam_pos;
            renderer.scene.cameras[0].eulerAngles = cam_euler;
            renderer.scene.cameras[0].renderMode = cameraMode.SelectedIndex == 0 ? RenderMode.orthographic : RenderMode.perspective;

            renderer.scene.objects[0].position = obj_pos;
            renderer.scene.objects[0].eulerAngles = obj_euler;

            renderer.drawAxis = Axis.Checked;
            renderer.useFlip = Obj_reflection.Checked;

            renderer.flipPlane.point = plane_pos;
            renderer.flipPlane.normal = plane_normal;

            Bitmap bitmap = new Bitmap(Draw_Window.Width, Draw_Window.Height);

            renderer.Render(bitmap);

            Draw_Window.CreateGraphics().DrawImage(bitmap, 0, 0,
                bitmap.Width, bitmap.Height);
        }

        private bool TryParceToVector3(string x, string y, string z, out Vector3 vec)
        {
            vec = Vector3.Zero;
            if (!float.TryParse(x, out float _x) || !float.TryParse(y, out float _y) || !float.TryParse(z, out float _z))
                return false;

            vec = Vector3.Create(_x, _y, _z);
            return true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cameraMode.SelectedIndex = 1;

            renderer = new Renderer
            {
                scene = new Scene()
            };

            renderer.scene.cameras.Add(new Camera());

            PolyObject obj = new PolyObject();

            obj.points.Add(Vector3.Create(0.5f, 0.5f, 0.5f)); //0
            obj.points.Add(Vector3.Create(0.5f, -0.5f, 0.5f)); //1
            obj.points.Add(Vector3.Create(-0.5f, -0.5f, 0.5f)); //2
            obj.points.Add(Vector3.Create(-0.5f, 0.5f, 0.5f)); //3
            obj.points.Add(Vector3.Create(0.5f, 0.5f, -0.5f)); //4
            obj.points.Add(Vector3.Create(0.5f, -0.5f, -0.5f)); //5
            obj.points.Add(Vector3.Create(-0.5f, -0.5f, -0.5f)); //6
            obj.points.Add(Vector3.Create(-0.5f, 0.5f, -0.5f)); //7

            obj.lines.Add(0); obj.lines.Add(1);
            obj.lines.Add(1); obj.lines.Add(2);
            obj.lines.Add(2); obj.lines.Add(3);
            obj.lines.Add(3); obj.lines.Add(0);
            obj.lines.Add(4); obj.lines.Add(5);
            obj.lines.Add(5); obj.lines.Add(6);
            obj.lines.Add(6); obj.lines.Add(7);
            obj.lines.Add(7); obj.lines.Add(4);
            obj.lines.Add(0); obj.lines.Add(4);
            obj.lines.Add(1); obj.lines.Add(5);
            obj.lines.Add(2); obj.lines.Add(6);
            obj.lines.Add(3); obj.lines.Add(7);

            obj.position = Vector3.Create(0.5f, 0.5f, 0.5f);
            obj.eulerAngles = Vector3.Create(45, 0, 0);
            renderer.scene.objects.Add(obj);
        }
    }
}
