﻿using System;

namespace Libraries
{
    public struct Vector3
    {
        public static Vector3 Zero = new Vector3(0, 0, 0);
        public static float Epsilon = Single.Epsilon;
        public float x, y, z;

        public float sqrMagnitude => x * x + y * y + z * z;
        public float magnitude => (float)Math.Sqrt(sqrMagnitude);
        public float angle => (float)Math.Atan2(y, x);
        public Vector3 normalized => this / magnitude;

        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        public Vector3(float x, float y)
        {
            this.x = x;
            this.y = y;
            this.z = 0;
        }

        public static Vector3 Create(float x, float y, float z)
        {
            Vector3 res;
            res.x = x;
            res.y = y;
            res.z = z;
            return res;
        }

        public static Vector3 Create(float x, float y)
        {
            Vector3 res;
            res.x = x;
            res.y = y;
            res.z = 0;
            return res;
        }

        public static Vector3 operator -(Vector3 vec)
        {
            vec.x = -vec.x;
            vec.y = -vec.y;
            vec.z = -vec.z;

            return vec;
        }

        public static Vector3 operator +(Vector3 left, Vector3 right)
        {
            left.x += right.x;
            left.y += right.y;
            left.z += right.z;
            return left;
        }

        public static Vector3 operator -(Vector3 left, Vector3 right)
        {
            left.x -= right.x;
            left.y -= right.y;
            left.z -= right.z;
            return left;
        }

        public static Vector3 operator *(float left, Vector3 right)
        {
            right.x *= left;
            right.y *= left;
            right.z *= left;
            return right;
        }

        public static Vector3 operator *(Vector3 left, float right)
        {
            return right * left;
        }

        public static Vector3 operator /(Vector3 left, float right)
        {
            left.x /= right;
            left.y /= right;
            left.z /= right;
            return left;
        }

        public static float Dot(Vector3 left, Vector3 right)
        {
            return left.x * right.x + left.y * right.y + left.z * right.z;
        }

        public static Vector3 Cross(Vector3 left, Vector3 right)
        {
            return Create(left.y * right.z - right.y * left.z, right.x * left.z - left.x * right.z, left.x * right.y - right.x * left.y);
        }

        public static Vector3 Project(Vector3 vec, Vector3 onNormal)
        {
            onNormal = onNormal.normalized;
            return onNormal * Dot(vec, onNormal);
        }

        public static float ProjectMagnitude(Vector3 vec, Vector3 onNormal)
        {
            onNormal = onNormal.normalized;
            return Math.Abs(Dot(vec, onNormal));
        }

        public static bool operator ==(Vector3 left, Vector3 right)
        {
            return (left - right).sqrMagnitude <= Epsilon;
        }

        public static bool operator !=(Vector3 left, Vector3 right)
        {
            return !(left == right);
        }

        public Vector3 Round(float step, MyMath.Direction dir = MyMath.Direction.None)
        {
            Vector3 res;
            res.x = MyMath.Round(x, step, dir);
            res.y = MyMath.Round(y, step, dir);
            res.z = MyMath.Round(z, step, dir);
            return res;
        }

        /// <summary>
        /// clamps in x between v1.x & v2.x and y between v1.y & v2.y
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public Vector3 Clamp(Vector3 v1, Vector3 v2)
        {
            var res = this;
            res.x = MyMath.Clamp(res.x, v1.x, v2.x);
            res.y = MyMath.Clamp(res.y, v1.y, v2.y);
            res.z = MyMath.Clamp(res.z, v1.z, v2.z);
            return res;
        }

        public static implicit operator Vector3(Vector2 v)
        {
            return new Vector3(v.x, v.y);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
    }
}
