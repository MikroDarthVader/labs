﻿using System;

namespace Libraries
{
    public struct Plane
    {
        public Vector3 point;

        private Vector3 _normal;
        public Vector3 normal
        {
            get
            {
                return _normal;
            }
            set
            {
                if (value == Vector3.Zero)
                    throw new Exception("not valid normal");

                _normal = value.normalized;
            }
        }

        public Plane(Vector3 point, Vector3 normal)
        {
            if (normal == Vector3.Zero)
                throw new Exception("not valid normal");

            this.point = point;
            _normal = normal.normalized;
        }

        public static Plane Create(Vector3 point, Vector3 normal)
        {
            if (normal == Vector3.Zero)
                throw new Exception("not valid normal");

            Plane res;

            res.point = point;
            res._normal = normal.normalized;

            return res;
        }

        public bool TryGetIntersectPointWithLine(Line3 line, out Vector3 result)
        {
            result = Vector3.Zero;

            float dist = Vector3.Dot(point - line.point, normal);
            float diff = Vector3.Dot(line.direction, normal);

            if (diff != 0)
            {
                result = line.point + line.direction * dist / diff;
                return true;
            }
            else
                return false;
        }

        public Vector3 ProjectPoint(Vector3 point)
        {
            return point - normal * Vector3.Dot(normal, point - this.point);
        }

        public float GetDistanceTo(Vector3 point)
        {
            return Math.Abs(Vector3.Dot(normal, point - this.point));
        }

        public float GetSignedDistanceTo(Vector3 point)
        {
            return Vector3.Dot(normal, point - this.point);
        }

        public Vector3 FlipPoint(Vector3 point)
        {
            return point - normal * Vector3.Dot(normal, point - this.point) * 2;
        }
    }
}
