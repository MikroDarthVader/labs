﻿using System.Collections.Generic;
using System.Drawing;

using Libraries;

public class PolyObject : BaseObject
{
    public List<Vector3> points;
    public List<int> lines;

    public PolyObject()
    {
        points = new List<Vector3>();
        lines = new List<int>();
    }
}
