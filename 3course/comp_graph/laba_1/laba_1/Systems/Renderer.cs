﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Libraries;

public class Renderer
{
    public Scene scene;

    public Color background;

    public bool drawAxis;
    public bool useFlip;

    public Plane flipPlane;

    public Renderer()
    {
        background = Color.LightSlateGray;
        useFlip = true;
        flipPlane = new Plane(Vector3.Zero, Vector3.Create(0, 0, 1));

        drawAxis = true;
    }

    public void Render(Bitmap to)
    {
        float[][] zBuffer = new float[to.Width][];
        for (int i = 0; i < zBuffer.Length; i++)
        {
            zBuffer[i] = new float[to.Height];
            for (int j = 0; j < zBuffer[i].Length; j++)
                zBuffer[i][j] = float.MaxValue;
        }

        for (int x = 0; x < to.Width; x++)
            for (int y = 0; y < to.Height; y++)
                to.SetPixel(x, y, background);

        Camera camera = scene.cameras[0];

        for (int i = 0; i < scene.objects.Count; i++)
        {
            PolyObject obj = scene.objects[i];
            for (int j = 0; j < obj.lines.Count; j += 2)
            {
                LineSegment3 side = LineSegment3.Create(obj.FromLocalToGlobal(obj.points[obj.lines[j]]),
                                                        obj.FromLocalToGlobal(obj.points[obj.lines[j + 1]]));

                RenderSegment(to, zBuffer, camera, side, Color.Black);

                if (useFlip)
                {
                    side = LineSegment3.Create(flipPlane.FlipPoint(obj.FromLocalToGlobal(obj.points[obj.lines[j]])),
                                               flipPlane.FlipPoint(obj.FromLocalToGlobal(obj.points[obj.lines[j + 1]])));

                    RenderSegment(to, zBuffer, camera, side, Color.Black);
                }
            }
        }

        PostRender(to, zBuffer, camera);
    }

    private void PostRender(Bitmap to, float[][] zBuffer, Camera camera)
    {
        if (drawAxis)
            RenderAxis(to, zBuffer, camera);
    }

    private void RenderAxis(Bitmap to, float[][] zBuffer, Camera camera)
    {
        RenderLine(to, zBuffer, camera, new Line3(Vector3.Zero, Vector3.Create(1, 0, 0)), Color.FromArgb(255, 255, 0, 0));
        RenderLine(to, zBuffer, camera, new Line3(Vector3.Zero, Vector3.Create(0, 1, 0)), Color.FromArgb(255, 0, 128, 0));
        RenderLine(to, zBuffer, camera, new Line3(Vector3.Zero, Vector3.Create(0, 0, 1)), Color.FromArgb(255, 0, 0, 255));
    }

    private static void RenderLine(Bitmap to, float[][] zBuffer, Camera camera, Line3 line, Color color)
    {
        GetCameraInfo(to, camera, out Plane screenPlane, out Plane endViewPlane, out Vector3 leftTopPixelCoord, out float camWidth, out float camHeight);

        Action<Bitmap, int, int, Color> onPixelSet = delegate (Bitmap bitmap, int x, int y, Color c)
        {
            Line3 ray = GetRayFromCameraPixel(to, camera, leftTopPixelCoord, camWidth, camHeight, x, y);

            if (!ray.TryGetNearestPointTo(line, out Vector3 point))
                return;

            float depth = screenPlane.GetDistanceTo(point);

            if (depth < zBuffer[x][y])
            {
                zBuffer[x][y] = depth;

                bitmap.SetPixel(x, y, color);
            }
        };

        Vector2 firstOnScreenPoint;
        Vector2 secondOnScreenPoint;

        if (screenPlane.TryGetIntersectPointWithLine(line, out Vector3 firstInWorldPoint) &&
            endViewPlane.TryGetIntersectPointWithLine(line, out Vector3 secondInWorldPoint))
        {
            firstOnScreenPoint = ProjectInWorldPointOnCameraPlane(camera, screenPlane, leftTopPixelCoord, camWidth / to.Width, firstInWorldPoint);
            secondOnScreenPoint = ProjectInWorldPointOnCameraPlane(camera, screenPlane, leftTopPixelCoord, camWidth / to.Width, secondInWorldPoint);

            GraphPrimitives.DrawSegment(to, firstOnScreenPoint, secondOnScreenPoint, color, onPixelSet);
        }
        else if (MyMath.InRange(Vector3.Dot(screenPlane.normal, line.point - screenPlane.point), 0, camera.maxDrawDist))
        {
            firstOnScreenPoint = ProjectInWorldPointOnCameraPlane(camera, screenPlane, leftTopPixelCoord, camWidth / to.Width, line.point);
            secondOnScreenPoint = ProjectInWorldPointOnCameraPlane(camera, screenPlane, leftTopPixelCoord, camWidth / to.Width, line.point + line.direction);

            if (firstOnScreenPoint == secondOnScreenPoint)
                return;

            Line2 onScreenLine = new Line2(firstOnScreenPoint, secondOnScreenPoint - firstOnScreenPoint);

            GraphPrimitives.DrawLine(to, onScreenLine, color, onPixelSet);
        }
    }

    private static void RenderSegment(Bitmap to, float[][] zBuffer, Camera camera, LineSegment3 segment, Color color)
    {
        if (segment.startPoint == segment.endPoint)
            return;

        GetCameraInfo(to, camera, out Plane screenPlane, out Plane endViewPlane, out Vector3 leftTopPixelCoord, out float camWidth, out float camHeight);

        float startPointDepth = screenPlane.GetSignedDistanceTo(segment.startPoint);
        float endPointDepth = screenPlane.GetSignedDistanceTo(segment.endPoint);

        if (startPointDepth < 0 && endPointDepth < 0 || startPointDepth > camera.maxDrawDist && endPointDepth > camera.maxDrawDist)
            return;

        Line3 line = (Line3)segment;

        Action<Bitmap, int, int, Color> onPixelSet = delegate (Bitmap bitmap, int x, int y, Color c)
        {
            Line3 ray = GetRayFromCameraPixel(to, camera, leftTopPixelCoord, camWidth, camHeight, x, y);

            if (!ray.TryGetNearestPointTo(line, out Vector3 point))
                return;

            float depth = screenPlane.GetDistanceTo(point);

            if (depth < zBuffer[x][y])
            {
                zBuffer[x][y] = depth;

                bitmap.SetPixel(x, y, color);
            }
        };

        Vector2 firstOnScreenPoint;
        Vector2 secondOnScreenPoint;

        if (screenPlane.TryGetIntersectPointWithLine(line, out Vector3 onScreenPoint) &&
            endViewPlane.TryGetIntersectPointWithLine(line, out Vector3 onEndViewPoint))
        {
            if (startPointDepth < 0)
                segment.startPoint = onScreenPoint;
            else if (startPointDepth > camera.maxDrawDist)
                segment.startPoint = onEndViewPoint;

            if (endPointDepth < 0)
                segment.endPoint = onScreenPoint;
            else if (endPointDepth > camera.maxDrawDist)
                segment.endPoint = onEndViewPoint;
        }

        firstOnScreenPoint = ProjectInWorldPointOnCameraPlane(camera, screenPlane, leftTopPixelCoord, camWidth / to.Width, segment.startPoint);
        secondOnScreenPoint = ProjectInWorldPointOnCameraPlane(camera, screenPlane, leftTopPixelCoord, camWidth / to.Width, segment.endPoint);

        GraphPrimitives.DrawSegment(to, firstOnScreenPoint, secondOnScreenPoint, color, onPixelSet);
    }

    private static void GetCameraInfo(Bitmap to, Camera camera, out Plane virtualScreenPlane, out Plane virtualEndViewPlane,
                                                   out Vector3 leftTopPixelCoord, out float camWidth, out float camHeight)
    {
        if (camera.renderMode == RenderMode.perspective)
        {
            float tan = (float)Math.Tan(camera.fieldOfView * Math.PI / 360);

            camWidth = camera.planeDist * tan * 2;
            camHeight = camWidth * to.Height / to.Width;

            float pixelSize = camWidth / to.Width;

            leftTopPixelCoord = camera.position + camera.planeDist * camera.Forward +
                                        ((camWidth - pixelSize) / 2) * camera.Left + ((camHeight - pixelSize) / 2) * camera.Up;

            virtualScreenPlane = new Plane(camera.position + camera.planeDist * camera.Forward, camera.Forward);
            virtualEndViewPlane = new Plane(camera.position + (camera.planeDist + camera.maxDrawDist) * camera.Forward, camera.Forward);
        }
        else
        {
            float pixelSize = camera.width / to.Width;

            camWidth = camera.width;
            camHeight = camera.width * to.Height / to.Width;
            leftTopPixelCoord = camera.position + camera.Up * (camHeight - pixelSize) / 2 + camera.Left * (camera.width - pixelSize) / 2;

            virtualScreenPlane = new Plane(camera.position, camera.Forward);
            virtualEndViewPlane = new Plane(camera.position + camera.maxDrawDist * camera.Forward, camera.Forward);
        }
    }

    private static Vector2 ProjectInWorldPointOnCameraPlane(Camera camera, Plane screenPlane, Vector3 leftTopPixelCoord,
                                                            float pixelSize, Vector3 inWorldPoint)
    {
        if (camera.renderMode == RenderMode.perspective)
        {
            float depth = Vector3.Dot(screenPlane.normal, inWorldPoint - screenPlane.point);
            inWorldPoint = screenPlane.point + (inWorldPoint - (screenPlane.point + depth * screenPlane.normal)) *
                                                            camera.planeDist / Math.Abs(camera.planeDist + depth);
        }

        return Vector2.Create(Vector3.Dot(inWorldPoint - leftTopPixelCoord, camera.Right) / pixelSize,
                              Vector3.Dot(inWorldPoint - leftTopPixelCoord, camera.Down) / pixelSize);
    }

    private static Line3 GetRayFromCameraPixel(Bitmap to, Camera camera, Vector3 leftTopPixelCoord, float camWidth, float camHeight, int pixel_x, int pixel_y)
    {
        Vector3 raySource = leftTopPixelCoord + camera.Right * pixel_x * camWidth / to.Width + camera.Down * pixel_y * camHeight / to.Height;

        if (camera.renderMode == RenderMode.perspective)
            return new Line3(raySource, raySource - camera.position);
        else
            return new Line3(raySource, camera.Forward);
    }
}
