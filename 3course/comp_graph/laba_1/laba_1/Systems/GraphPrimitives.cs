﻿using System;
using System.Drawing;

using Libraries;

public static class GraphPrimitives
{
    public static void DrawLine(Bitmap to, Line2 line, Color color, Action<Bitmap, int, int, Color> onPixelSet = null)
    {
        if (!line.TryGetPointByX(0, out Vector2 firstPoint))
            line.TryGetPointByY(0, out firstPoint);

        if (!line.TryGetPointByX(to.Width - 1, out Vector2 secondPoint))
            line.TryGetPointByY(to.Height - 1, out secondPoint);

        DrawSegment(to, firstPoint, secondPoint, color, onPixelSet);
    }

    public static void DrawSegment(Bitmap to, Vector2 p1, Vector2 p2, Color color, Action<Bitmap, int, int, Color> onPixelSet = null)
    {
        p1.x = MyMath.Round(p1.x);
        p1.y = MyMath.Round(p1.y);

        p2.x = MyMath.Round(p2.x);
        p2.y = MyMath.Round(p2.y);

        if (p1 == p2 || !FixSegmentPoints(to.Width, to.Height, ref p1, ref p2))
            return;

        if (Math.Abs(p2.y - p1.y) < Math.Abs(p2.x - p1.x))
        {
            if (p2.x > p1.x)
                DrawLineLow(to, p1, p2, color, onPixelSet);
            else
                DrawLineLow(to, p2, p1, color, onPixelSet);
        }
        else
        {
            if (p2.y > p1.y)
                DrawLineHigh(to, p1, p2, color, onPixelSet);
            else
                DrawLineHigh(to, p2, p1, color, onPixelSet);
        }
    }

    public static bool FixSegmentPoints(int width, int height, ref Vector2 p1, ref Vector2 p2)
    {
        int startCode, endCode, code;
        Vector2 point;

        startCode = GetPointCode(p1, 0, width - 1, 0, height - 1);
        endCode = GetPointCode(p2, 0, width - 1, 0, height - 1);

        while ((startCode | endCode) != 0)
        {
            if ((startCode & endCode) != 0)
                return false;

            if (startCode != 0)
            {
                code = startCode;
                point = p1;
            }
            else
            {
                code = endCode;
                point = p2;
            }

            if ((code & 1) != 0) // left
            {
                point.y += (p1.y - p2.y) * (-point.x) / (p1.x - p2.x);
                point.x = 0;
            }
            else if ((code & 2) != 0) // right
            {
                point.y += (p1.y - p2.y) * (width - 1 - point.x) / (p1.x - p2.x);
                point.x = width - 1;
            }
            else if ((code & 4) != 0) // bot
            {
                point.x += (p1.x - p2.x) * (-point.y) / (p1.y - p2.y);
                point.y = 0;
            }
            else if ((code & 8) != 0) // top
            {
                point.x += (p1.x - p2.x) * (height - 1 - point.y) / (p1.y - p2.y);
                point.y = height - 1;
            }

            if (code == startCode)
            {
                p1 = point;
                startCode = GetPointCode(p1, 0, width - 1, 0, height - 1);
            }
            else
            {
                p2 = point;
                endCode = GetPointCode(p2, 0, width - 1, 0, height - 1);
            }
        }

        return true;
    }

    private static int GetPointCode(Vector2 point, int xMin, int xMax, int yMin, int yMax)
    {
        return (point.x < xMin ? 1 : 0) +
               (point.x > xMax ? 2 : 0) +
               (point.y < yMin ? 4 : 0) +
               (point.y > yMax ? 8 : 0);
    }

    private static void DrawLineLow(Bitmap to, Vector2 p1, Vector2 p2, Color color, Action<Bitmap, int, int, Color> onPixelSet = null)
    {
        float dx = p2.x - p1.x;
        float dy = p2.y - p1.y;

        int yi = 1;
        if (dy < 0)
        {
            yi = -1;
            dy = -dy;
        }

        float D = 2 * dy - dx;
        int y = (int)p1.y;

        for (int x = (int)p1.x; x <= (int)p2.x; x++)
        {
            if (onPixelSet != null)
                onPixelSet(to, x, y, color);
            else
                to.SetPixel(x, y, color);

            if (D > 0)
            {
                y += yi;
                D -= 2 * dx;
            }

            D += 2 * dy;
        }
    }

    private static void DrawLineHigh(Bitmap to, Vector2 p1, Vector2 p2, Color color, Action<Bitmap, int, int, Color> onPixelSet = null)
    {
        float dx = p2.x - p1.x;
        float dy = p2.y - p1.y;

        int xi = 1;
        if (dx < 0)
        {
            xi = -1;
            dx = -dx;
        }

        float D = 2 * dx - dy;
        int x = (int)p1.x;

        for (int y = (int)p1.y; y <= (int)p2.y; y++)
        {
            if (onPixelSet != null)
                onPixelSet(to, x, y, color);
            else
                to.SetPixel(x, y, color);

            if (D > 0)
            {
                x += xi;
                D -= 2 * dy;
            }

            D += 2 * dx;
        }
    }
}
