﻿using System;
using System.Collections.Generic;

using Libraries;

public enum RenderMode
{
	orthographic,
	perspective
}

public class Camera : BaseObject
{
	public RenderMode renderMode;
	public float maxDrawDist;

	// orthographic mode
	public float width;

	// perspective mode
	public float fieldOfView;
	public float planeDist;

	public Camera()
	{
		renderMode = RenderMode.perspective;
		maxDrawDist = 15;

		width = 10;

		fieldOfView = 75;
		planeDist = 1;
	}
}

