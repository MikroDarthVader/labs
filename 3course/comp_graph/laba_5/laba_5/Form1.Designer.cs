﻿namespace laba_5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Screen = new System.Windows.Forms.PictureBox();
            this.Render_button = new System.Windows.Forms.Button();
            this.cam_pos_x = new System.Windows.Forms.TextBox();
            this.cam_pos_y = new System.Windows.Forms.TextBox();
            this.cam_pos_z = new System.Windows.Forms.TextBox();
            this.cam_euler_x = new System.Windows.Forms.TextBox();
            this.cam_euler_y = new System.Windows.Forms.TextBox();
            this.cam_euler_z = new System.Windows.Forms.TextBox();
            this.camera_mode = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Screen)).BeginInit();
            this.SuspendLayout();
            // 
            // Screen
            // 
            this.Screen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Screen.Location = new System.Drawing.Point(13, 13);
            this.Screen.Name = "Screen";
            this.Screen.Size = new System.Drawing.Size(749, 470);
            this.Screen.TabIndex = 0;
            this.Screen.TabStop = false;
            // 
            // Render_button
            // 
            this.Render_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Render_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Render_button.Location = new System.Drawing.Point(823, 141);
            this.Render_button.Name = "Render_button";
            this.Render_button.Size = new System.Drawing.Size(102, 27);
            this.Render_button.TabIndex = 1;
            this.Render_button.Text = "Render";
            this.Render_button.UseVisualStyleBackColor = true;
            this.Render_button.Click += new System.EventHandler(this.Render_button_Click);
            // 
            // cam_pos_x
            // 
            this.cam_pos_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_pos_x.Location = new System.Drawing.Point(823, 61);
            this.cam_pos_x.Name = "cam_pos_x";
            this.cam_pos_x.Size = new System.Drawing.Size(30, 20);
            this.cam_pos_x.TabIndex = 2;
            this.cam_pos_x.Text = "-10";
            // 
            // cam_pos_y
            // 
            this.cam_pos_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_pos_y.Location = new System.Drawing.Point(859, 61);
            this.cam_pos_y.Name = "cam_pos_y";
            this.cam_pos_y.Size = new System.Drawing.Size(30, 20);
            this.cam_pos_y.TabIndex = 3;
            this.cam_pos_y.Text = "0";
            // 
            // cam_pos_z
            // 
            this.cam_pos_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_pos_z.Location = new System.Drawing.Point(895, 61);
            this.cam_pos_z.Name = "cam_pos_z";
            this.cam_pos_z.Size = new System.Drawing.Size(30, 20);
            this.cam_pos_z.TabIndex = 4;
            this.cam_pos_z.Text = "0";
            // 
            // cam_euler_x
            // 
            this.cam_euler_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_euler_x.Location = new System.Drawing.Point(823, 87);
            this.cam_euler_x.Name = "cam_euler_x";
            this.cam_euler_x.Size = new System.Drawing.Size(30, 20);
            this.cam_euler_x.TabIndex = 5;
            this.cam_euler_x.Text = "0";
            // 
            // cam_euler_y
            // 
            this.cam_euler_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_euler_y.Location = new System.Drawing.Point(859, 87);
            this.cam_euler_y.Name = "cam_euler_y";
            this.cam_euler_y.Size = new System.Drawing.Size(30, 20);
            this.cam_euler_y.TabIndex = 6;
            this.cam_euler_y.Text = "0";
            // 
            // cam_euler_z
            // 
            this.cam_euler_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_euler_z.Location = new System.Drawing.Point(895, 87);
            this.cam_euler_z.Name = "cam_euler_z";
            this.cam_euler_z.Size = new System.Drawing.Size(30, 20);
            this.cam_euler_z.TabIndex = 7;
            this.cam_euler_z.Text = "0";
            // 
            // camera_mode
            // 
            this.camera_mode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.camera_mode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.camera_mode.FormattingEnabled = true;
            this.camera_mode.Items.AddRange(new object[] {
            "perspective",
            "orthographic"});
            this.camera_mode.Location = new System.Drawing.Point(823, 114);
            this.camera_mode.Name = "camera_mode";
            this.camera_mode.Size = new System.Drawing.Size(101, 21);
            this.camera_mode.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(860, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "Camera";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(768, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "Mode";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(771, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Euler";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(781, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 20);
            this.label4.TabIndex = 12;
            this.label4.Text = "Pos";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 495);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.camera_mode);
            this.Controls.Add(this.cam_euler_z);
            this.Controls.Add(this.cam_euler_y);
            this.Controls.Add(this.cam_euler_x);
            this.Controls.Add(this.cam_pos_z);
            this.Controls.Add(this.cam_pos_y);
            this.Controls.Add(this.cam_pos_x);
            this.Controls.Add(this.Render_button);
            this.Controls.Add(this.Screen);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Screen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Screen;
        private System.Windows.Forms.Button Render_button;
        private System.Windows.Forms.TextBox cam_pos_x;
        private System.Windows.Forms.TextBox cam_pos_y;
        private System.Windows.Forms.TextBox cam_pos_z;
        private System.Windows.Forms.TextBox cam_euler_x;
        private System.Windows.Forms.TextBox cam_euler_y;
        private System.Windows.Forms.TextBox cam_euler_z;
        private System.Windows.Forms.ComboBox camera_mode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

