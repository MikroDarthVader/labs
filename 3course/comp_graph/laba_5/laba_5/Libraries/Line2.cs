﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libraries
{
    public struct Line2
    {
        public Vector2 point;

        private Vector2 _direction;

        public Vector2 direction
        {
            get
            {
                return _direction;
            }
            set
            {
                if (value == Vector2.Zero)
                    throw new Exception("not valid direction");

                _direction = value;
            }
        }

        public Line2(Vector2 point, Vector2 direction)
        {
            if (direction == Vector2.Zero)
                throw new Exception("not valid direction");

            this.point = point;
            _direction = direction;
        }

        public static Line2 Create(Vector2 point, Vector2 direction)
        {
            if (direction == Vector2.Zero)
                throw new Exception("not valid direction");

            Line2 res;

            res.point = point;
            res._direction = direction;

            return res;
        }

        public void Set(Vector2 point, Vector2 direction)
        {
            this.point = point;
            this.direction = direction;
        }

        public static bool operator ==(Line2 left, Line2 right)
        {
            return left.point == right.point && left._direction == right._direction;
        }

        public static bool operator !=(Line2 left, Line2 right)
        {
            return !(left == right);
        }

        public bool TryGetPointByX(float xCoord, out Vector2 point)
        {
            point = Vector2.Zero;
            if (direction.x == 0)
                return false;

            point = this.point + ((xCoord - this.point.x) / direction.x) * direction;
            return true;
        }

        public bool TryGetPointByY(float yCoord, out Vector2 point)
        {
            point = Vector2.Zero;
            if (direction.y == 0)
                return false;

            point = this.point + ((yCoord - this.point.y) / direction.y) * direction;
            return true;
        }

        public static explicit operator Line2(LineSegment2 segment)
        {
            return Create(segment.startPoint, segment.endPoint - segment.startPoint);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
    }
}
