﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libraries
{
    public static class MyMath
    {
        public enum Direction
        {
            Up = 1,
            None = 0,
            Down = -1
        }

        public static Direction Switch(Direction normal)
        {
            return normal == Direction.Down ? Direction.Up : Direction.Down;
        }

        public static bool InRange(float value, float min, float max)
        {
            return value >= min && value <= max;
        }

        public static float Round(float value, float step, Direction dir)
        {
            if (value % step == 0)
                return value;
            return step * Round((value + (int)dir * step / 2) / step);
        }

        /// <summary>
        /// Round value to nearest step multiple
        /// </summary>
        /// <param name="value"></param>
        /// <param name="step"></param>
        /// <returns></returns>
        public static float Round(float value, float step)
        {
            return step * Round(value / step);
        }

        public static int Round(float value)
        {
            return (int)(value > 0 ? value + 0.5f : value - 0.5f);
        }

        public static void Swap<T>(ref T v1, ref T v2)
        {
            T tmp = v1;
            v1 = v2;
            v2 = tmp;
        }

        public static float Clamp(float val, float rangeStart, float rangeEnd)
        {
            if (rangeEnd < rangeStart)
                Swap(ref rangeStart, ref rangeEnd);

            if (val < rangeStart)
                return rangeStart;
            else if (val > rangeEnd)
                return rangeEnd;
            else return val;
        }

        public static int BinomialCoef(int n, int k)
        {
            if (n < 0 || !InRange(k, 0, n))
                throw new ArgumentException();

            int result = 1;
            for (int i = k + 1; i <= n; i++)
                result *= i;
            for (int i = 1; i <= n - k; i++)
                result /= i;

            return result;
        }
    }
}
