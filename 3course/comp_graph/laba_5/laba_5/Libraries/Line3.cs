﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libraries
{
    public struct Line3
    {
        public Vector3 point;

        private Vector3 _direction;

        public Vector3 direction
        {
            get
            {
                return _direction;
            }
            set
            {
                if (value == Vector3.Zero)
                    throw new Exception("not valid direction");

                _direction = value;
            }
        }

        public Line3(Vector3 point, Vector3 direction)
        {
            if (direction == Vector3.Zero)
                throw new Exception("not valid direction");

            this.point = point;
            _direction = direction;
        }

        public static Line3 Create(Vector3 point, Vector3 direction)
        {
            if (direction == Vector3.Zero)
                throw new Exception("not valid direction");

            Line3 res;

            res.point = point;
            res._direction = direction;

            return res;
        }

        public void Set(Vector3 point, Vector3 direction)
        {
            this.point = point;
            this.direction = direction;
        }

        public static float Distance(Line3 left, Line3 right)
        {
            float result;
            if (left.direction == right.direction || left.direction == -right.direction)
                result = Vector3.Cross(right.point - left.point, left.direction).magnitude / left.direction.magnitude;
            else
            {
                Vector3 cross = Vector3.Cross(left.direction, right.direction);
                float dot = Vector3.Dot(right.point - left.point, cross);
                Vector3 cross2 = Vector3.Cross(left.direction, right.direction);
                result = Math.Abs(dot) / cross2.magnitude;
            }

            return result;
        }

        public bool TryGetNearestPointTo(Line3 line, out Vector3 result)
        {
            result = Vector3.Zero;
            if (direction == line.direction || direction == -line.direction)
                return false;

            Plane plane = new Plane(line.point, direction - Vector3.Project(direction, line.direction));

            if (!plane.TryGetIntersectPointWithLine(this, out result))
                throw new ArithmeticException();

            return true;
        }

        public static bool operator ==(Line3 left, Line3 right)
        {
            return left.point == right.point && left._direction == right._direction;
        }

        public static bool operator !=(Line3 left, Line3 right)
        {
            return !(left == right);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
    }
}
