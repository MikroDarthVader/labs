﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Libraries;

namespace laba_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Renderer renderer;

        private void Form1_Load(object sender, EventArgs e)
        {
            cameraMode.SelectedIndex = 1;

            renderer = new Renderer
            {
                scene = new Scene()
            };

            Camera cam = new Camera();

            renderer.scene.cameras.Add(cam);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!TryParceToVector3(cam_pos_x.Text, cam_pos_y.Text, cam_pos_z.Text, out Vector3 cam_pos) ||
                !TryParceToVector3(cam_euler_x.Text, cam_euler_y.Text, cam_euler_z.Text, out Vector3 cam_euler) ||
                !float.TryParse(angle_x.Text, out float angleX) || !float.TryParse(angle_y.Text, out float angleY))
                return;

            renderer.scene.cameras[0].position = cam_pos;
            renderer.scene.cameras[0].eulerAngles = cam_euler;
            renderer.scene.cameras[0].renderMode = cameraMode.SelectedIndex == 0 ? RenderMode.orthographic : RenderMode.perspective;

            Bitmap bitmap = new Bitmap(Screen.Width, Screen.Height);

            BezierSurface surface = new BezierSurface(3, 3);
            surface.P[0][0] = Vector3.Create(-1, -1, 0);
            surface.P[0][1] = Vector3.Create(-1, 0, 0);
            surface.P[0][2] = Vector3.Create(-1, 1, 0);
            surface.P[1][0] = Vector3.Create(0, -1, 0);
            surface.P[1][1] = Vector3.Create(0, 0, 3);
            surface.P[1][2] = Vector3.Create(0, 1, 0);
            surface.P[2][0] = Vector3.Create(1, -1, 0);
            surface.P[2][1] = Vector3.Create(1, 0, 0);
            surface.P[2][2] = Vector3.Create(1, 1, 0);

            /*BezierSurface surface = new BezierSurface(2, 7);
            surface.P[0][0] = Vector3.Create(-1, 0, 0);
            surface.P[0][1] = Vector3.Create(-1, -1, 0);
            surface.P[0][2] = Vector3.Create(0, -1, 0);
            surface.P[0][3] = Vector3.Create(1, -1, 0);
            surface.P[0][4] = Vector3.Create(1, 0, 0);
            surface.P[0][5] = Vector3.Create(1, 1, 0);
            surface.P[0][6] = Vector3.Create(0, 1, 0);
            surface.P[1][0] = Vector3.Create(0, 0, 1);
            surface.P[1][1] = Vector3.Create(0, 0, 1);
            surface.P[1][2] = Vector3.Create(0, 0, 1);
            surface.P[1][3] = Vector3.Create(0, 0, 1);
            surface.P[1][4] = Vector3.Create(0, 0, 1);
            surface.P[1][5] = Vector3.Create(0, 0, 1);
            surface.P[1][6] = Vector3.Create(0, 0, 1);*/

            /*BezierSurface surface = new BezierSurface(4, 4);
            surface.P[0][0] = Vector3.Create(0, 0, 0);
            surface.P[0][1] = Vector3.Create(0, 1, 0);
            surface.P[0][2] = Vector3.Create(0, 2, 0);
            surface.P[0][3] = Vector3.Create(0, 3, 0);
            surface.P[1][0] = Vector3.Create(-1, 0, 0);
            surface.P[1][1] = Vector3.Create(-1, 1, 0);
            surface.P[1][2] = Vector3.Create(-1, 2, 0);
            surface.P[1][3] = Vector3.Create(-1, 3, 0);
            surface.P[2][0] = Vector3.Create(-2, 0, 0);
            surface.P[2][1] = Vector3.Create(-2, 1, 0);
            surface.P[2][2] = Vector3.Create(-2, 2, 0);
            surface.P[2][3] = Vector3.Create(-2, 3, 0);
            surface.P[3][0] = Vector3.Create(-3, 0, 0);
            surface.P[3][1] = Vector3.Create(-3, 1, 0);
            surface.P[3][2] = Vector3.Create(-3, 2, 0);
            surface.P[3][3] = Vector3.Create(-3, 3, 0);*/

            for (int i = 0; i < surface.P.Length; i++)
                for (int j = 0; j < surface.P[i].Length; j++)
                    surface.P[i][j] = ApplyAnglesToPoint(surface.P[i][j], angleX, angleY);

            renderer.Render(bitmap, surface);

            Screen.CreateGraphics().DrawImage(bitmap, 0, 0, bitmap.Width, bitmap.Height);
        }

        private bool TryParceToVector3(string x, string y, string z, out Vector3 vec)
        {
            vec = Vector3.Zero;
            if (!float.TryParse(x, out float _x) || !float.TryParse(y, out float _y) || !float.TryParse(z, out float _z))
                return false;

            vec = Vector3.Create(_x, _y, _z);
            return true;
        }

        private Vector3 ApplyAnglesToPoint(Vector3 point, float angleX, float angleY)
        {
            Vector2 temp = Vector2.Create(point.y, point.z).Rotate(angleX * (float)Math.PI / 180);
            point.y = temp.x;
            point.z = temp.y;

            temp = Vector2.Create(point.x, point.z).Rotate(angleY * (float)Math.PI / 180);
            point.x = temp.x;
            point.z = temp.y;

            return point;
        }
    }
}
