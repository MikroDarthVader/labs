﻿namespace laba_3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Screen = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.cam_pos_z = new System.Windows.Forms.TextBox();
            this.cam_pos_x = new System.Windows.Forms.TextBox();
            this.cam_pos_y = new System.Windows.Forms.TextBox();
            this.cam_euler_x = new System.Windows.Forms.TextBox();
            this.cam_euler_y = new System.Windows.Forms.TextBox();
            this.cam_euler_z = new System.Windows.Forms.TextBox();
            this.cameraMode = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.angle_x = new System.Windows.Forms.TextBox();
            this.angle_y = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Screen)).BeginInit();
            this.SuspendLayout();
            // 
            // Screen
            // 
            this.Screen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Screen.Location = new System.Drawing.Point(12, 12);
            this.Screen.Name = "Screen";
            this.Screen.Size = new System.Drawing.Size(815, 528);
            this.Screen.TabIndex = 0;
            this.Screen.TabStop = false;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(888, 239);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 29);
            this.button1.TabIndex = 1;
            this.button1.Text = "Render";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cam_pos_z
            // 
            this.cam_pos_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_pos_z.Location = new System.Drawing.Point(960, 141);
            this.cam_pos_z.Name = "cam_pos_z";
            this.cam_pos_z.Size = new System.Drawing.Size(30, 20);
            this.cam_pos_z.TabIndex = 2;
            this.cam_pos_z.Text = "1";
            // 
            // cam_pos_x
            // 
            this.cam_pos_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_pos_x.Location = new System.Drawing.Point(888, 141);
            this.cam_pos_x.Name = "cam_pos_x";
            this.cam_pos_x.Size = new System.Drawing.Size(30, 20);
            this.cam_pos_x.TabIndex = 3;
            this.cam_pos_x.Text = "-4";
            // 
            // cam_pos_y
            // 
            this.cam_pos_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_pos_y.Location = new System.Drawing.Point(924, 141);
            this.cam_pos_y.Name = "cam_pos_y";
            this.cam_pos_y.Size = new System.Drawing.Size(30, 20);
            this.cam_pos_y.TabIndex = 4;
            this.cam_pos_y.Text = "4";
            // 
            // cam_euler_x
            // 
            this.cam_euler_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_euler_x.Location = new System.Drawing.Point(888, 167);
            this.cam_euler_x.Name = "cam_euler_x";
            this.cam_euler_x.Size = new System.Drawing.Size(30, 20);
            this.cam_euler_x.TabIndex = 5;
            this.cam_euler_x.Text = "0";
            // 
            // cam_euler_y
            // 
            this.cam_euler_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_euler_y.Location = new System.Drawing.Point(924, 167);
            this.cam_euler_y.Name = "cam_euler_y";
            this.cam_euler_y.Size = new System.Drawing.Size(30, 20);
            this.cam_euler_y.TabIndex = 6;
            this.cam_euler_y.Text = "0";
            // 
            // cam_euler_z
            // 
            this.cam_euler_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_euler_z.Location = new System.Drawing.Point(960, 167);
            this.cam_euler_z.Name = "cam_euler_z";
            this.cam_euler_z.Size = new System.Drawing.Size(30, 20);
            this.cam_euler_z.TabIndex = 7;
            this.cam_euler_z.Text = "-45";
            // 
            // cameraMode
            // 
            this.cameraMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cameraMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cameraMode.FormattingEnabled = true;
            this.cameraMode.Items.AddRange(new object[] {
            "orthographic",
            "perspective"});
            this.cameraMode.Location = new System.Drawing.Point(888, 114);
            this.cameraMode.Name = "cameraMode";
            this.cameraMode.Size = new System.Drawing.Size(102, 21);
            this.cameraMode.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(925, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "Camera";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(833, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "Mode";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(846, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Pos";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(836, 167);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 20);
            this.label4.TabIndex = 12;
            this.label4.Text = "Euler";
            // 
            // angle_x
            // 
            this.angle_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.angle_x.Location = new System.Drawing.Point(924, 213);
            this.angle_x.Name = "angle_x";
            this.angle_x.Size = new System.Drawing.Size(30, 20);
            this.angle_x.TabIndex = 13;
            this.angle_x.Text = "0";
            // 
            // angle_y
            // 
            this.angle_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.angle_y.Location = new System.Drawing.Point(960, 213);
            this.angle_y.Name = "angle_y";
            this.angle_y.Size = new System.Drawing.Size(30, 20);
            this.angle_y.TabIndex = 14;
            this.angle_y.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(932, 190);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 20);
            this.label5.TabIndex = 15;
            this.label5.Text = "Angles";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1002, 552);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.angle_y);
            this.Controls.Add(this.angle_x);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cameraMode);
            this.Controls.Add(this.cam_euler_z);
            this.Controls.Add(this.cam_euler_y);
            this.Controls.Add(this.cam_euler_x);
            this.Controls.Add(this.cam_pos_y);
            this.Controls.Add(this.cam_pos_x);
            this.Controls.Add(this.cam_pos_z);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Screen);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Screen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Screen;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox cam_pos_z;
        private System.Windows.Forms.TextBox cam_pos_x;
        private System.Windows.Forms.TextBox cam_pos_y;
        private System.Windows.Forms.TextBox cam_euler_x;
        private System.Windows.Forms.TextBox cam_euler_y;
        private System.Windows.Forms.TextBox cam_euler_z;
        private System.Windows.Forms.ComboBox cameraMode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox angle_x;
        private System.Windows.Forms.TextBox angle_y;
        private System.Windows.Forms.Label label5;
    }
}

