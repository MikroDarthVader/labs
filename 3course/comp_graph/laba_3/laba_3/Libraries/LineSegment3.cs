﻿
namespace Libraries
{
    public struct LineSegment3
    {
        public Vector3 startPoint;
        public Vector3 endPoint;

        public LineSegment3(Vector3 startPoint, Vector3 endPoint)
        {
            this.startPoint = startPoint;
            this.endPoint = endPoint;
        }

        public static LineSegment3 Create(Vector3 startPoint, Vector3 endPoint)
        {
            LineSegment3 res;
            res.startPoint = startPoint;
            res.endPoint = endPoint;

            return res;
        }

        public static implicit operator LineSegment3(LineSegment2 segment)
        {
            return new LineSegment3(segment.startPoint, segment.endPoint);
        }
    }
}
