﻿using System;
using System.Collections.Generic;

namespace Libraries
{
    public class BezierCurve3
    {
        public List<Vector3> P;

        public BezierCurve3(Vector3 startPoint, Vector3 endPoint, params Vector3[] middlePoints)
        {
            P = new List<Vector3>();
            P.Add(startPoint);
            P.AddRange(middlePoints);
            P.Add(endPoint);
        }

        public Vector3 GetPoint(float t)
        {
            Vector3 result = Vector3.Zero;

            int n = P.Count - 1;
            for (int k = 0; k <= n; k++)
                result += MyMath.BinomialCoef(n, k) * (float)Math.Pow(1 - t, k) * (float)Math.Pow(t, n - k) * P[k];

            return result;
        }
    }
}