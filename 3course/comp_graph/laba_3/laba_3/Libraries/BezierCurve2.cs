﻿using System;
using System.Collections.Generic;

namespace Libraries
{
    public class BezierCurve2
    {
        public List<Vector2> P;

        public BezierCurve2(Vector2 startPoint, Vector2 endPoint, params Vector2[] middlePoints)
        {
            P = new List<Vector2>();
            P.Add(startPoint);
            P.AddRange(middlePoints);
            P.Add(endPoint);
        }

        public Vector2 GetPoint(float t)
        {
            Vector2 result = Vector2.Zero;

            int n = P.Count - 1;
            for (int k = 0; k <= n; k++)
            {
                result += MyMath.BinomialCoef(n, k) * (float)Math.Pow(1 - t, k) * (float)Math.Pow(t, n - k) * P[k];
            }

            return result;
        }
    }
}