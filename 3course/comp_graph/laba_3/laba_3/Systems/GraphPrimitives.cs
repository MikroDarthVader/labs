﻿using System;
using System.Drawing;

using Libraries;

public static class GraphPrimitives
{
    public static void DrawBezierCurve(Bitmap to, BezierCurve2 curve, float pixelByX, float pixelByY)
    {
        Color[][] pixels = new Color[to.Width][];
        for (int x = 0; x < to.Width; x++)
        {
            pixels[x] = new Color[to.Height];
            for (int y = 0; y < to.Height; y++)
                pixels[x][y] = Color.Empty;
        }

        float t1 = 0;
        float t2 = 1;

        Vector2 point1 = curve.GetPoint(t1);
        point1.x *= pixelByX;
        point1.y *= pixelByY;
        point1 = point1.Round(1);
        Vector2 point2 = curve.GetPoint(t2);
        point2.x *= pixelByX;
        point2.y *= pixelByY;
        point2 = point2.Round(1);

        if (MyMath.InRange(point1.x, 0, to.Width - 1) && MyMath.InRange(point1.y, 0, to.Height - 1))
            to.SetPixel((int)point1.x, (int)point1.y, Color.Black);
        if (MyMath.InRange(point2.x, 0, to.Width - 1) && MyMath.InRange(point2.y, 0, to.Height - 1))
            to.SetPixel((int)point2.x, (int)point2.y, Color.Black);

        DrawBezierCurveIter(pixels, curve, t1, t2, point1, point2, pixelByX, pixelByY);

        for (int x = 0; x < to.Width; x++)
            for (int y = 0; y < to.Height; y++)
                if (pixels[x][y] != Color.Empty)
                    to.SetPixel(x, y, pixels[x][y]);
    }

    private static void DrawBezierCurveIter(Color[][] pixels, BezierCurve2 curve, float t1, float t2, Vector2 point1, Vector2 point2, float pixelByX, float pixelByY)
    {
        Vector2 diff = point2 - point1;
        if (MyMath.InRange(diff.x, -1, 1) && MyMath.InRange(diff.y, -1, 1))
            return;

        float tMiddle = (t1 + t2) / 2;
        Vector2 pointMiddle = curve.GetPoint(tMiddle);
        pointMiddle.x *= pixelByX;
        pointMiddle.y *= pixelByY;
        pointMiddle = pointMiddle.Round(1);

        if (MyMath.InRange(pointMiddle.x, 0, pixels.Length - 1) && MyMath.InRange(pointMiddle.y, 0, pixels[0].Length - 1))
        {
            pixels[(int)pointMiddle.x][(int)pointMiddle.y] = Color.Black;

            for (int i = 0; i < 4; i++)
            {
                int x = (int)pointMiddle.x + ((((i / 2) * 2) - 1) + (((i % 2) * 2) - 1)) / 2;
                int y = (int)pointMiddle.y + ((((i / 2) * 2) - 1) + ((((i + 1) % 2) * 2) - 1)) / 2;

                if (MyMath.InRange(x, 0, pixels.Length - 1) && MyMath.InRange(y, 0, pixels[0].Length - 1) &&
                    pixels[x][y] != Color.Empty)
                    FixPoint(x, y, pixels);
            }
        }

        DrawBezierCurveIter(pixels, curve, t1, tMiddle, point1, pointMiddle, pixelByX, pixelByY);
        DrawBezierCurveIter(pixels, curve, tMiddle, t2, pointMiddle, point2, pixelByX, pixelByY);
    }

    public static void DrawFunct(Bitmap to, Func<float, float> funct, float x1, float x2, float pixelByX, float pixelByY)
    {
        Color[][] pixels = new Color[to.Width][];
        for (int x = 0; x < to.Width; x++)
        {
            pixels[x] = new Color[to.Height];
            for (int y = 0; y < to.Height; y++)
                pixels[x][y] = Color.Empty;
        }

        Vector2 firstPoint = Vector2.Create(x1, funct(x1 / pixelByX) * pixelByY);
        Vector2 secondPoint = Vector2.Create(x2, funct(x2 / pixelByX) * pixelByY);

        Vector2 firstPointRound = firstPoint.Round(1);
        Vector2 secondPointRound = secondPoint.Round(1);

        if (MyMath.InRange(firstPointRound.x, 0, to.Width - 1) && MyMath.InRange(firstPointRound.y, 0, to.Height - 1))
            pixels[(int)firstPointRound.x][(int)firstPointRound.y] = Color.Black;
        if (MyMath.InRange(secondPointRound.x, 0, to.Width - 1) && MyMath.InRange(secondPointRound.y, 0, to.Height - 1))
            pixels[(int)secondPointRound.x][(int)secondPointRound.y] = Color.Black;

        DrawFunctIter(pixels, funct, firstPoint, secondPoint, pixelByX, pixelByY);

        for (int x = 0; x < to.Width; x++)
            for (int y = 0; y < to.Height; y++)
                if (pixels[x][y] != Color.Empty)
                    to.SetPixel(x, y, pixels[x][y]);
    }

    private static void DrawFunctIter(Color[][] to, Func<float, float> funct, Vector2 point1, Vector2 point2, float pixelByX, float pixelByY)
    {
        Vector2 diff = point2.Round(1) - point1.Round(1);

        if (MyMath.InRange(diff.x, -1, 1) && MyMath.InRange(diff.y, -1, 1))
            return;

        float xMiddle = (point1.x + point2.x) / 2;
        Vector2 middlePoint = Vector2.Create(xMiddle, funct(xMiddle / pixelByX) * pixelByY);

        Vector2 middlePointRound = middlePoint.Round(1);

        if (MyMath.InRange(middlePointRound.x, 0, to.Length - 1) && MyMath.InRange(middlePointRound.y, 0, to[0].Length - 1))
        {
            to[(int)middlePointRound.x][(int)middlePointRound.y] = Color.Black;

            for (int i = 0; i < 4; i++)
            {
                int x = (int)middlePointRound.x + ((((i / 2) * 2) - 1) + (((i % 2) * 2) - 1)) / 2;
                int y = (int)middlePointRound.y + ((((i / 2) * 2) - 1) + ((((i + 1) % 2) * 2) - 1)) / 2;

                if (MyMath.InRange(x, 0, to.Length - 1) && MyMath.InRange(y, 0, to[0].Length - 1) &&
                    to[x][y] != Color.Empty)
                    FixPoint(x, y, to);
            }

            FixPoint((int)middlePointRound.x, (int)middlePointRound.y, to);
        }

        DrawFunctIter(to, funct, point1, middlePoint, pixelByX, pixelByY);
        DrawFunctIter(to, funct, middlePoint, point2, pixelByX, pixelByY);
    }

    private static void FixPoint(int x, int y, Color[][] pixels)
    {
        for (int i = 0; i < 4; i += 3)
            for (int j = 1; j < 3; j++)
            {
                int x1 = x + ((((i / 2) * 2) - 1) + (((i % 2) * 2) - 1)) / 2;
                int y1 = y + ((((i / 2) * 2) - 1) + ((((i + 1) % 2) * 2) - 1)) / 2;
                int x2 = x + ((((j / 2) * 2) - 1) + (((j % 2) * 2) - 1)) / 2;
                int y2 = y + ((((j / 2) * 2) - 1) + ((((j + 1) % 2) * 2) - 1)) / 2;

                if (MyMath.InRange(x1, 0, pixels.Length - 1) && MyMath.InRange(y1, 0, pixels[0].Length - 1) &&
                    MyMath.InRange(x2, 0, pixels.Length - 1) && MyMath.InRange(y2, 0, pixels[0].Length - 1) &&
                    pixels[x1][y1] != Color.Empty && pixels[x2][y2] != Color.Empty)
                    pixels[x][y] = Color.Empty;
            }
    }

    public static void DrawLine(Bitmap to, Line2 line, Color color, Action<Bitmap, int, int, Color> onPixelSet = null)
    {
        if (!line.TryGetPointByX(0, out Vector2 firstPoint))
            line.TryGetPointByY(0, out firstPoint);

        if (!line.TryGetPointByX(to.Width - 1, out Vector2 secondPoint))
            line.TryGetPointByY(to.Height - 1, out secondPoint);

        DrawSegment(to, firstPoint, secondPoint, color, onPixelSet);
    }

    public static void DrawSegment(Bitmap to, Vector2 p1, Vector2 p2, Color color, Action<Bitmap, int, int, Color> onPixelSet = null)
    {
        p1.x = MyMath.Round(p1.x);
        p1.y = MyMath.Round(p1.y);

        p2.x = MyMath.Round(p2.x);
        p2.y = MyMath.Round(p2.y);

        if (p1 == p2 || !FixPoints(to, ref p1, ref p2))
            return;

        if (Math.Abs(p2.y - p1.y) < Math.Abs(p2.x - p1.x))
        {
            if (p2.x > p1.x)
                DrawLineLow(to, p1, p2, color, onPixelSet);
            else
                DrawLineLow(to, p2, p1, color, onPixelSet);
        }
        else
        {
            if (p2.y > p1.y)
                DrawLineHigh(to, p1, p2, color, onPixelSet);
            else
                DrawLineHigh(to, p2, p1, color, onPixelSet);
        }
    }

    private static bool FixPoints(Bitmap to, ref Vector2 p1, ref Vector2 p2)
    {
        bool isP1Correct = MyMath.InRange(p1.x, 0, to.Width - 1) && MyMath.InRange(p1.y, 0, to.Height - 1);
        bool isP2Correct = MyMath.InRange(p2.x, 0, to.Width - 1) && MyMath.InRange(p2.y, 0, to.Height - 1);

        if (isP1Correct && isP2Correct)
            return true;

        Line2 line = new Line2(p1, p2 - p1);

        Vector2 firstEdgePoint;
        Vector2 secondEdgePoint;

        if (!line.TryGetPointByX(0, out firstEdgePoint) || !MyMath.InRange(firstEdgePoint.y, 0, to.Height - 1))
            if (!line.TryGetPointByX(to.Width - 1, out firstEdgePoint) || !MyMath.InRange(firstEdgePoint.y, 0, to.Height - 1))
                if (!line.TryGetPointByY(0, out firstEdgePoint) || !MyMath.InRange(firstEdgePoint.x, 0, to.Width - 1))
                    if (!line.TryGetPointByY(to.Height - 1, out firstEdgePoint) || !MyMath.InRange(firstEdgePoint.x, 0, to.Width - 1))
                        return false;

        if (!line.TryGetPointByY(to.Height - 1, out secondEdgePoint) || !MyMath.InRange(secondEdgePoint.x, 0, to.Width - 1))
            if (!line.TryGetPointByY(0, out secondEdgePoint) || !MyMath.InRange(secondEdgePoint.x, 0, to.Width - 1))
                if (!line.TryGetPointByX(to.Width - 1, out secondEdgePoint) || !MyMath.InRange(secondEdgePoint.y, 0, to.Height - 1))
                    if (!line.TryGetPointByX(0, out secondEdgePoint) || !MyMath.InRange(secondEdgePoint.y, 0, to.Height - 1))
                        return false;

        Vector2 p1_result = p1;
        Vector2 p2_result = p2;
        if ((p1.x != p2.x && MyMath.InRange(firstEdgePoint.x, Math.Min(p1.x, p2.x) + 1, Math.Max(p1.x, p2.x) - 1)) ||
             (p1.y != p2.y && MyMath.InRange(firstEdgePoint.y, Math.Min(p1.y, p2.y) + 1, Math.Max(p1.y, p2.y) - 1)))
            if (!isP1Correct)
            {
                isP1Correct = true;
                p1_result = firstEdgePoint;
            }
            else if (!isP2Correct)
            {
                isP2Correct = true;
                p2_result = firstEdgePoint;
            }

        if ((p1.x != p2.x && MyMath.InRange(secondEdgePoint.x, Math.Min(p1.x, p2.x) + 1, Math.Max(p1.x, p2.x) - 1)) ||
             (p1.y != p2.y && MyMath.InRange(secondEdgePoint.y, Math.Min(p1.y, p2.y) + 1, Math.Max(p1.y, p2.y) - 1)))
            if (!isP1Correct)
            {
                isP1Correct = true;
                p1_result = secondEdgePoint;
            }
            else if (!isP2Correct)
            {
                isP2Correct = true;
                p2_result = secondEdgePoint;
            }

        if (!isP1Correct || !isP2Correct)
            return false;

        p1 = p1_result;
        p2 = p2_result;

        return true;
    }

    private static void DrawLineLow(Bitmap to, Vector2 p1, Vector2 p2, Color color, Action<Bitmap, int, int, Color> onPixelSet = null)
    {
        float dx = p2.x - p1.x;
        float dy = p2.y - p1.y;

        int yi = 1;
        if (dy < 0)
        {
            yi = -1;
            dy = -dy;
        }

        float D = 2 * dy - dx;
        int y = (int)p1.y;

        for (int x = (int)p1.x; x <= (int)p2.x; x++)
        {
            if (onPixelSet != null)
                onPixelSet(to, x, y, color);
            else
                to.SetPixel(x, y, color);

            if (D > 0)
            {
                y += yi;
                D -= 2 * dx;
            }

            D += 2 * dy;
        }
    }

    private static void DrawLineHigh(Bitmap to, Vector2 p1, Vector2 p2, Color color, Action<Bitmap, int, int, Color> onPixelSet = null)
    {
        float dx = p2.x - p1.x;
        float dy = p2.y - p1.y;

        int xi = 1;
        if (dx < 0)
        {
            xi = -1;
            dx = -dx;
        }

        float D = 2 * dx - dy;
        int x = (int)p1.x;

        for (int y = (int)p1.y; y <= (int)p2.y; y++)
        {
            if (onPixelSet != null)
                onPixelSet(to, x, y, color);
            else
                to.SetPixel(x, y, color);

            if (D > 0)
            {
                x += xi;
                D -= 2 * dy;
            }

            D += 2 * dx;
        }
    }
}
