﻿using System;
using System.Collections.Generic;

public class Scene
{
    public List<Camera> cameras;

    public Scene()
    {
        cameras = new List<Camera>();
    }
}
