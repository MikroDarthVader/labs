﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Libraries;

public class Renderer
{
    public Scene scene;

    public Color background;

    public bool drawAxis;

    public Renderer()
    {
        background = Color.LightSlateGray;

        drawAxis = true;
    }

    public void Render(Bitmap to, BezierSurface surface)
    {
        float[][] zBuffer = new float[to.Width][];
        for (int i = 0; i < zBuffer.Length; i++)
        {
            zBuffer[i] = new float[to.Height];
            for (int j = 0; j < zBuffer[i].Length; j++)
                zBuffer[i][j] = float.MaxValue;
        }

        Camera camera = scene.cameras[0];

        for (int x = 0; x < to.Width; x++)
            for (int y = 0; y < to.Height; y++)
                to.SetPixel(x, y, background);

        List<BezierCurve3> curves = new List<BezierCurve3>();
        for (int i = 0; i < surface.P.Length; i++)
            for (int j = 0; j < surface.P[0].Length; j++)
                for (int k = 2; k < 4; k++)
                {
                    int i1 = i + ((((k / 2) * 2) - 1) + (((k % 2) * 2) - 1)) / 2;
                    int j1 = j + ((((k / 2) * 2) - 1) + ((((k + 1) % 2) * 2) - 1)) / 2;
                    if (MyMath.InRange(i1, 0, surface.P.Length - 1) && MyMath.InRange(j1, 0, surface.P[0].Length - 1))
                        curves.Add(new BezierCurve3(surface.P[i][j], surface.P[i1][j1]));
                }

        RenderBezierSurfaceFull(to, zBuffer, camera, surface, true, 15, 15);
        RenderBezierCurves(to, zBuffer, camera, curves.ToArray());

        PostRender(to, zBuffer, camera);
    }

    private void PostRender(Bitmap to, float[][] zBuffer, Camera camera)
    {
        if (drawAxis)
            RenderAxis(to, zBuffer, camera);
    }

    private void RenderAxis(Bitmap to, float[][] zBuffer, Camera camera)
    {
        RenderLine(to, zBuffer, camera, new Line3(Vector3.Zero, Vector3.Create(1, 0, 0)), Color.FromArgb(255, 255, 0, 0));
        RenderLine(to, zBuffer, camera, new Line3(Vector3.Zero, Vector3.Create(0, 1, 0)), Color.FromArgb(255, 0, 128, 0));
        RenderLine(to, zBuffer, camera, new Line3(Vector3.Zero, Vector3.Create(0, 0, 1)), Color.FromArgb(255, 0, 0, 255));
    }

    private void RenderBezierSurfaceFull(Bitmap to, float[][] zBuffer, Camera camera, BezierSurface surface, bool drawCells = false, int widthCellNumber = 1, int heightCellNumber = 1)
    {
        Color[][] colorBuffer = new Color[to.Width][];
        for (int i = 0; i < colorBuffer.Length; i++)
        {
            colorBuffer[i] = new Color[to.Height];
            for (int j = 0; j < colorBuffer[i].Length; j++)
                colorBuffer[i][j] = Color.Empty;
        }

        float[][] localZBuffer = new float[to.Width][];
        for (int i = 0; i < localZBuffer.Length; i++)
        {
            localZBuffer[i] = new float[to.Height];
            for (int j = 0; j < localZBuffer[i].Length; j++)
                localZBuffer[i][j] = float.MaxValue;
        }

        float[][] pixelHeights = new float[to.Width][];
        for (int i = 0; i < pixelHeights.Length; i++)
        {
            pixelHeights[i] = new float[to.Height];
            for (int j = 0; j < pixelHeights[i].Length; j++)
                pixelHeights[i][j] = float.NaN;
        }

        GetCameraInfo(to, camera, out Plane screenPlane, out _, out Vector3 leftTopPixelCoord, out float camWidth, out float camHeight);

        float u1 = 0;
        float u2 = 1;

        float v1 = 0;
        float v2 = 1;

        Vector2 t1 = Vector2.Create(u1, v1);
        Vector2 t2 = Vector2.Create(u2, v2);

        Vector3[] surfacePoints = new Vector3[4]
        {
            surface.GetPoint(u1, v1),
            surface.GetPoint(u2, v1),
            surface.GetPoint(u2, v2),
            surface.GetPoint(u1, v2)
        };

        float[] pointsDepth = new float[4];
        for (int i = 0; i < pointsDepth.Length; i++)
            pointsDepth[i] = Vector3.Dot(screenPlane.normal, surfacePoints[i] - screenPlane.point);

        bool[] onScreenPointsValid = new bool[4] { false, false, false, false };
        Vector2[] onScreenPoints = new Vector2[4];
        for (int i = 0; i < onScreenPoints.Length; i++)
        {
            if (MyMath.InRange(pointsDepth[i], 0, camera.maxDrawDist))
            {
                onScreenPoints[i] = ProjectInWorldPointOnCameraPlane(to, camera, screenPlane, leftTopPixelCoord, camWidth, camHeight, surfacePoints[i]).Round(1);
                onScreenPointsValid[i] = true;

                if (MyMath.InRange(onScreenPoints[i].x, 0, to.Width - 1) && MyMath.InRange(onScreenPoints[i].y, 0, to.Height - 1))
                {
                    localZBuffer[(int)onScreenPoints[i].x][(int)onScreenPoints[i].y] = pointsDepth[i];
                    pixelHeights[(int)onScreenPoints[i].x][(int)onScreenPoints[i].y] = surfacePoints[i].z;
                }
            }
        }

        RenderBezierSurfaceFullIter(to, localZBuffer, camera, surface, t1, t2, onScreenPointsValid, onScreenPoints, surfacePoints, pixelHeights);

        float minHeight = float.MaxValue;
        float maxHeight = float.MinValue;
        for (int x = 0; x < localZBuffer.Length; x++)
            for (int y = 0; y < localZBuffer[x].Length; y++)
            {
                if (pixelHeights[x][y] != float.NaN)
                {
                    if (pixelHeights[x][y] < minHeight)
                        minHeight = pixelHeights[x][y];
                    if (pixelHeights[x][y] > maxHeight)
                        maxHeight = pixelHeights[x][y];
                }
            }

        for (int x = 0; x < localZBuffer.Length; x++)
            for (int y = 0; y < localZBuffer[x].Length; y++)
                if (localZBuffer[x][y] < zBuffer[x][y])
                {
                    Color topColor = Color.DarkOrange;
                    Color bottomColor = Color.Purple;

                    Color color;
                    if (minHeight == maxHeight)
                        color = topColor;
                    else
                    {
                        float c = (pixelHeights[x][y] - minHeight) / (maxHeight - minHeight);
                        color = Color.FromArgb(MyMath.Round(topColor.R * c + bottomColor.R * (1 - c)),
                                               MyMath.Round(topColor.G * c + bottomColor.G * (1 - c)),
                                               MyMath.Round(topColor.B * c + bottomColor.B * (1 - c)));
                    }

                    if (!drawCells)
                    {
                        to.SetPixel(x, y, color);
                        zBuffer[x][y] = localZBuffer[x][y];
                    }
                    else
                        colorBuffer[x][y] = color;
                }

        if (!drawCells)
            return;

        RenderBezierSurfaceByCells(to, colorBuffer, localZBuffer, camera, surface, widthCellNumber, heightCellNumber);

        for (int x = 0; x < localZBuffer.Length; x++)
            for (int y = 0; y < localZBuffer[0].Length; y++)
                if (localZBuffer[x][y] < zBuffer[x][y])
                {
                    zBuffer[x][y] = localZBuffer[x][y];
                    to.SetPixel(x, y, colorBuffer[x][y]);
                }
    }

    private void RenderBezierSurfaceFullIter(Bitmap to, float[][] localZBuffer, Camera camera, BezierSurface surface, Vector2 t1, Vector2 t2,
                                         bool[] onScreenPointsValid, Vector2[] onScreenPoints,
                                         Vector3[] surfacePoints, float[][] pixelHeights)
    {
        if (onScreenPointsValid[0] && onScreenPointsValid[1] && onScreenPointsValid[2] && onScreenPointsValid[3])
        {
            bool isAllClose = true;
            for (int i = 0; isAllClose && i < onScreenPoints.Length - 1; i++)
                for (int j = i + 1; isAllClose && j < onScreenPoints.Length; j++)
                {
                    Vector2 diff = onScreenPoints[i] - onScreenPoints[j];
                    if (!MyMath.InRange(diff.x, -1, 1) || !MyMath.InRange(diff.y, -1, 1))
                        isAllClose = false;
                }

            if (isAllClose)
                return;
        }
        else
        {
            float inWorldPixelSizeSqr = camera.width * camera.width / (to.Width * to.Width);
            bool isAllClose = true;
            for (int i = 0; isAllClose && i < onScreenPoints.Length - 1; i++)
                for (int j = i + 1; isAllClose && j < onScreenPoints.Length; j++)
                {
                    if ((surfacePoints[i] - surfacePoints[j]).sqrMagnitude >= inWorldPixelSizeSqr)
                        isAllClose = false;
                }

            if (isAllClose)
                return;
        }

        GetCameraInfo(to, camera, out Plane screenPlane, out _, out Vector3 leftTopPixelCoord, out float camWidth, out float camHeight);

        Vector2 tMiddle = (t1 + t2) / 2;

        Vector2[] newT = new Vector2[5]
        {
            Vector2.Create(tMiddle.x, t1.y),
            Vector2.Create(t1.x, tMiddle.y),
            tMiddle,
            Vector2.Create(t2.x, tMiddle.y),
            Vector2.Create(tMiddle.x, t2.y)
        };

        Vector3[] newCurvePoints = new Vector3[5];
        float[] newPointsDepth = new float[5];
        Vector2[] newOnScreenPoints = new Vector2[5];
        bool[] newOnScreenPointsValid = new bool[5];
        for (int i = 0; i < newT.Length; i++)
        {
            newCurvePoints[i] = surface.GetPoint(newT[i].x, newT[i].y);
            newPointsDepth[i] = Vector3.Dot(screenPlane.normal, newCurvePoints[i] - screenPlane.point);

            newOnScreenPointsValid[i] = false;

            if (MyMath.InRange(newPointsDepth[i], 0, camera.maxDrawDist))
            {
                newOnScreenPoints[i] = ProjectInWorldPointOnCameraPlane(to, camera, screenPlane, leftTopPixelCoord, camWidth, camHeight, newCurvePoints[i]).Round(1);
                newOnScreenPointsValid[i] = true;
                if (MyMath.InRange(newOnScreenPoints[i].x, 0, to.Width - 1) && MyMath.InRange(newOnScreenPoints[i].y, 0, to.Height - 1) &&
                    newPointsDepth[i] < localZBuffer[(int)newOnScreenPoints[i].x][(int)newOnScreenPoints[i].y])
                {
                    localZBuffer[(int)newOnScreenPoints[i].x][(int)newOnScreenPoints[i].y] = newPointsDepth[i];
                    pixelHeights[(int)newOnScreenPoints[i].x][(int)newOnScreenPoints[i].y] = newCurvePoints[i].z;
                }
            }
        }

        RenderBezierSurfaceFullIter(to, localZBuffer, camera, surface, t1, tMiddle,
                                    new bool[] { onScreenPointsValid[0], newOnScreenPointsValid[0], newOnScreenPointsValid[2], newOnScreenPointsValid[1] },
                                    new Vector2[] { onScreenPoints[0], newOnScreenPoints[0], newOnScreenPoints[2], newOnScreenPoints[1] },
                                    new Vector3[] { surfacePoints[0], newCurvePoints[0], newCurvePoints[2], newCurvePoints[1] }, pixelHeights);
        RenderBezierSurfaceFullIter(to, localZBuffer, camera, surface, newT[0], newT[3],
                                    new bool[] { newOnScreenPointsValid[0], onScreenPointsValid[1], newOnScreenPointsValid[3], newOnScreenPointsValid[2] },
                                    new Vector2[] { newOnScreenPoints[0], onScreenPoints[1], newOnScreenPoints[3], newOnScreenPoints[2] },
                                    new Vector3[] { newCurvePoints[0], surfacePoints[1], newCurvePoints[3], newCurvePoints[2] }, pixelHeights);
        RenderBezierSurfaceFullIter(to, localZBuffer, camera, surface, newT[1], newT[4],
                                    new bool[] { newOnScreenPointsValid[1], newOnScreenPointsValid[2], newOnScreenPointsValid[4], onScreenPointsValid[3] },
                                    new Vector2[] { newOnScreenPoints[1], newOnScreenPoints[2], newOnScreenPoints[4], onScreenPoints[3] },
                                    new Vector3[] { newCurvePoints[1], newCurvePoints[2], newCurvePoints[4], surfacePoints[3] }, pixelHeights);
        RenderBezierSurfaceFullIter(to, localZBuffer, camera, surface, tMiddle, t2,
                                    new bool[] { newOnScreenPointsValid[2], newOnScreenPointsValid[3], onScreenPointsValid[2], newOnScreenPointsValid[4] },
                                    new Vector2[] { newOnScreenPoints[2], newOnScreenPoints[3], onScreenPoints[2], newOnScreenPoints[4] },
                                    new Vector3[] { newCurvePoints[2], newCurvePoints[3], surfacePoints[2], newCurvePoints[4] }, pixelHeights);
    }

    private void RenderBezierSurfaceByCells(Bitmap to, Color[][] colorBuffer, float[][] localZBuffer, Camera camera, BezierSurface surface, int widthCellNumber, int heightCellNumber)
    {
        float[][] summaryZBuffer = new float[to.Width][];
        for (int x = 0; x < summaryZBuffer.Length; x++)
        {
            summaryZBuffer[x] = new float[to.Height];
            for (int y = 0; y < summaryZBuffer[x].Length; y++)
                summaryZBuffer[x][y] = float.MaxValue;
        }

        float minDepth = float.MaxValue;
        float maxDepth = float.MinValue;

        for (int i = 0; i <= widthCellNumber; i++)
        {
            float[][] tempZBuffer = GenerateParametricCurveZBuffer(to, camera, delegate (float t) { return surface.GetPoint(i / (float)widthCellNumber, t); },
                                                                    out _);

            for (int x = 0; x < summaryZBuffer.Length; x++)
                for (int y = 0; y < summaryZBuffer[x].Length; y++)
                    if (tempZBuffer[x][y] < summaryZBuffer[x][y])
                    {
                        summaryZBuffer[x][y] = tempZBuffer[x][y];

                        if (tempZBuffer[x][y] < minDepth)
                            minDepth = tempZBuffer[x][y];
                        if (tempZBuffer[x][y] > maxDepth)
                            maxDepth = tempZBuffer[x][y];
                    }
        }

        for (int i = 0; i <= heightCellNumber; i++)
        {
            float[][] tempZBuffer = GenerateParametricCurveZBuffer(to, camera, delegate (float t) { return surface.GetPoint(t, i / (float)heightCellNumber); },
                                                                    out _);

            for (int x = 0; x < summaryZBuffer.Length; x++)
                for (int y = 0; y < summaryZBuffer[x].Length; y++)
                    if (tempZBuffer[x][y] < summaryZBuffer[x][y])
                    {
                        summaryZBuffer[x][y] = tempZBuffer[x][y];

                        if (tempZBuffer[x][y] < minDepth)
                            minDepth = tempZBuffer[x][y];
                        if (tempZBuffer[x][y] > maxDepth)
                            maxDepth = tempZBuffer[x][y];
                    }
        }

        for (int x = 0; x < summaryZBuffer.Length; x++)
            for (int y = 0; y < summaryZBuffer[x].Length; y++)
                if (summaryZBuffer[x][y] != float.MaxValue)
                {
                    Color nearColor = Color.Black;
                    Color farColor = Color.DimGray;

                    Color color;
                    if (minDepth == maxDepth)
                        color = nearColor;
                    else
                    {
                        float c = (summaryZBuffer[x][y] - minDepth) / (maxDepth - minDepth);
                        color = Color.FromArgb(MyMath.Round(farColor.R * c + nearColor.R * (1 - c)),
                                               MyMath.Round(farColor.G * c + nearColor.G * (1 - c)),
                                               MyMath.Round(farColor.B * c + nearColor.B * (1 - c)));

                        color = Color.FromArgb(MyMath.Round(color.R * 0.5f + colorBuffer[x][y].R * 0.5f),
                                               MyMath.Round(color.G * 0.5f + colorBuffer[x][y].G * 0.5f),
                                               MyMath.Round(color.B * 0.5f + colorBuffer[x][y].B * 0.5f));
                    }

                    colorBuffer[x][y] = color;
                }
    }

    private void RenderBezierCurves(Bitmap to, float[][] zBuffer, Camera camera, params BezierCurve3[] curves)
    {
        float summaryMinDepth = float.MaxValue;
        float summaryMaxDepth = 0;
        float[][] summaryZBuffer = new float[to.Width][];
        for (int x = 0; x < summaryZBuffer.Length; x++)
        {
            summaryZBuffer[x] = new float[to.Height];
            for (int y = 0; y < summaryZBuffer[x].Length; y++)
                summaryZBuffer[x][y] = float.MaxValue;
        }

        for (int i = 0; i < curves.Length; i++)
        {
            float[][] localZBuffer = GenerateParametricCurveZBuffer(to, camera, curves[i].GetPoint, out _);


            for (int x = 0; x < summaryZBuffer.Length; x++)
                for (int y = 0; y < summaryZBuffer[x].Length; y++)
                    if (localZBuffer[x][y] < summaryZBuffer[x][y])
                    {
                        summaryZBuffer[x][y] = localZBuffer[x][y];

                        if (localZBuffer[x][y] < summaryMinDepth)
                            summaryMinDepth = localZBuffer[x][y];
                        if (localZBuffer[x][y] > summaryMaxDepth)
                            summaryMaxDepth = localZBuffer[x][y];
                    }
        }

        for (int x = 0; x < summaryZBuffer.Length; x++)
            for (int y = 0; y < summaryZBuffer[x].Length; y++)
                if (summaryZBuffer[x][y] < zBuffer[x][y])
                {
                    Color nearColor = Color.Black;
                    Color farColor = Color.DarkSlateGray;

                    Color color;
                    if (summaryMinDepth == summaryMaxDepth)
                        color = nearColor;
                    else
                    {
                        float c = (summaryZBuffer[x][y] - summaryMinDepth) / (summaryMaxDepth - summaryMinDepth);
                        color = Color.FromArgb(MyMath.Round(farColor.R * c + nearColor.R * (1 - c)),
                                               MyMath.Round(farColor.G * c + nearColor.G * (1 - c)),
                                               MyMath.Round(farColor.B * c + nearColor.B * (1 - c)));
                    }

                    to.SetPixel(x, y, color);
                    zBuffer[x][y] = summaryZBuffer[x][y];
                }
    }

    private void RenderBezierCurve(Bitmap to, float[][] zBuffer, Camera camera, BezierCurve3 curve)
    {
        float[][] localZBuffer = GenerateParametricCurveZBuffer(to, camera, curve.GetPoint, out _);

        float minDepth = float.MaxValue;
        float maxDepth = 0;
        for (int x = 0; x < localZBuffer.Length; x++)
            for (int y = 0; y < localZBuffer[x].Length; y++)
                if (localZBuffer[x][y] < zBuffer[x][y])
                {
                    if (localZBuffer[x][y] < minDepth)
                        minDepth = localZBuffer[x][y];
                    if (localZBuffer[x][y] > maxDepth)
                        maxDepth = localZBuffer[x][y];
                }

        for (int x = 0; x < localZBuffer.Length; x++)
            for (int y = 0; y < localZBuffer[x].Length; y++)
                if (localZBuffer[x][y] < zBuffer[x][y])
                {
                    Color nearColor = Color.Black;
                    Color farColor = Color.DarkSlateGray;

                    Color color;
                    if (maxDepth == minDepth)
                        color = nearColor;
                    else
                    {
                        float c = (localZBuffer[x][y] - minDepth) / (maxDepth - minDepth);
                        color = Color.FromArgb(MyMath.Round(farColor.R * c + nearColor.R * (1 - c)),
                                               MyMath.Round(farColor.G * c + nearColor.G * (1 - c)),
                                               MyMath.Round(farColor.B * c + nearColor.B * (1 - c)));
                    }

                    to.SetPixel(x, y, color);
                    zBuffer[x][y] = localZBuffer[x][y];
                }
    }


    private float[][] GenerateParametricCurveZBuffer(Bitmap to, Camera camera, Func<float, Vector3> curveFunct, out float[][] pixelHeights)
    {
        pixelHeights = new float[to.Width][];
        for (int i = 0; i < pixelHeights.Length; i++)
        {
            pixelHeights[i] = new float[to.Height];
            for (int j = 0; j < pixelHeights[i].Length; j++)
                pixelHeights[i][j] = float.NaN;
        }

        float[][] result = new float[to.Width][];
        for (int i = 0; i < result.Length; i++)
        {
            result[i] = new float[to.Height];
            for (int j = 0; j < result[i].Length; j++)
                result[i][j] = float.MaxValue;
        }

        GetCameraInfo(to, camera, out Plane screenPlane, out _, out Vector3 leftTopPixelCoord, out float camWidth, out float camHeight);

        float t1 = 0;
        float t2 = 1;

        Vector3[] curvePoints = new Vector3[2] { curveFunct(t1), curveFunct(t2) };

        float[] pointsDepth = new float[2];
        for (int i = 0; i < pointsDepth.Length; i++)
            pointsDepth[i] = Vector3.Dot(screenPlane.normal, curvePoints[i] - screenPlane.point);

        bool[] onScreenPointsValid = new bool[2] { false, false };
        Vector2[] onScreenPoints = new Vector2[2];
        for (int i = 0; i < onScreenPoints.Length; i++)
            if (MyMath.InRange(pointsDepth[i], 0, camera.maxDrawDist))
            {
                onScreenPoints[i] = ProjectInWorldPointOnCameraPlane(to, camera, screenPlane, leftTopPixelCoord, camWidth, camHeight, curvePoints[i]).Round(1);
                onScreenPointsValid[i] = true;

                if (MyMath.InRange(onScreenPoints[i].x, 0, to.Width - 1) && MyMath.InRange(onScreenPoints[i].y, 0, to.Height - 1))
                {
                    result[(int)onScreenPoints[i].x][(int)onScreenPoints[i].y] = pointsDepth[i];
                    pixelHeights[(int)onScreenPoints[i].x][(int)onScreenPoints[i].y] = curvePoints[i].z;
                }
            }

        GenerateParametricCurveZBufferIter(to, result, camera, curveFunct, t1, t2, onScreenPointsValid, onScreenPoints, curvePoints, pixelHeights);

        return result;
    }

    private void GenerateParametricCurveZBufferIter(Bitmap to, float[][] zBuffer, Camera camera, Func<float, Vector3> curveFunct, float t1, float t2,
                                                bool[] onScreenPointsValid, Vector2[] onScreenPoints,
                                                Vector3[] curvePoints, float[][] pixelHeights)
    {
        if (t1 != 0 || t2 != 1)
            if (onScreenPointsValid[0] && onScreenPointsValid[1])
            {
                Vector2 diff = onScreenPoints[1] - onScreenPoints[0];
                if (MyMath.InRange(diff.x, -1, 1) && MyMath.InRange(diff.y, -1, 1))
                    return;
            }
            else if ((curvePoints[1] - curvePoints[0]).sqrMagnitude < (camera.width * camera.width / (to.Width * to.Width)))
                return;

        GetCameraInfo(to, camera, out Plane screenPlane, out _, out Vector3 leftTopPixelCoord, out float camWidth, out float camHeight);

        float tMiddle = (t2 + t1) / 2;

        Vector3 middleCurvePoint = curveFunct(tMiddle);
        float middlePointDepth = Vector3.Dot(screenPlane.normal, middleCurvePoint - screenPlane.point);

        Vector2 middleOnScreenPoint = Vector2.Zero;
        bool isMiddleOnScreenPointValid = false;

        if (MyMath.InRange(middlePointDepth, 0, camera.maxDrawDist))
        {
            middleOnScreenPoint = ProjectInWorldPointOnCameraPlane(to, camera, screenPlane, leftTopPixelCoord, camWidth, camHeight, middleCurvePoint).Round(1);
            isMiddleOnScreenPointValid = true;
            if (MyMath.InRange(middleOnScreenPoint.x, 0, to.Width - 1) && MyMath.InRange(middleOnScreenPoint.y, 0, to.Height - 1) &&
                middlePointDepth < zBuffer[(int)middleOnScreenPoint.x][(int)middleOnScreenPoint.y])
            {
                zBuffer[(int)middleOnScreenPoint.x][(int)middleOnScreenPoint.y] = middlePointDepth;
                pixelHeights[(int)middleOnScreenPoint.x][(int)middleOnScreenPoint.y] = middleCurvePoint.z;

                for (int i = 0; i < 4; i++)
                {
                    int x = (int)middleOnScreenPoint.x + ((((i / 2) * 2) - 1) + (((i % 2) * 2) - 1)) / 2;
                    int y = (int)middleOnScreenPoint.y + ((((i / 2) * 2) - 1) + ((((i + 1) % 2) * 2) - 1)) / 2;

                    if (MyMath.InRange(x, 0, zBuffer.Length - 1) && MyMath.InRange(y, 0, zBuffer[0].Length - 1) &&
                        zBuffer[x][y] != float.MaxValue)
                        FixPoint(x, y, zBuffer);
                }
            }
        }

        GenerateParametricCurveZBufferIter(to, zBuffer, camera, curveFunct, t1, tMiddle, new bool[] { onScreenPointsValid[0], isMiddleOnScreenPointValid },
                                           new Vector2[] { onScreenPoints[0], middleOnScreenPoint }, new Vector3[] { curvePoints[0], middleCurvePoint }, pixelHeights);
        GenerateParametricCurveZBufferIter(to, zBuffer, camera, curveFunct, tMiddle, t2, new bool[] { isMiddleOnScreenPointValid, onScreenPointsValid[1] },
                                           new Vector2[] { middleOnScreenPoint, onScreenPoints[1] }, new Vector3[] { middleCurvePoint, curvePoints[1] }, pixelHeights);
    }

    private void FixPoint(int x, int y, float[][] zBuffer)
    {
        for (int i = 0; i < 4; i += 3)
            for (int j = 1; j < 3; j++)
            {
                int x1 = x + ((((i / 2) * 2) - 1) + (((i % 2) * 2) - 1)) / 2;
                int y1 = y + ((((i / 2) * 2) - 1) + ((((i + 1) % 2) * 2) - 1)) / 2;
                int x2 = x + ((((j / 2) * 2) - 1) + (((j % 2) * 2) - 1)) / 2;
                int y2 = y + ((((j / 2) * 2) - 1) + ((((j + 1) % 2) * 2) - 1)) / 2;

                if (MyMath.InRange(x1, 0, zBuffer.Length - 1) && MyMath.InRange(y1, 0, zBuffer[0].Length - 1) &&
                    MyMath.InRange(x2, 0, zBuffer.Length - 1) && MyMath.InRange(y2, 0, zBuffer[0].Length - 1) &&
                    zBuffer[x1][y1] != float.MaxValue && zBuffer[x2][y2] != float.MaxValue)
                    zBuffer[x][y] = float.MaxValue;
            }
    }

    private static void RenderLine(Bitmap to, float[][] zBuffer, Camera camera, Line3 line, Color color)
    {
        GetCameraInfo(to, camera, out Plane screenPlane, out Plane endViewPlane, out Vector3 leftTopPixelCoord, out float camWidth, out float camHeight);

        void onPixelSet(Bitmap bitmap, int x, int y, Color c)
        {
            Line3 ray = GetRayFromCameraPixel(to, camera, leftTopPixelCoord, camWidth, camHeight, x, y);

            if (!ray.TryGetNearestPointTo(line, out Vector3 point))
                return;

            float depth = screenPlane.GetDistanceTo(point);

            if (depth < zBuffer[x][y])
            {
                zBuffer[x][y] = depth;

                bitmap.SetPixel(x, y, color);
            }
        }

        Vector2 firstOnScreenPoint = ProjectInWorldPointOnCameraPlane(to, camera, screenPlane, leftTopPixelCoord, camWidth, camHeight, line.point);
        Vector2 secondOnScreenPoint = ProjectInWorldPointOnCameraPlane(to, camera, screenPlane, leftTopPixelCoord, camWidth, camHeight, line.point + line.direction);

        if (firstOnScreenPoint == secondOnScreenPoint)
            return;

        Line2 onScreenLine = new Line2(firstOnScreenPoint, secondOnScreenPoint - firstOnScreenPoint);

        if (screenPlane.TryGetIntersectPointWithLine(line, out Vector3 firstInWorldPoint) &&
            endViewPlane.TryGetIntersectPointWithLine(line, out Vector3 secondInWorldPoint))
        {
            firstOnScreenPoint = ProjectInWorldPointOnCameraPlane(to, camera, screenPlane, leftTopPixelCoord, camWidth, camHeight, firstInWorldPoint);
            secondOnScreenPoint = ProjectInWorldPointOnCameraPlane(to, camera, screenPlane, leftTopPixelCoord, camWidth, camHeight, secondInWorldPoint);

            FixOnScreenPoint(ref firstOnScreenPoint, to, onScreenLine); // for avoiding infinity coordinates
            FixOnScreenPoint(ref secondOnScreenPoint, to, onScreenLine); // for avoiding infinity coordinates

            GraphPrimitives.DrawSegment(to, firstOnScreenPoint, secondOnScreenPoint, color, onPixelSet);
        }
        else if (MyMath.InRange(Vector3.Dot(screenPlane.normal, line.point - screenPlane.point), 0, camera.maxDrawDist))
            GraphPrimitives.DrawLine(to, onScreenLine, color, onPixelSet);
    }

    private static void FixOnScreenPoint(ref Vector2 point, Bitmap to, Line2 line)
    {
        bool pointChanged = false;
        Vector2 p = Vector2.Zero;
        if (point.x < 0)
            pointChanged = pointChanged || line.TryGetPointByX(0, out p);
        if (point.x > to.Width - 1)
            pointChanged = pointChanged || line.TryGetPointByX(to.Width - 1, out p);
        if (point.y < 0)
            pointChanged = pointChanged || line.TryGetPointByY(0, out p);
        if (point.y > to.Height - 1)
            pointChanged = pointChanged || line.TryGetPointByY(to.Height - 1, out p);

        if (pointChanged) point = p;
    }

    private static void GetCameraInfo(Bitmap to, Camera camera, out Plane virtualScreenPlane, out Plane virtualEndViewPlane,
                                               out Vector3 leftTopPixelCoord, out float camWidth, out float camHeight)
    {
        if (camera.renderMode == RenderMode.perspective)
        {
            float tan = (float)Math.Tan(camera.fieldOfView * Math.PI / 360);

            camWidth = camera.planeDist * tan * 2;
            camHeight = camWidth * to.Height / to.Width;

            float pixelSize = camWidth / to.Width;

            leftTopPixelCoord = camera.position + camera.planeDist * camera.Forward +
                                        ((camWidth - pixelSize) / 2) * camera.Left + ((camHeight - pixelSize) / 2) * camera.Up;

            virtualScreenPlane = new Plane(camera.position + camera.planeDist * camera.Forward, camera.Forward);
            virtualEndViewPlane = new Plane(camera.position + (camera.planeDist + camera.maxDrawDist) * camera.Forward, camera.Forward);
        }
        else
        {
            float pixelSize = camera.width / to.Width;

            camWidth = camera.width;
            camHeight = camera.width * to.Height / to.Width;
            leftTopPixelCoord = camera.position + camera.Up * (camHeight - pixelSize) / 2 + camera.Left * (camera.width - pixelSize) / 2;

            virtualScreenPlane = new Plane(camera.position, camera.Forward);
            virtualEndViewPlane = new Plane(camera.position + camera.maxDrawDist * camera.Forward, camera.Forward);
        }
    }

    private static Vector2 ProjectInWorldPointOnCameraPlane(Bitmap to, Camera camera, Plane screenPlane, Vector3 leftTopPixelCoord,
                                                        float camWidth, float camHeight, Vector3 inWorldPoint)
    {
        if (camera.renderMode == RenderMode.perspective)
        {
            float depth = Vector3.Dot(screenPlane.normal, inWorldPoint - screenPlane.point);

            inWorldPoint = screenPlane.point + (inWorldPoint - (screenPlane.point + depth * screenPlane.normal)) *
                                                            camera.planeDist / Math.Abs(camera.planeDist + depth);
        }

        return Vector2.Create(Vector3.Dot(inWorldPoint - leftTopPixelCoord, camera.Right) * to.Width / camWidth,
                              Vector3.Dot(inWorldPoint - leftTopPixelCoord, camera.Down) * to.Height / camHeight);
    }

    private static Line3 GetRayFromCameraPixel(Bitmap to, Camera camera, Vector3 leftTopPixelCoord, float camWidth, float camHeight, int pixel_x, int pixel_y)
    {
        Vector3 raySource = leftTopPixelCoord + camera.Right * pixel_x * camWidth / to.Width + camera.Down * pixel_y * camHeight / to.Height;

        if (camera.renderMode == RenderMode.perspective)
            return new Line3(raySource, raySource - camera.position);
        else
            return new Line3(raySource, camera.Forward);
    }
}
