﻿namespace laba_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Screen = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.cam_pos_z = new System.Windows.Forms.TextBox();
            this.cam_pos_x = new System.Windows.Forms.TextBox();
            this.cam_pos_y = new System.Windows.Forms.TextBox();
            this.cam_euler_x = new System.Windows.Forms.TextBox();
            this.cam_euler_y = new System.Windows.Forms.TextBox();
            this.cam_euler_z = new System.Windows.Forms.TextBox();
            this.cameraMode = new System.Windows.Forms.ComboBox();
            this.point_2_z = new System.Windows.Forms.TextBox();
            this.point_2_y = new System.Windows.Forms.TextBox();
            this.point_2_x = new System.Windows.Forms.TextBox();
            this.point_1_y = new System.Windows.Forms.TextBox();
            this.point_1_x = new System.Windows.Forms.TextBox();
            this.point_1_z = new System.Windows.Forms.TextBox();
            this.point_4_z = new System.Windows.Forms.TextBox();
            this.point_4_y = new System.Windows.Forms.TextBox();
            this.point_4_x = new System.Windows.Forms.TextBox();
            this.point_3_y = new System.Windows.Forms.TextBox();
            this.point_3_x = new System.Windows.Forms.TextBox();
            this.point_3_z = new System.Windows.Forms.TextBox();
            this.point_5_z = new System.Windows.Forms.TextBox();
            this.point_5_y = new System.Windows.Forms.TextBox();
            this.point_5_x = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.angle_y = new System.Windows.Forms.TextBox();
            this.angle_x = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Screen)).BeginInit();
            this.SuspendLayout();
            // 
            // Screen
            // 
            this.Screen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Screen.Location = new System.Drawing.Point(12, 12);
            this.Screen.Name = "Screen";
            this.Screen.Size = new System.Drawing.Size(664, 473);
            this.Screen.TabIndex = 0;
            this.Screen.TabStop = false;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(734, 355);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 30);
            this.button1.TabIndex = 1;
            this.button1.Text = "Render";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cam_pos_z
            // 
            this.cam_pos_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_pos_z.Location = new System.Drawing.Point(806, 102);
            this.cam_pos_z.Name = "cam_pos_z";
            this.cam_pos_z.Size = new System.Drawing.Size(30, 20);
            this.cam_pos_z.TabIndex = 2;
            this.cam_pos_z.Text = "0";
            // 
            // cam_pos_x
            // 
            this.cam_pos_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_pos_x.Location = new System.Drawing.Point(734, 102);
            this.cam_pos_x.Name = "cam_pos_x";
            this.cam_pos_x.Size = new System.Drawing.Size(30, 20);
            this.cam_pos_x.TabIndex = 3;
            this.cam_pos_x.Text = "-10";
            // 
            // cam_pos_y
            // 
            this.cam_pos_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_pos_y.Location = new System.Drawing.Point(770, 102);
            this.cam_pos_y.Name = "cam_pos_y";
            this.cam_pos_y.Size = new System.Drawing.Size(30, 20);
            this.cam_pos_y.TabIndex = 4;
            this.cam_pos_y.Text = "0";
            // 
            // cam_euler_x
            // 
            this.cam_euler_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_euler_x.Location = new System.Drawing.Point(734, 128);
            this.cam_euler_x.Name = "cam_euler_x";
            this.cam_euler_x.Size = new System.Drawing.Size(30, 20);
            this.cam_euler_x.TabIndex = 5;
            this.cam_euler_x.Text = "0";
            // 
            // cam_euler_y
            // 
            this.cam_euler_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_euler_y.Location = new System.Drawing.Point(770, 128);
            this.cam_euler_y.Name = "cam_euler_y";
            this.cam_euler_y.Size = new System.Drawing.Size(30, 20);
            this.cam_euler_y.TabIndex = 6;
            this.cam_euler_y.Text = "0";
            // 
            // cam_euler_z
            // 
            this.cam_euler_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_euler_z.Location = new System.Drawing.Point(806, 128);
            this.cam_euler_z.Name = "cam_euler_z";
            this.cam_euler_z.Size = new System.Drawing.Size(30, 20);
            this.cam_euler_z.TabIndex = 7;
            this.cam_euler_z.Text = "0";
            // 
            // cameraMode
            // 
            this.cameraMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cameraMode.FormattingEnabled = true;
            this.cameraMode.Items.AddRange(new object[] {
            "orthographic",
            "perspective"});
            this.cameraMode.Location = new System.Drawing.Point(734, 75);
            this.cameraMode.Name = "cameraMode";
            this.cameraMode.Size = new System.Drawing.Size(102, 21);
            this.cameraMode.TabIndex = 8;
            // 
            // point_2_z
            // 
            this.point_2_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.point_2_z.Location = new System.Drawing.Point(806, 200);
            this.point_2_z.Name = "point_2_z";
            this.point_2_z.Size = new System.Drawing.Size(30, 20);
            this.point_2_z.TabIndex = 14;
            this.point_2_z.Text = "0";
            // 
            // point_2_y
            // 
            this.point_2_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.point_2_y.Location = new System.Drawing.Point(770, 200);
            this.point_2_y.Name = "point_2_y";
            this.point_2_y.Size = new System.Drawing.Size(30, 20);
            this.point_2_y.TabIndex = 13;
            this.point_2_y.Text = "0";
            // 
            // point_2_x
            // 
            this.point_2_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.point_2_x.Location = new System.Drawing.Point(734, 200);
            this.point_2_x.Name = "point_2_x";
            this.point_2_x.Size = new System.Drawing.Size(30, 20);
            this.point_2_x.TabIndex = 12;
            this.point_2_x.Text = "1";
            // 
            // point_1_y
            // 
            this.point_1_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.point_1_y.Location = new System.Drawing.Point(770, 174);
            this.point_1_y.Name = "point_1_y";
            this.point_1_y.Size = new System.Drawing.Size(30, 20);
            this.point_1_y.TabIndex = 11;
            this.point_1_y.Text = "2";
            // 
            // point_1_x
            // 
            this.point_1_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.point_1_x.Location = new System.Drawing.Point(734, 174);
            this.point_1_x.Name = "point_1_x";
            this.point_1_x.Size = new System.Drawing.Size(30, 20);
            this.point_1_x.TabIndex = 10;
            this.point_1_x.Text = "2";
            // 
            // point_1_z
            // 
            this.point_1_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.point_1_z.Location = new System.Drawing.Point(806, 174);
            this.point_1_z.Name = "point_1_z";
            this.point_1_z.Size = new System.Drawing.Size(30, 20);
            this.point_1_z.TabIndex = 9;
            this.point_1_z.Text = "0";
            // 
            // point_4_z
            // 
            this.point_4_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.point_4_z.Location = new System.Drawing.Point(806, 252);
            this.point_4_z.Name = "point_4_z";
            this.point_4_z.Size = new System.Drawing.Size(30, 20);
            this.point_4_z.TabIndex = 20;
            this.point_4_z.Text = "3";
            // 
            // point_4_y
            // 
            this.point_4_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.point_4_y.Location = new System.Drawing.Point(770, 252);
            this.point_4_y.Name = "point_4_y";
            this.point_4_y.Size = new System.Drawing.Size(30, 20);
            this.point_4_y.TabIndex = 19;
            this.point_4_y.Text = "-3";
            // 
            // point_4_x
            // 
            this.point_4_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.point_4_x.Location = new System.Drawing.Point(734, 252);
            this.point_4_x.Name = "point_4_x";
            this.point_4_x.Size = new System.Drawing.Size(30, 20);
            this.point_4_x.TabIndex = 18;
            this.point_4_x.Text = "1";
            // 
            // point_3_y
            // 
            this.point_3_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.point_3_y.Location = new System.Drawing.Point(770, 226);
            this.point_3_y.Name = "point_3_y";
            this.point_3_y.Size = new System.Drawing.Size(30, 20);
            this.point_3_y.TabIndex = 17;
            this.point_3_y.Text = "0";
            // 
            // point_3_x
            // 
            this.point_3_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.point_3_x.Location = new System.Drawing.Point(734, 226);
            this.point_3_x.Name = "point_3_x";
            this.point_3_x.Size = new System.Drawing.Size(30, 20);
            this.point_3_x.TabIndex = 16;
            this.point_3_x.Text = "0";
            // 
            // point_3_z
            // 
            this.point_3_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.point_3_z.Location = new System.Drawing.Point(806, 226);
            this.point_3_z.Name = "point_3_z";
            this.point_3_z.Size = new System.Drawing.Size(30, 20);
            this.point_3_z.TabIndex = 15;
            this.point_3_z.Text = "3";
            // 
            // point_5_z
            // 
            this.point_5_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.point_5_z.Location = new System.Drawing.Point(806, 278);
            this.point_5_z.Name = "point_5_z";
            this.point_5_z.Size = new System.Drawing.Size(30, 20);
            this.point_5_z.TabIndex = 23;
            this.point_5_z.Text = "0";
            // 
            // point_5_y
            // 
            this.point_5_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.point_5_y.Location = new System.Drawing.Point(770, 278);
            this.point_5_y.Name = "point_5_y";
            this.point_5_y.Size = new System.Drawing.Size(30, 20);
            this.point_5_y.TabIndex = 22;
            this.point_5_y.Text = "-3";
            // 
            // point_5_x
            // 
            this.point_5_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.point_5_x.Location = new System.Drawing.Point(734, 278);
            this.point_5_x.Name = "point_5_x";
            this.point_5_x.Size = new System.Drawing.Size(30, 20);
            this.point_5_x.TabIndex = 21;
            this.point_5_x.Text = "2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(783, 151);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 20);
            this.label1.TabIndex = 24;
            this.label1.Text = "Points";
            // 
            // angle_y
            // 
            this.angle_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.angle_y.Location = new System.Drawing.Point(806, 329);
            this.angle_y.Name = "angle_y";
            this.angle_y.Size = new System.Drawing.Size(30, 20);
            this.angle_y.TabIndex = 27;
            this.angle_y.Text = "0";
            // 
            // angle_x
            // 
            this.angle_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.angle_x.Location = new System.Drawing.Point(770, 329);
            this.angle_x.Name = "angle_x";
            this.angle_x.Size = new System.Drawing.Size(30, 20);
            this.angle_x.TabIndex = 26;
            this.angle_x.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(783, 301);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 20);
            this.label2.TabIndex = 28;
            this.label2.Text = "Angles";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(771, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 20);
            this.label3.TabIndex = 29;
            this.label3.Text = "Camera";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(682, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 20);
            this.label4.TabIndex = 30;
            this.label4.Text = "Euler";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(692, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 20);
            this.label5.TabIndex = 31;
            this.label5.Text = "Pos";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(679, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 20);
            this.label6.TabIndex = 32;
            this.label6.Text = "Mode";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(848, 497);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.angle_y);
            this.Controls.Add(this.angle_x);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.point_5_z);
            this.Controls.Add(this.point_5_y);
            this.Controls.Add(this.point_5_x);
            this.Controls.Add(this.point_4_z);
            this.Controls.Add(this.point_4_y);
            this.Controls.Add(this.point_4_x);
            this.Controls.Add(this.point_3_y);
            this.Controls.Add(this.point_3_x);
            this.Controls.Add(this.point_3_z);
            this.Controls.Add(this.point_2_z);
            this.Controls.Add(this.point_2_y);
            this.Controls.Add(this.point_2_x);
            this.Controls.Add(this.point_1_y);
            this.Controls.Add(this.point_1_x);
            this.Controls.Add(this.point_1_z);
            this.Controls.Add(this.cameraMode);
            this.Controls.Add(this.cam_euler_z);
            this.Controls.Add(this.cam_euler_y);
            this.Controls.Add(this.cam_euler_x);
            this.Controls.Add(this.cam_pos_y);
            this.Controls.Add(this.cam_pos_x);
            this.Controls.Add(this.cam_pos_z);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Screen);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Screen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Screen;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox cam_pos_z;
        private System.Windows.Forms.TextBox cam_pos_x;
        private System.Windows.Forms.TextBox cam_pos_y;
        private System.Windows.Forms.TextBox cam_euler_x;
        private System.Windows.Forms.TextBox cam_euler_y;
        private System.Windows.Forms.TextBox cam_euler_z;
        private System.Windows.Forms.ComboBox cameraMode;
        private System.Windows.Forms.TextBox point_2_z;
        private System.Windows.Forms.TextBox point_2_y;
        private System.Windows.Forms.TextBox point_2_x;
        private System.Windows.Forms.TextBox point_1_y;
        private System.Windows.Forms.TextBox point_1_x;
        private System.Windows.Forms.TextBox point_1_z;
        private System.Windows.Forms.TextBox point_4_z;
        private System.Windows.Forms.TextBox point_4_y;
        private System.Windows.Forms.TextBox point_4_x;
        private System.Windows.Forms.TextBox point_3_y;
        private System.Windows.Forms.TextBox point_3_x;
        private System.Windows.Forms.TextBox point_3_z;
        private System.Windows.Forms.TextBox point_5_z;
        private System.Windows.Forms.TextBox point_5_y;
        private System.Windows.Forms.TextBox point_5_x;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox angle_y;
        private System.Windows.Forms.TextBox angle_x;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}

