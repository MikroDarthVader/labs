﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Libraries;

public class Renderer
{
    public Scene scene;

    public Color background;

    public bool drawAxis;

    public Renderer()
    {
        background = Color.LightSlateGray;

        drawAxis = true;
    }

    public void Render(Bitmap to, BezierCurve3 curve)
    {
        float[][] zBuffer = new float[to.Width][];
        for (int i = 0; i < zBuffer.Length; i++)
        {
            zBuffer[i] = new float[to.Height];
            for (int j = 0; j < zBuffer[i].Length; j++)
                zBuffer[i][j] = float.MaxValue;
        }

        Camera camera = scene.cameras[0];

        for (int x = 0; x < to.Width; x++)
            for (int y = 0; y < to.Height; y++)
                to.SetPixel(x, y, background);

        List<BezierCurve3> curves = new List<BezierCurve3>();
        curves.Add(curve);
        if (curves[0].P.Count > 2)
            for (int i = 0; i < curves[0].P.Count - 1; i++)
                curves.Add(new BezierCurve3(curves[0].P[i], curves[0].P[i + 1]));

        RenderBezierCurves(to, zBuffer, camera, curves.ToArray());

        PostRender(to, zBuffer, camera);
    }

    private void PostRender(Bitmap to, float[][] zBuffer, Camera camera)
    {
        if (drawAxis)
            RenderAxis(to, zBuffer, camera);
    }

    private void RenderAxis(Bitmap to, float[][] zBuffer, Camera camera)
    {
        RenderLine(to, zBuffer, camera, new Line3(Vector3.Zero, Vector3.Create(1, 0, 0)), Color.FromArgb(255, 255, 0, 0));
        RenderLine(to, zBuffer, camera, new Line3(Vector3.Zero, Vector3.Create(0, 1, 0)), Color.FromArgb(255, 0, 128, 0));
        RenderLine(to, zBuffer, camera, new Line3(Vector3.Zero, Vector3.Create(0, 0, 1)), Color.FromArgb(255, 0, 0, 255));
    }

    private void RenderBezierCurves(Bitmap to, float[][] zBuffer, Camera camera, params BezierCurve3[] curves)
    {
        float summaryMinDepth = float.MaxValue;
        float summaryMaxDepth = 0;
        float[][] summaryZBuffer = new float[to.Width][];
        for (int x = 0; x < summaryZBuffer.Length; x++)
        {
            summaryZBuffer[x] = new float[to.Height];
            for (int y = 0; y < summaryZBuffer[x].Length; y++)
                summaryZBuffer[x][y] = float.MaxValue;
        }

        for (int i = 0; i < curves.Length; i++)
        {
            float[][] localZBuffer = GenerateParametricCurveZBuffer(to, camera, curves[i].GetPoint, out float minDepth, out float maxDepth);

            if (minDepth < summaryMinDepth)
                summaryMinDepth = minDepth;

            if (maxDepth > summaryMaxDepth)
                summaryMaxDepth = maxDepth;

            for (int x = 0; x < summaryZBuffer.Length; x++)
                for (int y = 0; y < summaryZBuffer[x].Length; y++)
                    if (localZBuffer[x][y] < summaryZBuffer[x][y])
                        summaryZBuffer[x][y] = localZBuffer[x][y];
        }

        for (int x = 0; x < summaryZBuffer.Length; x++)
            for (int y = 0; y < summaryZBuffer[x].Length; y++)
                if (summaryZBuffer[x][y] < zBuffer[x][y])
                {
                    Color nearColor = Color.DarkOrange;
                    Color farColor = Color.Purple;

                    Color color;
                    if (summaryMinDepth == summaryMaxDepth)
                        color = nearColor;
                    else
                    {
                        float c = (summaryZBuffer[x][y] - summaryMinDepth) / (summaryMaxDepth - summaryMinDepth);
                        color = Color.FromArgb(MyMath.Round(farColor.R * c + nearColor.R * (1 - c)),
                                               MyMath.Round(farColor.G * c + nearColor.G * (1 - c)),
                                               MyMath.Round(farColor.B * c + nearColor.B * (1 - c)));
                    }

                    to.SetPixel(x, y, color);
                    zBuffer[x][y] = summaryZBuffer[x][y];
                }
    }

    private void RenderBezierCurve(Bitmap to, float[][] zBuffer, Camera camera, BezierCurve3 curve)
    {
        float[][] localZBuffer = GenerateParametricCurveZBuffer(to, camera, curve.GetPoint, out float minDepth, out float maxDepth);

        for (int x = 0; x < localZBuffer.Length; x++)
            for (int y = 0; y < localZBuffer[x].Length; y++)
                if (localZBuffer[x][y] < zBuffer[x][y])
                {
                    Color nearColor = Color.DarkOrange;
                    Color farColor = Color.Purple;

                    Color color;
                    if (maxDepth == minDepth)
                        color = nearColor;
                    else
                    {
                        float c = (localZBuffer[x][y] - minDepth) / (maxDepth - minDepth);
                        color = Color.FromArgb(MyMath.Round(farColor.R * c + nearColor.R * (1 - c)),
                                               MyMath.Round(farColor.G * c + nearColor.G * (1 - c)),
                                               MyMath.Round(farColor.B * c + nearColor.B * (1 - c)));
                    }

                    to.SetPixel(x, y, color);
                    zBuffer[x][y] = localZBuffer[x][y];
                }
    }

    private float[][] GenerateParametricCurveZBuffer(Bitmap to, Camera camera, Func<float, Vector3> curveFunct, out float minDepth, out float maxDepth)
    {
        GetCameraInfo(to, camera, out Plane screenPlane, out _, out Vector3 leftTopPixelCoord, out float camWidth, out float camHeight);

        minDepth = float.MaxValue;
        maxDepth = 0;

        float[][] result = new float[to.Width][];
        for (int i = 0; i < result.Length; i++)
        {
            result[i] = new float[to.Height];
            for (int j = 0; j < result[i].Length; j++)
                result[i][j] = float.MaxValue;
        }

        float t1 = 0;
        float t2 = 1;

        Vector3 firstCurvePoint = curveFunct(t1);
        Vector3 secondCurvePoint = curveFunct(t2);

        float firstPointDepth = Vector3.Dot(screenPlane.normal, firstCurvePoint - screenPlane.point);
        float secondPointDepth = Vector3.Dot(screenPlane.normal, secondCurvePoint - screenPlane.point);

        bool isFirstOnScreenPointValid = false;
        Vector2 firstOnScreenPoint = Vector2.Zero;
        if (MyMath.InRange(firstPointDepth, 0, camera.maxDrawDist))
        {
            firstOnScreenPoint = ProjectInWorldPointOnCameraPlane(to, camera, screenPlane, leftTopPixelCoord, camWidth, camHeight, firstCurvePoint).Round(1);
            isFirstOnScreenPointValid = true;

            if (MyMath.InRange(firstOnScreenPoint.x, 0, to.Width - 1) && MyMath.InRange(firstOnScreenPoint.y, 0, to.Height - 1))
                ProcessPoint(result, firstOnScreenPoint, firstPointDepth, ref minDepth, ref maxDepth);
        }

        bool isSecondOnScreenPointValid = false;
        Vector2 secondOnScreenPoint = Vector2.Zero;
        if (MyMath.InRange(secondPointDepth, 0, camera.maxDrawDist))
        {
            secondOnScreenPoint = ProjectInWorldPointOnCameraPlane(to, camera, screenPlane, leftTopPixelCoord, camWidth, camHeight, secondCurvePoint).Round(1);
            isSecondOnScreenPointValid = true;

            if (MyMath.InRange(secondOnScreenPoint.x, 0, to.Width - 1) && MyMath.InRange(secondOnScreenPoint.y, 0, to.Height - 1))
                ProcessPoint(result, secondOnScreenPoint, secondPointDepth, ref minDepth, ref maxDepth);
        }

        GenerateParametricCurveZBufferIter(to, result, camera, curveFunct, t1, t2, isFirstOnScreenPointValid, firstOnScreenPoint,
                                           isSecondOnScreenPointValid, secondOnScreenPoint, firstCurvePoint, secondCurvePoint, ref minDepth, ref maxDepth);

        return result;
    }

    private void GenerateParametricCurveZBufferIter(Bitmap to, float[][] zBuffer, Camera camera, Func<float, Vector3> curveFunct, float t1, float t2,
                                                    bool isOnScreenPoint1Valid, Vector2 onScreenPoint1, bool isOnScreenPoint2Valid, Vector2 onScreenPoint2,
                                                    Vector3 curvePoint1, Vector3 curvePoint2,
                                                    ref float minDepth, ref float maxDepth)
    {
        if (isOnScreenPoint1Valid && isOnScreenPoint2Valid)
        {
            Vector2 diff = onScreenPoint2 - onScreenPoint1;
            if (MyMath.InRange(diff.x, -1, 1) && MyMath.InRange(diff.y, -1, 1))
                return;
        }
        else if ((curvePoint2 - curvePoint1).sqrMagnitude < (camera.width * camera.width / (to.Width * to.Width)))
            return;

        GetCameraInfo(to, camera, out Plane screenPlane, out _, out Vector3 leftTopPixelCoord, out float camWidth, out float camHeight);

        float tMiddle = (t2 + t1) / 2;

        Vector3 middleCurvePoint = curveFunct(tMiddle);
        float middlePointDepth = Vector3.Dot(screenPlane.normal, middleCurvePoint - screenPlane.point);

        Vector2 middleOnScreenPoint = Vector2.Zero;
        bool isMiddleOnScreenPointValid = false;

        if (MyMath.InRange(middlePointDepth, 0, camera.maxDrawDist))
        {
            middleOnScreenPoint = ProjectInWorldPointOnCameraPlane(to, camera, screenPlane, leftTopPixelCoord, camWidth, camHeight, middleCurvePoint).Round(1);
            isMiddleOnScreenPointValid = true;
            if (MyMath.InRange(middleOnScreenPoint.x, 0, to.Width - 1) && MyMath.InRange(middleOnScreenPoint.y, 0, to.Height - 1))
            {
                ProcessPoint(zBuffer, middleOnScreenPoint, middlePointDepth, ref minDepth, ref maxDepth);

                for (int i = 0; i < 4; i++)
                {
                    int x = (int)middleOnScreenPoint.x + ((((i / 2) * 2) - 1) + (((i % 2) * 2) - 1)) / 2;
                    int y = (int)middleOnScreenPoint.y + ((((i / 2) * 2) - 1) + ((((i + 1) % 2) * 2) - 1)) / 2;

                    if (MyMath.InRange(x, 0, zBuffer.Length - 1) && MyMath.InRange(y, 0, zBuffer[0].Length - 1) &&
                        zBuffer[x][y] != float.MaxValue)
                        FixPoint(x, y, zBuffer);
                }
            }
        }

        GenerateParametricCurveZBufferIter(to, zBuffer, camera, curveFunct, t1, tMiddle, isOnScreenPoint1Valid, onScreenPoint1,
                                           isMiddleOnScreenPointValid, middleOnScreenPoint, curvePoint1, middleCurvePoint, ref minDepth, ref maxDepth);
        GenerateParametricCurveZBufferIter(to, zBuffer, camera, curveFunct, tMiddle, t2, isMiddleOnScreenPointValid, middleOnScreenPoint,
                                           isOnScreenPoint2Valid, onScreenPoint2, middleCurvePoint, curvePoint2, ref minDepth, ref maxDepth);
    }

    private void ProcessPoint(float[][] zBuffer, Vector2 onScreenPoint, float pointDepth, ref float minDepth, ref float maxDepth)
    {
        zBuffer[(int)onScreenPoint.x][(int)onScreenPoint.y] = pointDepth;
        if (pointDepth > maxDepth)
            maxDepth = pointDepth;
        if (pointDepth < minDepth)
            minDepth = pointDepth;
    }

    private void FixPoint(int x, int y, float[][] zBuffer)
    {
        for (int i = 0; i < 4; i += 3)
            for (int j = 1; j < 3; j++)
            {
                int x1 = x + ((((i / 2) * 2) - 1) + (((i % 2) * 2) - 1)) / 2;
                int y1 = y + ((((i / 2) * 2) - 1) + ((((i + 1) % 2) * 2) - 1)) / 2;
                int x2 = x + ((((j / 2) * 2) - 1) + (((j % 2) * 2) - 1)) / 2;
                int y2 = y + ((((j / 2) * 2) - 1) + ((((j + 1) % 2) * 2) - 1)) / 2;

                if (MyMath.InRange(x1, 0, zBuffer.Length - 1) && MyMath.InRange(y1, 0, zBuffer[0].Length - 1) &&
                    MyMath.InRange(x2, 0, zBuffer.Length - 1) && MyMath.InRange(y2, 0, zBuffer[0].Length - 1) &&
                    zBuffer[x1][y1] != float.MaxValue && zBuffer[x2][y2] != float.MaxValue)
                    zBuffer[x][y] = float.MaxValue;
            }
    }

    private static void RenderLine(Bitmap to, float[][] zBuffer, Camera camera, Line3 line, Color color)
    {
        GetCameraInfo(to, camera, out Plane screenPlane, out Plane endViewPlane, out Vector3 leftTopPixelCoord, out float camWidth, out float camHeight);

        Action<Bitmap, int, int, Color> onPixelSet = delegate (Bitmap bitmap, int x, int y, Color c)
        {
            Line3 ray = GetRayFromCameraPixel(to, camera, leftTopPixelCoord, camWidth, camHeight, x, y);

            if (!ray.TryGetNearestPointTo(line, out Vector3 point))
                return;

            float depth = screenPlane.GetDistanceTo(point);

            if (depth < zBuffer[x][y])
            {
                zBuffer[x][y] = depth;

                bitmap.SetPixel(x, y, color);
            }
        };

        Vector2 firstOnScreenPoint = ProjectInWorldPointOnCameraPlane(to, camera, screenPlane, leftTopPixelCoord, camWidth, camHeight, line.point);
        Vector2 secondOnScreenPoint = ProjectInWorldPointOnCameraPlane(to, camera, screenPlane, leftTopPixelCoord, camWidth, camHeight, line.point + line.direction);

        if (firstOnScreenPoint == secondOnScreenPoint)
            return;

        Line2 onScreenLine = new Line2(firstOnScreenPoint, secondOnScreenPoint - firstOnScreenPoint);

        if (screenPlane.TryGetIntersectPointWithLine(line, out Vector3 firstInWorldPoint) &&
            endViewPlane.TryGetIntersectPointWithLine(line, out Vector3 secondInWorldPoint))
        {
            firstOnScreenPoint = ProjectInWorldPointOnCameraPlane(to, camera, screenPlane, leftTopPixelCoord, camWidth, camHeight, firstInWorldPoint);
            secondOnScreenPoint = ProjectInWorldPointOnCameraPlane(to, camera, screenPlane, leftTopPixelCoord, camWidth, camHeight, secondInWorldPoint);

            FixOnScreenPoint(ref firstOnScreenPoint, to, onScreenLine); // for avoiding infinity coordinates
            FixOnScreenPoint(ref secondOnScreenPoint, to, onScreenLine); // for avoiding infinity coordinates

            GraphPrimitives.DrawSegment(to, firstOnScreenPoint, secondOnScreenPoint, color, onPixelSet);
        }
        else if (MyMath.InRange(Vector3.Dot(screenPlane.normal, line.point - screenPlane.point), 0, camera.maxDrawDist))
            GraphPrimitives.DrawLine(to, onScreenLine, color, onPixelSet);
    }

    private static void FixOnScreenPoint(ref Vector2 point, Bitmap to, Line2 line)
    {
        bool pointChanged = false;
        Vector2 p = Vector2.Zero;
        if (point.x < 0)
            pointChanged = pointChanged || line.TryGetPointByX(0, out p);
        if (point.x > to.Width - 1)
            pointChanged = pointChanged || line.TryGetPointByX(to.Width - 1, out p);
        if (point.y < 0)
            pointChanged = pointChanged || line.TryGetPointByY(0, out p);
        if (point.y > to.Height - 1)
            pointChanged = pointChanged || line.TryGetPointByY(to.Height - 1, out p);

        if (pointChanged) point = p;
    }

    private static void GetCameraInfo(Bitmap to, Camera camera, out Plane virtualScreenPlane, out Plane virtualEndViewPlane,
                                                   out Vector3 leftTopPixelCoord, out float camWidth, out float camHeight)
    {
        if (camera.renderMode == RenderMode.perspective)
        {
            float tan = (float)Math.Tan(camera.fieldOfView * Math.PI / 360);

            camWidth = camera.planeDist * tan * 2;
            camHeight = camWidth * to.Height / to.Width;

            float pixelSize = camWidth / to.Width;

            leftTopPixelCoord = camera.position + camera.planeDist * camera.Forward +
                                        ((camWidth - pixelSize) / 2) * camera.Left + ((camHeight - pixelSize) / 2) * camera.Up;

            virtualScreenPlane = new Plane(camera.position + camera.planeDist * camera.Forward, camera.Forward);
            virtualEndViewPlane = new Plane(camera.position + (camera.planeDist + camera.maxDrawDist) * camera.Forward, camera.Forward);
        }
        else
        {
            float pixelSize = camera.width / to.Width;

            camWidth = camera.width;
            camHeight = camera.width * to.Height / to.Width;
            leftTopPixelCoord = camera.position + camera.Up * (camHeight - pixelSize) / 2 + camera.Left * (camera.width - pixelSize) / 2;

            virtualScreenPlane = new Plane(camera.position, camera.Forward);
            virtualEndViewPlane = new Plane(camera.position + camera.maxDrawDist * camera.Forward, camera.Forward);
        }
    }

    private static Vector2 ProjectInWorldPointOnCameraPlane(Bitmap to, Camera camera, Plane screenPlane, Vector3 leftTopPixelCoord,
                                                            float camWidth, float camHeight, Vector3 inWorldPoint)
    {
        if (camera.renderMode == RenderMode.perspective)
        {
            float depth = Vector3.Dot(screenPlane.normal, inWorldPoint - screenPlane.point);
            inWorldPoint = screenPlane.point + (inWorldPoint - (screenPlane.point + depth * screenPlane.normal)) *
                                                            camera.planeDist / Math.Abs(camera.planeDist + depth);
        }

        return Vector2.Create(Vector3.Dot(inWorldPoint - leftTopPixelCoord, camera.Right) * to.Width / camWidth,
                              Vector3.Dot(inWorldPoint - leftTopPixelCoord, camera.Down) * to.Height / camHeight);
    }

    private static Line3 GetRayFromCameraPixel(Bitmap to, Camera camera, Vector3 leftTopPixelCoord, float camWidth, float camHeight, int pixel_x, int pixel_y)
    {
        Vector3 raySource = leftTopPixelCoord + camera.Right * pixel_x * camWidth / to.Width + camera.Down * pixel_y * camHeight / to.Height;

        if (camera.renderMode == RenderMode.perspective)
            return new Line3(raySource, raySource - camera.position);
        else
            return new Line3(raySource, camera.Forward);
    }
}
