﻿using System;
using System.Collections.Generic;

using Libraries;

public class BaseObject
{
    public Vector3 position;
    public Vector3 eulerAngles
    {
        get
        {
            return _eulerAngles;
        }
        set
        {
            _eulerAngles = value;

            rotMatrix = FromEulerToMatrix(_eulerAngles);
        }
    }

    private Vector3 _eulerAngles;
    private float[] rotMatrix;

    public BaseObject()
    {
        position = Vector3.Zero;
        eulerAngles = Vector3.Zero;
    }

    public Vector3 Forward
    {
        get
        {
            return Vector3.Create(rotMatrix[0], rotMatrix[3], rotMatrix[6]).normalized;
        }
    }

    public Vector3 Backward
    {
        get
        {
            return -Forward;
        }
    }

    public Vector3 Left
    {
        get
        {
            return Vector3.Create(rotMatrix[1], rotMatrix[4], rotMatrix[7]).normalized;
        }
    }

    public Vector3 Right
    {
        get
        {
            return -Left;
        }
    }

    public Vector3 Up
    {
        get
        {
            return Vector3.Create(rotMatrix[2], rotMatrix[5], rotMatrix[8]).normalized;
        }
    }

    public Vector3 Down
    {
        get
        {
            return -Up;
        }
    }

    public Vector3 FromLocalToGlobal(Vector3 point)
    {
        Vector3 rotatedPoint;

        rotatedPoint.x = rotMatrix[0] * point.x + rotMatrix[1] * point.y + rotMatrix[2] * point.z;
        rotatedPoint.y = rotMatrix[3] * point.x + rotMatrix[4] * point.y + rotMatrix[5] * point.z;
        rotatedPoint.z = rotMatrix[6] * point.x + rotMatrix[7] * point.y + rotMatrix[8] * point.z;

        return position + rotatedPoint;
    }

    private static float[] FromEulerToMatrix(Vector3 euler)
    {
        float[] result = new float[9];

        euler.x = euler.x % 360 * (float)Math.PI / 180;
        euler.y = euler.y % 360 * (float)Math.PI / 180;
        euler.z = euler.z % 360 * (float)Math.PI / 180;

        float A = (float)Math.Cos(euler.x);
        float B = (float)Math.Sin(euler.x);
        float C = (float)Math.Cos(euler.y);
        float D = (float)Math.Sin(euler.y);
        float E = (float)Math.Cos(euler.z);
        float F = (float)Math.Sin(euler.z);

        float AD = A * D;
        float BD = B * D;

        result[0] = C * E;
        result[1] = -C * F;
        result[2] = -D;
        result[3] = -BD * E + A * F;
        result[4] = BD * F + A * E;
        result[5] = -B * C;
        result[6] = AD * E + B * F;
        result[7] = -AD * F + B * E;
        result[8] = A * C;

        return result;
    }
}
