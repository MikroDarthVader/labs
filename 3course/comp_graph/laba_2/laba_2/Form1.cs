﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Libraries;

namespace laba_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Renderer renderer;

        private void Form1_Load(object sender, EventArgs e)
        {
            cameraMode.SelectedIndex = 1;

            renderer = new Renderer
            {
                scene = new Scene()
            };

            Camera cam = new Camera();

            renderer.scene.cameras.Add(cam);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!TryParceToVector3(cam_pos_x.Text, cam_pos_y.Text, cam_pos_z.Text, out Vector3 cam_pos) ||
                !TryParceToVector3(cam_euler_x.Text, cam_euler_y.Text, cam_euler_z.Text, out Vector3 cam_euler) || 
                !TryParceToVector3(point_1_x.Text, point_1_y.Text, point_1_z.Text, out Vector3 point_1) ||
                !TryParceToVector3(point_2_x.Text, point_2_y.Text, point_2_z.Text, out Vector3 point_2) ||
                !TryParceToVector3(point_3_x.Text, point_3_y.Text, point_3_z.Text, out Vector3 point_3) ||
                !TryParceToVector3(point_4_x.Text, point_4_y.Text, point_4_z.Text, out Vector3 point_4) ||
                !TryParceToVector3(point_5_x.Text, point_5_y.Text, point_5_z.Text, out Vector3 point_5) ||
                !float.TryParse(angle_x.Text, out float angleX) || !float.TryParse(angle_y.Text, out float angleY))
                return;

            renderer.scene.cameras[0].position = cam_pos;
            renderer.scene.cameras[0].eulerAngles = cam_euler;
            renderer.scene.cameras[0].renderMode = cameraMode.SelectedIndex == 0 ? RenderMode.orthographic : RenderMode.perspective;

            Bitmap bitmap = new Bitmap(Screen.Width, Screen.Height);

            renderer.Render(bitmap, new BezierCurve3(ApplyAnglesToPoint(point_1, angleX, angleY), ApplyAnglesToPoint(point_5, angleX, angleY),
                                                     ApplyAnglesToPoint(point_2, angleX, angleY), ApplyAnglesToPoint(point_3, angleX, angleY),
                                                     ApplyAnglesToPoint(point_4, angleX, angleY)));

            Screen.CreateGraphics().DrawImage(bitmap, 0, 0, bitmap.Width, bitmap.Height);
        }

        private bool TryParceToVector3(string x, string y, string z, out Vector3 vec)
        {
            vec = Vector3.Zero;
            if (!float.TryParse(x, out float _x) || !float.TryParse(y, out float _y) || !float.TryParse(z, out float _z))
                return false;

            vec = Vector3.Create(_x, _y, _z);
            return true;
        }

        private Vector3 ApplyAnglesToPoint(Vector3 point, float angleX, float angleY)
        {
            Vector2 temp = Vector2.Create(point.y, point.z).Rotate(angleX * (float)Math.PI / 180);
            point.y = temp.x;
            point.z = temp.y;

            temp = Vector2.Create(point.x, point.z).Rotate(angleY * (float)Math.PI / 180);
            point.x = temp.x;
            point.z = temp.y;

            return point;
        }
    }
}
