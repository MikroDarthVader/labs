﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Libraries;

namespace laba_4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        LineSegment2[] segments;

        private void Generate_button_Click(object sender, EventArgs e)
        {
            Bitmap to = new Bitmap(Screen.Width, Screen.Height);

            for (int x = 0; x < to.Width; x++)
                for (int y = 0; y < to.Height; y++)
                    to.SetPixel(x, y, Color.LightSlateGray);

            Vector2 screenLeftBottom = Vector2.Create(to.Width * 0.25f, to.Height * 0.25f);
            Vector2 screenRightUp = Vector2.Create(to.Width * 0.75f, to.Height * 0.75f);

            DrawScreenArea(to, screenLeftBottom, screenRightUp);

            segments = GenerateRandomSegments(20, Vector2.Create(0, 0), Vector2.Create(to.Width - 1, to.Height - 1));

            for (int i = 0; i < segments.Length; i++)
                GraphPrimitives.DrawSegment(to, segments[i].startPoint, segments[i].endPoint, Color.Yellow);

            Screen.CreateGraphics().DrawImage(to, 0, 0, to.Width, to.Height);
        }

        private void Cuttoff_button_Click(object sender, EventArgs e)
        {
            Bitmap to = new Bitmap(Screen.Width, Screen.Height);

            for (int x = 0; x < to.Width; x++)
                for (int y = 0; y < to.Height; y++)
                    to.SetPixel(x, y, Color.LightSlateGray);

            Vector2 screenLeftBottom = Vector2.Create(to.Width * 0.25f, to.Height * 0.25f);
            Vector2 screenRightUp = Vector2.Create(to.Width * 0.75f, to.Height * 0.75f);

            DrawScreenArea(to, screenLeftBottom, screenRightUp);

            for (int i = 0; i < segments.Length; i++)
                DrawSegment(to, segments[i].startPoint, segments[i].endPoint, screenLeftBottom, screenRightUp);

            Screen.CreateGraphics().DrawImage(to, 0, 0, to.Width, to.Height);
        }

        private void DrawScreenArea(Bitmap to, Vector2 leftBottom, Vector2 rightUp)
        {
            leftBottom -= Vector2.Create(1, 1);
            rightUp += Vector2.Create(1, 1);

            GraphPrimitives.DrawSegment(to, leftBottom, Vector2.Create(leftBottom.x, rightUp.y), Color.Black);
            GraphPrimitives.DrawSegment(to, Vector2.Create(leftBottom.x, rightUp.y), rightUp, Color.Black);
            GraphPrimitives.DrawSegment(to, rightUp, Vector2.Create(rightUp.x, leftBottom.y), Color.Black);
            GraphPrimitives.DrawSegment(to, Vector2.Create(rightUp.x, leftBottom.y), leftBottom, Color.Black);
        }

        private LineSegment2[] GenerateRandomSegments(int count, Vector2 minCoords, Vector2 maxCoords)
        {
            LineSegment2[] result = new LineSegment2[count];

            Random rnd = new Random();
            for (int i = 0; i < result.Length; i++)
                result[i] = new LineSegment2(Vector2.Create(rnd.Next((int)minCoords.x, (int)maxCoords.x), rnd.Next((int)minCoords.y, (int)maxCoords.y)),
                                             Vector2.Create(rnd.Next((int)minCoords.x, (int)maxCoords.x), rnd.Next((int)minCoords.y, (int)maxCoords.y)));

            return result;
        }

        private void DrawSegment(Bitmap to, Vector2 startPoint, Vector2 endPoint, Vector2 screenLeftBottom, Vector2 screenRightUp)
        {
            int startCode, endCode, code;
            Vector2 point;

            startCode = GetPointCode(startPoint, screenLeftBottom, screenRightUp);
            endCode = GetPointCode(endPoint, screenLeftBottom, screenRightUp);

            while ((startCode | endCode) != 0)
            {
                if ((startCode & endCode) != 0)
                    return;

                if (startCode != 0)
                {
                    code = startCode;
                    point = startPoint;
                }
                else
                {
                    code = endCode;
                    point = endPoint;
                }

                if ((code & 1) != 0) // left
                {
                    point.y += (startPoint.y - endPoint.y) * (screenLeftBottom.x - point.x) / (startPoint.x - endPoint.x);
                    point.x = screenLeftBottom.x;
                }
                else if ((code & 2) != 0) // right
                {
                    point.y += (startPoint.y - endPoint.y) * (screenRightUp.x - point.x) / (startPoint.x - endPoint.x);
                    point.x = screenRightUp.x;
                }
                else if ((code & 4) != 0) // bot
                {
                    point.x += (startPoint.x - endPoint.x) * (screenLeftBottom.y - point.y) / (startPoint.y - endPoint.y);
                    point.y = screenLeftBottom.y;
                }
                else if ((code & 8) != 0) // top
                {
                    point.x += (startPoint.x - endPoint.x) * (screenRightUp.y - point.y) / (startPoint.y - endPoint.y);
                    point.y = screenRightUp.y;
                }

                if (code == startCode)
                {
                    startPoint = point;
                    startCode = GetPointCode(startPoint, screenLeftBottom, screenRightUp);
                }
                else
                {
                    endPoint = point;
                    endCode = GetPointCode(endPoint, screenLeftBottom, screenRightUp);
                }
            }

            GraphPrimitives.DrawSegment(to, startPoint, endPoint, Color.Green);
        }

        private int GetPointCode(Vector2 point, Vector2 screenLeftBottom, Vector2 screenRightUp)
        {
            return (point.x < screenLeftBottom.x ? 1 : 0) +
                   (point.x > screenRightUp.x ? 2 : 0) +
                   (point.y < screenLeftBottom.y ? 4 : 0) +
                   (point.y > screenRightUp.y ? 8 : 0);
        }
    }
}
