﻿

namespace Libraries
{
    public struct LineSegment2
    {
        public Vector2 startPoint;
        public Vector2 endPoint;

        public LineSegment2(Vector2 startPoint, Vector2 endPoint)
        {
            this.startPoint = startPoint;
            this.endPoint = endPoint;
        }

        public static LineSegment2 Create(Vector2 startPoint, Vector2 endPoint)
        {
            LineSegment2 res;
            res.startPoint = startPoint;
            res.endPoint = endPoint;

            return res;
        }

        public static implicit operator LineSegment2(LineSegment3 segment)
        {
            return new LineSegment2(segment.startPoint, segment.endPoint);
        }
    }
}
