﻿using System;
using System.Collections.Generic;

public class Scene
{
    public List<Camera> cameras;
    public List<PolyObject> objects;
    public PointLightSource lightSource;

    public Scene()
    {
        cameras = new List<Camera>();
        objects = new List<PolyObject>();
    }
}
