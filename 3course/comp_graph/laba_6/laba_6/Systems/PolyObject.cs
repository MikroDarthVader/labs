﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Libraries;

public class PolyObject : BaseObject
{
    public List<Vector3> points;
    public List<int> poligons;
    public List<Color> poligonColors;

    public PolyObject()
    {
        points = new List<Vector3>();
        poligons = new List<int>();
        poligonColors = new List<Color>();
    }
}
