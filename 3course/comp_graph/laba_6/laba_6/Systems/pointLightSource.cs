﻿

public class PointLightSource : BaseObject
{
    public float Intensity;

    public PointLightSource(float intensity)
    {
        Intensity = intensity;
    }
}
