﻿namespace laba_6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Screen = new System.Windows.Forms.PictureBox();
            this.Render_button = new System.Windows.Forms.Button();
            this.cam_pos_x = new System.Windows.Forms.TextBox();
            this.cam_pos_y = new System.Windows.Forms.TextBox();
            this.cam_pos_z = new System.Windows.Forms.TextBox();
            this.cam_euler_x = new System.Windows.Forms.TextBox();
            this.cam_euler_y = new System.Windows.Forms.TextBox();
            this.cam_euler_z = new System.Windows.Forms.TextBox();
            this.camera_mode = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ls_pox_z = new System.Windows.Forms.TextBox();
            this.ls_pos_y = new System.Windows.Forms.TextBox();
            this.ls_pox_x = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ls_intensity = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.use_lights = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.Screen)).BeginInit();
            this.SuspendLayout();
            // 
            // Screen
            // 
            this.Screen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Screen.Location = new System.Drawing.Point(13, 13);
            this.Screen.Name = "Screen";
            this.Screen.Size = new System.Drawing.Size(749, 470);
            this.Screen.TabIndex = 0;
            this.Screen.TabStop = false;
            // 
            // Render_button
            // 
            this.Render_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Render_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Render_button.Location = new System.Drawing.Point(822, 278);
            this.Render_button.Name = "Render_button";
            this.Render_button.Size = new System.Drawing.Size(102, 27);
            this.Render_button.TabIndex = 1;
            this.Render_button.Text = "Render";
            this.Render_button.UseVisualStyleBackColor = true;
            this.Render_button.Click += new System.EventHandler(this.Render_button_Click);
            // 
            // cam_pos_x
            // 
            this.cam_pos_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_pos_x.Location = new System.Drawing.Point(823, 61);
            this.cam_pos_x.Name = "cam_pos_x";
            this.cam_pos_x.Size = new System.Drawing.Size(30, 20);
            this.cam_pos_x.TabIndex = 2;
            this.cam_pos_x.Text = "-10";
            // 
            // cam_pos_y
            // 
            this.cam_pos_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_pos_y.Location = new System.Drawing.Point(859, 61);
            this.cam_pos_y.Name = "cam_pos_y";
            this.cam_pos_y.Size = new System.Drawing.Size(30, 20);
            this.cam_pos_y.TabIndex = 3;
            this.cam_pos_y.Text = "0";
            // 
            // cam_pos_z
            // 
            this.cam_pos_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_pos_z.Location = new System.Drawing.Point(895, 61);
            this.cam_pos_z.Name = "cam_pos_z";
            this.cam_pos_z.Size = new System.Drawing.Size(30, 20);
            this.cam_pos_z.TabIndex = 4;
            this.cam_pos_z.Text = "0";
            // 
            // cam_euler_x
            // 
            this.cam_euler_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_euler_x.Location = new System.Drawing.Point(823, 87);
            this.cam_euler_x.Name = "cam_euler_x";
            this.cam_euler_x.Size = new System.Drawing.Size(30, 20);
            this.cam_euler_x.TabIndex = 5;
            this.cam_euler_x.Text = "0";
            // 
            // cam_euler_y
            // 
            this.cam_euler_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_euler_y.Location = new System.Drawing.Point(859, 87);
            this.cam_euler_y.Name = "cam_euler_y";
            this.cam_euler_y.Size = new System.Drawing.Size(30, 20);
            this.cam_euler_y.TabIndex = 6;
            this.cam_euler_y.Text = "0";
            // 
            // cam_euler_z
            // 
            this.cam_euler_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cam_euler_z.Location = new System.Drawing.Point(895, 87);
            this.cam_euler_z.Name = "cam_euler_z";
            this.cam_euler_z.Size = new System.Drawing.Size(30, 20);
            this.cam_euler_z.TabIndex = 7;
            this.cam_euler_z.Text = "0";
            // 
            // camera_mode
            // 
            this.camera_mode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.camera_mode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.camera_mode.FormattingEnabled = true;
            this.camera_mode.Items.AddRange(new object[] {
            "perspective",
            "orthographic"});
            this.camera_mode.Location = new System.Drawing.Point(823, 114);
            this.camera_mode.Name = "camera_mode";
            this.camera_mode.Size = new System.Drawing.Size(101, 21);
            this.camera_mode.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(860, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "Camera";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(768, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "Mode";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(771, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Euler";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(781, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 20);
            this.label4.TabIndex = 12;
            this.label4.Text = "Pos";
            // 
            // ls_pox_z
            // 
            this.ls_pox_z.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ls_pox_z.Location = new System.Drawing.Point(895, 183);
            this.ls_pox_z.Name = "ls_pox_z";
            this.ls_pox_z.Size = new System.Drawing.Size(30, 20);
            this.ls_pox_z.TabIndex = 15;
            this.ls_pox_z.Text = "3";
            // 
            // ls_pos_y
            // 
            this.ls_pos_y.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ls_pos_y.Location = new System.Drawing.Point(859, 183);
            this.ls_pos_y.Name = "ls_pos_y";
            this.ls_pos_y.Size = new System.Drawing.Size(30, 20);
            this.ls_pos_y.TabIndex = 14;
            this.ls_pos_y.Text = "3";
            // 
            // ls_pox_x
            // 
            this.ls_pox_x.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ls_pox_x.Location = new System.Drawing.Point(823, 183);
            this.ls_pox_x.Name = "ls_pox_x";
            this.ls_pox_x.Size = new System.Drawing.Size(30, 20);
            this.ls_pox_x.TabIndex = 13;
            this.ls_pox_x.Text = "-3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(826, 160);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 20);
            this.label5.TabIndex = 16;
            this.label5.Text = "Light Source";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(781, 183);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 20);
            this.label6.TabIndex = 17;
            this.label6.Text = "Pos";
            // 
            // ls_intensity
            // 
            this.ls_intensity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ls_intensity.Location = new System.Drawing.Point(895, 209);
            this.ls_intensity.Name = "ls_intensity";
            this.ls_intensity.Size = new System.Drawing.Size(30, 20);
            this.ls_intensity.TabIndex = 18;
            this.ls_intensity.Text = "30";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(820, 209);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 20);
            this.label7.TabIndex = 19;
            this.label7.Text = "Intensity";
            // 
            // use_lights
            // 
            this.use_lights.AutoSize = true;
            this.use_lights.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.use_lights.Location = new System.Drawing.Point(830, 235);
            this.use_lights.Name = "use_lights";
            this.use_lights.Size = new System.Drawing.Size(98, 24);
            this.use_lights.TabIndex = 20;
            this.use_lights.Text = "Use lights";
            this.use_lights.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 495);
            this.Controls.Add(this.use_lights);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ls_intensity);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ls_pox_z);
            this.Controls.Add(this.ls_pos_y);
            this.Controls.Add(this.ls_pox_x);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.camera_mode);
            this.Controls.Add(this.cam_euler_z);
            this.Controls.Add(this.cam_euler_y);
            this.Controls.Add(this.cam_euler_x);
            this.Controls.Add(this.cam_pos_z);
            this.Controls.Add(this.cam_pos_y);
            this.Controls.Add(this.cam_pos_x);
            this.Controls.Add(this.Render_button);
            this.Controls.Add(this.Screen);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Screen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Screen;
        private System.Windows.Forms.Button Render_button;
        private System.Windows.Forms.TextBox cam_pos_x;
        private System.Windows.Forms.TextBox cam_pos_y;
        private System.Windows.Forms.TextBox cam_pos_z;
        private System.Windows.Forms.TextBox cam_euler_x;
        private System.Windows.Forms.TextBox cam_euler_y;
        private System.Windows.Forms.TextBox cam_euler_z;
        private System.Windows.Forms.ComboBox camera_mode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ls_pox_z;
        private System.Windows.Forms.TextBox ls_pos_y;
        private System.Windows.Forms.TextBox ls_pox_x;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox ls_intensity;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox use_lights;
    }
}

