﻿using System;
using System.Drawing;

namespace Libraries
{
    public struct Vector2
    {
        public static Vector2 Zero = new Vector2(0, 0);
        public static float Epsilon = Single.Epsilon;
        public float x, y;

        public float sqrMagnitude => x * x + y * y;
        public float magnitude => (float)Math.Sqrt(sqrMagnitude);
        public float angle => (float)Math.Atan2(y, x);
        public Vector2 normalized => this / magnitude;

        public Vector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public static Vector2 Create(float x, float y)
        {
            Vector2 res;
            res.x = x;
            res.y = y;
            return res;
        }

        public static Vector2 Create(PointF toCopy)
        {
            Vector2 res;
            res.x = toCopy.X;
            res.y = toCopy.Y;
            return res;
        }

        public static Vector2 Create(Point toCopy)
        {
            Vector2 res;
            res.x = toCopy.X;
            res.y = toCopy.Y;
            return res;
        }

        public static Vector2 Create(Size toCopy)
        {
            Vector2 res;
            res.x = toCopy.Width;
            res.y = toCopy.Height;
            return res;
        }

        public static Vector2 operator +(Vector2 left, Vector2 right)
        {
            left.x += right.x;
            left.y += right.y;
            return left;
        }

        public static Vector2 operator -(Vector2 left, Vector2 right)
        {
            left.x -= right.x;
            left.y -= right.y;
            return left;
        }

        public static Vector2 operator *(float left, Vector2 right)
        {
            right.x *= left;
            right.y *= left;
            return right;
        }

        public static Vector2 operator *(Vector2 left, float right)
        {
            return right * left;
        }

        public static Vector2 operator /(Vector2 left, float right)
        {
            left.x /= right;
            left.y /= right;
            return left;
        }

        public static float Dot(Vector2 left, Vector2 right)
        {
            return left.x * right.x + left.y * right.y;
        }

        public static bool operator ==(Vector2 left, Vector2 right)
        {
            return (left - right).sqrMagnitude <= Epsilon;
        }

        public static bool operator !=(Vector2 left, Vector2 right)
        {
            return !(left == right);
        }

        public Vector2 Round(float step, MyMath.Direction dir = MyMath.Direction.None)
        {
            Vector2 res;
            res.x = MyMath.Round(x, step, dir);
            res.y = MyMath.Round(y, step, dir);
            return res;
        }

        /// <summary>
        /// clamps in x between v1.x & v2.x and y between v1.y & v2.y
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public Vector2 Clamp(Vector2 v1, Vector2 v2)
        {
            var res = this;
            res.x = MyMath.Clamp(res.x, v1.x, v2.x);
            res.y = MyMath.Clamp(res.y, v1.y, v2.y);
            return res;
        }

        public PointF point
        {
            get
            {
                var res = Point.Empty;
                res.X = (int)Math.Round(x);
                res.Y = (int)Math.Round(y);
                return res;
            }
        }

        public PointF pointF
        {
            get
            {
                var res = PointF.Empty;
                res.X = x;
                res.Y = y;
                return res;
            }
        }

        public SizeF sizeF
        {
            get
            {
                var res = SizeF.Empty;
                res.Width = x;
                res.Height = y;
                return res;
            }
        }

        /// <summary>
        /// angle in radians
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public Vector2 Rotate(float angle)
        {
            Vector2 res = this;
            res.x = (float)Math.Cos(angle) * x - (float)Math.Sin(angle) * y;
            res.y = (float)Math.Sin(angle) * x + (float)Math.Cos(angle) * y;
            return res;
        }

        public Vector2 Perpendicular()
        {
            Vector2 res = this;
            res.x = -y;
            res.y = x;
            return res;
        }

        public static implicit operator Vector2(Vector3 v)
        {
            return new Vector2(v.x, v.y);
        }

        public override string ToString()
        {
            return x.ToString() + ", " + y.ToString();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
    }
}
