﻿using System;
using System.Collections.Generic;

using Libraries;

class BezierSurface
{
    public Vector3[][] P;

    public BezierSurface(int n, int m)
    {
        P = new Vector3[n][];
        for (int i = 0; i < n; i++)
            P[i] = new Vector3[m];
    }

    public Vector3 GetPoint(float u, float v)
    {
        Vector3 result = Vector3.Zero;

        int n = P.Length - 1;
        int m = P[0].Length - 1;
        for (int i = 0; i <= n; i++)
            for (int j = 0; j <= m; j++)
                result += MyMath.BinomialCoef(n, i) * (float)Math.Pow(1 - u, n - i) * (float)Math.Pow(u, i) *
                          MyMath.BinomialCoef(m, j) * (float)Math.Pow(1 - v, m - j) * (float)Math.Pow(v, j) * 
                          P[i][j];

        return result;
    }
}
