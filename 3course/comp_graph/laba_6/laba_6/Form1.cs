﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Libraries;

namespace laba_6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Renderer renderer;

        private void Form1_Load(object sender, EventArgs e)
        {
            camera_mode.SelectedIndex = 1;

            renderer = new Renderer
            {
                scene = new Scene()
            };

            renderer.scene.cameras.Add(new Camera());
            renderer.scene.lightSource = new PointLightSource(30);
            renderer.scene.lightSource.position = Vector3.Create(-3, 3, 3);

            PolyObject obj = GenerateCube();

            obj.position = Vector3.Create(0, 0, 0);
            obj.eulerAngles = Vector3.Create(0, 0, 0);
            renderer.scene.objects.Add(obj);

            obj = GenerateCube();
            obj.position = Vector3.Create(0, 0, 2f);
            obj.eulerAngles = Vector3.Create(0, 0, 45);
            renderer.scene.objects.Add(obj);
        }

        private void Render_button_Click(object sender, EventArgs e)
        {
            if (!TryParceToVector3(cam_pos_x.Text, cam_pos_y.Text, cam_pos_z.Text, out Vector3 cam_pos) ||
                !TryParceToVector3(cam_euler_x.Text, cam_euler_y.Text, cam_euler_z.Text, out Vector3 cam_euler) ||
                !TryParceToVector3(ls_pox_x.Text, ls_pos_y.Text, ls_pox_z.Text, out Vector3 ls_pos) ||
                !float.TryParse(ls_intensity.Text, out float ls_inden))
                return;

            renderer.scene.cameras[0].position = cam_pos;
            renderer.scene.cameras[0].eulerAngles = cam_euler;
            renderer.scene.cameras[0].renderMode = camera_mode.SelectedIndex == 1 ? RenderMode.orthographic : RenderMode.perspective;

            renderer.scene.lightSource.position = ls_pos;
            renderer.scene.lightSource.Intensity = ls_inden;

            Bitmap bitmap = new Bitmap(Screen.Width, Screen.Height);

            renderer.Render(bitmap, use_lights.Checked);

            Screen.CreateGraphics().DrawImage(bitmap, 0, 0,
                bitmap.Width, bitmap.Height);
        }

        private bool TryParceToVector3(string x, string y, string z, out Vector3 vec)
        {
            vec = Vector3.Zero;
            if (!float.TryParse(x, out float _x) || !float.TryParse(y, out float _y) || !float.TryParse(z, out float _z))
                return false;

            vec = Vector3.Create(_x, _y, _z);
            return true;
        }

        private PolyObject GenerateCube()
        {
            PolyObject result = new PolyObject();

            result.points.Add(Vector3.Create(0.5f, 0.5f, 0.5f)); //0
            result.points.Add(Vector3.Create(0.5f, -0.5f, 0.5f)); //1
            result.points.Add(Vector3.Create(-0.5f, -0.5f, 0.5f)); //2
            result.points.Add(Vector3.Create(-0.5f, 0.5f, 0.5f)); //3
            result.points.Add(Vector3.Create(0.5f, 0.5f, -0.5f)); //4
            result.points.Add(Vector3.Create(0.5f, -0.5f, -0.5f)); //5
            result.points.Add(Vector3.Create(-0.5f, -0.5f, -0.5f)); //6
            result.points.Add(Vector3.Create(-0.5f, 0.5f, -0.5f)); //7

            result.poligons.Add(3);
            result.poligons.Add(2);
            result.poligons.Add(6);
            result.poligons.Add(3);
            result.poligons.Add(6);
            result.poligons.Add(7);
            result.poligons.Add(0);
            result.poligons.Add(1);
            result.poligons.Add(2);
            result.poligons.Add(0);
            result.poligons.Add(2);
            result.poligons.Add(3);
            result.poligons.Add(0);
            result.poligons.Add(3);
            result.poligons.Add(7);
            result.poligons.Add(0);
            result.poligons.Add(7);
            result.poligons.Add(4);
            result.poligons.Add(1);
            result.poligons.Add(0);
            result.poligons.Add(4);
            result.poligons.Add(1);
            result.poligons.Add(4);
            result.poligons.Add(5);
            result.poligons.Add(5);
            result.poligons.Add(4);
            result.poligons.Add(7);
            result.poligons.Add(5);
            result.poligons.Add(7);
            result.poligons.Add(6);
            result.poligons.Add(2);
            result.poligons.Add(1);
            result.poligons.Add(5);
            result.poligons.Add(2);
            result.poligons.Add(5);
            result.poligons.Add(6);

            result.poligonColors.Add(Color.Coral);
            result.poligonColors.Add(Color.Coral);
            result.poligonColors.Add(Color.CadetBlue);
            result.poligonColors.Add(Color.CadetBlue);
            result.poligonColors.Add(Color.Gold);
            result.poligonColors.Add(Color.Gold);
            result.poligonColors.Add(Color.Coral);
            result.poligonColors.Add(Color.Coral);
            result.poligonColors.Add(Color.CadetBlue);
            result.poligonColors.Add(Color.CadetBlue);
            result.poligonColors.Add(Color.Gold);
            result.poligonColors.Add(Color.Gold);

            return result;
        }
    }
}
