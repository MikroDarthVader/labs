function y = secondFunct(x)
% secondFunct is an optimized function
y = exp(x.^2).^0.1;
end

