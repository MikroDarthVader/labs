function [xmin, fmin, iterNum] = FibonacciSearch(f,interval,Ln,draw)
% FibonacciSearch searches minimum using three point division
% search algorithm
% [xmin, fmin, neval] = goldensectionsearch(f,interval,tol)
% f - an objective function handle
% interval = [a, b] - search interval
% Ln - searching interval length
% draw - draw plot if true
    a = interval(1);
    b = interval(2);
    
    %% VISUALIZATION
    if draw == true
        figure(3);
        h = 1e-5;
        x = a:h:b;
        y = feval(f,x);
        plot(x,y); hold on
        line([a b],[0 0],'Color','k'); %axis x
        line([a a],[0 feval(f,a)],'Marker','s');
        line([b b],[0 feval(f,b)],'Marker','s');
    end
    %% MAIN LOOP
    % O(this) = O(f)*n
    % n = log1.6((b-a)/Ln)
    F = [1, 1]; % FibonacciNumbers
    while F(numel(F)) < (b - a) / Ln
        F(numel(F) + 1) = F(numel(F)) + F(numel(F) - 1);
    end
    
    e = (b - a) / (F(numel(F)) + 1);
    
    L = abs(b - a);
    iterNum = 1;
    
    n = numel(F);
    x1 = a + L*F(n - 2)/F(n); %lambda
    x2 = a + L*F(n - 1)/F(n); %mu
    
    y1 = feval(f,x1);
    y2 = feval(f,x2);
    while iterNum ~= n - 2
        
        if y1 < y2
            b = x2;
            L = abs(b - a);
            x2 = x1;
            y2 = y1;
            x1 = a + L*F(n - iterNum - 2)/F(n - iterNum);
            y1 = feval(f,x1);
        else
            a = x1;
            L = abs(b - a);
            x1 = x2;
            y1 = y2;
            x2 = a + L*F(n - iterNum - 1)/F(n - iterNum);
            y2 = feval(f,x2);
        end
        
        iterNum = iterNum + 1;
        
        if draw == true && iterNum < 20
            col = hsv2rgb([rand(), 1, 0.5+0.5*rand()]);
            line([a a],[0 feval(f, a)],'Marker','s','Color',col);
            line([b b],[0 feval(f, b)],'Marker','s','Color',col);
        end
    end
    
    x2 = x1 + e;
    if feval(f,x1) > feval(f,x2)
        xmin = (x1 + b)/2;
    else
        xmin = (a + x2)/2;
    end
    
    fmin = feval(f, xmin);
end