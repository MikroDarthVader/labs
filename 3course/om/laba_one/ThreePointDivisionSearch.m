function [xmin, fmin, neval] = ThreePointDivisionSearch(f,interval,tol,draw)
% ThreePointDivisionSearch searches minimum using three point division
% search algorithm
% [xmin, fmin, neval] = goldensectionsearch(f,interval,tol)
% f - an objective function handle
% interval = [a, b] - search interval
% tol - set for bot range and function value
% draw - draw plot if true
    a = interval(1);
    b = interval(2);
    L = abs(b - a);
    neval = 0;
    
    %% VISUALIZATION
    % O(this) = O(f) * n
    % n = log2((b-a)/Ln)
    if draw == true
        figure(3); 
        h = max(1e-5,tol);
        x = a:h:b;
        y = feval(f,x);
        plot(x,y); hold on
        line([a b],[0 0],'Color','k'); %axis x
        line([a a],[0 feval(f,a)],'Marker','s');
        line([b b],[0 feval(f,b)],'Marker','s'); 
    end
    %% MAIN LOOP
    xm = (a + b) / 2;
    x1 = a + L/4;
    x2 = b - L/4; 
        
    ym = feval(f,xm);
    y1 = feval(f,x1);
    y2 = feval(f,x2);
        
    while L > tol
        
        if y1 < ym
            b = xm;
            xm = x1;
            ym = y1;
        elseif y1 >= ym && y2 >= ym
            a = x1;
            b = x2;
        else
            a = xm;
            xm = x2;
            ym = y2;
        end
        
        L = abs(b - a);
        x1 = a + L/4;
        x2 = b - L/4;
        y1 = feval(f,x1);
        y2 = feval(f,x2);
        
        neval = neval + 2;
        L = abs(b - a);
        
        if draw == true && neval < 20 % visualization
            col = hsv2rgb([rand(), 1, 0.5+0.5*rand()]);
            line([a a],[0 feval(f, a)],'Marker','s','Color',col);
            line([b b],[0 feval(f, b)],'Marker','s','Color',col);
        end
    end
    
    xmin = xm;
    fmin = feval(f, xmin);
end