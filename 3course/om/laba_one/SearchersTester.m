% TEST SEARCH

%% first funct init
% funct = @firstFunct;
% searchingValue = 4.6027029793717181410;

%% second funct init
funct = @secondFunct;
searchingValue = 0;

%% drawing plots
x = linspace(0,10,50);
y1 = sin(x);
title('Combine Plots')
semilogx(x,y1)

hold on

y2 = sin(x/2);
semilogx(x,y2)

y3 = 2*sin(x);
scatter(x,y3) 


interval = [-2, 10];
%set the number of points on a plot
N = 30;
tolspan = logspace(-14,0,N); %evenly spaced logarithmic sequence
Xmin_1  = zeros(1,N);
Xmin_2  = zeros(1,N);
Neval_1 = zeros(1,N);
Neval_2 = zeros(1,N);
i = 1;%iteration counter
for tol = tolspan %select the required tolerance
    [xmin, ~, neval] = ThreePointDivisionSearch(funct,interval,tol,false); %search for the xmin
    Xmin_1(i) = xmin; %save stats
    Neval_1(i) = neval;
    
    [xmin, ~, neval] = FibonacciSearch(funct,interval,tol,false); %search for the xmin
    Xmin_2(i) = xmin; %save stats
    Neval_2(i) = neval;
    
    i = i + 1;
end

%plot the number of evaluations
subplot(2,1,1);
semilogx(tolspan,Neval_1,'.-b','Color', [0, 0, 1]);
hold on
semilogx(tolspan,Neval_2,'.-b','Color', [0.2, 0.7, 0.3]);
xlabel('tol');
ylabel('Neval');
hold off;
%plot the error
subplot(2,1,2);
loglog(tolspan,abs(Xmin_1 - searchingValue),'s-b','Color', [0, 0, 1]); 
hold on
loglog(tolspan,abs(Xmin_2 - searchingValue),'s-b','Color', [0.2, 0.7, 0.3]);
xlabel('tol');
ylabel('err');
hold off;