select SUM(OrderQty) as amount, ProductID from Sales.SalesOrderDetail
group by ProductID
order by amount desc

select SUM(OrderQty) as amount, ProductID from Sales.SalesOrderDetail --where amount >= 2000
group by ProductID
having SUM(OrderQty) >= 2000
order by amount desc