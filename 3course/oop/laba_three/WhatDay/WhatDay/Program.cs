﻿using System;
using System.Collections;

enum MonthName
{
    January,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December
}

class WhatDay
{
    static ICollection DaysInMonths = new int[12] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    static ICollection DaysInLeapMonths = new int[12] { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    static void Main()
    {
        try
        {
            Console.WriteLine("Enter a year : ");
            bool isLeapYear = int.Parse(Console.ReadLine()) % 4 == 0;
            int maxDayNumber = isLeapYear ? 366 : 365;

            Console.WriteLine("Enter a day number between 1 and " + maxDayNumber + ": ");

            int dayNum = int.Parse(Console.ReadLine());
            if (dayNum < 1 || dayNum > maxDayNumber)
                throw new ArgumentOutOfRangeException("dayNum");

            int monthNum = 0;

            ICollection daysCollection = isLeapYear ? DaysInLeapMonths : DaysInMonths;

            foreach (int daysInMonth in daysCollection)
            {
                if (dayNum <= daysInMonth)
                {
                    break;
                }
                else
                {
                    dayNum -= daysInMonth;
                    monthNum++;
                }
            }

            Console.WriteLine(dayNum + ", " + (MonthName)monthNum);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
}