﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructType
{
    enum AccountType
    {
        Checking,
        Deposit
    }

    struct BankAccount
    {
        public long accNo;
        public decimal accBal;
        public AccountType accType;
    }

    class Program
    {
        static void Main(string[] args)
        {
            BankAccount goldAccount; // Объявление переменной типа BankAccount

            goldAccount.accNo = 100; // Инициализация полей структуры
            goldAccount.accBal = 60;
            goldAccount.accType = AccountType.Checking;

            Console.WriteLine(goldAccount.accNo); // Вывод полей структуры
            Console.WriteLine(goldAccount.accBal);
            Console.WriteLine(goldAccount.accType);
        }
    }
}
