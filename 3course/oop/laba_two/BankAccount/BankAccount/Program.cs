﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankAccount
{
    enum AccountType
    {
        Checking,
        Deposit
    }

    class Program
    {
        static void Main(string[] args)
        {
            AccountType goldAccount = AccountType.Checking; // Инициализация переменных типа AccountType
            AccountType platinumAccount = AccountType.Deposit;

            Console.WriteLine(goldAccount); // Вывод переменных в консоль
            Console.WriteLine(platinumAccount);
        }
    }
}
