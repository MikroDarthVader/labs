﻿using System.Collections.Generic;

namespace Libraries
{
    public class Figure
    {
        public readonly List<Vector2> Shape;
        public Vector2 position = Vector2.Zero;
        public float angle = 0;

        private float areaRadius;
        public Area bounds
        {
            set { position = value.position; areaRadius = value.radius; }
            get => Area.Create(position, areaRadius);
        }

        public Vector2 this[int i] => Shape[i].Rotate(angle) + position;

        public Figure(List<Vector2> Shape)
        {
            this.Shape = Shape;
        }

        public void SetAreaRadius(float radius) => areaRadius = radius;
    }
}
