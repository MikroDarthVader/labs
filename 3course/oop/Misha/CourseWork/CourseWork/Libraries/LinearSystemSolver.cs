﻿using System;

namespace Libraries
{
    public class LinearSystemSolver
    {
        /*data structures*/
        public class Equation
        {
            private readonly float[] coefficients;

            public Equation(int variablesCount) { coefficients = new float[variablesCount + 1]; }

            public float this[int i]
            {
                get => coefficients[i];
                set => coefficients[i] = value;
            }

            public int VariablesCount => coefficients.Length - 1;
            public float FreeMember
            {
                get => coefficients[VariablesCount];
                set => coefficients[VariablesCount] = value;
            }

            public static Equation operator -(Equation left, Equation right)
            {
                if (left.VariablesCount != right.VariablesCount)
                    throw new SystemIncorrectException();

                Equation res = new Equation(left.VariablesCount);

                for (int i = 0; i < left.coefficients.Length; i++)
                    res.coefficients[i] = left.coefficients[i] - right.coefficients[i];

                return res;
            }

            public static Equation operator /(Equation left, float right)
            {
                Equation res = new Equation(left.VariablesCount);

                for (int i = 0; i < left.coefficients.Length; i++)
                    res.coefficients[i] = left.coefficients[i] / right;

                return res;
            }
        }

        public class SystemUndefinedException : Exception
        {
            public SystemUndefinedException() : base("System is undefined. Solution cannot be found.") { }
        }

        public class SystemIncorrectException : Exception
        {
            public SystemIncorrectException() : base("System set incorrectly. Solution cannot be found.") { }
        }
        /*data structures*/

        private Equation[] equations;
        public Equation this[int i] => equations[i];
        public int equationsCount => equations.Length;

        public LinearSystemSolver(int variablesCount)
        {
            equations = new Equation[variablesCount];
            for (int i = 0; i < equations.Length; i++)
                equations[i] = new Equation(variablesCount);
        }

        public float[] Solve()
        {
            int variablesCount = equations.Length;

            for (int k = 0; k < variablesCount; k++)
            {
                int index = k;
                float max = Math.Abs(equations[k][k]);
                for (int i = k + 1; i < variablesCount; i++)
                {
                    if (Math.Abs(equations[i][k]) > max)
                    {
                        max = Math.Abs(equations[i][k]);
                        index = i;
                    }
                }
                if (max < float.Epsilon)
                    throw new SystemUndefinedException();

                MyMath.Swap(ref equations[k], ref equations[index]);

                for (int i = k; i < variablesCount; i++)
                {
                    if (Math.Abs(equations[i][k]) < float.Epsilon) continue;

                    equations[i] /= equations[i][k];
                    if (i != k) equations[i] -= equations[k];
                }
            }

            float[] res = new float[variablesCount];
            for (int k = variablesCount - 1; k >= 0; k--)
            {
                res[k] = equations[k].FreeMember;
                for (int i = 0; i < k; i++)
                    equations[i].FreeMember = equations[i].FreeMember - equations[i][k] * res[k];
            }
            return res;
        }
    }
}
