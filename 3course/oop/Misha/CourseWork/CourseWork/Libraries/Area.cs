﻿namespace Libraries
{
    public struct Area
    {
        public Vector2 position;
        public float radius;

        public bool Empty
        {
            get => radius < 0;
            set => radius = -1;
        }

        public static Area Create(Vector2 position, float radius)
        {
            Area res;
            res.position = position;
            res.radius = radius;
            return res;
        }

        public bool Includes(Vector2 point) => !Empty && ((position - point).sqrMagnitude <= radius * radius);
    }
}
