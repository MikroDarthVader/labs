﻿using Libraries;
using System.Drawing;

namespace Screen
{
    public abstract class UIObject
    {
        public Figure figure;
        public abstract bool alive { get; set; }

        protected UI ui;

        public UIObject(UI ui) => this.ui = ui;

        public abstract void Draw(Graphics graphics, bool selected);

        public virtual void Move(Vector2 mousePos) { }
        public virtual void OnSelect() { }
        public virtual void OnDeselect() { }
        public virtual void OnDestroy() { }
    }
}
