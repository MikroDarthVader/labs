﻿using ElectricCircuit;
using ElectricCircuit.Elements;
using Libraries;
using Screen.UIObjects;
using Screen.UIObjects.UIElements;
using System.Collections.Generic;
using System.Drawing;
using System;
using System.Text;

namespace Screen
{
    public class UI
    {
        public Circuit circuit;

        public List<UIObject> UIObjects;
        public float cellSize;
        public Action drawRequest;

        private UIElement selected;
        private UIObject moving;

        public UI(Action drawRequest, float cellSize)
        {
            UIObjects = new List<UIObject>();
            circuit = new Circuit();

            this.drawRequest = drawRequest;
            this.cellSize = cellSize;
        }

        public UI(Action drawCall, float cellSize, string fileData) : this(drawCall, cellSize)
        {
            FromFile(fileData);
        }

        public void CreateElement(Elements toCreate) => CreateElement(toCreate, null, null, true);

        private UIElement CreateElement(Elements toCreate, UINode input, UINode output, bool draw)
        {
            UIElement element = null;

            bool nodesAlreadyCreated = input != null && output != null;
            input = input ?? new UINode(this, Vector2.Create(3, 3) * cellSize);
            output = output ?? new UINode(this, Vector2.Create(8, 3) * cellSize);

            switch (toCreate)
            {
                case Elements.Источник:
                    element = new UIVoltageGenerator(this, input, output); break;
                case Elements.Ключ:
                    element = new UISwitch(this, input, output); break;
                case Elements.Резистор:
                    element = new UIResistor(this, input, output); break;
                case Elements.Лампа:
                    element = new UILamp(this, input, output); break;
                case Elements.Нагреватель:
                    element = new UIHeater(this, input, output); break;
            }

            if (!nodesAlreadyCreated)
            {
                UIObjects.Add(input);
                UIObjects.Add(output);
            }
            UIObjects.Add(element);
            if (draw) drawRequest();

            return element;
        }

        public void DestroySelected()
        {
            if (selected == null)
                return;

            UIObjects.Remove(selected);
            selected.alive = false;

            selected.OnDestroy();
        }

        private void GC()
        {
            for (int i = 0; i < UIObjects.Count; i++)
            {
                if (!UIObjects[i].alive)
                {
                    UIObjects[i].OnDestroy();
                    UIObjects.RemoveAt(i);
                    i--;
                }
            }
        }

        List<PointF> gridX, gridY;
        public void ClearDrawCache() => gridX = gridY = null;

        void DrawGrid(Graphics graphics, Vector2 windowSize)
        {
            Color c = Color.FromArgb(50, 50, 50);
            Pen p = new Pen(c);

            if (gridX == null || gridY == null)
            {
                gridX = new List<PointF>();
                bool mode;
                float x, y;
                for (x = cellSize, mode = true; x < windowSize.x; x += cellSize, mode = !mode)
                {
                    if (mode)
                        gridX.Add(new PointF(x, -1));
                    gridX.Add(new PointF(x, windowSize.y + 1));
                    if (!mode)
                        gridX.Add(new PointF(x, -1));
                }

                gridY = new List<PointF>();
                for (y = cellSize, mode = true; y < windowSize.y; y += cellSize, mode = !mode)
                {
                    if (mode)
                        gridY.Add(new PointF(-1, y));
                    gridY.Add(new PointF(windowSize.x + 1, y));
                    if (!mode)
                        gridY.Add(new PointF(-1, y));
                }
            }

            if (gridX.Count != 0 && gridY.Count != 0)
            {
                graphics.DrawLines(p, gridX.ToArray());
                graphics.DrawLines(p, gridY.ToArray());
            }
        }

        public void Draw(Graphics graphics, Vector2 windowSize)
        {
            GC();

            DrawGrid(graphics, windowSize);
            foreach (var obj in UIObjects)
                obj.Draw(graphics, selected == obj);
        }

        public UIObject FindInObject(Predicate<UIObject> match)
        {
            GC();
            return UIObjects.Find(match);
        }

        public T FindInArea<T>(Area area, T except)
        {
            return (T)(object)FindInObject(obj => area.Includes(obj.figure.position) && obj is T && obj != (object)except);
        }

        public struct SelectedInfo
        {
            public string paramName;
            public string paramValue;
            public string current;
            public string voltage;
            public string resistance;
        }
        public void SelectElement(Vector2 mousePos, out SelectedInfo selectedInfo)
        {
            var oldSel = selected;
            selected = (UIElement)FindInObject(obj => obj.figure.bounds.Includes(mousePos) && obj is UIElement);

            selectedInfo.paramName = selected == null ? "Параметр" : selected.paramName;
            selectedInfo.paramValue = selected == null ? "" : selected.paramValue.ToString();
            if (selected != null)
                selected.GetCalculationResults(out selectedInfo.current,
                                                out selectedInfo.voltage,
                                                out selectedInfo.resistance);
            else
                selectedInfo.current = selectedInfo.voltage = selectedInfo.resistance = "";

            if (oldSel != selected)
                drawRequest();
        }

        public void CalcuclateCircuit() => circuit.Calculate();

        public void SetParamOfSelected(float paramValue)
        {
            if (selected == null)
                return;

            selected.paramValue = paramValue;
        }

        public void MouseDown(Vector2 mousePos)
        {
            moving = FindInObject(obj => obj.figure.bounds.Includes(mousePos));
            moving?.OnSelect();
        }

        public void MouseUp()
        {
            if (moving == null)
                return;

            moving.OnDeselect();
            moving = null;
        }

        public void MoveElement(Vector2 mousePos, Vector2 bounds)
        {
            if (moving == null)
                return;

            var newPos = Vector2.Create(mousePos.x, mousePos.y).Clamp(Vector2.Zero, bounds);
            var oldPos = moving.figure.position;
            var oldAng = moving.figure.angle;

            moving.Move(newPos);

            if (moving.figure.position != oldPos || moving.figure.angle != oldAng)
                drawRequest();
        }

        public string ToFile()
        {
            GC();
            StringBuilder stb = new StringBuilder();
            foreach (var obj in UIObjects)
            {
                if (obj is UINode)
                {
                    stb.Insert(0, "n;" + ((UINode)obj).associatedNode.GetHashCode() + ";"
                                       + obj.figure.position.x + ';'
                                       + obj.figure.position.y + '\n');
                    continue;
                }

                string toAppend = "e;";
                if (obj is UIVoltageGenerator) toAppend += (int)Elements.Источник;
                else if (obj is UISwitch) toAppend += (int)Elements.Ключ;
                else if (obj is UILamp) toAppend += (int)Elements.Лампа;
                else if (obj is UIHeater) toAppend += (int)Elements.Нагреватель;
                else if (obj is UIResistor) toAppend += (int)Elements.Резистор;
                toAppend += ";" + ((UIElement)obj).paramValue + ";"
                                + ((UIElement)obj).drawShift + ";"
                                + ((UIElement)obj).associatedElement.input.GetHashCode() + ";"
                                + ((UIElement)obj).associatedElement.output.GetHashCode() + '\n';

                stb.Append(toAppend);
            }
            return stb.ToString();
        }

        public class ReadBrokenFileException : Exception
        { public ReadBrokenFileException() : base("Файл поврежден или не существует. Чтение невозможно") { } }

        private void FromFile(string fileData)
        {
            Dictionary<int, UINode> nodes = new Dictionary<int, UINode>();
            List<string> lines = new List<string>(fileData.Split('\n'));
            lines.Remove("");

            foreach (var line in lines)
            {
                var data = line.Split(';');
                bool res = true;

                if (data.Length == 0) throw new ReadBrokenFileException();

                switch (data[0])
                {
                    case "n":
                        {
                            if (data.Length != 4) throw new ReadBrokenFileException();

                            var node = new UINode(this, Vector2.Zero);
                            res &= int.TryParse(data[1], out var hash);
                            res &= float.TryParse(data[2], out node.figure.position.x);
                            res &= float.TryParse(data[3], out node.figure.position.y);
                            try { nodes.Add(hash, node); } catch (ArgumentException) { throw new ReadBrokenFileException(); }
                            UIObjects.Add(node);
                        }
                        break;

                    case "e":
                        {
                            if (data.Length != 6) throw new ReadBrokenFileException();

                            res &= int.TryParse(data[1], out var toCreate);
                            res &= int.TryParse(data[4], out var inputHash);
                            res &= int.TryParse(data[5], out var outputHash);
                            res &= nodes.TryGetValue(inputHash, out var input);
                            res &= nodes.TryGetValue(outputHash, out var output);

                            if (!Enum.IsDefined(typeof(Elements), toCreate)) throw new ReadBrokenFileException();

                            var element = CreateElement((Elements)toCreate, input, output, false);
                            res &= float.TryParse(data[2], out var paramValue);
                            element.paramValue = paramValue;
                            res &= float.TryParse(data[3], out element.drawShift);
                        }
                        break;
                    default: throw new ReadBrokenFileException();
                }
                if (!res) throw new ReadBrokenFileException();
            }
        }
    }
}
