﻿using ElectricCircuit;
using Libraries;
using System.Drawing;

namespace Screen.UIObjects
{
    public class UINode : UIObject
    {
        private bool _alive = true;
        public override bool alive
        {
            get => associatedNode.ConnectedElements.Count > 0 && _alive;
            set => _alive = value;
        }

        public Node associatedNode;
        public float size = 0.5f;

        public UINode(UI ui, Vector2 pos) : base(ui)
        {
            size *= ui.cellSize;
            figure = new Figure(null);
            figure.position = pos;
            figure.bounds = Area.Create(figure.position, size);

            associatedNode = new Node();
        }

        public override void Draw(Graphics graphics, bool selected)
        {
            Vector2 sizeV = Vector2.Create(size);
            graphics.FillEllipse(Brushes.White, new RectangleF((figure.position - sizeV / 2).pointF, sizeV.sizeF));
        }

        public override void Move(Vector2 mousePos)
        {
            figure.position = mousePos.Round(ui.cellSize);
        }

        public override void OnDeselect()
        {
            UINode nodeToConnect = ui.FindInArea(figure.bounds, this);

            if (nodeToConnect == null || !Circuit.CapableToConnect(associatedNode, nodeToConnect.associatedNode))
                return;

            associatedNode.Replace(nodeToConnect.associatedNode);
            nodeToConnect.alive = false;
        }
    }
}
