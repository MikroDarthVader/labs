﻿using ElectricCircuit.Elements;
using Libraries;
using System.Collections.Generic;
using System.Drawing;
using System;

namespace Screen.UIObjects.UIElements
{
    public class UIResistor : UIElement
    {
        public override string paramName => "Сопротивление";
        public override float paramValue
        {
            get => associatedElement.resistance;
            set
            {
                if (value < 0)
                    throw new ArgumentException("Сопротивление не может быть меньше нуля");

                ((Resistor)associatedElement).SetResistance(value);
            }
        }

        public UIResistor(UI ui, UINode input, UINode output) : base(ui, input, output)
        {
            float length = ui.cellSize * 2;
            associatedElement = new Resistor(ui.circuit, input.associatedNode, output.associatedNode);

            figure = new Figure(new List<Vector2>() {
                Vector2.Create(-length/2,0), //contact 0
                Vector2.Create(length/2,0), //contact 1

                Vector2.Create(-length/2,length/4), //left_up corner
                Vector2.Create(-length/2,-length/4), //left_down corner
                Vector2.Create(length/2,-length/4), //right_down corner
                Vector2.Create(length/2,length/4), //right_up corner
            });
            figure.SetAreaRadius(length / 2);
        }

        protected override void DrawFigure(Graphics graphics, Pen p)
        {
            graphics.DrawLine(p, figure[2].pointF, figure[3].pointF);
            graphics.DrawLine(p, figure[3].pointF, figure[4].pointF);
            graphics.DrawLine(p, figure[4].pointF, figure[5].pointF);
            graphics.DrawLine(p, figure[5].pointF, figure[2].pointF);
        }
    }
}
