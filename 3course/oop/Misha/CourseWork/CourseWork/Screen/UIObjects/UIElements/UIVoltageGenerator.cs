﻿using ElectricCircuit.Elements;
using Libraries;
using System.Collections.Generic;
using System.Drawing;
using System;

namespace Screen.UIObjects.UIElements
{
    public class UIVoltageGenerator : UIElement
    {
        public override string paramName => "Напряжение";
        public override float paramValue
        {
            get => ((VoltageGenerator)associatedElement).voltage;
            set
            {
                if (value < 0)
                    throw new ArgumentException("Напряжение на источнике не может быть ниже нуля");

                ((VoltageGenerator)associatedElement).voltage = value;
            }
        }

        public UIVoltageGenerator(UI ui, UINode input, UINode output) : base(ui, input, output)
        {
            float radius = ui.cellSize;
            associatedElement = new VoltageGenerator(ui.circuit, input.associatedNode, output.associatedNode);

            figure = new Figure(new List<Vector2>() {
                Vector2.Create(-radius,0), //contact 0
                Vector2.Create(radius,0), //contact 1
                Vector2.Create(-(radius/4)*3,0), //minus left
                Vector2.Create(-(radius/4),0), //minus right
                
                Vector2.Create((radius/4)*3,0), //plus left
                Vector2.Create((radius/4),0), //plus right
                Vector2.Create((radius/4)*2f,radius/4), //plus up
                Vector2.Create((radius/4)*2f,-radius/4), //plus down
            });
            figure.SetAreaRadius(radius);
        }

        protected override void DrawFigure(Graphics graphics, Pen p)
        {
            float radius = figure.bounds.radius;
            graphics.DrawEllipse(p, new RectangleF(figure.position.x - radius,
                                                        figure.position.y - radius,
                                                        radius * 2, radius * 2));
            graphics.DrawLine(p, figure[2].pointF, figure[3].pointF);
            graphics.DrawLine(p, figure[4].pointF, figure[5].pointF);
            graphics.DrawLine(p, figure[6].pointF, figure[7].pointF);
        }
    }
}
