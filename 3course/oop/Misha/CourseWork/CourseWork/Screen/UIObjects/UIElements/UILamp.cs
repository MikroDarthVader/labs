﻿using ElectricCircuit.Elements;
using Libraries;
using System.Collections.Generic;
using System.Drawing;
using System;

namespace Screen.UIObjects.UIElements
{
    public class UILamp : UIElement
    {
        public override string paramName => "Сопротивление";
        public override float paramValue
        {
            get => associatedElement.resistance;
            set
            {
                if (value < 0)
                    throw new ArgumentException("Сопротивление не может быть меньше нуля");

                ((Resistor)associatedElement).SetResistance(value);
            }
        }

        public UILamp(UI ui, UINode input, UINode output) : base(ui, input, output)
        {
            associatedElement = new Lamp(ui.circuit, input.associatedNode, output.associatedNode);

            float radius = ui.cellSize;
            figure = new Figure(new List<Vector2>() {
                Vector2.Create(-radius,0), //contact 0
                Vector2.Create(radius,0), //contact 1
                
                Vector2.Create((radius/4)*3,0).Rotate((float)Math.PI/4), //cross_right_up
                Vector2.Create((radius/4)*3,0).Rotate((float)((Math.PI/2) + Math.PI/4)), //cross_left_up
                Vector2.Create((radius/4)*3,0).Rotate(-(float)Math.PI/4), //cross_right_down
                Vector2.Create((radius/4)*3,0).Rotate(-(float)((Math.PI/2) + Math.PI/4)), //cross_left_down
            });
            figure.SetAreaRadius(radius);
        }

        protected override void DrawFigure(Graphics graphics, Pen p)
        {
            float radius = figure.bounds.radius;
            graphics.DrawEllipse(p, new RectangleF(figure.position.x - radius,
                                                        figure.position.y - radius,
                                                        radius * 2, radius * 2));
            graphics.DrawLine(p, figure[2].pointF, figure[5].pointF);
            graphics.DrawLine(p, figure[3].pointF, figure[4].pointF);
        }
    }
}
