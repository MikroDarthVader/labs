﻿using ElectricCircuit.Elements;
using Libraries;
using System.Collections.Generic;
using System.Drawing;
using System;

namespace Screen.UIObjects.UIElements
{
    public class UISwitch : UIElement
    {
        public override string paramName => "Состояние";
        public override float paramValue
        {
            get => ((Switch)associatedElement).closed ? 1 : -1;
            set => ((Switch)associatedElement).closed = value > 0;
        }

        public UISwitch(UI ui, UINode input, UINode output) : base(ui, input, output)
        {
            float length = ui.cellSize * 2;
            associatedElement = new Switch(ui.circuit, input.associatedNode, output.associatedNode);

            figure = new Figure(new List<Vector2>() {
                Vector2.Create(-length/2,0), //contact 0
                Vector2.Create(length/2,0), //contact 1
                Vector2.Create(length,0).Rotate(-(float)Math.PI/6) - Vector2.Create(length/2,0), //open state
            });
            figure.SetAreaRadius(length / 2);
        }

        protected override void DrawFigure(Graphics graphics, Pen p)
        {
            graphics.DrawLine(p, figure[0].pointF, ((Switch)associatedElement).closed ? figure[1].pointF : figure[2].pointF);
        }
    }
}
