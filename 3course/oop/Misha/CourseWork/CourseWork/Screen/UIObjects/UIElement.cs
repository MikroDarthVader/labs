﻿using ElectricCircuit;
using Libraries;
using System.Collections.Generic;
using System.Drawing;
using System;

namespace Screen.UIObjects
{
    public abstract class UIElement : UIObject
    {
        //0 and 1st points of figure are reserved for contacts
        public BaseElement associatedElement;

        private bool _alive = true;
        public override bool alive
        {
            get => _alive;
            set
            {
                if (!_alive)
                    throw new NotImplementedException();

                _alive = value;

                if (!_alive)
                    associatedElement.Destroy();
            }
        }

        public float drawShift;

        public UIElement(UI ui, UINode input, UINode output) : base(ui) { }

        protected abstract void DrawFigure(Graphics g, Pen p);

        public abstract string paramName { get; }
        public abstract float paramValue { get; set; }

        public void GetCalculationResults(out string current, out string voltage, out string resistance)
        {
            var info = ui.circuit.GetElementInfo(associatedElement);

            resistance = associatedElement.resistance.ToString();
            current = float.IsNaN(info.current) ? "Не вычислен" : info.current.ToString();
            voltage = float.IsNaN(info.voltage) ? "Не вычислен" : info.voltage.ToString();
        }

        public override void Draw(Graphics graphics, bool selected)
        {
            var inputPos = FindUINode(ui.UIObjects, associatedElement.input).figure.position;
            var outputPos = FindUINode(ui.UIObjects, associatedElement.output).figure.position;

            Vector2 shift = (outputPos - inputPos).normalized * drawShift;
            MyMath.Swap(ref shift.x, ref shift.y); //...
            shift.y = -shift.y; //rotating shift on angle90;

            figure.angle = (outputPos - inputPos).angle;
            figure.position = (outputPos + inputPos) / 2 + shift;
            DrawFigure(graphics, new Pen(Color.White, selected ? 3 : 1));

            if (drawShift != 0)
            {
                graphics.DrawLine(Pens.White, inputPos.pointF, (inputPos + shift).pointF);
                graphics.DrawLine(Pens.White, outputPos.pointF, (outputPos + shift).pointF);
            }
            graphics.DrawLine(Pens.White, (inputPos + shift).pointF, figure[0].pointF);
            graphics.DrawLine(Pens.White, (outputPos + shift).pointF, figure[1].pointF);
        }

        UINode FindUINode(List<UIObject> UIObjects, Node node)
        {
            return (UINode)UIObjects.Find(element => element is UINode && ((UINode)element).associatedNode == node);
        }

        public override void Move(Vector2 mousePos)
        {
            var p1 = FindUINode(ui.UIObjects, associatedElement.input).figure.position;
            var p2 = FindUINode(ui.UIObjects, associatedElement.output).figure.position;

            drawShift = ((p2.y - p1.y) * mousePos.x - (p2.x - p1.x) * mousePos.y + p2.x * p1.y - p2.y * p1.x) /
                (p1 - p2).magnitude;//dist between line(p1,p2) and point(mousePos)

            drawShift = MyMath.Round(drawShift, ui.cellSize);

            Vector2 shift = (p2 - p1).normalized * drawShift;
            MyMath.Swap(ref shift.x, ref shift.y); //...
            shift.y = -shift.y; //rotating shift on angle90;

            figure.angle = (p2 - p1).angle;
            figure.position = (p2 + p1) / 2 + shift;
        }

        public override void OnDestroy() => associatedElement.Destroy();
    }
}
