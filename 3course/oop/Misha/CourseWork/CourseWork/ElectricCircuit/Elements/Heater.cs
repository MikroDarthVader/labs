﻿namespace ElectricCircuit.Elements
{
    class Heater : Resistor
    {
        public Heater(Circuit circuit, Node input, Node output) : base(circuit, input, output) { }
    }
}
