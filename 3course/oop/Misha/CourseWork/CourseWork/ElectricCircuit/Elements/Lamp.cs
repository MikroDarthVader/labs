﻿namespace ElectricCircuit.Elements
{
    class Lamp : Resistor
    {
        public Lamp(Circuit circuit, Node input, Node output) : base(circuit, input, output) { }
    }
}
