﻿namespace ElectricCircuit.Elements
{
    class Switch : BaseElement
    {
        private bool _closed;
        public bool closed { get => _closed; set { _closed = value; circuit.SetModified(); } }
        public override float resistance { get => closed ? 0 : float.PositiveInfinity; }

        public Switch(Circuit circuit, Node input, Node output) : base(circuit, input, output) { }
    }
}
