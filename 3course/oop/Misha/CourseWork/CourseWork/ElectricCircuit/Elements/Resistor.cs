﻿namespace ElectricCircuit.Elements
{
    class Resistor : BaseElement
    {
        public float _resistance = 0;
        public override float resistance { get => _resistance; }

        public Resistor(Circuit circuit, Node input, Node output) : base(circuit, input, output) { }
        public void SetResistance(float resistance) { _resistance = resistance; circuit.SetModified(); }
    }
}
