﻿namespace ElectricCircuit.Elements
{
    public enum Elements
    {
        Источник,
        Ключ,
        Резистор,
        Лампа,
        Нагреватель
    }
}
