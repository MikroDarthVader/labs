﻿namespace ElectricCircuit.Elements
{
    class VoltageGenerator : BaseElement
    {
        private float _voltage;
        public float voltage { get => _voltage; set { _voltage = value; circuit.SetModified(); } }
        public override float resistance { get => 0; }

        public VoltageGenerator(Circuit circuit, Node input, Node output) : base(circuit, input, output) { }
    }
}
