﻿using ElectricCircuit.Elements;
using Libraries;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ElectricCircuit
{
    public class Circuit
    {
        /*data structures*/
        public class SyncException : Exception { }
        public class CalculateException : Exception
        {
            public CalculateException() : base(
                "В цепи присутствуют короткие замыкания или параллельно подключенные источники питания. Она не может быть расчитана")
            { }
        }

        public struct ElementInfo
        {
            public static ElementInfo NotCalculated = Create(float.NaN, float.NaN);

            public float current;
            public float voltage;

            public static ElementInfo Create(float current, float voltage)
            {
                ElementInfo res;
                res.current = current;
                res.voltage = voltage;
                return res;
            }
        }
        /*data structures*/

        private readonly List<BaseElement> elements;
        private Dictionary<BaseElement, ElementInfo> calculationResult;

        public Circuit()
        {
            elements = new List<BaseElement>();
        }

        public void AddElement(BaseElement element)
        {
            SetModified();
            elements.Add(element);
        }

        public void RemoveElement(BaseElement element)
        {
            SetModified();
            elements.Remove(element);
        }

        public void SetModified()
        {
            calculationResult = null;
        }

        public ElementInfo GetElementInfo(BaseElement element)
        {
            if (calculationResult == null || !calculationResult.ContainsKey(element))
            {
                if (element is VoltageGenerator)
                    return ElementInfo.Create(float.NaN, ((VoltageGenerator)element).voltage);
                return ElementInfo.NotCalculated;
            }

            return calculationResult[element];
        }

        public void Calculate()
        {
            List<List<BaseElement>> cycles = new List<List<BaseElement>>();
            calculationResult = new Dictionary<BaseElement, ElementInfo>();

            foreach (var element in elements)
            {
                if (cycles.Exists(cycle => cycle.Exists(x => x == element)))
                    continue;

                if (float.IsInfinity(element.resistance))
                {
                    calculationResult.Add(element, ElementInfo.Create(0, float.NaN));
                    continue;
                }

                if (FindCicle(element, out var cycleOrNot))
                    cycles.Add(cycleOrNot);
                else
                {
                    float voltage = element is VoltageGenerator ? ((VoltageGenerator)element).voltage : 0;
                    calculationResult.Add(cycleOrNot[0], ElementInfo.Create(0, voltage));
                }
            }

            List<BaseElement> calculatingElements = new List<BaseElement>();
            foreach (var cycle in cycles)
                foreach (var element in cycle)
                    if (!calculatingElements.Contains(element))
                        calculatingElements.Add(element);

            LinearSystemSolver solver = new LinearSystemSolver(calculatingElements.Count);
            int currenEquation = 0;

            foreach (var cycle in cycles)
            {
                var pervElement = cycle.Last();
                foreach (var element in cycle)
                {
                    bool inversed = element.isInput(pervElement.input);
                    int sign = inversed ? -1 : 1;

                    if (element is VoltageGenerator)
                        solver[currenEquation].FreeMember += ((VoltageGenerator)element).voltage * sign;
                    else
                        solver[currenEquation][calculatingElements.IndexOf(element)] = element.resistance;
                    pervElement = element;
                }
                currenEquation++;
            }

            List<Node> calculatingNodes = new List<Node>();
            foreach (var cycle in cycles)
            {
                bool firstElement = true;
                foreach (var element in cycle)
                {
                    if (firstElement && !calculatingNodes.Contains(element.output))
                        calculatingNodes.Add(element.output);

                    if (!calculatingNodes.Contains(element.input))
                        calculatingNodes.Add(element.input);

                    firstElement = false;
                }
            }

            for (int currentNode = 0; currenEquation < solver.equationsCount; currenEquation++, currentNode++)
            {
                foreach (var element in calculatingNodes[currentNode].ConnectedElements)
                {
                    if (!calculatingElements.Contains(element) || float.IsInfinity(element.resistance))
                        continue;

                    bool isInput = element.isInput(calculatingNodes[currentNode]);
                    int sign = isInput ? -1 : 1;

                    solver[currenEquation][calculatingElements.IndexOf(element)] = sign;
                }
            }

            float[] calculatedCurrents;
            try { calculatedCurrents = solver.Solve(); }
            catch (LinearSystemSolver.SystemUndefinedException) { throw new CalculateException(); }

            for (int i = 0; i < calculatedCurrents.Length; i++)
            {
                float voltage;
                if (calculatingElements[i] is VoltageGenerator)
                    voltage = ((VoltageGenerator)calculatingElements[i]).voltage;
                else
                    voltage = calculatedCurrents[i] * calculatingElements[i].resistance;
                calculationResult.Add(calculatingElements[i], ElementInfo.Create(Math.Abs(calculatedCurrents[i]),
                                                                                    Math.Abs(voltage)));
            }
        }

        /// <summary>
        /// returns false if out elements are out of cicle
        /// </summary>
        /// <param name="element"></param>
        /// <param name="cicle"></param>
        /// <returns></returns>
        private bool FindCicle(BaseElement first, out List<BaseElement> cycle)
        {
            return FindCicleIter(first, out cycle, null, first.input, first);
        }

        private bool FindCicleIter(BaseElement iter, out List<BaseElement> cycle, List<BaseElement> passed, Node from, BaseElement start)
        {
            Node next = iter.isInput(from) ? iter.output : iter.input;
            passed = passed == null ? new List<BaseElement>() { iter } : new List<BaseElement>(passed) { iter };

            if (iter != start && next == start.input)
            {
                cycle = new List<BaseElement>() { iter };
                return true;
            }

            int minCycleLength = int.MaxValue;
            bool cycled = false;
            cycle = null;

            foreach (var nextIter in next.ConnectedElements)
            {
                if (passed.Contains(nextIter) || float.IsInfinity(nextIter.resistance))
                    continue;

                if (FindCicleIter(nextIter, out List<BaseElement> tmp, passed, next, start))
                {
                    cycled = true;
                    if (tmp.Count < minCycleLength)
                    {
                        minCycleLength = tmp.Count;
                        cycle = tmp;
                    }
                }
            }

            if (cycle == null)
                cycle = new List<BaseElement>() { iter };
            else
                cycle.Add(iter);

            return cycled;
        }

        public static bool CapableToConnect(Node one, Node other)
        {
            if (one == other)
                return false;

            if (one.ConnectedElements.Exists(x => other.ConnectedElements.Contains(x)))
                return false;

            return true;
        }
    }
}
