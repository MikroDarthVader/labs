﻿using Libraries;
using Screen;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace ElectricCircuit
{
    public class Node
    {
        public List<BaseElement> ConnectedElements;

        public Node()
        {
            ConnectedElements = new List<BaseElement>();
        }

        public void Replace(Node other)
        {
            ConnectedElements.AddRange(other.ConnectedElements);
            foreach (var element in other.ConnectedElements)
            {
                if (element.isOutput(other))
                    element.SetOutput(this);
                else if (element.isInput(other))
                    element.SetInput(this);
                else
                    throw new Circuit.SyncException();
            }
        }
    }
}
