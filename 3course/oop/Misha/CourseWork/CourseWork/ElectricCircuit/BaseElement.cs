﻿namespace ElectricCircuit
{
    public abstract class BaseElement
    {
        public abstract float resistance { get; }
        public Node input { get; private set; }
        public Node output { get; private set; }
        public Circuit circuit;

        public BaseElement(Circuit circuit, Node input, Node output)
        {
            this.circuit = circuit;
            circuit.AddElement(this);

            this.input = input;
            this.output = output;
            input.ConnectedElements.Add(this);
            output.ConnectedElements.Add(this);
        }

        public void Destroy()
        {
            circuit.RemoveElement(this);
            input.ConnectedElements.Remove(this);
            output.ConnectedElements.Remove(this);
        }

        public bool isInput(Node node) => node == input;
        public bool isOutput(Node node) => node == output;
        public void SetInput(Node node) { circuit.SetModified(); input = node; }
        public void SetOutput(Node node) { circuit.SetModified(); output = node; }
    }
}
