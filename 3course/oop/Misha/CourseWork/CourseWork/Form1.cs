﻿using ElectricCircuit.Elements;
using Libraries;
using Screen;
using System;
using System.IO;
using System.Windows.Forms;

namespace CourseWork
{
    public partial class Form1 : Form
    {
        UI ui;

        public Form1()
        {
            InitializeComponent();
            ui = new UI(pictureBox1.Invalidate, 20);
            comboBox_creatingElement.DataSource = Enum.GetValues(typeof(Elements));
        }

        private void pictureBox1_SizeChanged(object sender, EventArgs e) => ui.ClearDrawCache();

        private void showExceptionDialog(Exception e)
        {
            MessageBox.Show(e.Message, "Исключение", MessageBoxButtons.OK);
        }

        /*buttons*/
        private void button_createElement_Click(object sender, EventArgs e)
        {
            Enum.TryParse(comboBox_creatingElement.Text, out Elements toCreate);
            ui.CreateElement(toCreate);
        }

        private void DeselectAll() => pictureBox1_MouseDoubleClick(null, null);
        private void button_setParam_Click(object sender, EventArgs e)
        {
            try { ui.SetParamOfSelected(float.Parse(textBox_paramValue.Text)); }
            catch (ArgumentException exc) { showExceptionDialog(exc); }
            catch (FormatException exc) { showExceptionDialog(exc); }
            DeselectAll();
        }

        private void button_deleteElement_Click(object sender, EventArgs e)
        {
            ui.DestroySelected();
            DeselectAll();
        }

        private void button_calculate_Click(object sender, EventArgs e)
        {
            try { ui.CalcuclateCircuit(); }
            catch (ElectricCircuit.Circuit.CalculateException exc) { showExceptionDialog(exc); }
            DeselectAll();
        }

        private void button_saveCircuit_Click(object sender, EventArgs e)
        {
            try { File.WriteAllText(Application.StartupPath + "/" + textBox_fileName.Text + ".cct", ui.ToFile()); }
            catch (IOException exc) { showExceptionDialog(exc); }
        }

        private void button_loadCircuit_Click(object sender, EventArgs e)
        {
            string fileText;
            try { fileText = File.ReadAllText(Application.StartupPath + "/" + textBox_fileName.Text + ".cct"); }
            catch (IOException) { showExceptionDialog(new UI.ReadBrokenFileException()); return; }

            try { ui = new UI(pictureBox1.Invalidate, ui.cellSize, fileText); ui.drawRequest(); }
            catch (UI.ReadBrokenFileException exc) { showExceptionDialog(exc); }
        }
        /*buttons*/

        /*mouse actions*/
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e) => ui.MouseDown(Vector2.Create(e.X, e.Y));
        private void pictureBox1_MouseUp(object sender, MouseEventArgs e) => ui.MouseUp();
        private void pictureBox1_MouseMove(object sender, MouseEventArgs e) => ui.MoveElement(Vector2.Create(e.X, e.Y),
                                                                                                Vector2.Create(pictureBox1.Size));
        private void pictureBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {

            Vector2 mousePos = e == null ? Vector2.Infinity : Vector2.Create(e.X, e.Y);
            ui.SelectElement(mousePos, out UI.SelectedInfo selectedInfo);

            textBox_paramValue.Text = selectedInfo.paramValue;
            label_paramName.Text = selectedInfo.paramName;
            button_setParam.Text = "Установить " + selectedInfo.paramName.ToLower();

            textBox_selectedCurrent.Text = selectedInfo.current;
            textBox_selectedVoltage.Text = selectedInfo.voltage;
            textBox_selectedResistance.Text = selectedInfo.resistance;
        }
        /*mouse actions*/

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            ui.Draw(e.Graphics, Vector2.Create(pictureBox1.Size));
        }
    }
}
