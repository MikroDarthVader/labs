﻿namespace CourseWork
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.comboBox_creatingElement = new System.Windows.Forms.ComboBox();
            this.button_createElement = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_deleteElement = new System.Windows.Forms.Button();
            this.button_setParam = new System.Windows.Forms.Button();
            this.textBox_paramValue = new System.Windows.Forms.TextBox();
            this.label_paramName = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button_calculate = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_selectedResistance = new System.Windows.Forms.TextBox();
            this.textBox_selectedVoltage = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_selectedCurrent = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button_saveCircuit = new System.Windows.Forms.Button();
            this.button_loadCircuit = new System.Windows.Forms.Button();
            this.textBox_fileName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox1.Location = new System.Drawing.Point(324, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(805, 575);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.SizeChanged += new System.EventHandler(this.pictureBox1_SizeChanged);
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBox1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDoubleClick);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // comboBox_creatingElement
            // 
            this.comboBox_creatingElement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_creatingElement.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.comboBox_creatingElement.FormattingEnabled = true;
            this.comboBox_creatingElement.Items.AddRange(new object[] {
            "Источник тока"});
            this.comboBox_creatingElement.Location = new System.Drawing.Point(7, 28);
            this.comboBox_creatingElement.Name = "comboBox_creatingElement";
            this.comboBox_creatingElement.Size = new System.Drawing.Size(293, 26);
            this.comboBox_creatingElement.TabIndex = 1;
            // 
            // button_createElement
            // 
            this.button_createElement.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_createElement.ForeColor = System.Drawing.Color.Black;
            this.button_createElement.Location = new System.Drawing.Point(7, 60);
            this.button_createElement.Name = "button_createElement";
            this.button_createElement.Size = new System.Drawing.Size(293, 32);
            this.button_createElement.TabIndex = 3;
            this.button_createElement.Text = "Создать";
            this.button_createElement.UseVisualStyleBackColor = true;
            this.button_createElement.Click += new System.EventHandler(this.button_createElement_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button_deleteElement);
            this.groupBox1.Controls.Add(this.button_setParam);
            this.groupBox1.Controls.Add(this.textBox_paramValue);
            this.groupBox1.Controls.Add(this.label_paramName);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(12, 116);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(306, 134);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Настройки элемента";
            // 
            // button_deleteElement
            // 
            this.button_deleteElement.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_deleteElement.ForeColor = System.Drawing.Color.Black;
            this.button_deleteElement.Location = new System.Drawing.Point(7, 96);
            this.button_deleteElement.Name = "button_deleteElement";
            this.button_deleteElement.Size = new System.Drawing.Size(293, 32);
            this.button_deleteElement.TabIndex = 3;
            this.button_deleteElement.Text = "Удалить элемент";
            this.button_deleteElement.UseVisualStyleBackColor = true;
            this.button_deleteElement.Click += new System.EventHandler(this.button_deleteElement_Click);
            // 
            // button_setParam
            // 
            this.button_setParam.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_setParam.ForeColor = System.Drawing.Color.Black;
            this.button_setParam.Location = new System.Drawing.Point(7, 58);
            this.button_setParam.Name = "button_setParam";
            this.button_setParam.Size = new System.Drawing.Size(293, 32);
            this.button_setParam.TabIndex = 3;
            this.button_setParam.Text = "Установить параметр";
            this.button_setParam.UseVisualStyleBackColor = true;
            this.button_setParam.Click += new System.EventHandler(this.button_setParam_Click);
            // 
            // textBox_paramValue
            // 
            this.textBox_paramValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.textBox_paramValue.Location = new System.Drawing.Point(136, 28);
            this.textBox_paramValue.Name = "textBox_paramValue";
            this.textBox_paramValue.Size = new System.Drawing.Size(164, 24);
            this.textBox_paramValue.TabIndex = 1;
            // 
            // label_paramName
            // 
            this.label_paramName.AutoSize = true;
            this.label_paramName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label_paramName.Location = new System.Drawing.Point(6, 31);
            this.label_paramName.Name = "label_paramName";
            this.label_paramName.Size = new System.Drawing.Size(77, 18);
            this.label_paramName.TabIndex = 0;
            this.label_paramName.Text = "Параметр";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button_createElement);
            this.groupBox2.Controls.Add(this.comboBox_creatingElement);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(306, 98);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Создание элемента";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button_calculate);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(12, 256);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(306, 190);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Цепь";
            // 
            // button_calculate
            // 
            this.button_calculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_calculate.ForeColor = System.Drawing.Color.Black;
            this.button_calculate.Location = new System.Drawing.Point(7, 28);
            this.button_calculate.Name = "button_calculate";
            this.button_calculate.Size = new System.Drawing.Size(293, 32);
            this.button_calculate.TabIndex = 3;
            this.button_calculate.Text = "Рассчитать ";
            this.button_calculate.UseVisualStyleBackColor = true;
            this.button_calculate.Click += new System.EventHandler(this.button_calculate_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.textBox_selectedResistance);
            this.groupBox4.Controls.Add(this.textBox_selectedVoltage);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.textBox_selectedCurrent);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F);
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(19, 322);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(293, 118);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Выделенный элемент";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label4.Location = new System.Drawing.Point(6, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 18);
            this.label4.TabIndex = 0;
            this.label4.Text = "Сопротивление";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label3.Location = new System.Drawing.Point(6, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 18);
            this.label3.TabIndex = 0;
            this.label3.Text = "Напряжение";
            // 
            // textBox_selectedResistance
            // 
            this.textBox_selectedResistance.Enabled = false;
            this.textBox_selectedResistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.textBox_selectedResistance.Location = new System.Drawing.Point(129, 88);
            this.textBox_selectedResistance.Name = "textBox_selectedResistance";
            this.textBox_selectedResistance.ReadOnly = true;
            this.textBox_selectedResistance.Size = new System.Drawing.Size(158, 24);
            this.textBox_selectedResistance.TabIndex = 1;
            // 
            // textBox_selectedVoltage
            // 
            this.textBox_selectedVoltage.Enabled = false;
            this.textBox_selectedVoltage.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.textBox_selectedVoltage.Location = new System.Drawing.Point(129, 58);
            this.textBox_selectedVoltage.Name = "textBox_selectedVoltage";
            this.textBox_selectedVoltage.ReadOnly = true;
            this.textBox_selectedVoltage.Size = new System.Drawing.Size(158, 24);
            this.textBox_selectedVoltage.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label2.Location = new System.Drawing.Point(6, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "Ток";
            // 
            // textBox_selectedCurrent
            // 
            this.textBox_selectedCurrent.Enabled = false;
            this.textBox_selectedCurrent.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.textBox_selectedCurrent.Location = new System.Drawing.Point(129, 28);
            this.textBox_selectedCurrent.Name = "textBox_selectedCurrent";
            this.textBox_selectedCurrent.ReadOnly = true;
            this.textBox_selectedCurrent.Size = new System.Drawing.Size(158, 24);
            this.textBox_selectedCurrent.TabIndex = 1;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button_saveCircuit);
            this.groupBox5.Controls.Add(this.button_loadCircuit);
            this.groupBox5.Controls.Add(this.textBox_fileName);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.groupBox5.ForeColor = System.Drawing.Color.White;
            this.groupBox5.Location = new System.Drawing.Point(12, 452);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(306, 134);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Файл";
            // 
            // button_saveCircuit
            // 
            this.button_saveCircuit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_saveCircuit.ForeColor = System.Drawing.Color.Black;
            this.button_saveCircuit.Location = new System.Drawing.Point(7, 96);
            this.button_saveCircuit.Name = "button_saveCircuit";
            this.button_saveCircuit.Size = new System.Drawing.Size(293, 32);
            this.button_saveCircuit.TabIndex = 3;
            this.button_saveCircuit.Text = "Сохранить цепь";
            this.button_saveCircuit.UseVisualStyleBackColor = true;
            this.button_saveCircuit.Click += new System.EventHandler(this.button_saveCircuit_Click);
            // 
            // button_loadCircuit
            // 
            this.button_loadCircuit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_loadCircuit.ForeColor = System.Drawing.Color.Black;
            this.button_loadCircuit.Location = new System.Drawing.Point(7, 58);
            this.button_loadCircuit.Name = "button_loadCircuit";
            this.button_loadCircuit.Size = new System.Drawing.Size(293, 32);
            this.button_loadCircuit.TabIndex = 3;
            this.button_loadCircuit.Text = "Загрузить цепь";
            this.button_loadCircuit.UseVisualStyleBackColor = true;
            this.button_loadCircuit.Click += new System.EventHandler(this.button_loadCircuit_Click);
            // 
            // textBox_fileName
            // 
            this.textBox_fileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.textBox_fileName.Location = new System.Drawing.Point(136, 28);
            this.textBox_fileName.Name = "textBox_fileName";
            this.textBox_fileName.Size = new System.Drawing.Size(164, 24);
            this.textBox_fileName.TabIndex = 1;
            this.textBox_fileName.Text = "Цепь_1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Имя файла";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(45)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(1141, 597);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox comboBox_creatingElement;
        private System.Windows.Forms.Button button_createElement;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label_paramName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button_setParam;
        private System.Windows.Forms.TextBox textBox_paramValue;
        private System.Windows.Forms.Button button_deleteElement;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button_calculate;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_selectedVoltage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_selectedCurrent;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_selectedResistance;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button_saveCircuit;
        private System.Windows.Forms.Button button_loadCircuit;
        private System.Windows.Forms.TextBox textBox_fileName;
        private System.Windows.Forms.Label label1;
    }
}

