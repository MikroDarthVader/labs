﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Part3
{
    public class OrientedGraphNode<T>
    {
        public T value;

        public Dictionary<OrientedGraphNode<T>, float> prevNodes;
        public Dictionary<OrientedGraphNode<T>, float> nextNodes;

        public OrientedGraphNode(T value)
        {
            this.value = value;

            prevNodes = new Dictionary<OrientedGraphNode<T>, float>();
            nextNodes = new Dictionary<OrientedGraphNode<T>, float>();
        }

        public override string ToString()
        {
            return value.ToString();
        }
    }

    public class OrientedGraph<T>
    {
        private List<OrientedGraphNode<T>> Nodes;

        private readonly Random rnd;

        public OrientedGraph()
        {
            Nodes = new List<OrientedGraphNode<T>>();

            rnd = new Random();
        }

        public void AddNode(T value)
        {
            if (Nodes.Exists(node => node.value.Equals(value)))
                throw new ArgumentException("Node already exist");

            Nodes.Add(new OrientedGraphNode<T>(value));
        }

        public void DeleteNode(T value)
        {
            OrientedGraphNode<T> nodeToDelete = Nodes.Find(node => node.value.Equals(value));

            if (nodeToDelete == null)
                throw new ArgumentException("Node doesn't exist");

            foreach (OrientedGraphNode<T> node in nodeToDelete.prevNodes.Keys)
                node.nextNodes.Remove(nodeToDelete);
            foreach (OrientedGraphNode<T> node in nodeToDelete.nextNodes.Keys)
                node.prevNodes.Remove(nodeToDelete);

            Nodes.Remove(nodeToDelete);
        }

        public void AddEdge(T fromValue, T toValue, float edgeValue)
        {
            OrientedGraphNode<T> from = Nodes.Find(node => node.value.Equals(fromValue));
            OrientedGraphNode<T> to = Nodes.Find(node => node.value.Equals(toValue));

            if (from == null)
            {
                from = new OrientedGraphNode<T>(fromValue);
                Nodes.Add(from);
            }
            if (to == null)
            {
                to = new OrientedGraphNode<T>(toValue);
                Nodes.Add(to);
            }

            if (from.nextNodes.ContainsKey(to))
                throw new ArgumentException("Edge already exist");

            from.nextNodes.Add(to, edgeValue);
            to.prevNodes.Add(from, edgeValue);
        }

        public void DeleteEdge(T fromValue, T toValue)
        {
            OrientedGraphNode<T> from = Nodes.Find(node => node.value.Equals(fromValue));
            OrientedGraphNode<T> to = Nodes.Find(node => node.value.Equals(toValue));

            if (from == null)
                throw new ArgumentException("Node with value == fromValue doesn't exist");
            if (to == null)
                throw new ArgumentException("Node with value == toValue doesn't exist");

            if (!from.nextNodes.ContainsKey(to))
                throw new ArgumentException("Edge doesn't exist");

            from.nextNodes.Remove(to);
            to.prevNodes.Remove(from);
        }

        public bool TryFindHamiltonianCycle(out List<T> nodes, out List<float> edges)
        {
            nodes = new List<T>();
            edges = new List<float>();

            float initTemperature = 10;
            float endTemperature = 0.00001f;

            List<OrientedGraphNode<T>> currCycle = GetRandomCycle();
            float currEnergy = CalculateEnergy(currCycle);

            float temperature = initTemperature;

            for (int i = 0;; i++)
            {
                List<OrientedGraphNode<T>> cycleCandidate = GenerateCandidate(currCycle);
                float candidateEnergy = CalculateEnergy(cycleCandidate);

                if (candidateEnergy < currEnergy)
                {
                    currEnergy = candidateEnergy;
                    currCycle = cycleCandidate;
                }
                else
                {
                    float p = GetTransitionProbability(candidateEnergy - currEnergy, temperature);

                    if (IsTransition(p))
                    {
                        currEnergy = candidateEnergy;
                        currCycle = cycleCandidate;
                    }
                }

                temperature = DecreaseTemperature(initTemperature, i);

                if (temperature <= endTemperature)
                    break;
            }

            if (currEnergy == float.PositiveInfinity)
                return false;
            else
            {
                for (int i = 0; i < currCycle.Count - 1; i++)
                {
                    nodes.Add(currCycle[i].value);
                    edges.Add(currCycle[i].nextNodes[currCycle[i + 1]]);
                }

                nodes.Add(currCycle[currCycle.Count - 1].value);
            }

            return true;
        }

        private List<OrientedGraphNode<T>> GetRandomCycle()
        {
            List<OrientedGraphNode<T>> result = new List<OrientedGraphNode<T>>();

            for (int i = Nodes.Count; i > 0; i--)
            {
                int number = rnd.Next(0, i);

                foreach (OrientedGraphNode<T> node in Nodes)
                {
                    if (result.Contains(node))
                        continue;

                    if (number == 0)
                    {
                        result.Add(node);
                        break;
                    }

                    number--;
                }
            }

            result.Add(result[0]);

            return result;
        }

        private List<OrientedGraphNode<T>> GenerateCandidate(List<OrientedGraphNode<T>> cycle)
        {
            List<OrientedGraphNode<T>> result = new List<OrientedGraphNode<T>>(cycle);

            int firstNum = rnd.Next(0, cycle.Count - 1);
            int secondNum = rnd.Next(0, cycle.Count - 1);

            OrientedGraphNode<T> temp = result[firstNum];
            result[firstNum] = result[secondNum];
            result[secondNum] = temp;

            result[result.Count - 1] = result[0];

            return result;
        }

        private float CalculateEnergy(List<OrientedGraphNode<T>> cycle)
        {
            float result = 0;

            for (int i = 0; i < cycle.Count - 1; i++)
                if (cycle[i].nextNodes.TryGetValue(cycle[i + 1], out float length))
                    result += length;
                else
                    return float.PositiveInfinity;

            return result;
        }

        private float DecreaseTemperature(float startTemperature, int i)
        {
            return startTemperature * 0.1f / i;
        }

        private float GetTransitionProbability(float deltaEnergy, float temperature)
        {
            return (float)Math.Exp(-deltaEnergy / temperature);
        }

        private bool IsTransition(float probability)
        {
            float value = (float)rnd.NextDouble();

            if (value <= probability)
                return true;
            else
                return false;
        }
    }
}
