﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Part3
{
    class Program
    {
        static void Main()
        {
            OrientedGraph<char> graph = new OrientedGraph<char>();
            FillGraph(graph);

            for (int j = 0; j < 10; j++)
            {
                Console.Write("Attempt ");
                Console.Write(j + 1);
                Console.Write(": ");
                if (graph.TryFindHamiltonianCycle(out List<char> nodes, out List<float> edges))
                {
                    Console.Write("result (sum = ");
                    Console.Write(edges.Sum());
                    Console.Write("): ");
                    for (int i = 0; i < nodes.Count - 1; i++)
                        Console.Write(nodes[i] + "-");
                    Console.WriteLine(nodes.Last());
                }
                else
                {
                    Console.WriteLine("Hamilton cycle not found");
                }
            }
        }

        static void FillGraph(OrientedGraph<char> graph)
        {
            graph.AddEdge('a', 'b', 9);
            graph.AddEdge('a', 'g', 6);
            graph.AddEdge('a', 'f', 5);

            graph.AddEdge('b', 'a', 7);
            graph.AddEdge('b', 'g', 7);
            graph.AddEdge('b', 'c', 5);

            graph.AddEdge('c', 'b', 5);
            graph.AddEdge('c', 'd', 4);

            graph.AddEdge('d', 'c', 3);
            graph.AddEdge('d', 'g', 4);
            graph.AddEdge('d', 'f', 6);

            graph.AddEdge('f', 'd', 9);
            graph.AddEdge('f', 'g', 6);
            graph.AddEdge('f', 'a', 7);

            graph.AddEdge('g', 'a', 9);
            graph.AddEdge('g', 'b', 5);
            graph.AddEdge('g', 'c', 5);
            graph.AddEdge('g', 'd', 3);
        }
    }
}
