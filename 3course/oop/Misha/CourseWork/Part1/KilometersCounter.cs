﻿namespace Part1
{
    public class KilometersCounter : Counter
    {
        public KilometersCounter(int maxKilometers) : base(0, maxKilometers)
        { }
    }
}
