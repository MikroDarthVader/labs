﻿using System;

namespace Part1
{
    public class Counter
    {
        public int Value { get; private set; }
        private int MinValue;
        private int MaxValue;

        public Counter(int minValue, int maxValue)
        {
            if (minValue > maxValue)
                throw new ArgumentException();

            MinValue = minValue;
            MaxValue = maxValue;

            Value = MinValue;
        }

        public void AddOne() => Value = MinValue + (Value + 1 - MinValue) % (MaxValue + 1 - MinValue);
    }
}
