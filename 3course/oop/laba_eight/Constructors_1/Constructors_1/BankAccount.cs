
class BankAccount
{
    private long accNo;
    private decimal accBal;
    private AccountType accType;

    private static long nextNumber = 0;

    public BankAccount()
    {
        accNo = NextNumber();
        accBal = 0;
        accType = AccountType.Checking;
    }

    public BankAccount(AccountType aType)
    {
        accNo = NextNumber();
        accBal = 0;
        accType = aType;
    }

    public BankAccount(decimal aBal)
    {
        accNo = NextNumber();
        accBal = aBal;
        accType = AccountType.Checking;
    }

    public BankAccount(decimal aBal, AccountType aType)
    {
        accNo = NextNumber();
        accBal = aBal;
        accType = aType;
    }

    public bool Withdraw(decimal amount)
    {
        bool sufficientFunds = accBal >= amount;
        if (sufficientFunds)
        {
            accBal -= amount;
        }
        return sufficientFunds;
    }

    public decimal Deposit(decimal amount)
    {
        accBal += amount;
        return accBal;
    }

    public long Number()
    {
        return accNo;
    }

    public decimal Balance()
    {
        return accBal;
    }

    public string Type()
    {
        return accType.ToString();
    }

    public void TransferForm(BankAccount accForm, decimal amount)
    {
        if (accForm.Withdraw(amount))
            Deposit(amount);
    }

    private static long NextNumber()
    {
        return nextNumber++;
    }
}
