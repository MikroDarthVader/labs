using System;

public class Test
{
    public static void Main()
    {
        BankAccount acc1 = new BankAccount();

        acc1.Deposit(500);
        acc1.Withdraw(200);

        Write(acc1);
    }

    static void Write(BankAccount toWrite)
    {
        Console.WriteLine("Account number is {0}", toWrite.Number());
        Console.WriteLine("Account balance is {0}", toWrite.Balance());
        Console.WriteLine("Account type is {0}", toWrite.Type());

        if (toWrite.Transactions().Count != 0)
        {
            Console.WriteLine("Account transactions:");
            foreach (BankTransaction transaction in toWrite.Transactions())
                Console.WriteLine("Time: {0}, Amount: {1}", transaction.When(), transaction.Amount());
        }
    }
}
