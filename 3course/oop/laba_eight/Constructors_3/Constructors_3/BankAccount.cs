using System.Collections;

public class BankAccount
{
    private long accNo;
    private decimal accBal;
    private AccountType accType;

    private static long nextNumber = 0;

    private Queue tranQueue;

    public BankAccount()
    {
        accNo = NextNumber();
        accBal = 0;
        accType = AccountType.Checking;

        tranQueue = new Queue();
    }

    public BankAccount(AccountType aType)
    {
        accNo = NextNumber();
        accBal = 0;
        accType = aType;

        tranQueue = new Queue();
    }

    public BankAccount(decimal aBal)
    {
        accNo = NextNumber();
        accBal = aBal;
        accType = AccountType.Checking;

        tranQueue = new Queue();
    }

    public BankAccount(decimal aBal, AccountType aType)
    {
        accNo = NextNumber();
        accBal = aBal;
        accType = aType;

        tranQueue = new Queue();
    }

    public bool Withdraw(decimal amount)
    {
        tranQueue.Enqueue(new BankTransaction(amount));

        bool sufficientFunds = accBal >= amount;
        if (sufficientFunds)
        {
            accBal -= amount;
        }
        return sufficientFunds;
    }

    public decimal Deposit(decimal amount)
    {
        tranQueue.Enqueue(new BankTransaction(amount));

        accBal += amount;
        return accBal;
    }

    public long Number()
    {
        return accNo;
    }

    public decimal Balance()
    {
        return accBal;
    }

    public Queue Transactions()
    {
        return tranQueue;
    }

    public string Type()
    {
        return accType.ToString();
    }

    public void TransferForm(BankAccount accForm, decimal amount)
    {
        if (accForm.Withdraw(amount))
            Deposit(amount);
    }

    private static long NextNumber()
    {
        return nextNumber++;
    }
}
