using System;

public class Test
{
    public static void Main()
    {
        BankAccount acc1 = new BankAccount();
        BankAccount acc2 = new BankAccount(AccountType.Deposit);
        BankAccount acc3 = new BankAccount(100);
        BankAccount acc4 = new BankAccount(500, AccountType.Deposit);

        acc1.Deposit(200);
        acc2.TransferForm(acc4, 300);
        acc3.Withdraw(50);

        Write(acc1);
        Console.WriteLine();
        Write(acc2);
        Console.WriteLine();
        Write(acc3);
        Console.WriteLine();
        Write(acc4);
    }

    static void Write(BankAccount toWrite)
    {
        Console.WriteLine("Account number is {0}", toWrite.Number());
        Console.WriteLine("Account balance is {0}", toWrite.Balance());
        Console.WriteLine("Account type is {0}", toWrite.Type());

        if (toWrite.Transactions().Count != 0)
        {
            Console.WriteLine("Account transactions:");
            foreach (BankTransaction transaction in toWrite.Transactions())
                Console.WriteLine("Time: {0}, Amount: {1}", transaction.When(), transaction.Amount());
        }
    }
}
