﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Collections;
using Ctype = System.Char;

namespace ColorToken
{
    class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                InnerMain(args);
            }
            catch (System.Exception caught)
            {
                Console.WriteLine(caught);
            }
            Console.ReadKey();
        }

        public static void InnerMain(string[] args)
        {
            SourceFile source = new SourceFile(args[0]);

            HTMLTokenVisitor visitor = new HTMLTokenVisitor();
            source.Accept(visitor);
        }
    }

    public interface IToken
    {
        string ToString();
    }

    public interface ILineStartToken
    {
        int Number();
    }

    public interface ILineEndToken
    {
        int Number();
    }

    public interface ICommentToken : IToken { }
    public interface IDirectiveToken : IToken { }
    public interface IIdentifierToken : IToken { }
    public interface IKeywordToken : IToken { }
    public interface IWhiteSpaceToken : IToken { }
    public interface IOtherToken : IToken { }

    public interface ITokenVisitor
    {
        void Visit(ILineStartToken t);
        void Visit(ILineEndToken t);

        void Visit(ICommentToken t);
        void Visit(IDirectiveToken t);
        void Visit(IIdentifierToken t);
        void Visit(IKeywordToken t);
        void Visit(IWhiteSpaceToken t);

        void Visit(IOtherToken t);
    }

    public class NullTokenVisitor : ITokenVisitor
    {
        /*static void Test()
        {
            new NullTokenVisitor();
        }*/
        public virtual void Visit(ILineStartToken t) { }
        public virtual void Visit(ILineEndToken t) { }
        public virtual void Visit(ICommentToken t) { }
        public virtual void Visit(IDirectiveToken t) { }
        public virtual void Visit(IIdentifierToken t) { }
        public virtual void Visit(IKeywordToken t) { }
        public virtual void Visit(IWhiteSpaceToken t) { }
        public virtual void Visit(IOtherToken t) { }
    }

    //public class HTMLTokenVisitor : NullTokenVisitor
    //public class HTMLTokenVisitor : ITokenVisitor
    public sealed class HTMLTokenVisitor : ITokenVisitor
    {
        private void FilteredWrite(IToken token)
        {
            string src = token.ToString();
            for(int i=0; i!=src.Length; i++)
            {
                string dst;
                switch(src[i])
                {
                    case '<':
                        dst = "&lt;"; break;
                    case '>':
                        dst = "&gt;"; break;
                    case '&':
                        dst = "&amp;"; break;
                    default:
                        dst = new string(src[i], 1); break;
                }
                Console.Write(dst);
            }
        }

        private void SpannedFilteredWrite(string spanName, IToken token)
        {
            Console.Write("<span class=\"{0}\">", spanName);
            FilteredWrite(token);
            Console.Write("</span>");
        }

        //public override void Visit(ILineStartToken line)
        public void Visit(ILineStartToken line)
        {
            Console.Write("<span class=\"line_number\">");
            Console.Write("{0,3}", line.Number());
            Console.Write("</span>");
        }
        //public override void Visit(ILineEndToken t) { Console.WriteLine(); }
        public void Visit(ILineEndToken t) { Console.WriteLine(); }
        //public override void Visit(IIdentifierToken token) { Console.Write(token.ToString()); }
        //public override void Visit(IIdentifierToken token)
        public void Visit(IIdentifierToken token)
        {
            SpannedFilteredWrite("identifier", token);
        }
        //public override void Visit(ICommentToken token) { FilteredWrite(token); }
        /*public override void Visit(ICommentToken token)
        {
            Console.Write("<span class=\"comment\">");
            FilteredWrite(token);
            Console.Write("</span>");
        }*/
        //public override void Visit(ICommentToken token)
        public void Visit(ICommentToken token)
        {
            SpannedFilteredWrite("comment", token);
        }
        //public override void Visit(IKeywordToken token) { Console.Write(token.ToString()); }
        /*public override void Visit(IKeywordToken token)
        {
            Console.Write("<span class=\"keyword\">");
            FilteredWrite(token);
            Console.Write("</span>");
        }*/
        //public override void Visit(IKeywordToken token)
        public  void Visit(IKeywordToken token)
        {
            SpannedFilteredWrite("keyword", token);
        }
        //public override void Visit(IWhiteSpaceToken token) { Console.Write(token.ToString()); }
        //public override void Visit(IOtherToken token) { FilteredWrite(token); }
        public void Visit(IWhiteSpaceToken token) { Console.Write(token.ToString()); }
        public void Visit(IOtherToken token) { FilteredWrite(token); }

        /*public void Visit(IDirectiveToken t)
        {
            throw new NotImplementedException();
        }*/
        public void Visit(IDirectiveToken token)
        {
            SpannedFilteredWrite("directive", token);
        }
    }

    public sealed class SourceFile
    {
        public SourceFile(string filename)
        {
            FileInfo source = new FileInfo(filename);
            StreamReader reader = source.OpenText();

            try
            {
                int count = (int)source.Length;
                contents = new char[count];
                reader.Read(contents, 0, count);
                SplitIntoLines();
            }
            finally
            {
                reader.Close();
            }
        }

        public void Accept(ITokenVisitor visitor)
        {
            LineStart.Token start = new LineStart.Token(1);
            LineEnd.Token end = new LineEnd.Token(1);
            foreach (Line line in lines)
            {
                visitor.Visit(start); start++;
                line.Accept(visitor);
                visitor.Visit(end); end++;
            }
        }

        private class Line
        {
            internal Line(Position begin, Position end, ref bool inComment)
            {
                while (begin != end)
                {
                    Position pos;
                    if (inComment || MultiLineComment.Match(begin, end))
                    {
                        pos = MultiLineComment.Eat(begin, end);
                        if (pos != end)
                        {
                            ++pos; ++pos; inComment = false;
                        }
                        else
                        {
                            inComment = true;
                        }
                        tokens.Add(MultiLineComment.MakeToken(begin, pos));
                    }
                    else if (OneLineComment.Match(begin, end))
                    {
                        pos = OneLineComment.Eat(begin, end);
                        tokens.Add(OneLineComment.MakeToken(begin, pos));
                    }
                    else if (WhiteSpace.Match(begin, end))
                    {
                        pos = WhiteSpace.Eat(begin, end);
                        tokens.Add(WhiteSpace.MakeToken(begin, pos));
                    }
                    else if (Identifier.Match(begin, end))
                    {
                        pos = Identifier.Eat(begin, end);
                        if (Keyword.Match(begin, pos))
                        {
                            tokens.Add(Keyword.MakeToken(begin, pos));
                        }
                        else
                        {
                            tokens.Add(Identifier.MakeToken(begin, pos));
                        }
                    }
                    else if (Directive.Match(begin, end))
                    {
                        pos = Directive.Eat(begin, end);
                        tokens.Add(Directive.MakeToken(begin, pos));
                    }
                    else
                    {
                        pos = end;
                        tokens.Add(Other.MakeToken(begin, end));
                    }

                    begin = pos;
                }
            }

            public void Accept(ITokenVisitor visitor)
            {
                foreach (Token token in tokens)
                {
                    token.Accept(visitor);
                }
            }

            private class NotWhiteSpace : Utility.IPredicate1
            {
                public virtual bool Execute(char current)
                {
                    return !Ctype.IsWhiteSpace(current);
                }
            }

            private static readonly NotWhiteSpace notWhiteSpace = new NotWhiteSpace();

            internal bool IsBlank()
            {
                return tokens.Count == 0 ||
                       (tokens.Count == 1
                    && tokens[0] is IWhiteSpaceToken);
            }

            private readonly ArrayList tokens = new ArrayList();
        }

        private void SplitIntoLines()
        {
            Position begin = new Position(contents, 0);
            Position end = new Position(contents, contents.Length);
            Position pos;
            bool inComment = false;
            while ((pos = Utility.Find(begin, end, '\r')) != end)
            {
                lines.Add(new Line(begin, pos, ref inComment));
                begin = ++pos;
                if (begin != end && begin.Get() == '\n')
                {
                    ++begin;
                }
            }

            // last line might not have a terminating newline
            if (begin != end)
            {
                lines.Add(new Line(begin, pos, ref inComment));
            }

            // remove trailing blank lines
            while (lines.Count > 0 && ((Line)lines[lines.Count - 1]).IsBlank())
            {
                lines.RemoveAt(lines.Count - 1);
            }
        }

        private readonly char[] contents;
        private ArrayList lines = new ArrayList();
    }

    internal abstract class Token : IToken
    {
        protected Token(Position begin, Position end)
        {
            this.begin = begin;
            this.end = end;
        }

        public override string ToString()
        {
            return Position.MakeString(begin, end);
        }

        internal abstract void Accept(ITokenVisitor visitor);

        private readonly Position begin, end;
    }

    internal struct Position // in a SourceFile
    {
        public Position(char[] contents, int index)
        {
            this.contents = contents;
            this.index = index;
        }

        public char Get()
        {
            return contents[index];
        }

        public static string MakeString(Position begin, Position end)
        {
            CheckSameWhole(begin, end);
            if (begin > end)
            {
                throw new System.ArgumentException("end not reachable from begin");
            }
            return new string(begin.contents, begin.index, end - begin);
        }

        public static Position operator ++(Position p)
        {
            p.index++;
            return p;
        }

        public static Position operator --(Position p)
        {
            p.index--;
            return p;
        }

        public static Position operator +(Position lhs, int rhs)
        {
            return new Position(lhs.contents, lhs.index + rhs);
        }

        public static Position operator +(int lhs, Position rhs)
        {
            return new Position(rhs.contents, rhs.index + lhs);
        }

        public static int operator -(Position lhs, Position rhs)
        {
            CheckSameWhole(lhs, rhs);
            return lhs.index - rhs.index;
        }

        public static bool operator ==(Position lhs, Position rhs)
        {
            return lhs.contents == rhs.contents
                && lhs.index == rhs.index;
        }

        public static bool operator !=(Position lhs, Position rhs)
        {
            return !(lhs == rhs);
        }

        public static bool operator <(Position lhs, Position rhs)
        {
            return lhs.contents == rhs.contents
                && lhs.index < rhs.index;
        }

        public static bool operator >(Position lhs, Position rhs)
        {
            return lhs.contents == rhs.contents
                && lhs.index > rhs.index;
        }

        public static bool operator <=(Position lhs, Position rhs)
        {
            return lhs.contents == rhs.contents
                && lhs.index <= rhs.index;
        }

        public static bool operator >=(Position lhs, Position rhs)
        {
            return lhs.contents == rhs.contents
                && lhs.index >= rhs.index;
        }

        private static void CheckSameWhole(Position lhs, Position rhs)
        {
            if (lhs.contents != rhs.contents)
            {
                string msg = "Position parameters refer to different char arrays";
                throw new System.ArgumentException(msg);
            }
        }

        private readonly char[] contents;
        private int index;
    }

    internal sealed class Utility
    {
        internal interface IPredicate1
        {
            bool Execute(char current);
        }

        internal interface IPredicate2
        {
            bool Execute(char lhs, char rhs);
        }

        internal static
        bool Equal(Position begin, Position end, string s)
        {
            int i = 0;
            while (begin != end && begin.Get() == s[i])
            {
                begin++;
                i++;
            }
            return begin == end;
        }

        internal static
        Position Find(Position begin, Position end, char value)
        {
            while (begin != end && begin.Get() != value)
            {
                ++begin;
            }
            return begin;
        }

        internal static
        Position FindIf(Position begin, Position end, IPredicate1 f)
        {
            while (begin != end && !f.Execute(begin.Get()))
            {
                ++begin;
            }
            return begin;
        }

        internal static
        Position AdjacentFind(Position begin, Position end, IPredicate2 f)
        {
            Position result = begin;
            if (begin != end)
            {
                while (++begin != end && !f.Execute(result.Get(), begin.Get()))
                {
                    result = begin;
                }
            }
            return (begin == end) ? end : result;
        }

        private Utility() { }
    }

    internal sealed class WhiteSpace
    {
        internal static
        bool Match(Position begin, Position end)
        {
            return begin != end && Ctype.IsWhiteSpace(begin.Get());
        }

        internal static
        Position Eat(Position begin, Position end)
        {
            return Utility.FindIf(begin, end, notWhiteSpace);
        }

        internal static
        IWhiteSpaceToken MakeToken(Position begin, Position end)
        {
            return new WhiteSpaceToken(begin, end);
        }

        private sealed class WhiteSpaceToken : Token, IWhiteSpaceToken
        {
            internal WhiteSpaceToken(Position begin, Position end)
              : base(begin, end)
            {
            }

            internal override void Accept(ITokenVisitor visitor)
            {
                visitor.Visit(this);
            }
        }

        private static readonly NotWhiteSpace notWhiteSpace
            = new NotWhiteSpace();

        private sealed class NotWhiteSpace : Utility.IPredicate1
        {
            public bool Execute(char current)
            {
                return !Ctype.IsWhiteSpace(current);
            }
        }

        private WhiteSpace() { }
    }

    internal sealed class OneLineComment
    {
        internal static bool Match(Position begin, Position end)
        {
            return end - begin >= 2 && Utility.Equal(begin, begin + 2, "//");
        }

        internal static Position Eat(Position begin, Position end)
        {
            return Utility.Find(begin, end, '\n');
        }

        internal static
        ICommentToken MakeToken(Position begin, Position end)
        {
            return new OneLineCommentToken(begin, end);
        }

        private sealed class OneLineCommentToken : Token, ICommentToken
        {
            internal OneLineCommentToken(Position begin, Position end)
              : base(begin, end)
            {
            }

            internal override void Accept(ITokenVisitor visitor)
            {
                visitor.Visit(this);
            }
        }

        private OneLineComment() { }
    }

    internal sealed class MultiLineComment
    {
        internal static bool Match(Position begin, Position end)
        {
            return (end - begin >= 2 && Utility.Equal(begin, begin + 2, "/*"));
        }

        internal static Position Eat(Position begin, Position end)
        {
            return Utility.AdjacentFind(begin, end, starForwardSlash);
        }

        internal static
        ICommentToken MakeToken(Position begin, Position end)
        {
            return new MultiLineCommentToken(begin, end);
        }

        private sealed class MultiLineCommentToken : Token, ICommentToken
        {
            internal MultiLineCommentToken(Position begin, Position end)
              : base(begin, end)
            {
            }

            internal override void Accept(ITokenVisitor visitor)
            {
                visitor.Visit(this);
            }
        }

        private static readonly StarForwardSlash starForwardSlash
            = new StarForwardSlash();

        private sealed class StarForwardSlash : Utility.IPredicate2
        {
            public bool Execute(char previous, char current)
            {
                return previous == '*' && current == '/';
            }
        }

        private MultiLineComment() { }
    }

    internal sealed class Directive
    {
        internal static
        bool Match(Position begin, Position end)
        {
            return begin != end && begin.Get() == '#';
        }

        internal static
        Position Eat(Position begin, Position end)
        {
            return end;
        }

        internal static
        IDirectiveToken MakeToken(Position begin, Position end)
        {
            return new DirectiveToken(begin, end);
        }

        private sealed class DirectiveToken : Token, IDirectiveToken
        {
            internal DirectiveToken(Position begin, Position end)
              : base(begin, end)
            {
            }

            internal override void Accept(ITokenVisitor visitor)
            {
                visitor.Visit(this);
            }
        }

        private Directive() { }
    }

    internal sealed class Identifier
    {
        internal static bool Match(Position begin, Position end)
        {
            if (begin == end)
            {
                return false;
            }
            char read = begin.Get();
            return Ctype.IsLetter(read) || read == '_';
        }

        internal static Position Eat(Position begin, Position end)
        {
            return Utility.FindIf(begin, end, notIdentifier);
        }    

        internal static
        IIdentifierToken MakeToken(Position begin, Position end)
        {
            return new IdentifierToken(begin, end);
        }

        private sealed class IdentifierToken : Token, IIdentifierToken
        {
            internal IdentifierToken(Position begin, Position end)
              : base(begin, end)
            {
            }

            internal override void Accept(ITokenVisitor visitor)
            {
                visitor.Visit(this);
            }
        }

        private static readonly NotIdentifier notIdentifier
            = new NotIdentifier();

        private sealed class NotIdentifier : Utility.IPredicate1
        {
            public bool Execute(char current)
            {
                return !(Ctype.IsLetter(current)
                      || Ctype.IsDigit(current)
                      || current == '_');
            }
        }

        private Identifier() { }
    }

    internal sealed class Other
    {
        internal static
        IOtherToken MakeToken(Position begin, Position end)
        {
            return new OtherToken(begin, end);
        }

        private sealed class OtherToken : Token, IOtherToken
        {
            internal OtherToken(Position begin, Position end)
              : base(begin, end)
            {
            }

            internal override void Accept(ITokenVisitor visitor)
            {
                visitor.Visit(this);
            }
        }

        private Other() { }
    }

    internal sealed class Keyword
    {
        internal static bool Match(Position begin, Position end)
        {
            int length = end - begin;
            foreach (string keyword in keywords)
            {
                if (length == keyword.Length && Utility.Equal(begin, end, keyword))
                {
                    return true;
                }
            }
            return false;
        }

        internal static
        IKeywordToken MakeToken(Position begin, Position end)
        {
            return new KeywordToken(begin, end);
        }

        private sealed class KeywordToken : Token, IKeywordToken
        {
            internal KeywordToken(Position begin, Position end)
              : base(begin, end)
            {
            }

            internal override void Accept(ITokenVisitor visitor)
            {
                visitor.Visit(this);
            }
        }

        private Keyword() { }

        private static readonly string[] keywords = new string[]
        {
            "abstract",
            "as",
            "base",
            "bool",
            "break",
            "byte",
            "case",
            "catch",
            "char",
            "checked",
            "class",
            "const",
            "continue",
            "decimal",
            "default",
            "delegate",
            "do",
            "double",
            "else",
            "enum",
            "event",
            "explicit",
            "extern",
            "false",
            "finally",
            "fixed",
            "float",
            "for",
            "foreach",
            "goto",
            "if",
            "implicit",
            "in",
            "int",
            "interface",
            "internal",
            "is",
            "lock",
            "long",
            "namespace",
            "new",
            "null",
            "object",
            "operator",
            "out",
            "override",
            "params",
            "private",
            "protected",
            "public",
            "readonly",
            "ref",
            "return",
            "sbyte",
            "sealed",
            "short",
            "sizeof",
            "stackalloc",
            "static",
            "string",
            "struct",
            "switch",
            "this",
            "throw",
            "true",
            "try",
            "typeof",
            "uint",
            "ulong",
            "unchecked",
            "unsafe",
            "ushort",
            "using",
            "virtual",
            "void",
            "while",
        };
    }

    internal sealed class LineEnd
    {
        internal sealed class Token : ILineEndToken
        {
            internal Token(int number)
            {
                this.number = number;
            }

            public static Token operator ++(Token t)
            {
                t.number++;
                return t;
            }

            int ILineEndToken.Number()
            {
                return number;
            }

            private int number;
        }
    }

    internal sealed class LineStart
    {
        internal sealed class Token : ILineStartToken
        {
            internal Token(int number)
            {
                this.number = number;
            }

            public static Token operator ++(Token t)
            {
                t.number++;
                return t;
            }

            int ILineStartToken.Number()
            {
                return number;
            }

            private int number;
        }
    }
}