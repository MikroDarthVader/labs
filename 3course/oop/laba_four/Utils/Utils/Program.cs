﻿using System;

namespace Utils
{
    class Test
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter first number: ");
            int a = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter second number: ");
            int b = int.Parse(Console.ReadLine());

            int factorial;
            try
            {
                Utils.Factorial(a, out factorial);
                Console.WriteLine("Factorial of " + a + " is " + factorial);

                Utils.RecursiveFactorial(a, out factorial);
                Console.WriteLine("Recursive factorial of " + a + " is " + factorial);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("Greater number:\n" + Utils.Greater(a, b));

            Console.WriteLine("Before swap:\n a - " + a + ", b - " + b);
            Utils.Swap(ref a, ref b);
            Console.WriteLine("After swap:\n a - " + a + ", b - " + b);
        }
    }

    class Utils
    {
        public static int Greater(int a, int b)
        {
            return a > b ? a : b;
        }

        public static void Swap(ref int a, ref int b)
        {
            a = b - a;
            b -= a;
            a += b;
        }

        public static bool Factorial(int n, out int result)
        {
            if (n < 0)
                throw new ArgumentException("n");

            result = 1;

            for (; n > 0; n--)
            {
                checked
                {
                    result *= n;
                }
            }

            return true;
        }

        public static bool RecursiveFactorial(int n, out int result)
        {
            if (n < 0)
                throw new ArgumentException("n");

            result = 1;

            result = RecursiveFactorialIter(n, result);

            return true;
        }

        private static int RecursiveFactorialIter(int n, int result)
        {
            if (n > 0)
            {
                checked
                {
                    result *= n;
                }

                return RecursiveFactorialIter(n - 1, result);
            }
            else
                return result;
        }
    }
}
