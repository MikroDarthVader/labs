
using System;

class BankAccount
{
    private static long nextAccNo;

    private long accNo;
    private decimal accBal;
    private AccountType accType;

    public void Populate(decimal balance)
    {
        accNo = NextNumber();
        accBal = balance;
        accType = AccountType.Checking;
    }

    public bool Withdraw(decimal amount)
    {
        if (amount < 0)
            throw new System.Exception("amount can't be less then zero");

        bool sufficientFunds = accBal >= amount;
        if (sufficientFunds)
        {
            accBal -= amount;
        }
        return sufficientFunds;
    }

    public decimal Deposit(decimal amount)
    {
        if (amount < 0)
            throw new System.Exception("amount can't be less then zero");

        accBal += amount;
        return accBal;
    }

    public long GetAccNo()
    {
        return accNo;
    }

    public decimal GetAccBal()
    {
        return accBal;
    }

    public AccountType GetAccType()
    {
        return accType;
    }

    private static long NextNumber()
    {
        return nextAccNo++;
    }
}
