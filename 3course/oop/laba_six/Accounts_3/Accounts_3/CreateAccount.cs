
using System;

class CreateAccount
{
    static void Main()
    {
        try
        {
            BankAccount berts = NewBankAccount();
            Write(berts);
            TestDeposit(berts);
            Write(berts);
            TestWithdraw(berts);
            Write(berts);

            BankAccount freds = NewBankAccount();
            Write(freds);
            TestDeposit(freds);
            Write(freds);
            TestWithdraw(freds);
            Write(freds);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    static BankAccount NewBankAccount()
    {
        BankAccount created = new BankAccount();

        Console.Write("Enter the account balance: ");
        decimal balance = decimal.Parse(Console.ReadLine());

        created.Populate(balance);

        return created;
    }

    static void TestDeposit(BankAccount acc)
    {
        Console.Write("Enter amount to deposit: ");
        decimal amount = decimal.Parse(Console.ReadLine());
        acc.Deposit(amount);
    }

    static void TestWithdraw(BankAccount acc)
    {
        Console.Write("Enter amount to withdraw: ");
        decimal amount = decimal.Parse(Console.ReadLine());
        acc.Withdraw(amount);
    }

    static void Write(BankAccount toWrite)
    {
        Console.WriteLine("Account number is {0}", toWrite.GetAccNo());
        Console.WriteLine("Account balance is {0}", toWrite.GetAccBal());
        Console.WriteLine("Account type is {0}", toWrite.GetAccType());
    }
}
