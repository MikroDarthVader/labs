
class BankAccount
{
    private long accNo;
    private decimal accBal;
    private AccountType accType;

    public void Populate(long number, decimal balance)
    {
        accNo = number;
        accBal = balance;
        accType = AccountType.Checking;
    }

    public long GetAccNo()
    {
        return accNo;
    }

    public decimal GetAccBal()
    {
        return accBal;
    }

    public AccountType GetAccType()
    {
        return accType;
    }
}
