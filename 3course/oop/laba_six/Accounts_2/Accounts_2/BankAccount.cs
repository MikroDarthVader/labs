
class BankAccount
{
    private static long nextAccNo;

    private long accNo;
    private decimal accBal;
    private AccountType accType;

    public void Populate(decimal balance)
    {
        accNo = NextNumber();
        accBal = balance;
        accType = AccountType.Checking;
    }

    public long GetAccNo()
    {
        return accNo;
    }

    public decimal GetAccBal()
    {
        return accBal;
    }

    public AccountType GetAccType()
    {
        return accType;
    }

    private static long NextNumber()
    {
        return nextAccNo++;
    }
}
