namespace Utils
{
    using System;

    /// <summary>
    ///   This the test harness
    /// </summary>

    public class Test
    {
        public static void Main()
        {
            string s;
            Console.WriteLine("Insert string:");
            s = Console.ReadLine();
            Utils.Reverse(ref s);
            Console.WriteLine("Reversed string: " + s);
        }
    }
}
