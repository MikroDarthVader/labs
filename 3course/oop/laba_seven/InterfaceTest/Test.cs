namespace Utils
{
    using System;
    public class Test
    {
        public static void Main()
        {
            int i = 0;
            long ul = 0;
            string s = "Test";
            Console.WriteLine("int i = 0 is formattable: " + Utils.IsItFormattable(i));
            Console.WriteLine("int ul = 0 is formattable: " + Utils.IsItFormattable(ul));
            Console.WriteLine("int s = \"Test\" is formattable: " + Utils.IsItFormattable(s));
        }
    }
}
