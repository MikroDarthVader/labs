using System;
using System.IO;

public class CopyFileUpper
{
    public static void Main()
    {
        string sFrom, sTo;
        StreamReader srFrom;
        StreamWriter swTo;

        Console.WriteLine("Enter input file name:");
        sFrom = Console.ReadLine();

        Console.WriteLine("Enter ouput file name:");
        sTo = Console.ReadLine();
        try
        {
            srFrom = new StreamReader(sFrom);
            swTo = new StreamWriter(sTo);

            while (srFrom.Peek() != -1)
            {
                string sBuffer = srFrom.ReadLine();
                sBuffer = sBuffer.ToUpper();
                swTo.WriteLine(sBuffer);
            }

            srFrom.Close();
            swTo.Close();
        }
        catch (FileNotFoundException e)
        {
            Console.WriteLine(e);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }
}
