using System;

public class Test
{
    public static void Main()
    {
        BankAccount b1, b2;

        b1 = new BankAccount();
        b1.Populate(100);

        b2 = new BankAccount();
        b2.Populate(100);

        Console.WriteLine("      Before transfer:");
        Console.WriteLine("\n   b1:");
        Write(b1);

        Console.WriteLine("\n   b2:");
        Write(b2);

        b1.TransferForm(b2, 10);

        Console.WriteLine("\n\n      After transfer:");
        Console.WriteLine("\n   b1:");
        Write(b1);

        Console.WriteLine("\n   b2:");
        Write(b2);
        Console.WriteLine();
    }

    static void Write(BankAccount toWrite)
    {
        Console.WriteLine("Account number is {0}", toWrite.Number());
        Console.WriteLine("Account balance is {0}", toWrite.Balance());
        Console.WriteLine("Account type is {0}", toWrite.Type());
    }
}
