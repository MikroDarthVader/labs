﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Divider
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0, j = 0;
            string temp;

            Console.WriteLine("Enter first number:"); //Вывод сообщения “Enter first number:" с помощью метода WriteLine()
            temp = Console.ReadLine(); //присвоение переменной temp строки введенной пользователем с консоли с пощью метода ReadLine()

            try //блок try для обработки ввода пользователем 2 чисел с последующим их делением. В случае возникновения ошибки “выбрасывает” исключение.
            {
                i = int.Parse(temp); //Преобразование введенной пользователем строки в число

                Console.WriteLine("Enter second number:");//Вывод сообщения “Enter second number”
                temp = Console.ReadLine();

                j = int.Parse(temp);

                int k = i / j;//Деление числа i на j

                Console.WriteLine("{0} / {1} = {2}", i, j, k);//Вывод результата на экран

            }
            catch (DivideByZeroException)// Исключение при делении на 0
            {
                Console.WriteLine("You can't divide by {0}", j);
            }
            catch (FormatException)// Исключение при некорректном вводе
            {
                Console.WriteLine("You entered not a number");
            }
            catch (OverflowException)// Исключение при вводе числа превышающего размеры типа int
            {
                Console.WriteLine("You entered not an Int32 value");
            }
        }
    }
}
