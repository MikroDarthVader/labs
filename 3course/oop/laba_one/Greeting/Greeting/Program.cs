﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Greeting
{
    class Greeter
    {
        static void Main(string[] args)
        {
            string myName;//Инициализируется переменная myName типа string
            Console.WriteLine("Please, enter your name: ");// Вызов метода WriteLine() класса Console 
            myName = Console.ReadLine(); // Чтение строки введенной пользователем 
            Console.WriteLine("Hello, {0}", myName);// Вывод приветственного сообщения “Hello ,<myName>” в консоль с помощью метода  WriteLine() 
        }
    }
}
