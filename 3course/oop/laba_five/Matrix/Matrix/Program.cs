﻿using System;

namespace Matrix
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][] a;
            int[][] b;

            Console.WriteLine("Enter first matrix: ");
            a = ReadMatrix();

            Console.WriteLine("Enter second matrix: ");
            b = ReadMatrix();

            Console.WriteLine("\nFirst matrix: ");
            WriteMatrix(a);
            Console.WriteLine("\nSecond matrix: ");
            WriteMatrix(b);
            Console.WriteLine("\nMultiply result matrix: ");
            WriteMatrix(MatrixMultiply(a, b));
        }

        static int[][] ReadMatrix()
        {
            int[][] matrix = new int[2][];
            for (int i = 0; i < 2; i++)
            {
                matrix[i] = new int[2];
                for (int j = 0; j < 2; j++)
                    matrix[i][j] = int.Parse(Console.ReadLine());
            }

            return matrix;
        }

        static void WriteMatrix(int[][] matrix)
        {
            for (int i = 0; i < 2; i++)
                Console.WriteLine(matrix[i][0] + " " + matrix[i][1]);
        }

        static int[][] MatrixMultiply(int[][] a, int[][] b)
        {
            int[][] result = new int[2][];
            for (int i = 0; i < 2; i++)
                result[i] = new int[2];

            result[0][0] = a[0][0] * b[0][0] + a[0][1] * b[1][0];
            result[0][1] = a[0][0] * b[0][1] + a[0][1] * b[1][1];
            result[1][0] = a[1][0] * b[0][0] + a[1][1] * b[1][0];
            result[1][1] = a[1][0] * b[0][1] + a[1][1] * b[1][1];

            return result;
        }
    }
}
