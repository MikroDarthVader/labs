﻿using System;
using System.IO;

namespace FileDetails
{
    class FileDetails
    {
        static void Main(string[] args)
        {
            string fileName = args[0];
            FileStream stream = new FileStream(fileName, FileMode.Open);
            StreamReader reader = new StreamReader(stream);

            int numOfSymbols = 0;
            int numOfLines = 0;
            int numOfVowels = 0;
            int numOfConsonant = 0;


            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                numOfLines++;

                foreach (char c in line)
                {
                    numOfSymbols++;

                    if ("AEIOUYaeiouy".IndexOf(c) != -1)
                        numOfVowels++;
                    else if ("qwrtpsdfghjklzxcvbnm".IndexOf(c) != -1 || "qwrtpsdfghjklzxcvbnm".ToUpper().IndexOf(c) != -1)
                        numOfConsonant++;
                }
            }

            Console.WriteLine("Number of symbols: " + numOfSymbols);
            Console.WriteLine("Number of lines: " + numOfLines);
            Console.WriteLine("Number of vowels: " + numOfVowels);
            Console.WriteLine("Number of consonant: " + numOfConsonant);

            reader.Close();
            stream.Close();
        }
    }
}
