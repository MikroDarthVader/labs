﻿using System;
using System.IO;

class Program
{
    private delegate void Log(string msg);

    static void Main()
    {
        DOSomething(LogToFile);
    }

    private static void DOSomething(Log log)
    {
        log(DateTime.Now.ToString());
    }

    private static void LogToFile(string msg)
    {
        string myDocsPath = Environment.CurrentDirectory;
        string logFilePath = Path.Combine(myDocsPath, "log.txt");
        File.AppendAllText(logFilePath, msg + Environment.NewLine);
    }
}
