﻿using System;
using System.IO;

class Program
{
    private delegate void Log(string msg);

    static void Main()
    {
        DoSomething(LogToFile);
        DoSomething(delegate (string message) { Console.WriteLine(message); });
        DoSomething(message => Console.WriteLine(message));
        DoSomething(Console.WriteLine);
    }

    private static void DoSomething(Log log)
    {
        log(DateTime.Now.ToString());
    }

    private static void LogToFile(string msg)
    {
        string myDocsPath = Environment.CurrentDirectory;
        string logFilePath = Path.Combine(myDocsPath, "log.txt");
        File.AppendAllText(logFilePath, msg + Environment.NewLine);
    }
}
