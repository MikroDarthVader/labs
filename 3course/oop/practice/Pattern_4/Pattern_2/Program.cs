﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Threading;

namespace Pattern_2
{
    class Program
    {

        //Упр. 1
        //class Simulator : IEnumerable
        //{
        //    string[] moves = { "5", "3", "1", "6", "7" };
        //    public IEnumerator GetEnumerator()
        //    {
        //        foreach (string element in moves)
        //            yield return element;
        //    }
        //}

        //interface IObserver
        //{
        //    void Update(string state);
        //}

        //class Observer : IObserver
        //{
        //    string name;
        //    Subject subject;
        //    string state;
        //    string gap;

        //    public Observer(Subject subject, string name, string gap)
        //    {
        //        this.subject = subject;
        //        this.name = name;
        //        this.gap = gap;
        //        subject.Notify += Update;
        //    }

        //    public void Update(string subjectState)
        //    {
        //        state = subjectState;
        //        Console.WriteLine(gap + name + ": " + state);
        //    }
        //}

        //class Subject
        //{
        //    public delegate void Callback(string s);
        //    public event Callback Notify;
        //    Simulator simulator = new Simulator();
        //    const int speed = 200;
        //    public string SubjectState { get; set; }
        //    public void Go() { new Thread(new ThreadStart(Run)).Start(); }

        //    void Run()
        //    {
        //        foreach (string s in simulator)
        //        {
        //            Console.WriteLine("Subject: " + s);
        //            SubjectState = s;
        //            Notify(s);
        //            Thread.Sleep(speed); //milliseconds
        //        }
        //    }
        //}


        //static void Main(string[] args)
        //{
        //    Subject subject = new Subject();
        //    Observer observer = new Observer(subject, "Center", "\t\t");
        //    Observer observer1 = new Observer(subject, "Right", "\t\t\t\t");
        //    subject.Go();
        //    Console.Read();
        //}

        //Упр. 2

        interface IObserver
        {
            void Update(object ob);
        }

        class WeatherStation : IObserver
        {
            string Name { get; set; }
            Weather weather;
            public WeatherStation(string name, Weather sub)
            {
                Name = name;
                weather = sub;
                weather.RegisterObserver(this);
                weather.Notify += Update;
            }

            public void Update(object o)
            {
                WeatherProperties wProp = (WeatherProperties)o;
                Console.WriteLine($"Station \"{Name}\"");
                Console.WriteLine($"    Temperature: {wProp.Temperature} Celsius");
                Console.WriteLine($"    Pressure: {wProp.Pressure} mm");
                Console.WriteLine($"    Precipitation: {wProp.Precipitation} meter(s)");
                Console.WriteLine($"    AirHumadity: {wProp.AirHumidity}%");
            }
        }

        class Weather
        {
            public static Random rnd = new Random();

            List<IObserver> observers = new List<IObserver>();

            public delegate void Callback(object o);

            public event Callback Notify;

            WeatherProperties wProp = new WeatherProperties();

            public void RegisterObserver(IObserver observer)
            {
                observers.Add(observer);
            }

            public void WeatherChanges()
            {
                wProp.Temperature = rnd.Next(0, 80) - 40; //{-40, 40} must be
                wProp.Pressure = rnd.Next(756, 766);
                wProp.Precipitation = rnd.Next(0, 3);
                wProp.AirHumidity = rnd.Next(20, 100);

                Notify(wProp);
                Thread.Sleep(500);
            }
        }

        class WeatherProperties
        {
            public int Temperature { get; set; }
            public int Precipitation { get; set; }
            public int AirHumidity { get; set; }
            public int Pressure { get; set; }
        }


        static void Main(string[] args)
        {
            Weather weather = new Weather();
            WeatherStation station1 = new WeatherStation("GEO", weather);
            WeatherStation station2 = new WeatherStation("WC", weather);
            WeatherStation station3 = new WeatherStation("Hurricane", weather);
            for (int i = 0; i < 5; i++)
            {
                weather.WeatherChanges();
                Console.WriteLine();
            }
            Console.ReadKey();
        }


    }
}
