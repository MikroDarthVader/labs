﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern_3
{
    class Program
    {
        //Упр. 1
        //class Singleton
        //{
        //    protected static int name = 0;
        //    protected Singleton()
        //    {
        //        name += 1;
        //        Console.WriteLine("Singleton has been planted");
        //    }


        //    private sealed class SingletonCreator
        //    {
        //        private static readonly Singleton instance = new Singleton();
        //        public static Singleton Instance { get { return instance; } }
        //    }

        //    public string getName { get { return name.ToString(); } }

        //    public static Singleton Instance { get { return SingletonCreator.Instance; } }
        //}

        //class FirstClass
        //{
        //    Singleton single = Singleton.Instance;
        //    public FirstClass() { }
        //    public string getName()
        //    {
        //        return single.getName;
        //    }
        //}

        //class SecondClass
        //{
        //    Singleton single = Singleton.Instance;
        //    public SecondClass() { }
        //    public string getName() { return single.getName; }
        //}



        //static void Main(string[] args)
        //{
        //    FirstClass first = new FirstClass();
        //    SecondClass second = new SecondClass();
        //    Console.WriteLine("Singleton name in firstclass: " + first.getName());
        //    Console.WriteLine("Singleton name in secondclass: " + second.getName());
        //    Console.Read();

        //    Console.ReadKey();
        //}

        //Упр. 2


        //Упр. 2

        class MySingleton
        {
            private static MySingleton _instance = null;
            private static int count = 0;

            protected MySingleton()
            {
                count++;
            }

            public int Count { get { return count; } }

            public static MySingleton GetInstance()
            {
                if (_instance == null)
                    _instance = new MySingleton();

                return _instance;
            }
        }

        class MyClass
        {
            private static int count = 0;

            public MyClass()
            {
                count++;
            }

            public int getCount { get { return count; } }

        }

        static void Main(string[] args)
        {
            MyClass[] notSingletons = new MyClass[10];
            MySingleton[] singletons = new MySingleton[10];

            for (int i = 0; i < 10; i++)
            {
                notSingletons[i] = new MyClass();
                singletons[i] = MySingleton.GetInstance();
                Console.WriteLine($"notSingletons.count[{i}]: {notSingletons[i].getCount}" +
                    $" \tsingletons.count[{i}]: {singletons[i].Count}");
            }

            Console.ReadKey();
        }

    }
}
