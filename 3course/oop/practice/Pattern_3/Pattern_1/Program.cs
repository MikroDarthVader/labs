﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern_1
{
    class Program
    {

        class Duck
        {
            protected FlyBehavior flyBehavior;
            protected QuackBehavior quackBehavior;
            protected SwimBehavior swimBehavior; //added property

            public Duck() { }
            public void performFly() { flyBehavior.fly(); }
            public void performQuack() { quackBehavior.quack(); }
            public void Swim() { swimBehavior.swim(); }
        }

        class MallardDuck : Duck
        {
            public MallardDuck()
            {
                quackBehavior = new Quack();
                flyBehavior = new FlyWithWings();
                swimBehavior = new SwimWithPaws();
                Console.WriteLine("I MallarDuck");
            }
        }

        class WoodDuck : Duck //added class
        {
            public WoodDuck()
            {
                quackBehavior = new Silence();
                flyBehavior = new FlyNoWay();
                swimBehavior = new DeadSwimming(); //added
                Console.WriteLine("I WoodDuck");
            }
        }

        interface SwimBehavior //added interface
        {
            void swim();
        }

        interface FlyBehavior
        {
            void fly();
        }

        class SwimWithPaws : SwimBehavior //added class
        {
            public void swim()
            {
                Console.WriteLine("I swim with my paws");
            }
        }

        class DeadSwimming : SwimBehavior //added class
        {
            public void swim()
            {
                Console.WriteLine("I'm sailing dead weight");
            }
        }

        class FlyWithWings : FlyBehavior
        {
            public void fly()
            {
                Console.WriteLine("I can fly");
            }
        }

        class FlyNoWay : FlyBehavior
        {
            public void fly()
            {
                Console.WriteLine("I can't fly");
            }
        }

        interface QuackBehavior
        {
            void quack();
        }

        class Quack : QuackBehavior
        {
            public void quack()
            {
                Console.WriteLine("Quack");
            }
        }

        class Silence : QuackBehavior
        {
            public void quack()
            {
                Console.WriteLine("Silence");
            }
        }

        static void Main(string[] args)
        {
            Duck mallard = new MallardDuck();
            mallard.performQuack();
            mallard.performFly();

            Duck wood = new WoodDuck();
            wood.performQuack();
            wood.performFly();

            Console.ReadKey();
        }

    }
}
