﻿using System.Collections.Generic;
using System.Linq;
using System;

using MyMath;
using Systems;

namespace SystemsUsage
{
    public class Bot : ICheckersPlayer
    {
        private Game game;
        private Board board;

        private Random rnd;

        private static float EatPawnPriority = 1;
        private static float BecomeQueenPriority = 2;

        public Bot(Game game)
        {
            this.game = game;
            board = game.checkersGame.board;

            rnd = new Random();
        }

        public void OnMove(PawnColor color)
        {
            List<MoveInfo> movesInfo = GetPossibleMoves(color);

            Dictionary<MoveInfo, float> movesInfoWithPriority = movesInfo.ToDictionary(info => info,
                                                                                       info => info.eatenPawns.Count * EatPawnPriority +
                                                                                              (info.isPawnBecomeQueen ? BecomeQueenPriority : 0));

            List<MoveInfo> chosenMovesInfo = new List<MoveInfo>();
            float chosenMoveInfoPriority = float.MinValue;
            foreach (KeyValuePair<MoveInfo, float> pair in movesInfoWithPriority)
            {
                if (pair.Value > chosenMoveInfoPriority)
                {
                    chosenMovesInfo.Clear();
                    chosenMovesInfo.Add(pair.Key);

                    chosenMoveInfoPriority = pair.Value;
                }
                else if(pair.Value == chosenMoveInfoPriority)
                    chosenMovesInfo.Add(pair.Key);
            }

            game.checkersGame.Move(this, chosenMovesInfo[rnd.Next(0, chosenMovesInfo.Count)].move);
        }

        private List<MoveInfo> GetPossibleMoves(PawnColor pawnColor)
        {
            int forward;
            if (pawnColor == PawnColor.white)
                forward = 1;
            else
                forward = -1;

            LinkedList<MoveInfo> result = new LinkedList<MoveInfo>();

            Dictionary<CellPos, Pawn> pawns = board.GetAllPawns(pawnColor);

            foreach (KeyValuePair<CellPos, Pawn> pair in pawns)
            {
                CellPos pos = pair.Key;
                Pawn pawn = pair.Value;

                if (!pawn.isQueen)
                    BasePawnMovesCalcs(pos, pawnColor, forward, result);
                else
                    QueenPawnMovesCalcs(pos, pawnColor, result);
            }

            List<MoveInfo> moveWithEating = result.Where(info => info.eatenPawns.Count != 0).ToList();

            if (moveWithEating.Count == 0)
                return result.ToList();
            else
                return moveWithEating;
        }

        private void BasePawnMovesCalcs(CellPos pawnPos, PawnColor pawnColor, int forward, LinkedList<MoveInfo> movesStorage)
        {
            Stack<CellPos> moves = new Stack<CellPos>();
            Stack<KeyValuePair<CellPos, Pawn>> eatenPawns = new Stack<KeyValuePair<CellPos, Pawn>>();
            moves.Push(pawnPos);

            BasePawnMoveCalcsIter(moves, eatenPawns, pawnColor, forward, movesStorage);
        }

        private void BasePawnMoveCalcsIter(Stack<CellPos> moves, Stack<KeyValuePair<CellPos, Pawn>> eatenPawns, PawnColor pawnColor, int forward, LinkedList<MoveInfo> movesStorage)
        {
            LinkedList<CellPos> movesWithoutEating = new LinkedList<CellPos>();
            Dictionary<CellPos, CellPos> movesWithEating = new Dictionary<CellPos, CellPos>();

            CellPos pos = moves.Peek();

            for (int x = -1; x <= 1; x += 2)
                for (int y = -1; y <= 1; y += 2)
                {
                    Vector2Int shift = new Vector2Int(x, y);

                    if (moves.Count == 1 && CellPos.IsPosValid(pos.asVector + shift) && !board.HasPawn(pos + shift) && shift.y == forward)
                        movesWithoutEating.AddLast(pos + shift);

                    if (CellPos.IsPosValid(pos.asVector + shift) && !eatenPawns.ToList().Exists(pair => pair.Key == pos + shift) &&
                            board.TryGetPawn(pos + shift, out Pawn tempPawn) && tempPawn.color != pawnColor &&
                            CellPos.IsPosValid(pos.asVector + shift * 2) && (!board.HasPawn(pos + shift * 2) || moves.Last() == pos + shift * 2))
                        movesWithEating.Add(pos + shift * 2, pos + shift);
                }


            if (moves.Count == 1 && movesWithEating.Count == 0 && movesWithoutEating.Count == 0)
                return;

            if (movesWithEating.Count != 0)
            {
                foreach (KeyValuePair<CellPos, CellPos> pair in movesWithEating)
                {
                    moves.Push(pair.Key);
                    eatenPawns.Push(new KeyValuePair<CellPos, Pawn>(pair.Value, board[pair.Value]));
                    bool isPawnBecomeQueen = moves.Peek().y == (pawnColor == PawnColor.white ? 7 : 0);

                    if (isPawnBecomeQueen)
                        QueenPawnMovesCalcsIter(moves, eatenPawns, isPawnBecomeQueen, pawnColor, movesStorage);
                    else
                        BasePawnMoveCalcsIter(moves, eatenPawns, pawnColor, forward, movesStorage);

                    moves.Pop();
                    eatenPawns.Pop();
                }
            }
            else if (moves.Count == 1 && movesWithoutEating.Count != 0)
            {
                foreach (CellPos endMovePos in movesWithoutEating)
                {
                    moves.Push(endMovePos);

                    movesStorage.AddLast(new MoveInfo(moves.Reverse().ToList(),
                                                      false,
                                                      new Dictionary<CellPos, Pawn>()));

                    moves.Pop();
                }
            }
            else
                movesStorage.AddLast(new MoveInfo(moves.Reverse().ToList(),
                                                  false,
                                                  eatenPawns.Reverse().ToDictionary(pair => pair.Key, pair => pair.Value)));
        }

        private void QueenPawnMovesCalcs(CellPos pawnPos, PawnColor pawnColor, LinkedList<MoveInfo> movesStorage)
        {
            Stack<CellPos> moves = new Stack<CellPos>();
            Stack<KeyValuePair<CellPos, Pawn>> eatenPawns = new Stack<KeyValuePair<CellPos, Pawn>>();
            moves.Push(pawnPos);

            QueenPawnMovesCalcsIter(moves, eatenPawns, false, pawnColor, movesStorage);
        }

        private void QueenPawnMovesCalcsIter(Stack<CellPos> moves, Stack<KeyValuePair<CellPos, Pawn>> eatenPawns, bool isPawnBecomeQueen, PawnColor pawnColor, LinkedList<MoveInfo> movesStorage)
        {
            LinkedList<CellPos> movesWithoutEating = new LinkedList<CellPos>();
            Dictionary<CellPos, CellPos> movesWithEating = new Dictionary<CellPos, CellPos>();

            CellPos pos = moves.Peek();

            for (int x = -1; x <= 1; x += 2)
                for (int y = -1; y <= 1; y += 2)
                {
                    Vector2Int shift = new Vector2Int(x, y);

                    CellPos eatenPawn = default;
                    bool anotherPawnReached = false;
                    for (int i = 1; CellPos.IsPosValid(pos.asVector + shift * i); i++)
                    {
                        if (!board.TryGetPawn(pos + shift * i, out Pawn tempPawn) || moves.Last() == pos + shift * i)
                        {
                            if (anotherPawnReached)
                                movesWithEating.Add(pos + shift * i, eatenPawn);
                            else
                                movesWithoutEating.AddLast(pos + shift * i);
                        }
                        else
                        {
                            if (tempPawn.color == pawnColor)
                                break;
                            else if (anotherPawnReached)
                                break;
                            else if (!eatenPawns.ToList().Exists(pair => pair.Key == pos + shift * i))
                            {
                                anotherPawnReached = true;
                                eatenPawn = pos + shift * i;
                            }
                        }
                    }
                }

            if (moves.Count == 1 && movesWithEating.Count == 0 && movesWithoutEating.Count == 0)
                return;

            if (movesWithEating.Count != 0)
            {
                foreach (KeyValuePair<CellPos, CellPos> pair in movesWithEating)
                {
                    moves.Push(pair.Key);
                    eatenPawns.Push(new KeyValuePair<CellPos, Pawn>(pair.Value, board[pair.Value]));

                    QueenPawnMovesCalcsIter(moves, eatenPawns, isPawnBecomeQueen, pawnColor, movesStorage);

                    moves.Pop();
                    eatenPawns.Pop();
                }
            }
            else if (moves.Count == 1 && movesWithoutEating.Count != 0)
            {
                foreach (CellPos endMovePos in movesWithoutEating)
                {
                    moves.Push(endMovePos);

                    movesStorage.AddLast(new MoveInfo(moves.Reverse().ToList(),
                                                      isPawnBecomeQueen,
                                                      new Dictionary<CellPos, Pawn>()));

                    moves.Pop();
                }
            }
            else
                movesStorage.AddLast(new MoveInfo(moves.Reverse().ToList(),
                                                  isPawnBecomeQueen,
                                                  eatenPawns.Reverse().ToDictionary(pair => pair.Key, pair => pair.Value)));
        }
    }
}
