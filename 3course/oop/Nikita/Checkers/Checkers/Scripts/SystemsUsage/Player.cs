﻿using System.Collections.Generic;

using Systems;

namespace SystemsUsage
{
    public class Player : ICheckersPlayer
    {
        private Game game;

        private bool NeedToMove;

        public Player(Game game)
        {
            this.game = game;

            NeedToMove = false;
        }

        public void OnMove(PawnColor color)
        {
            NeedToMove = true;
        }

        public void TryMove(List<CellPos> moves)
        {
            if (!NeedToMove)
            {
                game.ui.ThrowMessage("Wait for your turn");
                return;
            }

            try
            {
                NeedToMove = false;
                game.checkersGame.Move(this, moves);
                game.ui.UpdateWithBoard(game.checkersGame.board);
            }
            catch (NotValidMovesException e)
            {
                NeedToMove = true;
                game.ui.ThrowMessage(e.Message);
            }
        }
    }
}
