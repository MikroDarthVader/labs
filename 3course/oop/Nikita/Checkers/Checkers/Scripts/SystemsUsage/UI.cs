﻿using System.Collections.Generic;
using System.Linq;

using Checkers;

using Systems;

namespace SystemsUsage
{
    public class UI
    {
        private MainWindow mainWindow;

        public UI(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
        }

        public void ThrowMessage(string msg)
        {
            mainWindow.ThrowErrorMessage(msg);
        }

        public void UpdateWithBoard(Board board)
        {
            mainWindow.SyncWithBoard(board);
        }

        public void OnEndGame()
        {
            mainWindow.OnEndGame();
        }

        public static bool TryParseToMoves(string str, bool isWhiteDown, ref List<CellPos> moves)
        {
            LinkedList<CellPos> result = new LinkedList<CellPos>();

            if (str.Length < 5 || (str.Length + 1) % 3 != 0)
                return false;

            for (int i = 0; i < str.Length; i += 3)
            {
                if (!"ABCDEFGH".Contains(str[i]))
                    return false;

                if (!"12345678".Contains(str[i + 1]))
                    return false;

                if (i + 2 != str.Length && str[i + 2] != '-')
                    return false;

                int x = str[i] - 'A';
                int y = str[i + 1] - '1';
                result.AddLast(isWhiteDown ? new CellPos(x, y) : new CellPos(7 - x, 7 - y));
            }

            moves = result.ToList();
            return true;
        }
    }
}
