﻿using System.Collections.Generic;
using System.Linq;
using System.IO;

using Checkers;

using Systems;

namespace SystemsUsage
{
    public class Game
    {
        private static Game Instance;

        public static Game GetInstance()
        {
            if (Instance == null)
                Instance = new Game();

            return Instance;
        }

        private Game()
        { }

        private static string GameHistoryFileName = "GameHistory.txt";

        public CheckersGame checkersGame;

        public UI ui;
        public Player player;
        public Bot bot;

        public void Init(MainWindow mainWindow)
        {
            checkersGame = new CheckersGame();

            player = new Player(this);
            bot = new Bot(this);
            ui = new UI(mainWindow);
        }

        public bool Start(bool isPlayerWhite)
        {
            checkersGame.OnGameEnd += OnGameEnd;

            try
            {
                if (isPlayerWhite)
                    checkersGame.InitGame(player, bot);
                else
                    checkersGame.InitGame(bot, player);

                checkersGame.StartGame();

                return true;
            }
            catch (CheckerGameStatesException e)
            {
                ui.ThrowMessage(e.Message);
                return false;
            }
        }

        public void OnGameEnd()
        {
            WriteGameToFile(checkersGame.MovesInfo);

            ui.OnEndGame();
        }

        public bool LoadReplay(string gameName)
        {
            if (!TryLoadGameFromFile(gameName, out List<MoveInfo> movesInfo))
                return false;

            try
            {
                checkersGame.ReplayGame(movesInfo);
                return true;
            }
            catch (CheckerGameStatesException e)
            {
                ui.ThrowMessage(e.Message);
                return false;
            }
        }

        public bool CloseReplay()
        {
            try
            {
                checkersGame.StopReplay();
                return true;
            }
            catch (CheckerGameStatesException e)
            {
                ui.ThrowMessage(e.Message);
                return false;
            }
        }

        public List<string> GetAllReplayNames()
        {
            List<string> result = new List<string>();

            if (!File.Exists(GameHistoryFileName))
                return result;

            string[] lines = File.ReadAllLines(GameHistoryFileName);

            for (int i = 0; i < lines.Length; i += 2)
                result.Add(lines[i]);

            return result;
        }

        public void WriteGameToFile(List<MoveInfo> movesInfo)
        {
            int lineCount = 0;
            if (File.Exists(GameHistoryFileName))
                lineCount = File.ReadAllLines(GameHistoryFileName).Length;

            StreamWriter writer = File.AppendText(GameHistoryFileName);

            if (lineCount != 0)
                writer.WriteLine();
            writer.WriteLine("Game " + (lineCount / 2 + 1));

            for (int i = 0; i < movesInfo.Count; i++)
            {
                if (i != 0)
                    writer.Write(";");

                writer.Write(((movesInfo[i].move[0].x / 2 + 4 * movesInfo[i].move[0].y) | (movesInfo[i].isPawnBecomeQueen ? 32 : 0)) + 10);
                for (int j = 1; j < movesInfo[i].move.Count; j++)
                    writer.Write(movesInfo[i].move[j].x / 2 + 4 * movesInfo[i].move[j].y + 10);

                foreach (KeyValuePair<CellPos, Pawn> pair in movesInfo[i].eatenPawns)
                    writer.Write(((pair.Key.x / 2 + 4 * pair.Key.y) | (pair.Value.isQueen ? 32 : 0)) + 10);
            }

            writer.Close();
        }

        public bool TryLoadGameFromFile(string gameName, out List<MoveInfo> movesInfo)
        {
            movesInfo = null;

            if (!File.Exists(GameHistoryFileName))
            {
                ui.ThrowMessage("File doesn't exist");
                return false;
            }

            List<string> lines = File.ReadAllLines(GameHistoryFileName).ToList();
            int recordIndex = lines.FindIndex(str => str == gameName);
            string record = lines[recordIndex + 1];

            string[] moves = record.Split(new char[] { ';' });

            List<MoveInfo> result = new List<MoveInfo>();

            for (int i = 0; i < moves.Length; i++)
            {
                if (moves[i].Length % 2 != 0 || moves[i].Length < 4 || (moves[i].Length != 4 && moves[i].Length / 2 % 2 == 0))
                {
                    ui.ThrowMessage("Can't read file");
                    return false;
                }

                List<CellPos> move = new List<CellPos>();
                Dictionary<CellPos, Pawn> eatenPawns = new Dictionary<CellPos, Pawn>();
                bool isPawnBecomeQueen = false;

                for (int j = 0; j < (moves[i].Length == 4 ? 2 : moves[i].Length / 4 + 1); j++)
                {
                    if (!int.TryParse(moves[i].Substring(j * 2, 2), out int value))
                    {
                        ui.ThrowMessage("Can't read file");
                        return false;
                    }

                    value -= 10;

                    if (j == 0)
                    {
                        isPawnBecomeQueen = (value & 32) == 32;
                        value &= 31;
                    }

                    CellPos pos = new CellPos(value % 4 * 2 + value / 4 % 2, value / 4);

                    if (pos.x < 0 || pos.x > 7 || pos.y < 0 || pos.y > 7)
                    {
                        ui.ThrowMessage("Can't read file");
                        return false;
                    }

                    move.Add(pos);
                }

                for (int j = 0; j < (moves[i].Length == 4 ? 0 : moves[i].Length / 4); j++)
                {
                    if (!int.TryParse(moves[i].Substring(moves[i].Length - (moves[i].Length / 4 - 1 - j + 1) * 2, 2), out int value))
                    {
                        ui.ThrowMessage("Can't read file");
                        return false;
                    }

                    value -= 10;
                    Pawn pawn = new Pawn(i % 2 == 0 ? PawnColor.black : PawnColor.white, (value & 32) == 1);

                    value &= 31;
                    CellPos pos = new CellPos(value % 4 * 2 + value / 4 % 2, value / 4);

                    if (pos.x < 0 || pos.x > 7 || pos.y < 0 || pos.y > 7)
                    {
                        ui.ThrowMessage("Can't read file");
                        return false;
                    }

                    eatenPawns.Add(pos, pawn);
                }

                if (move.Count - eatenPawns.Count != 1 && (move.Count != 2 || eatenPawns.Count != 0))
                {
                    ui.ThrowMessage("Can't read file");
                    return false;
                }

                result.Add(new MoveInfo(move, isPawnBecomeQueen, eatenPawns));
            }

            CheckersGame testGame = new CheckersGame();
            testGame.ReplayGame(result);

            try
            {
                while (testGame.TrySetNextMove()) ;
            }
            catch
            {
                ui.ThrowMessage("Can't read file");
                return false;
            }

            movesInfo = result;
            return true;
        }
    }
}
