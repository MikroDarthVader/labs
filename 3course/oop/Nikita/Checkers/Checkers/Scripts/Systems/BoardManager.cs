﻿using System;
using System.Collections.Generic;
using System.Linq;

using MyMath;

namespace Systems
{
    public class BoardManager
    {
        private Board board;

        public List<MoveInfo> MovesInfo;
        private int MoveInfoIndex;

        private int InMoveInfoIndex;

        public BoardManager(Board board)
        {
            this.board = board;

            MovesInfo = new List<MoveInfo>();
        }

        public void SetupStartConfiguration(List<MoveInfo> movesInfo = null)
        {
            board.Clear();

            for (int y = 0; y < 3; y++) // setup white pawns
                for (int x = y % 2; x < 8; x += 2)
                    board.PutPawnOnBoard(new Pawn(PawnColor.white), new CellPos(x, y));
            for (int y = 5; y < 8; y++) // setup black pawns
                for (int x = y % 2; x < 8; x += 2)
                    board.PutPawnOnBoard(new Pawn(PawnColor.black), new CellPos(x, y));

            if (movesInfo == null)
                MovesInfo.Clear();
            else
                MovesInfo = movesInfo;

            MoveInfoIndex = -1;
        }

        public bool TrySetPrevMove()
        {
            if (MoveInfoIndex == -1)
                return false;

            MoveInfo moveInfo = MovesInfo[MoveInfoIndex];

            CellPos from = moveInfo.move[InMoveInfoIndex];
            CellPos to = moveInfo.move[InMoveInfoIndex - 1];

            bool isPawnBecomeQueen = false;

            if (moveInfo.isPawnBecomeQueen)
                for (int i = 0; i < moveInfo.move.Count; i++)
                    if (moveInfo.move[i].y == (board[from].color == PawnColor.white ? 7 : 0))
                    {
                        isPawnBecomeQueen = moveInfo.move[i] == from;
                        break;
                    }

            Pawn pawn = board.RemovePawnFromBoard(from);
            board.PutPawnOnBoard(new Pawn(pawn.color, isPawnBecomeQueen ? false : pawn.isQueen), to);

            int j = 0;
            foreach (KeyValuePair<CellPos, Pawn> pair in moveInfo.eatenPawns)
            {
                if (j == InMoveInfoIndex - 1)
                {
                    board.PutPawnOnBoard(pair.Value, pair.Key);
                    break;
                }

                j++;
            }

            InMoveInfoIndex--;

            if (InMoveInfoIndex == 0)
            {
                MoveInfoIndex--;

                if (MoveInfoIndex != -1)
                    InMoveInfoIndex = MovesInfo[MoveInfoIndex].move.Count - 1;
            }

            return true;
        }

        public bool TrySetNextMove()
        {
            if (MovesInfo.Count == 0 || MoveInfoIndex == MovesInfo.Count - 1 && InMoveInfoIndex == MovesInfo[MoveInfoIndex].move.Count - 1)
                return false;

            InMoveInfoIndex++;

            if (MoveInfoIndex == -1 || InMoveInfoIndex == MovesInfo[MoveInfoIndex].move.Count)
            {
                MoveInfoIndex++;
                InMoveInfoIndex = 1;
            }

            MoveInfo moveInfo = MovesInfo[MoveInfoIndex];

            CellPos from = moveInfo.move[InMoveInfoIndex - 1];
            CellPos to = moveInfo.move[InMoveInfoIndex];

            bool isPawnBecomeQueen = false;

            if (moveInfo.isPawnBecomeQueen)
                for (int i = 0; i < moveInfo.move.Count; i++)
                    if (moveInfo.move[i].y == (board[from].color == PawnColor.white ? 7 : 0))
                    {
                        isPawnBecomeQueen = moveInfo.move[i] == to;
                        break;
                    }

            Pawn pawn = board.RemovePawnFromBoard(from);
            board.PutPawnOnBoard(new Pawn(pawn.color, isPawnBecomeQueen ? true : pawn.isQueen), to);

            int j = 0;
            foreach (KeyValuePair<CellPos, Pawn> pair in moveInfo.eatenPawns)
            {
                if (j == InMoveInfoIndex - 1)
                {
                    board.RemovePawnFromBoard(pair.Key);
                    break;
                }

                j++;
            }

            return true;
        }

        public void Move(Pawn pawnToMove, List<CellPos> move)
        {
            while (MoveInfoIndex != MovesInfo.Count - 1)
                TrySetNextMove();

            if (move.Count < 2)
                throw new NotValidMovesException("not valid moves count");

            Dictionary<CellPos, Pawn> eatenPawns = new Dictionary<CellPos, Pawn>();
            bool isPawnBecomeQueen = false;

            for (int i = 0; i < move.Count - 1; i++) // iterate by moves
            {
                Vector2Int shift = move[i + 1] - move[i];

                if (Math.Abs(shift.x) != Math.Abs(shift.y))
                    throw new NotValidMovesException("not diagonal move");

                // pawn to eat research
                bool isPawnEaten = false;
                CellPos pawnToEatPos = default;

                Vector2Int dir = new Vector2Int(Math.Sign(shift.x), Math.Sign(shift.y));

                for (CellPos pos = move[i] + dir; pos != move[i + 1]; pos += dir)
                {
                    if (board.TryGetPawn(pos, out Pawn pawn) && !eatenPawns.ContainsKey(pos) && pos != move[0])
                    {
                        if (isPawnEaten)
                            throw new NotValidMovesException("attempt to eat two or more pawns in one move");

                        if (pawn.color == pawnToMove.color)
                            throw new NotValidMovesException("attempt to eat your pawn");

                        isPawnEaten = true;
                        pawnToEatPos = pos;
                    }
                }

                if (board.HasPawn(move[i + 1]))
                    throw new NotValidMovesException("attempt to move on cell that already have a pawn");

                if (isPawnEaten)
                {
                    if (Math.Abs(shift.x) > 2 && !pawnToMove.isQueen)
                        throw new NotValidMovesException("attempt to move base pawn like a queen");

                    eatenPawns.Add(pawnToEatPos, board[pawnToEatPos]);
                }
                else
                {
                    if (!pawnToMove.isQueen)
                    {
                        if (pawnToMove.color == PawnColor.white && shift.y < 0)
                            throw new NotValidMovesException("attempt to move base pawn backward dir without eating");
                        if (pawnToMove.color == PawnColor.black && shift.y > 0)
                            throw new NotValidMovesException("attempt to move base pawn backward dir without eating");

                        if (Math.Abs(shift.x) > 1)
                            throw new NotValidMovesException("attempt to move base pawn like a queen");
                    }

                    if (move.Count > 2)
                        throw new NotValidMovesException("attempting several moves without eating pawns");

                    if (IsExistsMovesWithEating(pawnToMove.color))
                        throw new NotValidMovesException("attempting moves without eating pawns if moves with eating exists");
                }

                if (!pawnToMove.isQueen && move[i + 1].y == (pawnToMove.color == PawnColor.white ? 7 : 0))
                {
                    isPawnBecomeQueen = true;
                    pawnToMove.isQueen = true;
                }
            }

            Pawn removedPawn = board.RemovePawnFromBoard(move[0]);
            board[move[move.Count - 1]] = new Pawn(pawnToMove.color, pawnToMove.isQueen);
            eatenPawns.ToList().ForEach(pair => board.RemovePawnFromBoard(pair.Key));

            if (eatenPawns.Count != 0 && IsPawnCanEat(move[move.Count - 1], board[move[move.Count - 1]]))
            {
                board.RemovePawnFromBoard(move[move.Count - 1]);
                board.PutPawnOnBoard(removedPawn, move[0]);
                eatenPawns.ToList().ForEach(pair => board.PutPawnOnBoard(pair.Value, pair.Key));

                throw new NotValidMovesException("move can't ends if pawn have opportunity to eat");
            }

            MovesInfo.Add(new MoveInfo(move, isPawnBecomeQueen, eatenPawns));
            MoveInfoIndex++;
            InMoveInfoIndex = move.Count - 1;
        }

        public bool IsExistsMovesWithEating(PawnColor color)
        {
            Dictionary<CellPos, Pawn> pawns = board.GetAllPawns(color);

            foreach (KeyValuePair<CellPos, Pawn> pair in pawns)
                if (IsPawnCanEat(pair.Key, pair.Value))
                    return true;

            return false;
        }

        public bool IsPawnCanEat(CellPos pos, Pawn pawn)
        {
            if (!pawn.isQueen)
            {
                for (int x = -1; x <= 1; x += 2)
                    for (int y = -1; y <= 1; y += 2)
                    {
                        Vector2Int shift = new Vector2Int(x, y);

                        if (CellPos.IsPosValid(pos.asVector + shift) && board.TryGetPawn(pos + shift, out Pawn tempPawn) && tempPawn.color != pawn.color &&
                            CellPos.IsPosValid(pos.asVector + shift * 2) && !board.HasPawn(pos + shift * 2))
                            return true;
                    }
            }
            else
            {
                for (int x = -1; x <= 1; x += 2)
                    for (int y = -1; y <= 1; y += 2)
                    {
                        Vector2Int shift = new Vector2Int(x, y);

                        bool isPrevEnemyPawn = false;
                        for (int i = 1; CellPos.IsPosValid(pos.asVector + shift * i); i++)
                        {
                            if (!board.TryGetPawn(pos + shift * i, out Pawn tempPawn))
                            {
                                if (!isPrevEnemyPawn)
                                    continue;
                                else
                                    return true;
                            }

                            if (tempPawn.color == pawn.color)
                                break;

                            if (isPrevEnemyPawn)
                                break;

                            isPrevEnemyPawn = true;
                        }
                    }
            }

            return false;
        }

        public bool IsPawnCanMove(CellPos pos, Pawn pawn)
        {
            int forward;
            if (pawn.color == PawnColor.white)
                forward = 1;
            else
                forward = -1;

            if (!pawn.isQueen)
            {
                for (int x = -1; x <= 1; x += 2)
                    for (int y = -1; y <= 1; y += 2)
                    {
                        Vector2Int shift = new Vector2Int(x, y);

                        if (CellPos.IsPosValid(pos.asVector + shift) && !board.HasPawn(pos + shift) && shift.y == forward)
                            return true;

                        if (CellPos.IsPosValid(pos.asVector + shift) && board.TryGetPawn(pos + shift, out Pawn tempPawn) && tempPawn.color != pawn.color &&
                            CellPos.IsPosValid(pos.asVector + shift * 2) && !board.HasPawn(pos + shift * 2))
                            return true;
                    }
            }
            else
            {
                for (int x = -1; x <= 1; x += 2)
                    for (int y = -1; y <= 1; y += 2)
                    {
                        Vector2Int shift = new Vector2Int(x, y);

                        bool isPrevEnemyPawn = false;
                        for (int i = 1; CellPos.IsPosValid(pos.asVector + shift * i); i++)
                        {
                            if (!board.TryGetPawn(pos + shift * i, out Pawn tempPawn))
                                return true;

                            if (tempPawn.color == pawn.color)
                                break;

                            if (isPrevEnemyPawn)
                                break;

                            isPrevEnemyPawn = true;
                        }
                    }
            }

            return false;
        }
    }
}
