﻿using System;

using MyMath;

namespace Systems
{
    public struct CellPos
    {
        private Vector2Int _pos;

        public Vector2Int asVector
        {
            get
            {
                return _pos;
            }
        }

        public int x
        {
            get
            {
                return _pos.x;
            }
        }

        public int y
        {
            get
            {
                return _pos.y;
            }
        }

        public CellPos(Vector2Int pos) : this(pos.x, pos.y)
        { }

        public CellPos(int x, int y)
        {
            if (!IsCoordValid(x) || !IsCoordValid(y))
                throw new NotValidCoordException();

            _pos.x = x;
            _pos.y = y;
        }

        public static CellPos operator +(CellPos left, Vector2Int right)
        {
            Vector2Int result = left._pos + right;

            if (!IsPosValid(result))
                throw new NotValidCoordException();

            return new CellPos(result);
        }

        public static Vector2Int operator -(CellPos left, CellPos right)
        {
            return left._pos - right._pos;
        }

        public static bool operator ==(CellPos left, CellPos right)
        {
            return left._pos == right._pos;
        }

        public static bool operator !=(CellPos left, CellPos right)
        {
            return left._pos != right._pos;
        }

        public static bool IsPosValid(Vector2Int pos)
        {
            return IsCoordValid(pos.x) && IsCoordValid(pos.y);
        }

        public static bool IsCoordValid(int coord)
        {
            return coord >= 0 && coord <= 7;
        }

        public override string ToString()
        {
            return _pos.ToString();
        }

        private class NotValidCoordException : Exception
        {
            public NotValidCoordException() : base("Not valid coord")
            { }
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
