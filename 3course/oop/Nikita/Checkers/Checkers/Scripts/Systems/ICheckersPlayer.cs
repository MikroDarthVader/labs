﻿
namespace Systems
{
    public interface ICheckersPlayer
    {
        void OnMove(PawnColor color);
    }
}
