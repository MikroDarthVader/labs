﻿namespace Systems
{
    public enum PawnColor { white, black }

    public struct Pawn
    {
        public readonly PawnColor color;
        public bool isQueen;

        public Pawn(PawnColor color, bool isQueen = false)
        {
            this.color = color;
            this.isQueen = isQueen;
        }

        public override string ToString()
        {
            return color + ", " + isQueen;
        }
    }
}
