﻿using System.Collections.Generic;

namespace Systems
{
    public class MoveInfo
    {
        public List<CellPos> move;
        public Dictionary<CellPos, Pawn> eatenPawns;
        public bool isPawnBecomeQueen;

        public MoveInfo(List<CellPos> move, bool isPawnBecomeQueen, Dictionary<CellPos, Pawn> eatenPawns)
        {
            this.move = move;
            this.isPawnBecomeQueen = isPawnBecomeQueen;
            this.eatenPawns = eatenPawns;
        }
    }
}
