﻿using System;
using System.Collections.Generic;

namespace Systems
{
    public class CheckersGame
    {
        public enum GameStates
        {
            NotInited,
            Inited,
            IsRunning,
            IsReplaying
        }

        public Action OnGameEnd;

        public readonly Board board;

        private BoardManager boardManager;

        private ICheckersPlayer whitePlayer;
        private ICheckersPlayer blackPlayer;

        public GameStates state { get; private set; }
        private PawnColor currMoveColor;

        public List<MoveInfo> MovesInfo
        {
            get
            {
                return boardManager.MovesInfo;
            }
        }

        public CheckersGame()
        {
            state = GameStates.NotInited;

            board = new Board();
            boardManager = new BoardManager(board);
        }

        public void InitGame(ICheckersPlayer whitePlayer, ICheckersPlayer blackPlayer)
        {
            if (state != GameStates.NotInited)
                throw new CheckerGameStatesException("Can't init game if it already inited or already started");

            boardManager.SetupStartConfiguration();

            state = GameStates.Inited;
            this.whitePlayer = whitePlayer;
            this.blackPlayer = blackPlayer;
        }

        public void StartGame()
        {
            if (state != GameStates.Inited)
                throw new CheckerGameStatesException("Can't start game if it not inited or already started");

            state = GameStates.IsRunning;
            currMoveColor = PawnColor.white;

            whitePlayer.OnMove(currMoveColor);
        }

        public void ReplayGame(List<MoveInfo> movesInfo)
        {
            if (state != GameStates.NotInited)
                throw new CheckerGameStatesException("Can't replay game if it already inited or already started");

            state = GameStates.IsReplaying;

            boardManager.SetupStartConfiguration(movesInfo);
        }

        public void StopReplay()
        {
            if (state != GameStates.IsReplaying)
                throw new CheckerGameStatesException("Can't stop replay if it not replaying");

            state = GameStates.NotInited;
        }

        public bool TrySetPrevMove()
        {
            return boardManager.TrySetPrevMove();
        }

        public bool TrySetNextMove()
        {
            return boardManager.TrySetNextMove();
        }

        public void Move(ICheckersPlayer player, List<CellPos> move)
        {
            if (state != GameStates.IsRunning)
                throw new CheckerGameStatesException("Game not started");

            if (player != whitePlayer && player != blackPlayer)
                throw new Exception("Player not from this game");

            if (currMoveColor == PawnColor.white ? player != whitePlayer : player != blackPlayer)
                throw new Exception("Now another player move");

            if (!board.TryGetPawn(move[0], out Pawn pawnToMove))
                throw new NotValidMovesException("pawn not exist");

            if (pawnToMove.color != currMoveColor)
                throw new NotValidMovesException("not valid pawn color");

            boardManager.Move(pawnToMove, move);

            AfterMove();
        }

        private void AfterMove()
        {
            Dictionary<CellPos, Pawn> pawns = board.GetAllPawns(currMoveColor == PawnColor.white ? PawnColor.black : PawnColor.white);

            if (pawns.Count == 0)
            {
                EndGame();
                return;
            }

            bool allPawnsBlocked = true;
            foreach (KeyValuePair<CellPos, Pawn> pair in pawns)
                if (boardManager.IsPawnCanMove(pair.Key, pair.Value))
                {
                    allPawnsBlocked = false;
                    break;
                }

            if (allPawnsBlocked)
            {
                EndGame();
                return;
            }

            currMoveColor = currMoveColor == PawnColor.white ? PawnColor.black : PawnColor.white;

            if (currMoveColor == PawnColor.white)
                whitePlayer.OnMove(currMoveColor);
            else
                blackPlayer.OnMove(currMoveColor);
        }

        private void EndGame()
        {
            OnGameEnd?.Invoke();
            OnGameEnd = null;

            state = GameStates.NotInited;
        }
    }
}
