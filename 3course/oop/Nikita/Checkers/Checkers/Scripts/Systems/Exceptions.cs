﻿using System;

namespace Systems
{
    public class CheckerGameStatesException : Exception
    {
        public CheckerGameStatesException() : base("Not valid states")
        { }

        public CheckerGameStatesException(string info) : base("Not valid states (" + info + ")")
        { }
    }

    public class NotValidMovesException : Exception
    {
        public NotValidMovesException() : base("Not valid moves")
        { }

        public NotValidMovesException(string info) : base("Not valid moves (" + info + ")")
        { }
    }
}
