﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Systems
{
    public class Board
    {
        private readonly Dictionary<CellPos, Pawn> Pawns;

        public Board()
        {
            Pawns = new Dictionary<CellPos, Pawn>(32);
        }

        public Pawn this[CellPos pos]
        {
            get
            {
                return Pawns[pos];
            }
            set
            {
                Pawns[pos] = value;
            }
        }

        public bool TryGetPawn(CellPos pos, out Pawn pawn)
        {
            pawn = default;

            if (pos.x % 2 != pos.y % 2 || !Pawns.TryGetValue(pos, out pawn))
                return false;

            return true;
        }

        public bool HasPawn(CellPos pos)
        {
            if (pos.x % 2 != pos.y % 2 || !Pawns.ContainsKey(pos))
                return false;

            return true;
        }

        public void PutPawnOnBoard(Pawn pawn, CellPos pos)
        {
            if (pos.x % 2 != pos.y % 2)
                throw new Exception("Not valid cell");

            if (Pawns.ContainsKey(pos))
                throw new Exception("Cell already have pawn");

            Pawns.Add(pos, pawn);
        }

        public void MovePawn(CellPos from, CellPos to)
        {
            Pawn pawn = RemovePawnFromBoard(from);
            PutPawnOnBoard(pawn, to);
        }

        public Pawn RemovePawnFromBoard(CellPos pos)
        {
            if (pos.x % 2 != pos.y % 2 || !Pawns.ContainsKey(pos))
                throw new Exception("Cell has no pawn");

            Pawn result = Pawns[pos];
            Pawns.Remove(pos);

            return result;
        }

        public Dictionary<CellPos, Pawn> GetAllPawns(PawnColor color)
        {
            return Pawns.Where(x => x.Value.color == color).ToDictionary(pair => pair.Key, pair => pair.Value);
        }

        public void Clear()
        {
            Pawns.Clear();
        }
    }
}
