﻿
namespace MyMath
{
    public struct Vector2Int
    {
        public int x;
        public int y;

        public static Vector2Int zero = new Vector2Int(0, 0);

        public Vector2Int(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public static Vector2Int operator +(Vector2Int left, Vector2Int right)
        {
            return new Vector2Int(left.x + right.x, left.y + right.y);
        }

        public static Vector2Int operator -(Vector2Int value)
        {
            return new Vector2Int(-value.x, -value.y);
        }

        public static Vector2Int operator -(Vector2Int left, Vector2Int right)
        {
            return left + (-right);
        }

        public static Vector2Int operator *(Vector2Int left, int right)
        {
            return new Vector2Int(left.x * right, left.y * right);
        }

        public static bool operator ==(Vector2Int left, Vector2Int right)
        {
            return left.x == right.x && left.y == right.y;
        }

        public static bool operator !=(Vector2Int left, Vector2Int right)
        {
            return !(left == right);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return x.ToString() + ", " + y.ToString();
        }
    }
}
