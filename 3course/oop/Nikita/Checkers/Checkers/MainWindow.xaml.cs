﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;

using SystemsUsage;
using Systems;
using System.Windows.Media;

namespace Checkers
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private Game game;
        private bool isPlayerWhite;
        private LinkedList<Ellipse> Pawns;
        private Queue<string> ErrorMessages;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            game = Game.GetInstance();
            Pawns = new LinkedList<Ellipse>();
            ErrorMessages = new Queue<string>();

            GameNameSetter.ItemsSource = game.GetAllReplayNames();
            InitBoardCells();
            game.Init(this);
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            if (!game.Start(PawnColorSetter.SelectedIndex == 0))
                return;

            isPlayerWhite = PawnColorSetter.SelectedIndex == 0;

            Moves.Visibility = Visibility.Visible;
            MoveButton.Visibility = Visibility.Visible;
            PrevMoveButton.Visibility = Visibility.Visible;
            NextMoveButton.Visibility = Visibility.Visible;

            SyncWithBoard(game.checkersGame.board);
        }

        private void LoadReplayButton_Click(object sender, RoutedEventArgs e)
        {
            if (!game.LoadReplay((string)GameNameSetter.SelectedItem))
                return;

            CloseReplayButton.Visibility = Visibility.Visible;
            PrevMoveButton.Visibility = Visibility.Visible;
            NextMoveButton.Visibility = Visibility.Visible;

            isPlayerWhite = true;
            SyncWithBoard(game.checkersGame.board);
        }

        private void CloseReplayButton_Click(object sender, RoutedEventArgs e)
        {
            if (!game.CloseReplay())
                return;

            CloseReplayButton.Visibility = Visibility.Hidden;
            PrevMoveButton.Visibility = Visibility.Hidden;
            NextMoveButton.Visibility = Visibility.Hidden;
        }

        private void PrevMoveButton_Click(object sender, RoutedEventArgs e)
        {
            if (!game.checkersGame.TrySetPrevMove())
                ThrowErrorMessage("First move already reached");
            else
                SyncWithBoard(game.checkersGame.board);
        }

        private void NextMoveButton_Click(object sender, RoutedEventArgs e)
        {
            if (!game.checkersGame.TrySetNextMove())
                ThrowErrorMessage("Last move already reached");
            else
                SyncWithBoard(game.checkersGame.board);
        }

        public void OnEndGame()
        {
            Moves.Visibility = Visibility.Hidden;
            MoveButton.Visibility = Visibility.Hidden;
            PrevMoveButton.Visibility = Visibility.Hidden;
            NextMoveButton.Visibility = Visibility.Hidden;

            GameNameSetter.ItemsSource = game.GetAllReplayNames();
        }

        private void Moves_TextChanged(object sender, TextChangedEventArgs e)
        {
            var selectionStart = Moves.SelectionStart;
            var selectionLength = Moves.SelectionLength;
            Moves.Text = Moves.Text.ToUpper();
            Moves.SelectionStart = selectionStart;
            Moves.SelectionLength = selectionLength;
        }

        private void Moves_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                List<CellPos> moves = new List<CellPos>();
                if (UI.TryParseToMoves(Moves.Text, isPlayerWhite, ref moves))
                {
                    game.player.TryMove(moves);
                    Moves.Text = "";
                    SyncWithBoard(game.checkersGame.board);
                }
                else
                    ThrowErrorMessage("Wrong format. Try something like 'E2-G4-E6'");
            }
        }

        private void MoveButton_Click(object sender, RoutedEventArgs e)
        {
            List<CellPos> moves = new List<CellPos>();
            if (UI.TryParseToMoves(Moves.Text, isPlayerWhite, ref moves))
            {
                game.player.TryMove(moves);
                Moves.Text = "";
                SyncWithBoard(game.checkersGame.board);
            }
            else
                ThrowErrorMessage("Wrong format. Try something like 'E2-G4-E6'");
        }

        private void BoardGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (game.checkersGame.state != CheckersGame.GameStates.IsRunning)
                return;

            if (e.ChangedButton == MouseButton.Left)
            {
                Point pos = e.GetPosition(BoardGrid);
                if (pos.X < 0 || pos.X > BoardGrid.Width || pos.Y < 0 || pos.Y > BoardGrid.Height)
                    return;

                Moves.Text += Moves.Text.Length != 0 ? "-" : "";

                Moves.Text += (char)('A' + (int)(pos.X / (BoardGrid.Width / 8)));
                Moves.Text += (char)('1' + 7 - (int)(pos.Y / (BoardGrid.Height / 8)));

                Moves.SelectionStart = Moves.Text.Length;
            }
        }

        public void ThrowErrorMessage(string msg)
        {
            if (ErrorText.Visibility == Visibility.Hidden)
            {
                ErrorText.Text = msg;
                ErrorText.Visibility = Visibility.Visible;
                ErrorOkButton.Visibility = Visibility.Visible;
            }
            else
                ErrorMessages.Enqueue(msg);
        }

        private void ErrorOkButton_Click(object sender, RoutedEventArgs e)
        {
            if (ErrorMessages.Count == 0)
            {
                ErrorText.Text = "";
                ErrorText.Visibility = Visibility.Hidden;
                ErrorOkButton.Visibility = Visibility.Hidden;
            }
            else
                ErrorText.Text = ErrorMessages.Dequeue();
        }

        private void InitBoardCells()
        {
            Label whiteLabel = (Label)BoardGrid.Children[0];
            Label blackLabel = (Label)BoardGrid.Children[1];

            for (int y = 0; y < 8; y++)
                for (int x = 0; x < 8; x++)
                {
                    Label cell = new Label();
                    cell.Background = (x % 2 != y % 2) ? blackLabel.Background : whiteLabel.Background;
                    cell.BorderBrush = Brushes.Black;
                    cell.BorderThickness = new Thickness(1);
                    BoardGrid.Children.Add(cell);
                    Grid.SetColumn(cell, x);
                    Grid.SetRow(cell, y);

                    Border border = new Border();
                    border.BorderThickness = 1;
                    border.BorderBrush = Color.Black;
                    BoardGrid.Children.Add(border);
                    Grid.SetColumn(border, x);
                    Grid.SetRow(border, y);
                }
        }

        public void SyncWithBoard(Board board)
        {
            foreach (Ellipse ellipse in Pawns)
                BoardGrid.Children.Remove(ellipse);
            Pawns.Clear();

            Ellipse whitePawn = (Ellipse)BoardGrid.Children[3];
            Ellipse blackPawn = (Ellipse)BoardGrid.Children[2];

            for (int x = 0; x < 8; x++)
                for (int y = 0; y < 8; y++)
                {
                    if (!board.TryGetPawn(new CellPos(x, y), out Pawn pawn))
                        continue;

                    Ellipse ellipse;
                    if (pawn.color == PawnColor.white)
                        ellipse = new Ellipse { Fill = whitePawn.Fill, Stroke = whitePawn.Stroke, Height = whitePawn.Height, Width = whitePawn.Width, Margin = whitePawn.Margin };
                    else
                        ellipse = new Ellipse { Fill = blackPawn.Fill, Stroke = blackPawn.Stroke, Height = blackPawn.Height, Width = blackPawn.Width, Margin = blackPawn.Margin };

                    BoardGrid.Children.Add(ellipse);
                    Pawns.AddLast(ellipse);

                    Grid.SetColumn(ellipse, isPlayerWhite ? x : 7 - x);
                    Grid.SetRow(ellipse, isPlayerWhite ? 7 - y : y);

                    if (pawn.isQueen)
                    {
                        if (pawn.color == PawnColor.white)
                            ellipse = new Ellipse { Fill = blackPawn.Fill, Stroke = blackPawn.Stroke, Height = blackPawn.Height / 2, Width = blackPawn.Width / 2, Margin = blackPawn.Margin };
                        else
                            ellipse = new Ellipse { Fill = whitePawn.Fill, Stroke = whitePawn.Stroke, Height = whitePawn.Height / 2, Width = whitePawn.Width / 2, Margin = whitePawn.Margin };

                        BoardGrid.Children.Add(ellipse);
                        Pawns.AddLast(ellipse);

                        Grid.SetColumn(ellipse, isPlayerWhite ? x : 7 - x);
                        Grid.SetRow(ellipse, isPlayerWhite ? 7 - y : y);
                    }
                }
        }
    }
}
