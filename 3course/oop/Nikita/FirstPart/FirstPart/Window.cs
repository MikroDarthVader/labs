﻿
namespace FirstPart
{
    abstract class Window
    {
        public float OnScreenPosByX;
        public float OnScreenPosByY;

        public float Width;
        public float Height;

        public Window(float onScreenPosByX, float onScreenPosByY, float width, float height)
        {
            OnScreenPosByX = onScreenPosByX;
            OnScreenPosByY = onScreenPosByY;

            Width = width;
            Height = height;
        }

        public abstract bool Update();
    }
}
