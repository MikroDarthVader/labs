﻿using System;

namespace FirstPart
{
    class Button : WindowElement
    {
        public Action OnButtonDown;
        public Action OnButtonUp;

        public Button(float inWindowPosByX, float inWindowPosByY) : base(inWindowPosByX, inWindowPosByY)
        {}
    }
}
