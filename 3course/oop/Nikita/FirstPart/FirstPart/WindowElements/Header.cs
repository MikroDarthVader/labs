﻿
namespace FirstPart
{
    class Header : WindowElement
    {
        public string Text;

        public Header(float inWindowPosByX, float inWindowPosByY, string text) : base(inWindowPosByX, inWindowPosByY)
        {
            Text = text;
        }
    }
}
