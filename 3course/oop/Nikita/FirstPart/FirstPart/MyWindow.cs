﻿using System;

namespace FirstPart
{
    class MyWindow : Window
    {
        private Header header;
        private Button button;

        public Header Header
        {
            get
            {
                return header;
            }
        }

        public Button Button
        {
            get
            {
                return button;
            }
        }

        public MyWindow(float onScreenPosByX, float onScreenPosByY, float width, float height) : base(onScreenPosByX, onScreenPosByY, width, height)
        {
            header = new Header(0, 0, "header");

            button = new Button(100, 100);
            button.OnButtonDown += ThrowMessage;
        }

        public override bool Update()
        {
            if (Console.KeyAvailable) // instead of checking if the button was pressed
            {
                ConsoleKeyInfo key = Console.ReadKey();
                Console.Write("\r"); // delete symbol from console

                if (key.Key == ConsoleKey.Escape)
                    return false;

                button.OnButtonDown?.Invoke();
            }

            return true;
        }

        private void ThrowMessage()
        {
            Console.WriteLine("onButtonDown");
        }
    }
}
