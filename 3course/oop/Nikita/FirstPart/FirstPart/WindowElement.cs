﻿
namespace FirstPart
{
    abstract class WindowElement
    {
        public float InWindowPosByX;
        public float InWindowPosByY;

        public WindowElement(float inWindowPosByX, float inWindowPosByY)
        {
            InWindowPosByX = inWindowPosByX;
            InWindowPosByY = inWindowPosByY;
        }
    }
}
