﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ThirdPart
{
    class Program
    {
        static void Main()
        {
            OrientedGraph<char> graph = new OrientedGraph<char>();
            FillGraph(graph);

            if (graph.TryFindHamiltonianCycle('b', out List<char> nodes, out List<float> edges))
            {
                Console.Write("result: ");
                for (int i = 0; i < nodes.Count - 1; i++)
                    Console.Write(nodes[i] + "-");
                Console.WriteLine(nodes.Last());
            }
            else
            {
                Console.WriteLine("Hamilton cycle not found");
            }
        }

        static void FillGraph(OrientedGraph<char> graph)
        {
            graph.AddEdge('a', 'b', 3);
            graph.AddEdge('a', 'g', 8);
            graph.AddEdge('a', 'f', 1);
            graph.AddEdge('b', 'a', 8);
            graph.AddEdge('b', 'c', 1);
            graph.AddEdge('b', 'g', 7);
            graph.AddEdge('c', 'b', 5);
            graph.AddEdge('c', 'g', 8);
            graph.AddEdge('c', 'd', 8);
            graph.AddEdge('d', 'c', 4);
            graph.AddEdge('d', 'g', 5);
            graph.AddEdge('d', 'f', 7);
            graph.AddEdge('f', 'd', 3);
            graph.AddEdge('f', 'g', 4);
            graph.AddEdge('f', 'a', 8);
            graph.AddEdge('g', 'a', 4);
            graph.AddEdge('g', 'b', 3);
            graph.AddEdge('g', 'c', 1);
            graph.AddEdge('g', 'd', 8);
            graph.AddEdge('g', 'f', 7);
        }
    }
}
