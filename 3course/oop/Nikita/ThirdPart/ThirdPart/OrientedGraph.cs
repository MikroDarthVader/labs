﻿using System;
using System.Collections.Generic;

namespace ThirdPart
{
    public class OrientedGraphNode<T>
    {
        public T value;

        public Dictionary<OrientedGraphNode<T>, float> prevNodes;
        public Dictionary<OrientedGraphNode<T>, float> nextNodes;

        public OrientedGraphNode(T value)
        {
            this.value = value;

            prevNodes = new Dictionary<OrientedGraphNode<T>, float>();
            nextNodes = new Dictionary<OrientedGraphNode<T>, float>();
        }
    }

    public class OrientedGraph<T>
    {
        private List<OrientedGraphNode<T>> Nodes;

        public OrientedGraph()
        {
            Nodes = new List<OrientedGraphNode<T>>();
        }

        public void AddNode(T value)
        {
            if (Nodes.Exists(node => node.value.Equals(value)))
                throw new ArgumentException("Node already exist");

            Nodes.Add(new OrientedGraphNode<T>(value));
        }

        public void DeleteNode(T value)
        {
            OrientedGraphNode<T> nodeToDelete = Nodes.Find(node => node.value.Equals(value));

            if (nodeToDelete == null)
                throw new ArgumentException("Node doesn't exist");

            foreach (OrientedGraphNode<T> node in nodeToDelete.prevNodes.Keys)
                node.nextNodes.Remove(nodeToDelete);
            foreach (OrientedGraphNode<T> node in nodeToDelete.nextNodes.Keys)
                node.prevNodes.Remove(nodeToDelete);

            Nodes.Remove(nodeToDelete);
        }

        public void AddEdge(T fromValue, T toValue, float edgeValue)
        {
            OrientedGraphNode<T> from = Nodes.Find(node => node.value.Equals(fromValue));
            OrientedGraphNode<T> to = Nodes.Find(node => node.value.Equals(toValue));

            if (from == null)
            {
                from = new OrientedGraphNode<T>(fromValue);
                Nodes.Add(from);
            }
            if (to == null)
            {
                to = new OrientedGraphNode<T>(toValue);
                Nodes.Add(to);
            }

            if (from.nextNodes.ContainsKey(to))
                throw new ArgumentException("Edge already exist");

            from.nextNodes.Add(to, edgeValue);
            to.prevNodes.Add(from, edgeValue);
        }

        public void DeleteEdge(T fromValue, T toValue)
        {
            OrientedGraphNode<T> from = Nodes.Find(node => node.value.Equals(fromValue));
            OrientedGraphNode<T> to = Nodes.Find(node => node.value.Equals(toValue));

            if (from == null)
                throw new ArgumentException("Node with value == fromValue doesn't exist");
            if (to == null)
                throw new ArgumentException("Node with value == toValue doesn't exist");

            if (!from.nextNodes.ContainsKey(to))
                throw new ArgumentException("Edge doesn't exist");

            from.nextNodes.Remove(to);
            to.prevNodes.Remove(from);
        }

        public bool TryFindHamiltonianCycle(T firstValue, out List<T> nodes, out List<float> edges)
        {
            nodes = new List<T>();
            edges = new List<float>();

            OrientedGraphNode<T> firstNode = Nodes.Find(node => node.value.Equals(firstValue));
            if (firstNode == null)
                throw new ArgumentException("Node with value == firstValue doesn't exist");

            nodes.Add(firstNode.value);

            LinkedList<OrientedGraphNode<T>> PassedNodes = new LinkedList<OrientedGraphNode<T>>();

            OrientedGraphNode<T> currNode = null;

            float min = float.MaxValue;
            foreach (KeyValuePair<OrientedGraphNode<T>, float> pair in firstNode.nextNodes)
                if (pair.Value < min)
                {
                    min = pair.Value;
                    currNode = pair.Key;
                }

            if (currNode == null && Nodes.Count != 1)
            {
                nodes.Clear();
                edges.Clear();
                return false;
            }

            edges.Add(min);
            nodes.Add(currNode.value);
            PassedNodes.AddLast(currNode);

            while (PassedNodes.Count != Nodes.Count)
            {
                OrientedGraphNode<T> nextNode = null;
                min = float.MaxValue;
                foreach (KeyValuePair<OrientedGraphNode<T>, float> pair in currNode.nextNodes)
                    if (!PassedNodes.Contains(pair.Key) && pair.Key != firstNode && pair.Value < min)
                    {
                        min = pair.Value;
                        nextNode = pair.Key;
                    }

                if (nextNode == null)
                {
                    min = float.MaxValue;
                    foreach (KeyValuePair<OrientedGraphNode<T>, float> pair in currNode.nextNodes)
                        if (!PassedNodes.Contains(pair.Key) && pair.Value < min)
                        {
                            min = pair.Value;
                            nextNode = pair.Key;
                        }
                }

                if (nextNode == null && (currNode != firstNode || PassedNodes.Count != Nodes.Count))
                {
                    nodes.Clear();
                    edges.Clear();
                    return false;
                }

                edges.Add(min);
                nodes.Add(nextNode.value);
                PassedNodes.AddLast(nextNode);

                currNode = nextNode;
            }

            return true;
        }
    }
}
