﻿
using Libraries.JSONReader;
using Libraries.SimulationBaseSystem;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Systems
{
    public class EthernetModel : SimulationModel
    {
        private uint _deltaTime;

        public override uint DeltaTime => _deltaTime;

        public override string TimeUnit => "nc";

        public List<NetworkDevice> devices;
        public List<Communicator> communicators;
        public LinkedList<Frame> frames;

        public EthernetModel(uint deltaTime = 1)
        {
            _deltaTime = deltaTime;

            devices = new List<NetworkDevice>();
            communicators = new List<Communicator>();
            frames = new LinkedList<Frame>();
        }

        public override void Init()
        {
            for (int i = 0; i < devices.Count; i++)
            {
                devices[i].OnFrameCreate += delegate (Frame frame) { lock (frames) { frames.AddLast(frame); } };
                devices[i].Init();
            }
            for (int i = 0; i < communicators.Count; i++)
            {
                communicators[i].OnFrameCreate += delegate (Frame frame) { lock (frames) { frames.AddLast(frame); } };
                communicators[i].Init();
            }
        }

        public void Clear()
        {
            devices.Clear();
            communicators.Clear();
            frames.Clear();
        }

        public override void NextStep()
        {
            foreach (NetworkDevice device in devices)
                device.Update(_deltaTime);

            foreach (Communicator communicator in communicators)
                communicator.Update(_deltaTime);

            lock (frames)
            {
                frames = new LinkedList<Frame>(frames.Where(frame => !frame.deprecated));

                foreach (Frame frame in new LinkedList<Frame>(frames))
                    frame.Update(_deltaTime);
            }
        }

        public bool ReadFromJSONData(JSONDataSet data, out string errorMessage)
        {
            errorMessage = null;

            if (data.TryGetData("Network devices", out var devicesData))
            {
                foreach (JSONData deviceData in devicesData)
                {
                    NetworkDevice newDevice = null;

                    if (deviceData.TryGetData<int>("MAC-address", out var MAC))
                        newDevice = new NetworkDevice(MAC);
                    else
                    {
                        errorMessage = "All devices must have a MAC address";
                        return false;
                    }

                    if (deviceData.TryGetData<bool>("Send frames", out var sendFrames))
                        newDevice.SendFrames = sendFrames;
                    if (deviceData.TryGetData<uint>("Time between frames", out var betweenFramesTime))
                        newDevice.BetweenFramesTime = betweenFramesTime;
                    if (deviceData.TryGetData<JSONDataSet>("Frame destination MAC-addresses", out var targetMACs))
                        newDevice.TargetMACs = targetMACs.Select(element => element.GetValue<int>()).ToList();

                    if (deviceData.TryGetData<bool>("Use STP", out var STP))
                        newDevice.STP = STP;
                    if (deviceData.TryGetData<uint>("Time between BPDU frames on init", out var BPDUFramesDelayInit))
                        newDevice.BPDUFramesDelayInit = BPDUFramesDelayInit;
                    if (deviceData.TryGetData<uint>("Time between BPDU frames after init", out var BPDUFramesDelayWork))
                        newDevice.BPDUFramesDelayWork = BPDUFramesDelayWork;
                    if (deviceData.TryGetData<uint>("End init time", out var timeToInit))
                        newDevice.TimeToInit = timeToInit;

                    devices.Add(newDevice);
                }
            }

            if (data.TryGetData("Communicators", out var commsData))
            {
                foreach (JSONData commData in commsData)
                {
                    if (!commData.TryGetData<int>("MAC-address", out var MAC))
                    {
                        errorMessage = "All communicators must have a MAC address";
                        return false;
                    }

                    Communicator newComm;

                    if (commData.TryGetData<int>("Ports number", out var portsCount))
                        newComm = new Communicator(MAC, portsCount);
                    else
                        newComm = new Communicator(MAC);

                    if (commData.TryGetData<bool>("Use STP", out var STP))
                        newComm.STP = STP;
                    if (commData.TryGetData<uint>("Priority", out var priority))
                        newComm.Priority = priority;
                    if (commData.TryGetData<uint>("Time between BPDU frames on init", out var BPDUFramesDelayInit))
                        newComm.BPDUFramesDelayInit = BPDUFramesDelayInit;
                    if (commData.TryGetData<uint>("Time between BPDU frames after init", out var BPDUFramesDelayWork))
                        newComm.BPDUFramesDelayWork = BPDUFramesDelayWork;
                    if (commData.TryGetData<uint>("End init time", out var timeToInit))
                        newComm.TimeToInit = timeToInit;

                    communicators.Add(newComm);
                }
            }

            LinkedList<int> MACs = new LinkedList<int>();
            for (int i = 0; i < devices.Count; i++)
            {
                if (MACs.Contains(devices[i].MAC))
                {
                    errorMessage = "MAC addresses must be unique";
                    return false;
                }

                MACs.AddLast(devices[i].MAC);
            }

            for (int i = 0; i < communicators.Count; i++)
            {
                if (MACs.Contains(communicators[i].MAC))
                {
                    errorMessage = "MAC addresses must be unique";
                    return false;
                }

                MACs.AddLast(communicators[i].MAC);
            }

            if (data.TryGetData("Cables", out var cablesData))
            {
                foreach (JSONData cableData in cablesData)
                {
                    if (!cableData.TryGetData<float>("Length", out var length))
                    {
                        errorMessage = "All cables must have length";
                        return false;
                    }
                    var cable = new TwistedPair(length);

                    if (cableData.TryGetData<int>("First connected MAC-address", out var MAC_from))
                    {
                        Port portFrom = null;

                        NetworkDevice device;
                        if ((device = devices.Find(x => x.MAC == MAC_from)) != null)
                            portFrom = device.port;
                        Communicator comm;
                        if ((comm = communicators.Find(x => x.MAC == MAC_from)) != null)
                            portFrom = comm.GetEmptyPort();

                        if (portFrom != null)
                        {
                            portFrom.Cable = cable;
                            cable.firstPort = portFrom;
                        }
                    }

                    if (cableData.TryGetData<int>("Second connected MAC-address", out var MAC_to))
                    {
                        Port portTo = null;

                        NetworkDevice device;
                        if ((device = devices.Find(x => x.MAC == MAC_to)) != null)
                            portTo = device.port;
                        Communicator comm;
                        if ((comm = communicators.Find(x => x.MAC == MAC_to)) != null)
                            portTo = comm.GetEmptyPort();

                        if (portTo != null)
                        {
                            portTo.Cable = cable;
                            cable.secondPort = portTo;
                        }
                    }

                    if (cable.firstPort == null || cable.secondPort == null)
                    {
                        errorMessage = "Cable from MAC " + MAC_from + " to MAC " + MAC_to + " cannot be connected";
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
