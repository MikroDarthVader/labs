﻿using System;

namespace Systems
{
    public class Port
    {
        public bool Active;

        public TwistedPair Cable;

        public Action<Frame> OnFrameGet;

        public Port()
        {
            Active = false;
        }

        public void SendFrame(Frame frame)
        {
            if (Cable == null)
                return;

            Active = true;

            frame.transmissible = true;
            frame.fromPort = this;
            frame.toPort = Cable.firstPort != this ? Cable.firstPort : Cable.secondPort;
            frame.cable = Cable;
            frame.transmitionTime = 0;
        }
    }
}
