﻿using System;
using System.Collections.Generic;

namespace Systems
{
    public class NetworkDevice
    {
        public readonly int MAC;

        public bool SendFrames;
        public uint BetweenFramesTime;
        public List<int> TargetMACs;

        public Port port;
        public Queue<Frame> portBuffer;

        public Action<Frame> OnFrameCreate;

        public bool STP;
        public uint BPDUFramesDelayInit;
        public uint BPDUFramesDelayWork;
        public uint TimeToInit;

        public bool STPInited;

        public NetworkDevice(int MAC)
        {
            this.MAC = MAC;
            SendFrames = false;
            BetweenFramesTime = 20;
            TargetMACs = new List<int>();

            STP = false;
            BPDUFramesDelayInit = 100;
            BPDUFramesDelayWork = 400;
            TimeToInit = 1000;
            STPInited = false;

            port = new Port();
            port.OnFrameGet += ProcessFrame;
            portBuffer = new Queue<Frame>();

            targetIndex = 0;
        }

        public void Init()
        {
            timeToInitEnd = TimeToInit;
            BPDUFramesDelay = BPDUFramesDelayInit;
        }

        private uint timeToInitEnd;
        private uint BPDUFramesDelay;
        private uint timeToNextBPDUSignal;

        private int targetIndex;
        private uint timeToNextSignal;
        public void Update(uint deltaTime)
        {
            if (STP)
            {
                if (!STPInited)
                    if (timeToInitEnd < deltaTime)
                    {
                        BPDUFramesDelay = BPDUFramesDelayWork;
                        STPInited = true;
                    }
                    else
                        timeToInitEnd -= deltaTime;

                if (timeToNextBPDUSignal < deltaTime)
                {
                    timeToNextBPDUSignal = BPDUFramesDelay;

                    Frame frame = new Frame();
                    frame.BPDU = true;
                    frame.FromDevice = true;
                    frame.SetToPortDesignated = true;

                    portBuffer.Enqueue(frame);
                    OnFrameCreate?.Invoke(frame);
                }
                else
                    timeToNextBPDUSignal -= deltaTime;
            }

            if ((!STP || STPInited) && SendFrames)
                if (timeToNextSignal < deltaTime)
                {
                    if (port.Active)
                        return;

                    Frame frame = new Frame
                    {
                        MAC_from = MAC,
                        MAC_to = TargetMACs.Count != 0 ? TargetMACs[targetIndex] : -1
                    };

                    if (TargetMACs.Count != 0)
                        targetIndex = (targetIndex + 1) % TargetMACs.Count;

                    portBuffer.Enqueue(frame);
                    OnFrameCreate?.Invoke(frame);

                    timeToNextSignal = BetweenFramesTime;
                }
                else
                    timeToNextSignal -= deltaTime;

            if (portBuffer.Count != 0 && !port.Active)
                port.SendFrame(portBuffer.Dequeue());
        }

        private void ProcessFrame(Frame frame)
        {
            frame.deprecated = true;

            if (frame.MAC_to != MAC || frame.MAC_to == -1 || frame.BPDU)
                return;

            // something important
        }
    }
}
