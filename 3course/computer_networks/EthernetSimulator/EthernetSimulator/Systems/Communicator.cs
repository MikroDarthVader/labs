﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Systems
{
    public class Communicator
    {
        public readonly int MAC;

        public struct MAC_table_row
        {
            public int MAC;
            public int portIndex;

            public MAC_table_row(int MAC, int portIndex)
            {
                this.MAC = MAC;
                this.portIndex = portIndex;
            }

            public static bool operator ==(MAC_table_row left, MAC_table_row right)
            {
                return left.MAC == right.MAC && left.portIndex == right.portIndex;
            }

            public static bool operator !=(MAC_table_row left, MAC_table_row right)
            {
                return !(left == right);
            }
        }

        public List<Port> Ports;
        public List<Queue<Frame>> PortsBuffer;

        public LinkedList<MAC_table_row> MAC_table;

        public Action<Frame> OnFrameCreate;

        public bool STP;
        public uint Priority;
        public uint BPDUFramesDelayInit;
        public uint BPDUFramesDelayWork;
        public uint TimeToInit;

        public int RootCommMAC;
        public uint RootCommPriority;
        public int ToRootCost;
        public int RootPortInd;
        public LinkedList<int> DesignatedPortsInd;
        public bool STPInited;

        public Communicator(int MAC, int portsCount = 0)
        {
            this.MAC = MAC;

            Ports = new List<Port>();
            for (int i = 0; i < portsCount; i++)
            {
                Ports.Add(new Port());
                Ports.Last().OnFrameGet += ProcessFrame;
            }

            PortsBuffer = new List<Queue<Frame>>();
            for (int i = 0; i < portsCount; i++)
                PortsBuffer.Add(new Queue<Frame>());

            MAC_table = new LinkedList<MAC_table_row>();

            STP = false;
            Priority = 0x8000;
            RootCommMAC = MAC;
            RootCommPriority = Priority;
            ToRootCost = 0;
            RootPortInd = -1;
            DesignatedPortsInd = new LinkedList<int>();
            BPDUFramesDelayInit = 100;
            BPDUFramesDelayWork = 400;
            TimeToInit = 1000;
            STPInited = false;
        }

        public void Init()
        {
            timeToInitEnd = TimeToInit;
            BPDUFramesDelay = BPDUFramesDelayInit;

            if (STP)
                for (int i = 0; i < Ports.Count; i++)
                    DesignatedPortsInd.AddLast(i);
        }

        private uint timeToInitEnd;
        private uint BPDUFramesDelay;
        private uint timeToNextBPDUSignal;
        public void Update(uint deltaTime)
        {
            if (STP)
            {
                if (!STPInited)
                    if (timeToInitEnd < deltaTime)
                    {
                        BPDUFramesDelay = BPDUFramesDelayWork;
                        STPInited = true;
                    }
                    else
                        timeToInitEnd -= deltaTime;

                if (timeToNextBPDUSignal < deltaTime)
                {
                    timeToNextBPDUSignal = BPDUFramesDelay;

                    for (int i = 0; i < PortsBuffer.Count; i++)
                    {
                        Frame frame = new Frame();
                        frame.BPDU = true;
                        frame.RootCommMAC = RootCommMAC;
                        frame.RootCommPriority = RootCommPriority;
                        frame.ToRootCost = ToRootCost;
                        frame.CurrentCost = 1;
                        if (RootPortInd == i)
                            frame.SetToPortDesignated = true;

                        PortsBuffer[i].Enqueue(frame);
                        OnFrameCreate?.Invoke(frame);
                    }
                }
                else
                    timeToNextBPDUSignal -= deltaTime;
            }

            for (int i = 0; i < PortsBuffer.Count; i++)
            {
                if (PortsBuffer[i].Count != 0 && !Ports[i].Active)
                    Ports[i].SendFrame(PortsBuffer[i].Dequeue());
            }
        }

        public Port GetEmptyPort()
        {
            Port foundEmpty = Ports.Find(x => x.Cable == null);
            if (foundEmpty != null)
                return foundEmpty;

            Ports.Add(new Port());
            Ports.Last().OnFrameGet += ProcessFrame;

            PortsBuffer.Add(new Queue<Frame>());

            return Ports.Last();
        }

        private void ProcessFrame(Frame frame)
        {
            int toPortIndex = Ports.FindIndex(port => port == frame.toPort);

            if (frame.BPDU)
            {
                if (!frame.FromDevice)
                {
                    if (frame.RootCommPriority < RootCommPriority || frame.RootCommMAC < RootCommMAC)
                    {
                        RootCommPriority = frame.RootCommPriority;
                        RootCommMAC = frame.RootCommMAC;
                        DesignatedPortsInd.Clear();
                    }

                    if (MAC != RootCommMAC)
                    {
                        if (RootPortInd == -1 || frame.ToRootCost + frame.CurrentCost < ToRootCost)
                        {
                            RootPortInd = toPortIndex;
                            ToRootCost = frame.ToRootCost + frame.CurrentCost;
                        }
                    }
                }

                if (frame.SetToPortDesignated)
                    DesignatedPortsInd.AddLast(toPortIndex);
            }
            else
            {
                MAC_table_row newRow = new MAC_table_row(frame.MAC_from, toPortIndex);
                if (!MAC_table.Contains(newRow))
                    MAC_table.AddLast(newRow);

                List<MAC_table_row> whereResult = MAC_table.Where(row => row.MAC == frame.MAC_to &&
                                                                         (!STP || row.portIndex == RootPortInd || DesignatedPortsInd.Contains(row.portIndex))).ToList();
                if (whereResult.Count != 0)
                {
                    foreach (MAC_table_row row in whereResult)
                    {
                        Frame newFrame = new Frame();
                        newFrame.MAC_from = frame.MAC_from;
                        newFrame.MAC_to = frame.MAC_to;
                        PortsBuffer[row.portIndex].Enqueue(newFrame);
                        OnFrameCreate?.Invoke(newFrame);
                    }
                }
                else
                {
                    for (int i = 0; i < Ports.Count; i++)
                        if (i != toPortIndex && (!STP || i == RootPortInd || DesignatedPortsInd.Contains(i)))
                        {
                            Frame newFrame = new Frame();
                            newFrame.MAC_from = frame.MAC_from;
                            newFrame.MAC_to = frame.MAC_to;
                            PortsBuffer[i].Enqueue(newFrame);
                            OnFrameCreate?.Invoke(newFrame);
                        }
                }
            }

            frame.deprecated = true;
        }
    }
}
