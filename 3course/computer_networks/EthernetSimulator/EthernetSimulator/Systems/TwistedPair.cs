﻿

namespace Systems
{
    public class TwistedPair
    {
        public float Length; // in centimeter
        public Port firstPort;
        public Port secondPort;

        public TwistedPair(float length)
        {
            Length = length;
        }
    }
}
