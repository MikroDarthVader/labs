﻿

namespace Systems
{
    public class Frame
    {
        public bool deprecated;
        public bool transmissible;

        public int MAC_from;
        public int MAC_to;

        public bool BPDU;
        public int RootCommMAC;
        public uint RootCommPriority;
        public int ToRootCost;
        public int CurrentCost;
        public bool SetToPortDesignated;
        public bool FromDevice;

        public Port fromPort;
        public Port toPort;
        public TwistedPair cable;
        public uint transmitionTime;

        public Frame()
        {
            deprecated = false;
            transmissible = false;

            BPDU = false;
        }

        public void Update(uint deltaTime)
        {
            if (!transmissible)
                return;

            transmitionTime += deltaTime;

            if (transmitionTime * 20 >= cable.Length)
            {
                transmissible = false;

                fromPort.Active = false;
                if (cable.firstPort != fromPort)
                    cable.firstPort.OnFrameGet?.Invoke(this);
                else
                    cable.secondPort.OnFrameGet?.Invoke(this);
            }
        }
    }
}
