﻿
using System;

namespace Libraries.SimulationBaseSystem
{
    public class SimulationManager
    {
        public SimulationModel model;

        private uint CurrentStep;

        public ulong GetCurrentObjTime()
        {
            return CurrentStep * (ulong)model.DeltaTime;
        }

        public string GetTimeUnit()
        {
            return model.TimeUnit;
        }

        public SimulationManager()
        { }

        public SimulationManager(SimulationModel model)
        {
            this.model = model;
            Init();
        }

        public void Init()
        {
            model.Init();
            CurrentStep = 0;
        }

        public void NextStep()
        {
            try
            {
                checked
                {
                    CurrentStep++;
                }
            }
            catch
            {
                return;
            }

            model.NextStep();
        }
    }
}