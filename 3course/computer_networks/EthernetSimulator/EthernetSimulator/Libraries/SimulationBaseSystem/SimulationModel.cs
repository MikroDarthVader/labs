﻿
namespace Libraries.SimulationBaseSystem
{
    public abstract class SimulationModel
    {
        public abstract uint DeltaTime { get; }

        public abstract string TimeUnit { get; }

        public abstract void Init();

        public abstract void NextStep();
    }
}
