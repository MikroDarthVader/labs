﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace Libraries.DataStructures
{
    public class GraphNode<T, K>
    {
        public T value;

        public Dictionary<GraphNode<T, K>, K> nodes;

        public GraphNode(T value)
        {
            this.value = value;

            nodes = new Dictionary<GraphNode<T, K>, K>();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T">node value type</typeparam>
    /// <typeparam name="K">edge value type</typeparam>
    public class Graph<T, K>
    {
        public List<GraphNode<T, K>> Nodes;

        public Graph()
        {
            Nodes = new List<GraphNode<T, K>>();
        }

        public void AddNode(T value)
        {
            Nodes.Add(new GraphNode<T, K>(value));
        }

        public void DeleteNode(GraphNode<T, K> nodeToDelete)
        {
            foreach (GraphNode<T, K> node in nodeToDelete.nodes.Keys)
                node.nodes.Remove(nodeToDelete);

            Nodes.Remove(nodeToDelete);
        }

        public void AddEdge(GraphNode<T, K> from, GraphNode<T, K> to, K edgeValue)
        {
            if (from.nodes.ContainsKey(to))
                throw new ArgumentException("Edge already exist");

            from.nodes.Add(to, edgeValue);
            to.nodes.Add(from, edgeValue);
        }

        public void DeleteEdge(GraphNode<T, K> from, GraphNode<T, K> to)
        {
            if (!from.nodes.ContainsKey(to))
                throw new ArgumentException("Edge doesn't exist");

            from.nodes.Remove(to);
            to.nodes.Remove(from);
        }

        public bool TryGetEdgeValue(GraphNode<T, K> from, GraphNode<T, K> to, out K edgeValue)
        {
            if (!from.nodes.TryGetValue(to, out edgeValue))
                return false;

            return true;
        }

        public void DeleteAllEdges()
        {
            for (int i = 0; i < Nodes.Count; i++)
                Nodes[i].nodes.Clear();
        }

        public void Clear()
        {
            Nodes.Clear();
        }
    }
}
