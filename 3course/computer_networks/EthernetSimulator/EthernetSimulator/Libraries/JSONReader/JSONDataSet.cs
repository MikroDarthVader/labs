﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libraries.JSONReader
{
    public class JSONDataSet : List<JSONData>
    {
        public JSONData this[string name]
        {
            get => Find(data => data.name == name);
        }

        public bool TryGetData(string name, out JSONData data)
        {
            int index = FindIndex(d => d.name == name);
            if (index != -1)
            {
                data = this[index];
                return true;
            }
            data = default;
            return false;
        }
    }
}
