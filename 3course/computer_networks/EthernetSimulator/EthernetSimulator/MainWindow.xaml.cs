﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Libraries.SimulationBaseSystem;
using Libraries.DataStructures;
using Systems;
using System.Threading;
using System.Windows.Threading;
using System.Windows.Controls;

using Frame = Systems.Frame;
using System.IO;
using Libraries.JSONReader;

namespace EthernetSimulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private static uint deltaTime = 1;

        private class NodeInfo
        {
            public bool isCommunicator;
            public Communicator communicator;
            public NetworkDevice device;

            public Ellipse visualElement;
        }

        private class EdgeInfo
        {
            public TwistedPair cable;

            public Line visualElement;
        }

        private SimulationManager simulation;

        private Timer simulationThread;
        private bool isThreadPaused;

        private Dictionary<TwistedPair, EdgeInfo> cablesTable;
        private Dictionary<Port, NodeInfo> portsTable;
        private Graph<NodeInfo, EdgeInfo> drawableEthernet;

        private LinkedList<Ellipse> frameEllipses;
        private LinkedList<Ellipse> portsStatusEllipses;

        private Dictionary<Shape, Point> NodesRelativePositions;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            simulation = new SimulationManager();

            cablesTable = new Dictionary<TwistedPair, EdgeInfo>();
            portsTable = new Dictionary<Port, NodeInfo>();
            drawableEthernet = new Graph<NodeInfo, EdgeInfo>();

            frameEllipses = new LinkedList<Ellipse>();
            portsStatusEllipses = new LinkedList<Ellipse>();

            NodesRelativePositions = new Dictionary<Shape, Point>();

            simulationThread = new Timer(new TimerCallback(delegate
            {
                simulation.NextStep();

                Dispatcher.BeginInvoke(new Action(() =>
                {
                    AfterSimulationStep();
                }), DispatcherPriority.Background);
            }));

            simulationThread.Change(Timeout.Infinite, Timeout.Infinite);

            isThreadPaused = true;
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            if (simulation.model == null)
                return;

            if (isThreadPaused)
            {
                if (!int.TryParse(StepTimeEnter.Text, out int stepTime))
                    return;

                simulationThread.Change(0, stepTime);
                isThreadPaused = false;
            }
            else
            {
                Pause_Click(null, null);
                Start_Click(sender, e);
            }
        }

        private void Pause_Click(object sender, RoutedEventArgs e)
        {
            if (simulation.model == null)
                return;

            if (!isThreadPaused)
            {
                simulationThread.Change(Timeout.Infinite, Timeout.Infinite);
                isThreadPaused = true;
            }
        }

        private void NextIter_Click(object sender, RoutedEventArgs e)
        {
            if (simulation.model == null)
                return;

            if (isThreadPaused)
            {
                simulation.NextStep();
                AfterSimulationStep();
            }
        }

        private void AfterSimulationStep()
        {
            if (Show_port_checkBox.IsChecked == true)
                DrawPortsStatus();
            DrawFrames();

            string content = simulation.GetCurrentObjTime().ToString();

            if (content.Length < 10)
                while (content.Length != 10)
                    content = content.Insert(0, "0");

            content = content.Insert(content.Length - 9, ",");

            SimulationTime.Content = content + " s";
        }

        private void InitDrawing(EthernetModel model, JSONDataSet data)
        {
            Dictionary<Port, NodeInfo> newPortsTable = new Dictionary<Port, NodeInfo>();
            Graph<NodeInfo, EdgeInfo> newDrawableEthernet = new Graph<NodeInfo, EdgeInfo>();

            for (int i = 0; i < model.devices.Count; i++)
            {
                NodeInfo nodeInfo = new NodeInfo();
                nodeInfo.isCommunicator = false;
                nodeInfo.device = model.devices[i];
                nodeInfo.visualElement = new Ellipse()
                {
                    Fill = Brushes.ForestGreen,
                    Width = 50,
                    Height = 50
                };
                SetupMarginOnDisplay(nodeInfo.visualElement, new Point(0.25, 0.5));
                nodeInfo.visualElement.MouseLeftButtonDown += new MouseButtonEventHandler(delegate (object sender, MouseButtonEventArgs e)
                {
                    if (!isThreadPaused)
                        return;

                    var device = newDrawableEthernet.Nodes.Find(node => node.value.visualElement == (Ellipse)sender).value.device;

                    communicatorInfo.Visibility = Visibility.Hidden;

                    NetDev_MAC_label.Content = "MAC: " + device.MAC;
                    deviceInfo.Visibility = Visibility.Visible;

                    MovingShape = (Shape)sender;
                    Shift = e.GetPosition((Shape)sender);
                });

                newDrawableEthernet.AddNode(nodeInfo);

                newPortsTable.Add(model.devices[i].port, nodeInfo);
            }

            for (int i = 0; i < model.communicators.Count; i++)
            {
                NodeInfo nodeInfo = new NodeInfo();
                nodeInfo.isCommunicator = true;
                nodeInfo.communicator = model.communicators[i];
                nodeInfo.visualElement = new Ellipse()
                {
                    Fill = Brushes.DeepSkyBlue,
                    Width = 50,
                    Height = 50,
                };
                SetupMarginOnDisplay(nodeInfo.visualElement, new Point(0.75, 0.5));
                nodeInfo.visualElement.MouseLeftButtonDown += new MouseButtonEventHandler(delegate (object sender, MouseButtonEventArgs e)
                {
                    if (!isThreadPaused)
                        return;

                    var communicator = newDrawableEthernet.Nodes.Find(node => node.value.visualElement == (Ellipse)sender).value.communicator;

                    deviceInfo.Visibility = Visibility.Hidden;

                    Comm_MAC_label.Content = "MAC: " + communicator.MAC;

                    Comm_MACtable_label.Content = "MAC table (" + communicator.MAC_table.Count + ")";
                    Comm_MACTable_listBox.Items.Clear();
                    foreach (var row in communicator.MAC_table)
                    {
                        var newItem = new Label();
                        newItem.Foreground = new SolidColorBrush(Color.FromArgb(0xFF, 0xC5, 0xC5, 0xC5));
                        newItem.Content = row.portIndex + " - " + row.MAC;
                        Comm_MACTable_listBox.Items.Add(newItem);
                    }

                    Comm_PortBuffers_label.Content = "Port buffers (" + communicator.PortsBuffer.Count + ")";
                    Comm_PortBuffers_listBox.Items.Clear();
                    for (int j = 0; j < communicator.PortsBuffer.Count; j++)
                    {
                        var newItem = new Label();
                        newItem.Foreground = new SolidColorBrush(Color.FromArgb(0xFF, 0xC5, 0xC5, 0xC5));
                        newItem.Content = j + " - " + communicator.PortsBuffer[j].Count;
                        Comm_PortBuffers_listBox.Items.Add(newItem);
                    }

                    communicatorInfo.Visibility = Visibility.Visible;

                    MovingShape = (Shape)sender;
                    Shift = e.GetPosition((Shape)sender);
                });

                newDrawableEthernet.AddNode(nodeInfo);

                for (int j = 0; j < model.communicators[i].Ports.Count; j++)
                    newPortsTable.Add(model.communicators[i].Ports[j], nodeInfo);
            }

            if (data.TryGetData("Network devices", out var devicesData))
            {
                foreach (JSONData deviceData in devicesData)
                {
                    if (!deviceData.TryGetData<JSONDataSet>("On screen position", out var positionData))
                        continue;
                    if (!TryReadPointFromData(positionData, out var point))
                    {
                        DisplayErrorMessage("Error loading file", "Can't read position of device(MAC: " + deviceData["MAC-address"].GetValue<int>() + ")." +
                                            "\n The device will be set to the default position");
                        continue;
                    }

                    if (point.X < 0 || point.X > 1 || point.Y < 0 || point.Y > 1)
                    {
                        DisplayErrorMessage("Error loading file", "Not valid position of device(MAC: " + deviceData["MAC-address"].GetValue<int>() + ")." +
                                            "\n The device will be set to the default position");
                        continue;
                    }

                    foreach (var node in newDrawableEthernet.Nodes)
                    {
                        var nodeInfo = node.value;

                        if (!nodeInfo.isCommunicator && deviceData["MAC-address"].GetValue<int>() == nodeInfo.device.MAC)
                        {
                            SetupMarginOnDisplay(nodeInfo.visualElement, point);

                            break;
                        }
                    }
                }
            }

            if (data.TryGetData("Communicators", out var commsData))
            {
                foreach (JSONData commData in commsData)
                {
                    if (!commData.TryGetData<JSONDataSet>("On screen position", out var positionData))
                        continue;
                    if (!TryReadPointFromData(positionData, out var point))
                    {
                        DisplayErrorMessage("Error loading file", "Can't read position of communicator(MAC: " + commData["MAC-address"].GetValue<int>() + ")." +
                                            "\n The communicator will be set to the default position");
                        continue;
                    }

                    if (point.X < 0 || point.X > 1 || point.Y < 0 || point.Y > 1)
                    {
                        DisplayErrorMessage("Error loading file", "Not valid position of communicator(MAC: " + commData["MAC-address"].GetValue<int>() + ")." +
                                            "\n The communicator will be set to the default position");
                        continue;
                    }

                    foreach (var node in newDrawableEthernet.Nodes)
                    {
                        var nodeInfo = node.value;

                        if (nodeInfo.isCommunicator && commData["MAC-address"].GetValue<int>() == nodeInfo.communicator.MAC)
                        {
                            SetupMarginOnDisplay(nodeInfo.visualElement, point);

                            break;
                        }
                    }
                }
            }

            foreach (var edgeInfo in cablesTable.Values)
                Display.Children.Remove(edgeInfo.visualElement);
            cablesTable.Clear();

            portsTable = newPortsTable;

            foreach (var nodeInfo in drawableEthernet.Nodes.Select(x => x.value))
                Display.Children.Remove(nodeInfo.visualElement);
            drawableEthernet = newDrawableEthernet;
            foreach (var nodeInfo in drawableEthernet.Nodes.Select(x => x.value))
                Display.Children.Add(nodeInfo.visualElement);

            foreach (var ellipse in frameEllipses)
                Display.Children.Remove(ellipse);
            frameEllipses.Clear();

            foreach (var ellipse in portsStatusEllipses)
                Display.Children.Remove(ellipse);
            portsStatusEllipses.Clear();

            NodesRelativePositions.Clear();

            foreach (var nodeInfo in drawableEthernet.Nodes)
                NodesRelativePositions.Add(nodeInfo.value.visualElement, GetRelativeMarginOnDisplay(nodeInfo.value.visualElement));

            RedrawCables();
        }

        private void RedrawCables()
        {
            LinkedList<TwistedPair> drawedCables = new LinkedList<TwistedPair>();

            for (int i = 0; i < drawableEthernet.Nodes.Count; i++)
            {
                NodeInfo nodeInfo = drawableEthernet.Nodes[i].value;
                if (nodeInfo.isCommunicator)
                {
                    for (int j = 0; j < nodeInfo.communicator.Ports.Count; j++)
                        TryDrawCable(nodeInfo.communicator.Ports[j].Cable, drawedCables);
                }
                else
                {
                    TryDrawCable(nodeInfo.device.port.Cable, drawedCables);
                }
            }
        }

        private void TryDrawCable(TwistedPair cable, LinkedList<TwistedPair> drawedCables)
        {
            if (cable == null)
                return;

            if (!drawedCables.Contains(cable))
            {
                drawedCables.AddLast(cable);

                portsTable.TryGetValue(cable.firstPort, out NodeInfo firstNodeInfo);
                portsTable.TryGetValue(cable.secondPort, out NodeInfo secondNodeInfo);
                Vector firstMiddlePos = new Vector(firstNodeInfo.visualElement.Margin.Left + firstNodeInfo.visualElement.Width / 2,
                                                firstNodeInfo.visualElement.Margin.Top + firstNodeInfo.visualElement.Height / 2);
                Vector secondMiddlePos = new Vector(secondNodeInfo.visualElement.Margin.Left + secondNodeInfo.visualElement.Width / 2,
                                              secondNodeInfo.visualElement.Margin.Top + secondNodeInfo.visualElement.Height / 2);

                if (firstMiddlePos == secondMiddlePos)
                    return;

                Vector diff = secondMiddlePos - firstMiddlePos;
                diff.Normalize();
                Vector firstPoint = firstMiddlePos + diff * firstNodeInfo.visualElement.Width / 2;
                Vector secondPoint = secondMiddlePos - diff * secondNodeInfo.visualElement.Width / 2;

                if (!drawableEthernet.TryGetEdgeValue(drawableEthernet.Nodes.Find(node => node.value == firstNodeInfo),
                                                      drawableEthernet.Nodes.Find(node => node.value == secondNodeInfo), out EdgeInfo edgeInfo))
                {
                    edgeInfo = new EdgeInfo();
                    edgeInfo.cable = cable;
                    edgeInfo.visualElement = new Line()
                    {
                        StrokeThickness = 4,
                        Stroke = Brushes.White,
                        X1 = firstPoint.X,
                        Y1 = firstPoint.Y,
                        X2 = secondPoint.X,
                        Y2 = secondPoint.Y
                    };
                    Display.Children.Add(edgeInfo.visualElement);

                    drawableEthernet.AddEdge(drawableEthernet.Nodes.Find(node => node.value == firstNodeInfo),
                                             drawableEthernet.Nodes.Find(node => node.value == secondNodeInfo), edgeInfo);

                    cablesTable.Add(cable, edgeInfo);
                }
                else
                {
                    edgeInfo.visualElement.X1 = firstPoint.X;
                    edgeInfo.visualElement.Y1 = firstPoint.Y;
                    edgeInfo.visualElement.X2 = secondPoint.X;
                    edgeInfo.visualElement.Y2 = secondPoint.Y;
                }
            }
        }

        private void DrawFrames()
        {
            foreach (Ellipse ellipse in frameEllipses)
                Display.Children.Remove(ellipse);
            frameEllipses.Clear();

            EthernetModel model = (EthernetModel)simulation.model;

            lock (model.frames)
            {
                foreach (Frame frame in model.frames)
                {
                    if (!frame.transmissible)
                        continue;

                    if (!cablesTable.TryGetValue(frame.cable, out EdgeInfo edgeInfo))
                        continue;

                    Vector firstPoint = new Vector(edgeInfo.visualElement.X1, edgeInfo.visualElement.Y1);
                    Vector secondPoint = new Vector(edgeInfo.visualElement.X2, edgeInfo.visualElement.Y2);

                    float passedLength = frame.transmitionTime * 20 / frame.cable.Length;

                    if (frame.fromPort == frame.cable.secondPort)
                        passedLength = 1 - passedLength;

                    Vector diff = secondPoint - firstPoint;
                    Vector pos = firstPoint + diff * passedLength;

                    Ellipse ellipse = new Ellipse()
                    {
                        Width = 15,
                        Height = 15,
                        Fill = frame.BPDU ? Brushes.HotPink : Brushes.GreenYellow,
                        Margin = new Thickness(pos.X - 15 / 2, pos.Y - 15 / 2, 0, 0)
                    };
                    Display.Children.Add(ellipse);

                    frameEllipses.AddLast(ellipse);
                }
            }
        }

        private void DrawPortsStatus()
        {
            foreach (var ellipse in portsStatusEllipses)
                Display.Children.Remove(ellipse);
            portsStatusEllipses.Clear();

            var rootPorts = new LinkedList<Port>();
            var designatedPorts = new LinkedList<Port>();

            foreach (var nodeInfo in drawableEthernet.Nodes)
            {
                if (nodeInfo.value.isCommunicator)
                {
                    if (nodeInfo.value.communicator.RootPortInd != -1)
                        rootPorts.AddLast(nodeInfo.value.communicator.Ports[nodeInfo.value.communicator.RootPortInd]);
                    foreach (var ind in nodeInfo.value.communicator.DesignatedPortsInd)
                        designatedPorts.AddLast(nodeInfo.value.communicator.Ports[ind]);
                }
            }

            foreach (var edgeInfo in cablesTable.Values)
            {
                double size = 10;
                Point lineStart = new Point(edgeInfo.visualElement.X1, edgeInfo.visualElement.Y1);
                Point lineEnd = new Point(edgeInfo.visualElement.X2, edgeInfo.visualElement.Y2);
                Vector dir = lineEnd - lineStart;

                if (dir.Length == 0)
                    continue;

                dir.Normalize();
                dir *= size * 0.5;

                if (rootPorts.Contains(edgeInfo.cable.firstPort))
                {

                    Ellipse ellipse = new Ellipse()
                    {
                        Width = size,
                        Height = size,
                        Fill = Brushes.Black,
                        Margin = new Thickness(lineStart.X - dir.X - size / 2, lineStart.Y - dir.Y - size / 2, 0, 0)
                    };
                    Display.Children.Add(ellipse);

                    portsStatusEllipses.AddLast(ellipse);
                }

                if (rootPorts.Contains(edgeInfo.cable.secondPort))
                {
                    Ellipse ellipse = new Ellipse()
                    {
                        Width = size,
                        Height = size,
                        Fill = Brushes.Black,
                        Margin = new Thickness(lineEnd.X + dir.X - size / 2, lineEnd.Y + dir.Y - size / 2, 0, 0)
                    };
                    Display.Children.Add(ellipse);

                    portsStatusEllipses.AddLast(ellipse);
                }

                if (designatedPorts.Contains(edgeInfo.cable.firstPort))
                {
                    Ellipse ellipse = new Ellipse()
                    {
                        Width = size,
                        Height = size,
                        Fill = Brushes.Brown,
                        Margin = new Thickness(lineStart.X - dir.X - size / 2, lineStart.Y - dir.Y - size / 2, 0, 0)
                    };
                    Display.Children.Add(ellipse);

                    portsStatusEllipses.AddLast(ellipse);
                }

                if (designatedPorts.Contains(edgeInfo.cable.secondPort))
                {
                    Ellipse ellipse = new Ellipse()
                    {
                        Width = size,
                        Height = size,
                        Fill = Brushes.Brown,
                        Margin = new Thickness(lineEnd.X + dir.X - size / 2, lineEnd.Y + dir.Y - size / 2, 0, 0)
                    };
                    Display.Children.Add(ellipse);

                    portsStatusEllipses.AddLast(ellipse);
                }
            }
        }

        private Shape MovingShape;
        private Point Shift;

        private void Display_MouseMove(object sender, MouseEventArgs e)
        {
            if (MovingShape != null)
            {
                Point mousePos = e.GetPosition(SimulationArea);
                Point minPos = new Point(0, 0);
                Point maxPos = new Point(SimulationArea.ActualWidth, SimulationArea.ActualHeight);

                Point shapePos = new Point(mousePos.X - Shift.X, mousePos.Y - Shift.Y);

                if (shapePos.X > maxPos.X - MovingShape.ActualWidth) shapePos.X = maxPos.X - MovingShape.ActualWidth;
                if (shapePos.Y > maxPos.Y - MovingShape.ActualHeight) shapePos.Y = maxPos.Y - MovingShape.ActualHeight;
                if (shapePos.X < minPos.X) shapePos.X = minPos.X;
                if (shapePos.Y < minPos.Y) shapePos.Y = minPos.Y;

                MovingShape.Margin = new Thickness(shapePos.X, shapePos.Y, 0, 0);

                NodesRelativePositions[MovingShape] = GetRelativeMarginOnDisplay(MovingShape);

                RedrawCables();
                if (Show_port_checkBox.IsChecked == true)
                    DrawPortsStatus();
                DrawFrames();
            }
        }

        private void SimulationArea_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (NodesRelativePositions == null || simulation.model == null)
                return;

            foreach (var shapeNpoint in NodesRelativePositions)
                SetupMarginOnDisplay(shapeNpoint.Key, shapeNpoint.Value);

            RedrawCables();
            if (Show_port_checkBox.IsChecked == true)
                DrawPortsStatus();
            DrawFrames();
        }

        private void Display_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            MovingShape = null;
        }

        private void Comm_MACTable_button_Click(object sender, RoutedEventArgs e)
        {
            Comm_MACTable_listBox.Height = Comm_MACTable_listBox.Height == 0 ? double.NaN : 0;
        }

        private void Comm_PortBuffers_button_Click(object sender, RoutedEventArgs e)
        {
            Comm_PortBuffers_listBox.Height = Comm_PortBuffers_listBox.Height == 0 ? double.NaN : 0;
        }

        private void Show_port_checkBox_Click(object sender, RoutedEventArgs e)
        {
            if (Show_port_checkBox.IsChecked == true)
            {
                DrawPortsStatus();
            }
            else
            {
                foreach (var ellipse in portsStatusEllipses)
                    Display.Children.Remove(ellipse);

                portsStatusEllipses.Clear();
            }
        }

        private void Load_File_button_Click(object sender, RoutedEventArgs e)
        {
            if (!isThreadPaused)
                return;

            string dir = Directory.GetCurrentDirectory();
            dir = dir.Remove(dir.LastIndexOf('\\'));
            dir = dir.Remove(dir.LastIndexOf('\\'));
            dir += "\\Saves";

            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
            {
                FileName = "Document", // Default file name
                DefaultExt = ".json", // Default file extension
                Filter = "Text documents|*.json", // Filter files by extension
                InitialDirectory = dir
            };

            // Show open file dialog box
            bool? result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == true)
                ReadFile(dlg.FileName);
        }

        private void ReadFile(string fileName)
        {
            var ethernetModel = new EthernetModel(deltaTime);
            try
            {
                var data = JSONParser.ParseFile(fileName);

                if (!ethernetModel.ReadFromJSONData(data, out string errorMessage))
                {
                    DisplayErrorMessage("Error loading file", errorMessage);
                    return;
                }

                InitDrawing(ethernetModel, data);
            }
            catch (JSONReaderException e)
            {
                DisplayErrorMessage("Error loading file", e.Message);
                return;
            }

            simulation.model = ethernetModel;
            simulation.Init();

            SimulationTime.Content = "0,000000000 s";
        }

        private void DisplayErrorMessage(string caption, string message)
        {
            MessageBox.Show(message, caption, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private bool TryReadPointFromData(JSONDataSet data, out Point result)
        {
            result = default;

            if (!data.TryGetData("x", out var xData))
                return false;
            double x = xData.GetValue<float>();

            if (!data.TryGetData("y", out var yData))
                return false;
            double y = yData.GetValue<float>();

            result = new Point(x, y);
            return true;
        }

        private void SetupMarginOnDisplay(Shape shape, Point pos)
        {
            pos.X = (SimulationArea.ActualWidth - shape.Width) * pos.X;
            pos.Y = (SimulationArea.ActualHeight - shape.Height) * pos.Y;

            shape.Margin = new Thickness(pos.X, pos.Y, 0, 0);
        }

        private Point GetRelativeMarginOnDisplay(Shape shape)
        {
            return new Point(shape.Margin.Left / (SimulationArea.ActualWidth - shape.Width),
                             shape.Margin.Top / (SimulationArea.ActualHeight - shape.Height));
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            simulationThread.Dispose();
            Dispatcher.ExitAllFrames();
        }

        private void Configuration_file_item_Selected(object sender, RoutedEventArgs e)
        {
            Help_Tab_Simulation.Visibility = Visibility.Hidden;
            Help_Configuration_file.Visibility = Visibility.Visible;
        }

        private void Tab_simulation_item_Selected(object sender, RoutedEventArgs e)
        {
            Help_Configuration_file.Visibility = Visibility.Hidden;
            Help_Tab_Simulation.Visibility = Visibility.Visible;
        }
    }
}
