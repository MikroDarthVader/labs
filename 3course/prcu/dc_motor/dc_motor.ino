void runMot(int pinF, int pinB, int power)
{
  power = constrain(power,-255, 255);
  if(power>0)
  {
    analogWrite(pinF, power);
    analogWrite(pinB, 0);
  }
  else
  {
    analogWrite(pinB, -power);
    analogWrite(pinF, 0);
  }
}

void setup()
{
  pinMode(8,OUTPUT);
  pinMode(9,OUTPUT);
  pinMode(10,OUTPUT);
  pinMode(11,OUTPUT);
  
  Serial.begin(9600);
}

void loop()
{
  if(Serial.available() > 0)
  {
    int power = Serial.parseInt();
    runMot(8,9, power);
    runMot(10,11, power);
  }
}
