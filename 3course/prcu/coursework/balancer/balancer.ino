#include <DueTimer.h>

double Kp = 6;
double Kd = 0;
double Ki = 20;
#define sampleTime  0.005
#define targetAngle 12.0
#define maxSpeed 60

#define angleUpdateTime  0.001

volatile float angle;

void setup()
{
    Serial.begin(115200);
    MPUSetup(true);
    Timer.getAvailable().attachInterrupt(pidRoutine).setFrequency(1/sampleTime).start();
    //Timer6.attachInterrupt(mpuRoutine).setFrequency(1/angleUpdateTime).start();
    StartMotors();
}

float sec(long microsec) { return microsec / 1000000;}

long lastLoopTime;
void loop()
{
  angle = MPUUpdate(true);
  UpdateMotors();
  /*if(Serial.available() > 0)
  {
    Kp = Serial.parseFloat();
    Kd = Serial.parseFloat();
    Ki = Serial.parseFloat();
    Serial.print("Kp: ");
    Serial.print(Kp);
    Serial.print(", Kd: ");
    Serial.print(Kd);
    Serial.print(", Ki: ");
    Serial.print(Ki);
  }*/
  //Serial.println(angle);
}

float errorSum, prevError;
void pidRoutine()
{
    
  float error = angle - targetAngle;
  errorSum = errorSum + error;  
  errorSum = constrain(errorSum, -300, 300);
  
  double motSpeed = Kp*(error) + Ki*(errorSum)*sampleTime + Kd*(error - prevError)/sampleTime;

  prevError = error;
  
  motSpeed = constrain(motSpeed, -maxSpeed, maxSpeed);
  /*Serial.println(angle);
  Serial.print("  ");
  Serial.println(motSpeed);*/
    
  SetMotorsSpeed(motSpeed);
}
