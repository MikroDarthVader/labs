#include "Wire.h"
#include "I2Cdev.h"
//#include "MPU6050.h"
#include "MPU6050_6Axis_MotionApps20.h"

MPU6050 mpu;

// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];

volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
    mpuInterrupt = true;
}

void MPUSetup(bool enableOutput)
{
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
      Wire.begin();
  #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
      Fastwire::setup(400, true);
  #endif
    
  mpu.initialize();
  
  if(enableOutput)
    Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

  devStatus = mpu.dmpInitialize();
    
  mpu.setXAccelOffset(-2154);
  mpu.setYAccelOffset(-1022);
  mpu.setZAccelOffset(1221);
  mpu.setXGyroOffset(63);
  mpu.setYGyroOffset(-33);
  mpu.setZGyroOffset(-18);
  
  if (devStatus == 0)
  {
    if(enableOutput)
      Serial.println(F("Enabling DMP..."));
    mpu.setDMPEnabled(true);

    if(enableOutput)
      Serial.println(F("Enabling interrupt detection (Arduino external interrupt 0)..."));
    attachInterrupt(2, dmpDataReady, RISING);
    mpuIntStatus = mpu.getIntStatus();

    if(enableOutput)
      Serial.println(F("DMP ready! Waiting for first interrupt..."));
    dmpReady = true;

    packetSize = mpu.dmpGetFIFOPacketSize();
  } else if(enableOutput){
    // ERROR!
    // 1 = initial memory load failed
    // 2 = DMP configuration updates failed
    // (if it's going to break, usually the code will be 1)
    Serial.print(F("DMP Initialization failed (code "));
    Serial.print(devStatus);
    Serial.println(F(")"));
  }
}

//float accX, accZ, gyroY;
float MPUUpdate(bool enableOutput)
{
  /*accX = mpu.getAccelerationX();
  accZ = mpu.getAccelerationZ();  
  gyroY = mpu.getRotationY();*/
  if (!dmpReady)
    return ypr[1] * 180/M_PI;

  if (!mpuInterrupt && fifoCount < packetSize)
    return ypr[1] * 180/M_PI;

  mpuInterrupt = false;
  mpuIntStatus = mpu.getIntStatus();

  fifoCount = mpu.getFIFOCount();

  if ((mpuIntStatus & 0x10) || fifoCount == 1024)
  {
    mpu.resetFIFO();
    if(enableOutput)
      Serial.println(F("FIFO overflow!"));
  } 
  else if (mpuIntStatus & 0x02) 
  {
    while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

    mpu.getFIFOBytes(fifoBuffer, packetSize);
    fifoCount -= packetSize;
    
    mpu.dmpGetQuaternion(&q, fifoBuffer);
    mpu.dmpGetGravity(&gravity, &q);
    mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
  }
  
  return ypr[1] * 180/M_PI;
}

/*float prevAngle;
long lastDataTime;
float GetAngleRoutine(float passedTimeFromLastUpdate, float insensitivityPeriodSec,float maxDegPSec)
{ 
  float accAngle = atan2(accX, accZ)*RAD_TO_DEG;
  float gyroRate = map(gyroY, -32768, 32767, -250, 250);
  float gyroAngle = (float)gyroRate*0.005;
  float someConst = insensitivityPeriodSec / (insensitivityPeriodSec + passedTimeFromLastUpdate);
  float angle = someConst*(prevAngle + gyroAngle) + (1-someConst)*(accAngle);

  long currTime = micros();
  if(!lastDataTime)
    lastDataTime = currTime;
  
  if(abs(angle - prevAngle) > maxDegPSec*(currTime - lastDataTime))
    angle = prevAngle;
  else  
    prevAngle = angle;

  lastDataTime = currTime;
  return angle;
}*/
