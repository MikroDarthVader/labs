#include<Stepper.h>
#include <DueTimer.h>

float wheelDiameterInCm = 9.387;
const int stepsPerTurnover = 200;

Stepper leftMot(200, 11, 10, 9, 8);
Stepper rightMot(200, 4, 5, 6, 7);

float translateLinearSpeedToSteps(float cmPerSec)
{
  float turnsPerSec = cmPerSec / (M_PI * wheelDiameterInCm);
  float msPerStep =  1000000 / (stepsPerTurnover * turnsPerSec);
  return msPerStep;
}

volatile float lSpeed;
volatile float rSpeed;

long lastLStep, lastRStep;
public

void StartMotors()
{
  digitalWrite(12,HIGH);
   
  leftMot.setSpeed(1000000);
  rightMot.setSpeed(1000000);
  
  Timer.getAvailable().attachInterrupt(UpdateMotors).setFrequency(10000).start();
}

void UpdateMotors()
{ 
  float maxDiffms = 250;
  float lStepTime = translateLinearSpeedToSteps(lSpeed);
  float rStepTime = translateLinearSpeedToSteps(rSpeed);
  long microsec = micros();
  
  if(microsec >= lastLStep + abs(lStepTime))
  {
    lastLStep += abs(lStepTime);
    if(microsec > lastLStep + maxDiffms)
      lastLStep = microsec;
      
    leftMot.step(lStepTime > 0 ? 1 : -1);
  }
      
  if(microsec > lastRStep + abs(rStepTime))
  {
    lastRStep += abs(rStepTime);
    if(microsec > lastRStep + maxDiffms)
      lastRStep = microsec;
      
    rightMot.step(rStepTime > 0 ? 1 : -1);
  }
}

void SetLSpeed(float cmPerSec){ lSpeed = cmPerSec; }
void SetRSpeed(float cmPerSec){ rSpeed = cmPerSec; }
void SetMotorsSpeed(float cmPerSec){ lSpeed = rSpeed = cmPerSec; }
