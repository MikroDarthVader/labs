#include <Stepper.h>
#include <DueTimer.h>
Stepper leftMot(200, 8, 9, 10, 11);
Stepper rightMot(200, 4, 5, 6, 7);

volatile float cmPerSec;
float wheelDiameterInCm = 9.387;
const int stepsPerTurnover = 200;

float GetStepFreq(float cmPerSec)
{
      float turnsPerSec = cmPerSec / (M_PI * wheelDiameterInCm);
      //float msPerStep =  1000000 / (stepsPerTurnover * turnsPerSec);
      float stepsPerSec = stepsPerTurnover * turnsPerSec;
      return abs(stepsPerSec);
}

void doStep()
{
  leftMot.step(cmPerSec > 0 ? 1 : -1);
  rightMot.step(cmPerSec > 0 ? 1 : -1);
  Timer8.attachInterrupt(doStep).setFrequency(GetStepFreq(cmPerSec)).start();
}

void setup() {
  digitalWrite(12,HIGH);
  Serial.begin(9600);
  
  leftMot.setSpeed(1000000);
  rightMot.setSpeed(1000000);
  
  cmPerSec = 60;
  
  Timer8.attachInterrupt(doStep).setFrequency(GetStepFreq(cmPerSec)).start();
}

void loop() {
  if(Serial.available() > 0)
  {
    cmPerSec = Serial.parseFloat();
    Serial.print("parsed speed: ");
    Serial.println(cmPerSec);
  }
}
