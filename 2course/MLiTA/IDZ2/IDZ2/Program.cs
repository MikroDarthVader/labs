﻿using System;

static class Program
{
    static void Main(string[] args)
    {
        while (true)
        {
            string s = Console.ReadLine();
            Console.WriteLine(GrammarAnalyzer.analyze(s));
        }
    }
}

public static class GrammarAnalyzer
{
    public static string analyze(string s)
    {
        try
        {
            int lastIndex = analyzeIter(s, 0);
            if (lastIndex < s.Length - 1)
                return "Exception at symbol by index: " + (lastIndex + 1);
            return "Parsed successfully";
        }
        catch (AnalyzerException e)
        {
            return "Exception at symbol by index: " + e.ErrorSymbol;
        }
    }

    static int analyzeIter(string s, int index)
    {
        char c = getc(s, index);
        if (c == '0')
        {
            index = analyzeIter(s, index + 1) + 1;
            c = getc(s, index);
            if (c == '1' || c == '0')
                return index;
            else
                throw new AnalyzerException(index);
        }
        else if (c == '1')
        {
            index = analyzeIter(s, index + 1) + 1;
            c = getc(s, index);
            if (c == '0')
                return index;
            else
                throw new AnalyzerException(index);
        }
        else if (c == '*')
            return index;
        else
            throw new AnalyzerException(index);
    }

    static char getc(string s, int index)
    {
        if (index < 0 || index >= s.Length)
            throw new AnalyzerException(index);
        else
            return s[index];
    }

    class AnalyzerException : Exception
    {
        public readonly int ErrorSymbol;

        public AnalyzerException(int ErrorSymbol) : base()
        {
            this.ErrorSymbol = ErrorSymbol;
        }
    }
}