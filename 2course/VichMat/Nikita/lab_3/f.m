function y = f(x)
   y = 1000./(x.^2 - 6*x + 76);
