function y = df(x)
  y = (-1000*(2*x -6))./(x.^2 - 6*x +76).^2;
