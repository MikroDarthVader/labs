function [q, sigma]=syst(t,y)
m=length(t);
A=zeros(m);
for i=1:m
    for k=1:m-1
        A(i,k)=t(i)^(m-k-1);
    end
    A(i,m)=(-1)^i;
end
[i,k]=size(y);
if i<k, y=y'; end
q=A\y; q=q';
sigma=q(m);
q(m)=[];