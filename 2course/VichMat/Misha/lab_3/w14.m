disp ('1-� ������., O(h^4)')
x=-1;
format long g
y=f(x);
dy=df(x);
m=50;
k=0:m;
h=2.^(-k);
r=[];
for i=0:m
    r=[r,df(x)-df4(x,h(i+1))];
end
format short g
res14=[k',h',r'];
semilogy(k,abs(r))
grid on
title('df4')
