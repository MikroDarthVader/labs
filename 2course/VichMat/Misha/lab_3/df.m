function y = df(x)
  y = (-1000*(2*x -5))./(x.^2 - 5*x +68).^2;
