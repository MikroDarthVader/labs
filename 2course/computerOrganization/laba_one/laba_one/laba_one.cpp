// laba_one.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

void printCharBits(int num, int minLenght, int maxLenght, int curLenght = 0)
{
	if ((num || curLenght < minLenght) && curLenght < maxLenght)
	{
		printCharBits(num >> 1, minLenght, maxLenght, ++curLenght);
		num &= 1;
		cout << num;
	}
}

void SetBit(unsigned int *value, int bitVal, int bitPos)
{
	if (bitVal == 1)
		*value |= (1 << bitPos);
	else
		*value &= ~(1 << bitPos);
}

void AddBits(unsigned int *value, int leadingDigit, int *bitsToAdd, int bitsToAddLenght)
{
	if (bitsToAddLenght <= 0 || bitsToAddLenght > 31 || leadingDigit < 0 || leadingDigit > 31 || leadingDigit - bitsToAddLenght + 1 < 0)
		return;

	for (int i = leadingDigit; i > leadingDigit - bitsToAddLenght; i--)
		SetBit(value, bitsToAdd[leadingDigit - i], i);
}

int main()
{
	cout << "Enter unsigned char value" << endl;
	unsigned char charVal;
	cin >> charVal;
	cout << "Entered value: " << (int)charVal << endl << "Conversion value: ";
	printCharBits(charVal, 8, 8);
	cout << endl;

	cout << "Enter float value" << endl;
	float floatVal;
	cin >> floatVal;
	unsigned int *temp = (unsigned int *)&floatVal;
	cout << "Conversion value: ";
	printCharBits(*temp, 32, 32);
	cout << endl;

	cout << "Enter leading digit" << endl;
	int leadingDigit;
	cin >> leadingDigit;
	cout << "Enter bits to add" << endl;
	int *bits = (int *)malloc(32 * sizeof(int));
	int bitsLength = 0;
	char c;

	getchar();
	while ((c = getchar()) != '\n')
	{
		bits[bitsLength] = c - '0';
		bitsLength++;
	}
	AddBits(temp, leadingDigit, bits, bitsLength);
	cout << "Converison value after bitsAdd: " << endl;
	printCharBits(*temp, 32, 32);
	cout << endl;
	cout << "Entered value after bitAdd: " << endl;
	cout << *((float *)temp) << endl;
	cout << endl;

	return 0;
}

