#include "conio.h"
#include "dos.h"

int main()
{
	textbackground(0);
	clrscr();
	window(20, 5, 60, 15);
	textbackground(1);
	clrscr();
	textcolor(2);
	_setcursortype (_NOCURSOR);

	int input = 0;
	int isExit = 0;
	int Speed = 0;
	int currPos = 0 ;
	while (!isExit)
	{
		delay(700);

		clrscr();

		gotoxy(currPos % 40 + 1, currPos / 40 + 1);
		putch('*');

		while (kbhit())
		{
			input = getch();
			if (input == 59) Speed--;
			if (input == 60) Speed++;
		}
		if (input == 27) isExit = 1;
		input = 0;

		currPos = (currPos + Speed + 440) % (440);
	};
	getch();
	return 0;
}