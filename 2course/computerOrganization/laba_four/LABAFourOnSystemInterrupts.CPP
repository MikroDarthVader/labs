#include "conio.h"
#include "dos.h"

int my_getch()
{
	union REGS r;
	r.h.ah = 8;
	int86(0x21, &r, &r);
	
	return r.h.al;
}

void my_putc(char c)
{
	union REGS r;
	r.h.ah = 6;
	r.h.dl = c;

	int86(0x21, &r, &r);
}

int my_kbhit()
{
	union REGS r;
	r.h.ah = 11;
	int86(0x21, &r, &r);

	return (r.h.al == 255) ? 1 : 0;
}

int main()
{

	textbackground(0);
	clrscr();
	window(20, 5, 60, 15);
	textbackground(1);
	clrscr();
	textcolor(2);
	_setcursortype (_NOCURSOR);

	int input = 0;
	int needExit = 0;
	int Speed = 0;
	int currPos = 0 ;
	while (!needExit)
	{
		delay(700);

		clrscr();

		gotoxy(currPos % 41 + 1, currPos / 41 + 1);
		my_putc('*');

		while (my_kbhit())
		{
			input = my_getch();
			if (input == 59) Speed--;
			if (input == 60) Speed++;
			if (input == 27) needExit = 1;
		}

		currPos = (currPos + Speed + 451) % (451);
	};
	my_getch();
	return 0;
}