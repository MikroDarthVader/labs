#pragma once

float RoundWithDir(float num, float step, bool up);

class ChartMaker
{
public:
	ChartMaker(int PixelPerUnit_X, int PixelPerUnit_Y, float(*Funct)(float x), bool UsingInterval = false, float IntervalStart = 0, float IntervalEnd = 0);
	~ChartMaker();

	int CoordAxisStartPoint_X;
	int CoordAxisStartPoint_Y;
	int PixelPerUnit_X;
	int PixelPerUnit_Y;
	int ChartColor;
	int AxisColor;

	void UpdateScreen();
	float GetUnitScale();
	void ChangeUnitScale(bool up);
	float FindMaxFunctValueOnInterval(float step = 0.001);


	bool isLMBDown, isRMBDown;
	int prevMousePosX, prevMousePosY;
	void OnMouseMove(int mousePosX, int mousePosY);
	void OnMouseLMBUp(int mousePosX, int mousePosY);
	void OnMouseLMBDown(int mousePosX, int mousePosY);
	void OnMouseRMBUp(int mousePosX, int mousePosY);
	void OnMouseRMBDown(int mousePosX, int mousePosY);
private:
	bool UsingInterval;
	float IntervalStart;
	float IntervalEnd;
	float MaxValueOnInterval;
	int UnitScale;
	const int ScaleSensitivity = 15; // from 0 to 100
	float(*Funct)(float x);

	void CreateAndSetupScreen();
	void DrawCoordAxis();
	void DrawChart();
	void DrawMaxValue();
};

