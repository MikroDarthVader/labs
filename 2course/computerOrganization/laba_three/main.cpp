#include "ChartMaker.h"
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include "graphics/graphics.h"

using namespace std;


float function(float x) // funct that used for draw chart
{
	return powf(sinf(x / 4), 2) + sqrtf(x);
}

const float PI = 3.141592;

ChartMaker* chartmaker;
int main(int argc, char* argv[])
{
	chartmaker = new ChartMaker(4, 4, function, true, 3 * PI / 2, 17 * PI);
	chartmaker->UpdateScreen();

	getchar();
	return 0;
}

void GlobalOnMouseMove(int mousePosX, int mousePosY)
{
	if (chartmaker)
		chartmaker->OnMouseMove(mousePosX, mousePosY);
}

void GlobalOnMouseLMBDown(int mousePosX, int mousePosY)
{
	if (chartmaker)
		chartmaker->OnMouseLMBDown(mousePosX, mousePosY);
}

void GlobalOnMouseLMBUp(int mousePosX, int mousePosY)
{
	if (chartmaker)
		chartmaker->OnMouseLMBUp(mousePosX, mousePosY);
}

void GlobalOnMouseRMBDown(int mousePosX, int mousePosY)
{
	if (chartmaker)
		chartmaker->OnMouseRMBDown(mousePosX, mousePosY);
}

void GlobalOnMouseRMBUp(int mousePosX, int mousePosY)
{
	if (chartmaker)
		chartmaker->OnMouseRMBUp(mousePosX, mousePosY);
}

void ChartMaker::CreateAndSetupScreen()
{
	int i, gd, gm;

	gd = DETECT;
	initgraph(&gd, &gm, "");
	if (graphresult() != 0)
		exit(255);

	setbkcolor(COLOR(200, 200, 200));
	settextstyle(SANS_SERIF_FONT, 0, 1);

	registermousehandler(WM_MOUSEMOVE, GlobalOnMouseMove); // binding functs to mouse actions
	registermousehandler(WM_LBUTTONDOWN, GlobalOnMouseLMBDown);
	registermousehandler(WM_LBUTTONUP, GlobalOnMouseLMBUp);
	registermousehandler(WM_RBUTTONDOWN, GlobalOnMouseRMBDown);
	registermousehandler(WM_RBUTTONUP, GlobalOnMouseRMBUp);
}

