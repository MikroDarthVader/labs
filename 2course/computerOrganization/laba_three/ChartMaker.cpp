#include "ChartMaker.h"
#include "graphics/graphics.h"

float RoundWithDir(float num, float step, bool up)
{
	return step * round(num / step + (up ? 0.5 : -0.5));
}

int Clamp(int value, int min, int max)
{
	if (value > max) value = max;
	else if (value < min) value = min;

	return value;
}

char *ftoa(float value, int maxNumAfterComma) // from float to string
{
	char *buffer = new char[100];
	char result[100];
	result[0] = '\0';

	strcat(result, itoa((int)value, buffer, 10));

	if (maxNumAfterComma == 0)
		return result;

	value = abs(value - (int)value);

	value *= pow(10, maxNumAfterComma);
	int val = (int)value;
	while (val % 10 == 0)
		val /= 10;

	strcat(result, ".");
	strcat(result, itoa(val, buffer, 10));

	delete[] buffer;
	return result;
}

ChartMaker::ChartMaker(int PixelPerUnit_X, int PixelPerUnit_Y, float(*Funct)(float x), bool UsingInterval, float IntervalStart, float IntervalEnd)
{
	CreateAndSetupScreen();
	this->CoordAxisStartPoint_X = getmaxx() / 2;
	this->CoordAxisStartPoint_Y = getmaxy() / 2;
	UnitScale = 1;
	this->PixelPerUnit_X = PixelPerUnit_X;
	this->PixelPerUnit_Y = PixelPerUnit_Y;
	this->Funct = Funct;
	AxisColor = COLOR(0, 0, 0);
	ChartColor = COLOR(0, 0, 150);

	this->UsingInterval = UsingInterval;
	if (UsingInterval)
	{
		this->IntervalStart = IntervalStart;
		this->IntervalEnd = IntervalEnd;
	}

	isLMBDown = isRMBDown = false;

	MaxValueOnInterval = FindMaxFunctValueOnInterval(0.00001);

	ChangeUnitScale(true);
}

ChartMaker::~ChartMaker()
{
	closegraph();
}

void ChartMaker::DrawCoordAxis()
{
	setcolor(AxisColor);

	/*axis*/
	if (CoordAxisStartPoint_X >= 0 && CoordAxisStartPoint_X < getmaxx()) // if axis on the screen
		line(CoordAxisStartPoint_X, 0, CoordAxisStartPoint_X, getmaxy());

	if (CoordAxisStartPoint_Y >= 0 && CoordAxisStartPoint_Y < getmaxy()) // if axis on the screen
		line(0, CoordAxisStartPoint_Y, getmaxx(), CoordAxisStartPoint_Y);
	/*axis*/

	/*Unit marks and numbers*/
	float markLength = 5; // length of mark on axis in pixels

	int num;
	char *buffer = new char[17];

	int firstLeftMarkDistFromStartPoint = RoundWithDir(-CoordAxisStartPoint_X, PixelPerUnit_X, false); // get distance from CoordAxisStartPoint on x-axis to leftmost visible mark

	num = (firstLeftMarkDistFromStartPoint / PixelPerUnit_X) * UnitScale; // get number for leftmost visible mark on x-axis
	for (int x = CoordAxisStartPoint_X + firstLeftMarkDistFromStartPoint; x <= getmaxx() + PixelPerUnit_Y; x += PixelPerUnit_X, num += UnitScale) // cycle over all visible marks on x-axis
	{
		if (num != 0)
		{
			line(x, CoordAxisStartPoint_Y - markLength, x, CoordAxisStartPoint_Y + markLength);
			buffer = itoa(num, buffer, 10);
			outtextxy(x - textwidth(buffer) / 2, CoordAxisStartPoint_Y - markLength - textheight(buffer), buffer);
		}
	}

	int firstTopMarkDistFromStartPoint = RoundWithDir(-CoordAxisStartPoint_Y, PixelPerUnit_Y, false);  // get distance from CoordAxisStartPoint on y-axis to topmost visible mark
	num = (-firstTopMarkDistFromStartPoint / PixelPerUnit_Y) * UnitScale; // get number for topmost visible mark on y-axis
	for (int y = CoordAxisStartPoint_Y + firstTopMarkDistFromStartPoint; y <= getmaxy() + PixelPerUnit_Y; y += PixelPerUnit_Y, num -= UnitScale)  // cycle over all visible marks on y-axis
	{
		if (num != 0)
		{
			line(CoordAxisStartPoint_X - markLength, y, CoordAxisStartPoint_X + markLength, y);
			buffer = itoa(num, buffer, 10);
			outtextxy(CoordAxisStartPoint_X - markLength - textwidth(buffer), y - textheight(buffer) / 2, buffer);
		}
	}

	delete[] buffer;
	/*Unit marks and numbers*/
}

void ChartMaker::DrawChart()
{
	setcolor(ChartColor);
	bool firstChartPoint = true;
	int prevPosInScreen_Y;
	for (int x = 0; x < getmaxx(); x++)
	{
		float inCoordAxisPos_X = (float)(x - CoordAxisStartPoint_X) / PixelPerUnit_X * UnitScale; // translate x-pos in screen to x-pos in chart
		float inCoordAxisPos_Y = Funct(inCoordAxisPos_X); // calc y-pos in chart for x-pos in chart
		int posInScreen_Y = CoordAxisStartPoint_Y - (int)(inCoordAxisPos_Y / UnitScale * PixelPerUnit_Y); // translate y-pos in chart to y pos in screen

		if (!isnan(inCoordAxisPos_Y) && !isinf(inCoordAxisPos_Y) && (!UsingInterval || inCoordAxisPos_X >= IntervalStart && inCoordAxisPos_X <= IntervalEnd)) // if y-pos in chart valide
		{
			if (!firstChartPoint) // connect previous y-pos in chart with current y-pos in chart using line
				line(x - 1, prevPosInScreen_Y, x, posInScreen_Y);

			prevPosInScreen_Y = posInScreen_Y;
			firstChartPoint = false;
		}
	}
}

void ChartMaker::DrawMaxValue()
{
	if (!UsingInterval)
		return;

	char text[100] = "������������ �������� �� ����������: ";
	outtextxy(0, 0, strcat(text, ftoa(MaxValueOnInterval, 6)));
}

void ChartMaker::UpdateScreen()
{
	cleardevice();
	DrawCoordAxis();
	DrawChart();
	DrawMaxValue();
}

float ChartMaker::FindMaxFunctValueOnInterval(float step)
{
	if (!UsingInterval)
		return 0;

	float maxValue = Funct(IntervalStart);
	for (float currPos = IntervalStart + step; currPos <= IntervalEnd; currPos += step) // reseraching max value on interval with cycle
	{
		float value = Funct(currPos);
		if (value > maxValue)
			maxValue = value;
	}

	return maxValue;
}

float ChartMaker::GetUnitScale()
{
	return UnitScale;
}

void ChartMaker::ChangeUnitScale(bool up)
{
	if (!up && UnitScale == 1)
		return;

	UnitScale = (up ? UnitScale * 10 : (UnitScale / 10));
	PixelPerUnit_X = (up ? PixelPerUnit_X * 10 : (PixelPerUnit_X / 10));
	PixelPerUnit_Y = (up ? PixelPerUnit_Y * 10 : (PixelPerUnit_Y / 10));
}

void ChartMaker::OnMouseMove(int mousePosX, int mousePosY)
{
	if (isLMBDown) // chart moving
	{
		CoordAxisStartPoint_X += mousePosX - prevMousePosX; // move start chart point with mouse move
		CoordAxisStartPoint_Y += mousePosY - prevMousePosY;
		prevMousePosX = mousePosX;
		prevMousePosY = mousePosY;

		char buffer[17];
		int leftNumberWidth = textwidth(itoa((int)RoundWithDir(-CoordAxisStartPoint_X, PixelPerUnit_X, true) * GetUnitScale() / PixelPerUnit_X, buffer, 10));
		int rightNumberWidth = textwidth(itoa((int)RoundWithDir(getmaxx() - CoordAxisStartPoint_X, PixelPerUnit_X, true) * GetUnitScale() / PixelPerUnit_X, buffer, 10));

		if (leftNumberWidth >= PixelPerUnit_X || rightNumberWidth >= PixelPerUnit_X)
			ChangeUnitScale(true);
	}
	else if (isRMBDown) // chart scaling
	{
		if ((mousePosX - prevMousePosX) / 20 != 0)
		{
			PixelPerUnit_X += (mousePosX - prevMousePosX) * ScaleSensitivity / 100;
			PixelPerUnit_Y = PixelPerUnit_X;
			prevMousePosX = mousePosX;

			char buffer[17];
			int leftNumberWidth = textwidth(itoa((int)RoundWithDir(-CoordAxisStartPoint_X, PixelPerUnit_X, true) * GetUnitScale() / PixelPerUnit_X, buffer, 10));
			int rightNumberWidth = textwidth(itoa((int)RoundWithDir(getmaxx() - CoordAxisStartPoint_X, PixelPerUnit_X, true) * GetUnitScale() / PixelPerUnit_X, buffer, 10));

			if (leftNumberWidth * 3 / 2 >= PixelPerUnit_X || rightNumberWidth * 3 / 2 >= PixelPerUnit_X)
				ChangeUnitScale(true);
			else if (leftNumberWidth * 15 <= PixelPerUnit_X && rightNumberWidth * 15 <= PixelPerUnit_X)
				ChangeUnitScale(false);
		}
	}

	if (isLMBDown || isRMBDown)
		UpdateScreen();
}

void ChartMaker::OnMouseLMBDown(int mousePosX, int mousePosY)
{
	if (!isRMBDown)
	{
		isLMBDown = true; // set up chart pos changing mode
		prevMousePosX = mousePosX;
		prevMousePosY = mousePosY;
	}
}

void ChartMaker::OnMouseLMBUp(int mousePosX, int mousePosY)
{
	isLMBDown = false;
}

void ChartMaker::OnMouseRMBDown(int mousePosX, int mousePosY)
{
	if (!isLMBDown)
	{
		isRMBDown = true; // set up chart scale changing mode
		prevMousePosX = mousePosX;
	}
}

void ChartMaker::OnMouseRMBUp(int mousePosX, int mousePosY)
{
	isRMBDown = false;
}

