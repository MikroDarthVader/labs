﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fractals
{
    public partial class Form1 : Form
    {
        FractalTree fractal;
        Graphics g1;
        Graphics g2;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (g1 != null)
                g1.Clear(pictureBox1.BackColor);
            if (g2 != null)
                g2.Clear(pictureBox2.BackColor);

            g1 = pictureBox1.CreateGraphics();
            g2 = pictureBox2.CreateGraphics();

            int heightNum = Convert.ToInt32(textBox1.Text);
            SubWindow[] windows = GenerateWindows(heightNum, g1);

            fractal = new FractalTree(new line(new vector(windows[0].size.X * 3 / 10, windows[0].size.Y * 3 / 5), new vector(windows[0].size.X * 7 / 10, windows[0].size.Y * 3 / 5)));
            fractal.GenerateTree(heightNum);
            fractal.DrawTree(g2, new vector(pictureBox2.Width / 2, 20), (pictureBox2.Height - 40) / (heightNum == 1 ? 1 : (heightNum - 1)), pictureBox2.Width - 20);

            for (int i = 0; i < heightNum; i++)
            {
                windows[i].DrawBorders();
                fractal.DrawFractal(windows[i], i + 1);
            }
        }

        private SubWindow[] GenerateWindows(int num, Graphics g)
        {
            SubWindow[] windows = new SubWindow[(int)Math.Pow(Math.Ceiling(Math.Sqrt(num)), 2)];
            
            int border = (Math.Min(pictureBox1.Width, pictureBox1.Height) - 1) / (int)Math.Sqrt(windows.Length);
            Point size = new Point(border, border);

            for (int i = 0; i < windows.Length; i++)
                windows[i] = new SubWindow(new Point(border * (i % (int)Math.Sqrt(windows.Length)), border * (i / (int)Math.Sqrt(windows.Length))), size, g);

            return windows;
        }
    }
}
