﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace fractals
{
    class SubWindow
    {
        public Point pos { get { return r.Location; } set { r.Location = value; } }
        public Point size { get { return new Point(r.Size); } set { r.Size = new Size(value); } }

        private Graphics g;
        private Rectangle r;

        public SubWindow(Point pos, Point size, Graphics g)
        {
            this.g = g;
            r = new Rectangle();
            this.size = size;
            this.pos = pos;
        }

        private Point ToGlobal(Point innerPoint)
        {
            return new Point(innerPoint.X + pos.X, innerPoint.Y + pos.Y);
        }

        public void DrawLine(Pen pen, float x1, float y1, float x2, float y2)
        {
            g.DrawLine(pen, pos.X + x1, pos.Y + y1, pos.X + x2, pos.Y + y2);
        }

        public void DrawBorders()
        {
            g.DrawRectangle(Pens.Black, r);
        }
    }
}
