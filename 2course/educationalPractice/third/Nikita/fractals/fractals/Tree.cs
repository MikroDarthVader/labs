﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fractals
{
    class Tree<T>
    {
        public class TreeNode<L>
        {
            public List<TreeNode<L>> children;

            public L data;

            public TreeNode(L data)
            {
                this.data = data;
                children = new List<TreeNode<L>>();
            }
        }

        protected TreeNode<T> root;

        public Tree()
        {}

        public void AddNode(T data, TreeNode<T> parent = null)
        {
            if (root == null)
                root = new TreeNode<T>(data);
            else
                parent.children.Add(new TreeNode<T>(data));
        }
    }
}
