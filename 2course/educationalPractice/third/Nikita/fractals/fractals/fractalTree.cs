﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace fractals
{
    struct vector
    {
        public float x;
        public float y;

        public vector(float x, float y)
        {
            this.x = x;
            this.y = y;
        }
    }

    struct line
    {
        public vector start;
        public vector end;

        public line(vector start, vector end)
        {
            this.start = start;
            this.end = end;
        }
    }

    class FractalTree : Tree<line>
    {
        Color[] HeightColors;

        public FractalTree(line line)
        {
            root = new TreeNode<line>(line);
        }

        public void GenerateTree(int maxHeight)
        {
            HeightColors = new Color[maxHeight];

            Random rnd = new Random();
            for (int i = 0; i < HeightColors.Length; i++)
                HeightColors[i] = Color.FromArgb(rnd.Next() % 256, rnd.Next() % 256, rnd.Next() % 256);

            GenerateTreeIter(root, maxHeight);
        }

        private void GenerateTreeIter(TreeNode<line> currNode, int currHeight)
        {
            if (currHeight == 1)
                return;

            line line = currNode.data;
            vector point;
            point.x = (line.start.x + line.end.x) / 2 + (line.end.y - line.start.y) / 2;
            point.y = (line.start.y + line.end.y) / 2 - (line.end.x - line.start.x) / 2;

            AddNode(new line(line.start, point), currNode);
            AddNode(new line(line.end, point), currNode);

            foreach (TreeNode<line> node in currNode.children)
                GenerateTreeIter(node, currHeight - 1);
        }

        public void DrawFractal(SubWindow window, int drawHeight)
        {
            DrawFractalIter(root, drawHeight, window, 1);
        }

        private void DrawFractalIter(TreeNode<line> currNode, int drawHeight, SubWindow window, int currHeight)
        {
            if (currHeight == drawHeight)
            {
                line line = currNode.data;
                window.DrawLine(new Pen(HeightColors[currHeight - 1]), line.start.x, line.start.y, line.end.x, line.end.y);
                return;
            }
            else
            {
                foreach (TreeNode<line> node in currNode.children)
                    DrawFractalIter(node, drawHeight, window, currHeight + 1);
            }
        }

        public void DrawTree(Graphics g, vector rootCoord, int maxHeightForLayer, int maxWidth)
        {
            DrawTreeIter(root, g, rootCoord, maxHeightForLayer, maxWidth, 1);
        }

        private void DrawTreeIter(TreeNode<line> currNode, Graphics g, vector nodeCoord, int maxHeightForLayer, int maxWidth, int currHeight)
        {
            if (currNode.children.Count != 0)
            {
                vector leftChildPos = new vector(nodeCoord.x - maxWidth / 4, nodeCoord.y + maxHeightForLayer);
                vector rightChildPos = new vector(nodeCoord.x + maxWidth / 4, nodeCoord.y + maxHeightForLayer);

                g.DrawLine(new Pen(HeightColors[currHeight]), nodeCoord.x, nodeCoord.y, leftChildPos.x, leftChildPos.y);
                g.DrawLine(new Pen(HeightColors[currHeight]), nodeCoord.x, nodeCoord.y, rightChildPos.x, rightChildPos.y);

                DrawTreeIter(currNode.children[0], g, leftChildPos, maxHeightForLayer, maxWidth / 2, currHeight + 1);
                DrawTreeIter(currNode.children[1], g, rightChildPos, maxHeightForLayer, maxWidth / 2, currHeight + 1);
            }

            g.FillEllipse(new SolidBrush(HeightColors[currHeight - 1]), nodeCoord.x - 2, nodeCoord.y - 2, 4, 4);
        }
    }
}
