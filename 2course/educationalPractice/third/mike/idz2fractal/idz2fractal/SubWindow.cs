﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace idz2fractal
{
    public class SubWindow
    {
        public PointF pos { get { return r.Location; } set { r.Location = value; } }
        public PointF scale { get { return r.Size.ToPointF(); } set { r.Size = new SizeF(value); } }
        public Graphics graphics;

        private RectangleF r;

        public SubWindow(PointF pos, PointF scale, Graphics g)
        {
            graphics = g;
            r = new RectangleF();
            this.scale = scale;
            this.pos = pos;
        }

        public PointF ToGlobal(PointF innerPoint)
        {
            return new PointF((innerPoint.X * scale.X + pos.X),
                                (innerPoint.Y * scale.Y + pos.Y));
        }

        public PointF ToLocal(PointF outerPoint)
        {
            return new PointF((outerPoint.X - pos.X) / scale.X,
                                (outerPoint.Y - pos.Y) / scale.Y);
        }

        /// <summary>
        /// global coordinates for square
        /// </summary>
        /// <param name="innerPos"></param>
        /// <param name="innerSize"></param>
        /// <returns></returns>
        public RectangleF ToGlobal(PointF innerPos, float innerSize)
        {
            return new RectangleF(ToGlobal(innerPos), new SizeF(innerSize*scale.X, innerSize*scale.Y));
        }

        public void DrawBorders()
        {
            graphics.DrawRectangle(Pens.Black, r.Location.X, r.Location.Y, r.Width, r.Height);
        }
    }
}
