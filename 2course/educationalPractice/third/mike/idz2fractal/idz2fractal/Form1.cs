﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace idz2fractal
{
    public partial class Form1 : Form
    {
        class FractNodeData
        {
            public float radius;//0-1
            public PointF pos;//{0-1,0-1}

            public FractNodeData(float radius, PointF pos)
            {
                this.radius = radius;
                this.pos = pos;
            }
        }

        static Tree<FractNodeData> fractTree;
        static Color[] layerColors;
        static SubWindow treeWindow;
        static Graphics graph;

        public Form1()
        {
            InitializeComponent();
        }

        void GenerateLeyerColors(int num)
        {
            int maxIntens = 220;
            Random r = new Random();
            layerColors = new Color[num];
            for (int i = 0; i < num; i++)
                layerColors[i] = Color.FromArgb(r.Next() % maxIntens, r.Next() % maxIntens, r.Next() % maxIntens);
        }

        void GenerateTree(int depth)
        {
            fractTree = new Tree<FractNodeData>(new FractNodeData(0.5f, new PointF(0.5f, 0.5f)), 7, depth);
            GenerateTreeIter(fractTree.root, 1, depth);
        }

        void GenerateTreeIter(Tree<FractNodeData>.Node parent, int curDepth, int maxDepth)
        {
            if (curDepth == maxDepth)
                return;

            float radius = parent.data.radius / 3;
            PointF[] positions = new PointF[] { new PointF(0, 2/3f),//x0,y1
                                                new PointF((float)Math.Cos(Math.PI/6)*2/3f, 1/3f),//x1,y1
                                                new PointF((float)Math.Cos(Math.PI/6)*2/3f, -1/3f),//x1,-y1
                                                new PointF(0, -2/3f),//x0,-y1
                                                new PointF(-(float)Math.Cos(Math.PI/6)*2/3f, -1/3f),//-x1,-y1
                                                new PointF(-(float)Math.Cos(Math.PI/6)*2/3f, 1/3f),//-x1,y1
                                                new PointF(0, 0)//x0,y0
            };

            for (int i = 0; i < 7; i++)
            {
                PointF circlePos = new PointF(parent.data.pos.X + positions[i].X * parent.data.radius,
                                                parent.data.pos.Y + positions[i].Y * parent.data.radius);
                parent.descendants[i] = new Tree<FractNodeData>.Node((curDepth == maxDepth - 1) ? 0 : 7,
                                                                     new FractNodeData(radius, circlePos));

                GenerateTreeIter(parent.descendants[i], curDepth + 1, maxDepth);
            }
        }

        void DrawCircle(SubWindow w, Pen pen, PointF center, float radius)
        {
            w.graphics.DrawEllipse(pen, w.ToGlobal(new PointF(center.X - radius, center.Y - radius), radius * 2));
        }

        void FillCircle(SubWindow w, Brush brush, PointF center, float radius)
        {
            w.graphics.FillEllipse(brush, w.ToGlobal(new PointF(center.X - radius, center.Y - radius), radius * 2));
        }

        void DrawFractIter(Tree<FractNodeData>.Node node, SubWindow w, int maxDepth, int depth = 1)
        {
            if (depth != maxDepth)
                foreach (Tree<FractNodeData>.Node i in node.descendants)
                    DrawFractIter(i, w, maxDepth, depth + 1);
            else
                FillCircle(w, Brushes.FloralWhite, node.data.pos, node.data.radius);

            DrawCircle(w, new Pen(layerColors[depth - 1]), node.data.pos, node.data.radius);
        }

        private SubWindow[] GenerateWindows(int num, Graphics g)
        {
            SubWindow[] windows = new SubWindow[(int)Math.Pow(Math.Ceiling(Math.Sqrt(num)), 2)];

            int border = (Math.Min(pictureBox1.Width, pictureBox1.Height) - 1) / (int)Math.Sqrt(windows.Length);

            for (int i = 0; i < windows.Length; i++)
                windows[i] = new SubWindow(new Point(border * (i % (int)Math.Sqrt(windows.Length)),
                                            border * (i / (int)Math.Sqrt(windows.Length))),
                                               new PointF(border, border), g);

            return windows;
        }

        void DrawTreeIter(SubWindow w, Tree<FractNodeData>.Node node, int depth, int[] displayedOnLayer, PointF parentPos)
        {
            int nodesOnLayer = Pow(fractTree.destCount, depth - 1);

            PointF step = new PointF(1f / (nodesOnLayer + 1), 1f / (fractTree.maxDepth + 1));
            PointF pos = new PointF(step.X * (1 + displayedOnLayer[depth - 1]),
                                    step.Y * (1 + depth - 1));

            displayedOnLayer[depth - 1]++;

            DrawCircle(w, new Pen(layerColors[depth - 1]), pos, 0.01f);
            if (!parentPos.IsEmpty)
                w.graphics.DrawLine(new Pen(layerColors[depth - 2]), w.ToGlobal(pos), w.ToGlobal(parentPos));

            if (node.descendants != null)
                foreach (Tree<FractNodeData>.Node dest in node.descendants)
                    DrawTreeIter(w, dest, depth + 1, displayedOnLayer, pos);
        }

        int Pow(int num, int pow)
        {
            int val = 1;
            for (int i = 0; i < pow; i++)
                val *= num;
            return val;
        }

        void DrawFract(int maxDepth)
        {
            if (graph == null)
                graph = pictureBox1.CreateGraphics();

            SubWindow[] windows = GenerateWindows(fractTree.maxDepth, graph);
            for (int i = 0; i < maxDepth; i++)
            {
                windows[i].DrawBorders();
                DrawFractIter(fractTree.root, windows[i], i + 1);
            }
        }

        void DrawTree()
        {
            if (pictureBox1.Height < pictureBox1.Width)
                treeWindow = new SubWindow(new PointF(pictureBox1.Height - 2, 0),
                                            new PointF(pictureBox1.Width - pictureBox1.Height, pictureBox1.Height - 1), graph);
            else
                treeWindow = new SubWindow(new PointF(0, pictureBox1.Width - 1),
                                            new PointF(pictureBox1.Width - 1, pictureBox1.Height - pictureBox1.Width), graph);

            treeWindow.DrawBorders();
            int[] displayedOnLayer = new int[fractTree.maxDepth];
            for (int i = 0; i < fractTree.maxDepth; i++)
                displayedOnLayer[i] = 0;
            DrawTreeIter(treeWindow, fractTree.root, 1, displayedOnLayer, PointF.Empty);
        }

        void Generate(int depth)
        {
            GenerateTree(depth);
            GenerateLeyerColors(depth);
        }

        void ClearGraphics()
        {
            if (graph == null)
                graph = pictureBox1.CreateGraphics();

            graph.Clear(Color.White);
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
                return;

            int parsedValue;
            if (int.TryParse(textBox1.Text, out parsedValue) && (parsedValue >= 1))
                Task.Run(delegate
                {
                    Generate(parsedValue);
                    ClearGraphics();
                    DrawTree();
                });
            Focus();
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (graph == null || treeWindow == null)
                return;

            PointF localPos = treeWindow.ToLocal(e.Location);
            float stepY = 1f / (fractTree.maxDepth + 1);

            if (localPos.X < 0 || localPos.X > 1 ||
                (localPos.Y < stepY) ||
                (localPos.Y > 1))
                return;

            int depth = (int)(localPos.Y / stepY);
            Task.Run(delegate
            {
                ClearGraphics();
                DrawTree();
                DrawFract(depth);
            });
        }
    }
}
