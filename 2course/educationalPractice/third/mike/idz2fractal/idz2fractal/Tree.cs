﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace idz2fractal
{
    public class Tree<T>
    {
        public Node root { get; private set; }
        public int destCount { get; private set; }
        public int maxDepth { get; private set; }

        public class Node
        {
            public T data;
            public Node[] descendants;

            public Node(int destCount, T data)
            {
                this.data = data;
                descendants = new Node[destCount];
            }
        }

        public Tree(T data, int destCount, int maxDepth)
        {
            this.destCount = destCount;
            this.maxDepth = maxDepth;
            root = new Node(destCount, data);
        }
    }
}
