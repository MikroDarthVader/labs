﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace second_nikita
{
    public partial class second : Form
    {
        Thread drawThread;

        public second()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            a = Convert.ToDouble(textBox1.Text);
            angleCurveSpeed = Convert.ToDouble(textBox2.Text);
            angleRhombusSpeed = Convert.ToDouble(textBox5.Text);
            rhombusWidth = Convert.ToInt32(textBox4.Text);
            rhombusHeight = Convert.ToInt32(textBox3.Text);
            rhombusThickness = Convert.ToInt32(textBox6.Text);

            switch (comboBox1.SelectedIndex)
            {
                case (0): rhombusColor = Brushes.Green; break;
                case (1): rhombusColor = Brushes.Red; break;
                case (2): rhombusColor = Brushes.Blue; break;
            }

            switch (comboBox2.SelectedIndex)
            {
                case (0): isScaling = true; break;
                case (1): isScaling = false; break;
            }

            if (drawThread == null)
            {
                drawThread = new Thread((ThreadStart)(delegate { Draw(pictureBox1); }));
                drawThread.Start();
            }
        }

        private void second_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (drawThread != null)
                drawThread.Abort();
        }

        double a;

        double angleCurveSpeed;
        double angleRhombusSpeed;
        int rhombusWidth;
        int rhombusHeight;
        int rhombusThickness;
        Brush rhombusColor;
        bool isScaling;

        int dt = 16;

        private void Draw(PictureBox pictureBox)
        {
            List<Point> curve = new List<Point>();
            Rhombus rhombus = new Rhombus(rhombusWidth, rhombusHeight, new Point(pictureBox.Width / 2, pictureBox.Height / 2));

            var g = pictureBox.CreateGraphics();
            while (true)
            {
                for (double angle = 0; angle < Math.PI; angle += angleCurveSpeed * dt * Math.PI / (180 * 1000))
                {
                    g.Clear(pictureBox.BackColor);
                    DrawAxis(pictureBox, g);
                    DrawCurve(pictureBox, g, curve, angle);

                    rhombus.pos = new vector(curve.Last().X, curve.Last().Y);
                    rhombus.Rotate(angleRhombusSpeed * dt * Math.PI / (180 * 1000), curve.Last());
                    ((isScaling ? (float)(Math.Cos(angle * 10) * 0.25 + 0.75) : 1) * rhombus).Draw(g, new Pen(rhombusColor, rhombusThickness));
                    Thread.Sleep(dt);
                }

                curve.Clear();
                g.Clear(pictureBox.BackColor);
            }
        }

        private void DrawAxis(PictureBox pictureBox, Graphics graphic)
        {
            graphic.DrawLine(Pens.Black, pictureBox.Width / 2, 0, pictureBox.Width / 2, pictureBox.Height);
            graphic.DrawLine(Pens.Black, 0, pictureBox.Height / 2, pictureBox.Width, pictureBox.Height / 2);
        }

        private void DrawCurve(PictureBox pictureBox, Graphics graphic, List<Point> curve, double currAngle)
        {
            Point curvePoint = new Point();

            curvePoint.X = (int)(pictureBox.Width / 2 + 50 * CurveFunct(a, currAngle) * Math.Cos(currAngle));
            curvePoint.Y = (int)(pictureBox.Height / 2 - 50 * CurveFunct(a, currAngle) * Math.Sin(currAngle));

            curve.Add(curvePoint);

            if (curve.Count > 1)
                graphic.DrawLines(Pens.Blue, curve.ToArray());
        }

        private double CurveFunct(double a, double angle)
        {
            return a * Math.Cos(3 * angle);
        }
    }
}
