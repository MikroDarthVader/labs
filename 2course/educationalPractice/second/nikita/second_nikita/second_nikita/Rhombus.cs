﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace second_nikita
{
    struct vector
    {
        public float x;
        public float y;

        public vector(float x, float y)
        {
            this.x = x;
            this.y = y;
        }
    }

    class Rhombus
    {
        public vector[] anglePoints = new vector[4];
        public vector pos;

        private float width;
        private float height;

        public Rhombus(float width, float height, Point pos)
        {
            this.pos = new vector();
            this.pos.x = pos.X;
            this.pos.y = pos.Y;

            this.width = width;
            this.height = height;

            anglePoints[0] = new vector(-width / 2, 0);
            anglePoints[1] = new vector(0, height / 2);
            anglePoints[2] = new vector(width / 2, 0);
            anglePoints[3] = new vector(0, -height / 2);
        }

        public Rhombus()
        {
        }

        public void Draw(Graphics g, Pen pen)
        {
            for (int i = 0; i < anglePoints.Length; i++)
            {
                vector first = anglePoints[i];
                vector second = anglePoints[(i + 1) % anglePoints.Length];

                g.DrawLine(pen, pos.x + first.x, pos.y - first.y, pos.x + second.x, pos.y - second.y);
            }
        }

        public void Rotate(double angle, Point around)
        {
            for (int i = 0; i < anglePoints.Length; i++)
            {
                vector localResult = new vector();

                localResult.x = (float)(Math.Cos(angle) * (pos.x + anglePoints[i].x - around.X) - Math.Sin(angle) * (around.Y - pos.y + anglePoints[i].y));
                localResult.y = (float)(Math.Sin(angle) * (pos.x + anglePoints[i].x - around.X) + Math.Cos(angle) * (around.Y - pos.y + anglePoints[i].y));

                anglePoints[i] = localResult;
            }
        }

        public Rhombus Copy()
        {
            Rhombus result = new Rhombus();

            for (int i = 0; i < anglePoints.Length; i++)
                result.anglePoints[i] = anglePoints[i];

            result.pos = pos;
            result.width = width;
            result.height = height;

            return result;
        }

        public static Rhombus operator *(float factor, Rhombus other)
        {
            Rhombus result = other.Copy();
            for (int i = 0; i < result.anglePoints.Length; i++)
            {
                result.anglePoints[i].x *= factor;
                result.anglePoints[i].y *= factor;
            }
            result.width = result.width * factor;
            result.width = result.width * factor;

            return result;
        }
    }
}
