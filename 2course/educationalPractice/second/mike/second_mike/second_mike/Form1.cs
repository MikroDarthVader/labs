﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace second_mike
{
    public partial class Form1 : Form
    {
        static float radius, maxAng;
        static float angularSpeed, timeForIter;
        static Parallelogram par;

        public Form1()
        {
            InitializeComponent();
        }

        static Timer renderer = new Timer();
        static float dt = 0.017f; //~60 fps

        private void textBox6_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
                return;

            float parsedValue;
            if (float.TryParse(textBox6.Text, out parsedValue) && (parsedValue >= 0))
                radius = parsedValue;
            textBox6.Text = radius.ToString();
            textBox1.Focus();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
                return;

            float parsedValue;
            if (float.TryParse(textBox1.Text, out parsedValue) && (parsedValue >= 0))
                timeForIter = parsedValue;
            textBox1.Text = timeForIter.ToString();
            textBox2.Focus();
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
                return;

            float parsedValue;
            if (float.TryParse(textBox2.Text, out parsedValue))
                angularSpeed = parsedValue * (float)Math.PI / 180;
            textBox2.Text = (angularSpeed * 180 / (float)Math.PI).ToString();
            textBox3.Focus();
        }

        private void textBox3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
                return;

            float parsedValue;
            if (float.TryParse(textBox3.Text, out parsedValue) && (parsedValue >= 0) && (parsedValue <= 180))
                par.angle = parsedValue * (float)Math.PI / 180;
            textBox3.Text = (par.angle * 180 / (float)Math.PI).ToString();
            textBox4.Focus();
        }

        private void textBox4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
                return;

            float parsedValue;
            if (float.TryParse(textBox4.Text, out parsedValue) && (parsedValue >= 0))
                par.side1 = parsedValue;
            textBox4.Text = par.side1.ToString();
            textBox5.Focus();
        }

        private void textBox5_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
                return;

            float parsedValue;
            if (float.TryParse(textBox5.Text, out parsedValue) && (parsedValue >= 0))
                par.side2 = parsedValue;
            textBox5.Text = par.side2.ToString();
            this.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
                filler = colorDialog1.Color;
        }

        static float time = 0;

        private void Form1_Load(object sender, EventArgs e)
        {
            radius = 30;
            maxAng = (float)Math.PI * 1.5f;
            timeForIter = 3f;
            angularSpeed = (float)Math.PI / 10;
            par = new Parallelogram(100, 80, (float)Math.PI / 8, new PointF(200, 200));

            textBox6.Text = radius.ToString();
            textBox1.Text = timeForIter.ToString();
            textBox2.Text = (angularSpeed * 180 / (float)Math.PI).ToString();
            textBox3.Text = (par.angle * 180 / (float)Math.PI).ToString();
            textBox4.Text = par.side1.ToString();
            textBox5.Text = par.side2.ToString();

            graphics = pictureBox1.CreateGraphics();
            pictureBox = pictureBox1;
            renderer.Tick += new EventHandler(render);
            renderer.Interval = (int)(dt * 1000);
            renderer.Start();
        }
    }
}
