﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace second_mike
{
    public partial class Form1 : Form
    {
        static List<PointF> path = new List<PointF>();
        static Graphics graphics;
        static PictureBox pictureBox;
        static Color filler;

        private static float lastAng = 0;
        private static void render(object sender, EventArgs e)
        {
            graphics.Clear(Color.White);

            graphics.DrawLine(Pens.Black, pictureBox.Width / 2, 0, pictureBox.Width / 2, pictureBox.Height);
            graphics.DrawLine(Pens.Black, 0, pictureBox.Height / 2, pictureBox.Width, pictureBox.Height / 2);

            par.rot = time * (float)Math.PI * 2 * angularSpeed;

            float ang = (time * maxAng / timeForIter) % maxAng;
            par.setPos(radius * (float)Math.Cos(ang) + radius * ang * (float)Math.Sin(ang) + pictureBox.Width / 2,
                        -(radius * (float)Math.Sin(ang) - radius * ang * (float)Math.Cos(ang)) + pictureBox.Height / 2);
            par.scale = 1 + (float)(Math.Sin(time*4) / 2 + 0.5);

            if (lastAng > ang)
                path.Clear();
            else
                path.Add(par.getPos());

            par.Draw(graphics, new SolidBrush(filler));

            if (path.Count > 1)
                graphics.DrawLines(Pens.Red, path.ToArray());
            graphics.DrawEllipse(Pens.Red, pictureBox.Width / 2 - radius,
                                        pictureBox.Height / 2 - radius,
                                        radius * 2, radius * 2);

            lastAng = ang;
            time += dt;
        }
    }
}
