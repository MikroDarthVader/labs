﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace second_mike
{
    class Parallelogram
    {
        private PointF[] points;//from left upper by clockwise
        private float _side1, _side2, _angle, _rot;//horizontal, vertical, angle between (in point 2 from 0), ..
        private PointF _pos;

        public float side1 { get { return _side1; } set { _side1 = value; updateTransform(); } }
        public float side2 { get { return _side2; } set { _side2 = value; updateTransform(); } }
        public float angle { get { return _angle; } set { _angle = value; updateTransform(); } }
        public float rot { get { return _rot; } set { _rot = value; updateTransform(); } }
        public void setPos(float x, float y) { _pos.X = x; _pos.Y = y; updateTransform(); }
        public PointF getPos() { return _pos; }
        public float scale;

        public Parallelogram(float side1 = 0, float side2 = 0, float angle = 0, PointF pos = new PointF(), float rot = 0, float scale = 1)
        {
            points = new PointF[4];
            _side1 = Math.Abs(side1);
            _side2 = Math.Abs(side2);
            _angle = angle;
            _pos = pos;
            _rot = rot;
            this.scale = scale;
            updateTransform();
        }

        private void updateTransform()
        {
            float x = (float)(-side1 - Math.Cos(angle) * side2) / 2;
            float y = (float)(Math.Sin(angle) * side2) / 2;
            points[0].X = _pos.X + (float)(x * Math.Cos(rot) - y * Math.Sin(rot)) * scale;
            points[0].Y = _pos.Y + (float)(y * Math.Cos(rot) + x * Math.Sin(rot)) * scale;

            x = (float)(side1 - Math.Cos(angle) * side2) / 2;
            y = (float)(Math.Sin(angle) * side2) / 2;
            points[1].X = _pos.X + (float)(x * Math.Cos(rot) - y * Math.Sin(rot)) * scale;
            points[1].Y = _pos.Y + (float)(y * Math.Cos(rot) + x * Math.Sin(rot)) * scale;

            x = (float)(side1 + Math.Cos(angle) * side2) / 2;
            y = -(float)(Math.Sin(angle) * side2) / 2;
            points[2].X = _pos.X + (float)(x * Math.Cos(rot) - y * Math.Sin(rot)) * scale;
            points[2].Y = _pos.Y + (float)(y * Math.Cos(rot) + x * Math.Sin(rot)) * scale;

            x = (float)(-side1 + Math.Cos(angle) * side2) / 2;
            y = -(float)(Math.Sin(angle) * side2) / 2;
            points[3].X = _pos.X + (float)(x * Math.Cos(rot) - y * Math.Sin(rot)) * scale;
            points[3].Y = _pos.Y + (float)(y * Math.Cos(rot) + x * Math.Sin(rot)) * scale;
        }

        public void Draw(Graphics g, Brush fill)
        {
            g.FillPolygon(fill, points);
            g.DrawPolygon(Pens.Black, points);
        }
    }
}
