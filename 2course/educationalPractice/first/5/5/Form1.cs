﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _5
{
    public partial class Form1 : Form
    {
        Point first;
        Point second;

        public Form1()
        {
            InitializeComponent();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawLine(Pens.Green, first, second);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            first.X = Convert.ToInt32(textBox1.Text);
            first.Y = Convert.ToInt32(textBox2.Text);
            second.X = Convert.ToInt32(textBox3.Text);
            second.Y = Convert.ToInt32(textBox4.Text);
            pictureBox1.Refresh();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            first.X = Convert.ToInt32(textBox1.Text);
            first.Y = Convert.ToInt32(textBox2.Text);
            second.X = Convert.ToInt32(textBox3.Text);
            second.Y = Convert.ToInt32(textBox4.Text);
        }
    }
}
