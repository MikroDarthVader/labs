﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _5._3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Graphics graphics = pictureBox1.CreateGraphics();
            Brush brush = Brushes.Orange;
            graphics.Clear(SystemColors.Control);
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    graphics.FillRectangle(brush, 60, 60, 120, 180); break;
                case 1:
                    graphics.FillEllipse(brush, 60, 60, 120, 180); break;
                case 2:
                    graphics.FillEllipse(brush, 60, 60, 120, 120); break;
            }
        }
    }
}
