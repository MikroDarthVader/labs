﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "D:/Downloads/stud/cbl/progalet/labs/2course/educationalPractice/first/4/text.txt";
            openFileDialog1.Filter = "Текстовыефайлы(*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.Filter = "Текстовыефайлы(*.txt)|*.txt|All files (*.*)|*.*";
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (textBox1.Modified == false)
                return;

            var box = MessageBox.Show("Save changes?", "Save???", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);

            if (box == DialogResult.No)
                return;
            if (box == DialogResult.Cancel)
                e.Cancel = true;
            if (box == DialogResult.Yes)
            {
                saveFileDialog1.FileName = openFileDialog1.FileName;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    Save();
                else
                    e.Cancel = true;
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();

            if (openFileDialog1.FileName == null)
                return;

            try
            {
                var MyReader = new StreamReader(openFileDialog1.FileName, Encoding.GetEncoding(1251));
                textBox1.Text = MyReader.ReadToEnd();
                MyReader.Close();
            }
            catch (FileNotFoundException exception)
            {
                MessageBox.Show(exception.Message + "\nFile not found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch(Exception exception)
            {
                MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = openFileDialog1.FileName;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                Save();
        }

        private void Save()
        {
            try
            {
                var MyWriter = new StreamWriter(saveFileDialog1.FileName, false, Encoding.GetEncoding(1251));
                MyWriter.Write(textBox1.Text);
                MyWriter.Close();
                textBox1.Modified = false;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
