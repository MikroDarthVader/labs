﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _5._2
{
    public partial class Form1 : Form
    {
        Point anchor;
        Point arcs;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            anchor.X = Convert.ToInt32(textBox1.Text);
            anchor.Y = Convert.ToInt32(textBox2.Text);
            arcs.X = Convert.ToInt32(textBox3.Text);
            arcs.Y = Convert.ToInt32(textBox4.Text);
            pictureBox1.Refresh();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            anchor.X = Convert.ToInt32(textBox1.Text);
            anchor.Y = Convert.ToInt32(textBox2.Text);
            arcs.X = Convert.ToInt32(textBox3.Text);
            arcs.Y = Convert.ToInt32(textBox4.Text);
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Pen pen = Pens.Black;
            e.Graphics.DrawEllipse(pen, anchor.X, anchor.Y, arcs.X, arcs.Y);
        }
    }
}
