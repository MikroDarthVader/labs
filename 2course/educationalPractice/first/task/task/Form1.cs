﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void PaintCircle(int cX, int cY, int centX, int centY, int radius)
        {
            Graphics graphics = pictureBox1.CreateGraphics();
            graphics.DrawEllipse(Pens.Black, cX + centX - radius, cY - centY - radius, radius * 2, radius * 2);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Point[] points = new Point[64];

            Point bigCenter = new Point(120, 120);
            int R2 = 90;
            int k = 5;
            int R1 = R2 / k;

            Graphics graphics = pictureBox1.CreateGraphics();
            int i = 0;
            for (double angle = 0; angle <= 6.3; angle += 0.1, i++)
            {
                graphics.Clear(BackColor);

                double x = R1 * (k - 1) * (Math.Cos(angle) + Math.Cos((k - 1) * angle) / (k - 1));
                double y = R1 * (k - 1) * (Math.Sin(angle) - Math.Sin((k - 1) * angle) / (k - 1));

                points[i] = new Point(bigCenter.X + (int)x, bigCenter.Y + (int)y);
                graphics.DrawLines(Pens.Red, points);

                PaintCircle(bigCenter.X, bigCenter.Y, 0, 0, R2);

                double x1 = (R2 - R1) * Math.Sin(angle + 1.57);
                double y1 = (R2 - R1) * Math.Cos(angle + 1.57);
                PaintCircle(bigCenter.X, bigCenter.Y, (int)x1, (int)y1, R1);

                System.Threading.Thread.Sleep(40);
            }
        }
    }
}
