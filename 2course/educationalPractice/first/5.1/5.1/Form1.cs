﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _5._1
{
    public partial class Form1 : Form
    {
        Point first;
        Point second;
        Point third;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            first.X = Convert.ToInt32(textBox1.Text);
            first.Y = Convert.ToInt32(textBox2.Text);
            second.X = Convert.ToInt32(textBox3.Text);
            second.Y = Convert.ToInt32(textBox4.Text);
            third.X = Convert.ToInt32(textBox5.Text);
            third.Y = Convert.ToInt32(textBox6.Text);
            pictureBox1.Refresh();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            first.X = Convert.ToInt32(textBox1.Text);
            first.Y = Convert.ToInt32(textBox2.Text);
            second.X = Convert.ToInt32(textBox3.Text);
            second.Y = Convert.ToInt32(textBox4.Text);
            third.X = Convert.ToInt32(textBox5.Text);
            third.Y = Convert.ToInt32(textBox6.Text);
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawLine(Pens.Black, first, second);
            e.Graphics.DrawLine(Pens.Red, second, third);
            e.Graphics.DrawLine(Pens.Yellow, third, first);
        }
    }
}
