#pragma once

class ArrayMultiplicity
{
private:
	char tag;
	char *values;

	bool Contains(char c);
public:
	ArrayMultiplicity();
	ArrayMultiplicity(char tag);
	~ArrayMultiplicity();

	ArrayMultiplicity(const ArrayMultiplicity& other);

	void SetRandom(int min, int max);
	void Print();

	ArrayMultiplicity operator|(const ArrayMultiplicity& other);
	ArrayMultiplicity operator ~();
	ArrayMultiplicity operator&(const ArrayMultiplicity& other);
	ArrayMultiplicity& operator = (const ArrayMultiplicity& other);
};