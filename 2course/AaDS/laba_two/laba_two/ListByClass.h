#pragma once

template <typename T> class PointerList
{
private:
	template <typename T> class PointerListNode
	{
	public:
		PointerListNode()
		{
			this->Value = nullptr;
			this->next = nullptr;
			this->prev = nullptr;
		}
		PointerListNode(T *Value, PointerListNode *prev, PointerListNode *next) 
		{
			this->Value = Value;
			this->next = next;
			this->prev = prev;
		}
		~PointerListNode()
		{
			if (Value)
				free(Value);
		}

		T *Value;
		PointerListNode *next;
		PointerListNode *prev;
	};

	unsigned int Count;
	PointerListNode<T> *first;
	PointerListNode<T> *last;

public:
	PointerList()
	{
		Count = 0;
		first = nullptr;
		last = nullptr;
	}
	~PointerList()
	{
		if (first)
			free(first);
		if (last)
			free(last);
	}

	T* begin() 
	{
		return &data[0];
	}

	T* end() 
	{
		return  &data[sizeOfArray];
	}

	void Add(T *record)
	{
		PointerListNode<T> *node;
		if (!first)
		{
			node = first = new PointerListNode(record, nullptr, nullptr);
		}
		else
		{
			node = new PointerListNode(record, last, nullptr);
			last->next = node;
		}

		last = node;
		Count++;
	}
	bool Remove(int index)
	{
		if (index >= Count || index < 0)
			return false;

		PointerListNode<T> node;
		for (int i = 0, node = self->first; i != index; i++, node = node->next);

		if (node->prev)
			node->prev->next = node->next;
		else
			self->first = node->next;

		if (node->next)
			node->next->prev = node->prev;
		else
			self->last = node->prev;

		delete node;

		self->Count--;

		return true;
	}
};


#define for_each(self, ind, val) \
	PointerListNode *listNode = self.first; \
	void *val = listNode ? listNode->Value : NULL; \
	int ind = 0; \
	for(; ind < self.Count && listNode ; listNode = listNode->next, val = listNode? listNode->Value : NULL, ind++)
