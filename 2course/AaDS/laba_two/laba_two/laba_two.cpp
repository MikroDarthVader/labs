// ConsoleApplication1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

//#define Array
//#define List
//#define BitArray
#define MachineWord

uint64_t getTimeNanoSec()
{
	chrono::high_resolution_clock m_clock;
	return chrono::duration_cast<std::chrono::nanoseconds>(m_clock.now().time_since_epoch()).count();
}

long long int Random(long long int min, long long int max)
{
	uint64_t nanosec = getTimeNanoSec();
	return nanosec % (max + 1 - min) + min;
}

void *copyChar(void *c)
{
	char *res = (char *)malloc(1);
	*res = *(char *)c;
	return res;
}

char getCharByNum(int num)
{
	return 'A' + num + (num > 25 ? 6 : 0);
}

int getNumByChar(char c)
{
	return c - 'A' - (c >= 'a' ? 6 : 0);
}

char getRandChar()
{
	int rnd = Random(0, 51);
	return getCharByNum(rnd);
}

int main()
{
	{
		int activeModule;

#if defined(Array)
		ArrayMultiplicity result = ArrayMultiplicity('R'), A = ArrayMultiplicity('A'), B = ArrayMultiplicity('B'), C = ArrayMultiplicity('C'), D = ArrayMultiplicity('D');
		ArrayMultiplicity *values[4] = { &A, &B, &C, &D };
#elif defined(List)
		ListMultiplicity result = ListMultiplicity('R'), A = ListMultiplicity('A'), B = ListMultiplicity('B'), C = ListMultiplicity('C'), D = ListMultiplicity('D');
		ListMultiplicity *values[4] = { &A, &B, &C, &D };
#elif defined(BitArray)
		bitArrayMultiplicity result = bitArrayMultiplicity('R'), A = bitArrayMultiplicity('A'), B = bitArrayMultiplicity('B'), C = bitArrayMultiplicity('C'), D = bitArrayMultiplicity('D');
		bitArrayMultiplicity *values[4] = { &A, &B, &C, &D };
#elif defined(MachineWord)
		machineWordMultiplicity result = machineWordMultiplicity('R'), A = machineWordMultiplicity('A'), B = machineWordMultiplicity('B'), C = machineWordMultiplicity('C'), D = machineWordMultiplicity('D');
		machineWordMultiplicity *values[4] = { &A, &B, &C, &D };
#else
		return;
#endif

		for (int i = 0; i < 4; i++)
		{
			values[i]->SetRandom(26, 26); // 52 - max size
			values[i]->Print();
		}

		int iterNum = 100000;
		uint64_t sumTime = getTimeNanoSec();
		for (int i = 0; i < iterNum; i++)
		{
			result = (A & ~B) | (C & D);
		}
		sumTime = getTimeNanoSec() - sumTime;
		cout << endl;
		result.Print();

		cout << "Average elapsed time: " << (sumTime / iterNum) / 1000000000.f << " sec (" << (sumTime / iterNum) << " nanosec)" << endl << endl;

	}
	system("pause");
}