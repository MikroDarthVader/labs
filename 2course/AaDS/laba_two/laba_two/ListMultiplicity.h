#pragma once

#include "List.h"

class ListMultiplicity
{
private:
	char tag;
	PointerList values;

	bool Contains(char c);
public:
	ListMultiplicity(char tag);
	~ListMultiplicity();
	ListMultiplicity(const ListMultiplicity& other);

	void SetRandom(int min, int max);
	void Print();

	ListMultiplicity operator | (const ListMultiplicity& other);
	ListMultiplicity operator ~ ();
	ListMultiplicity operator & (const ListMultiplicity& other);
	ListMultiplicity& operator = (const ListMultiplicity& other);
};