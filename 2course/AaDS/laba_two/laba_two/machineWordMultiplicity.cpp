#include "stdafx.h"

//#define FunctTracking

machineWordMultiplicity::machineWordMultiplicity(char tag)
{
#if defined (FunctTracking)
	cout << "Created machineWordMultiplicity with tag '" << tag << "'" << endl;
#endif // FunctTracking

	this->tag = tag;
}

machineWordMultiplicity::~machineWordMultiplicity()
{
#if defined (FunctTracking)
	cout << "Destroed machineWordMultiplicity with tag '" << tag << "'" << endl;
#endif // FunctTracking
}

machineWordMultiplicity::machineWordMultiplicity(const machineWordMultiplicity& other)
{
#if defined (FunctTracking)
	cout << "Copied machineWordMultiplicity with tag '" << other.tag << "'" << endl;
#endif // FunctTracking
	tag = other.tag;
	values = other.values;
}

void machineWordMultiplicity::Print()
{
	cout << tag << ":    ";
	for (int i = 0; i < 52; i++)
	{
		if ((values & (1ULL << i)) != 0)
			cout << getCharByNum(i);
	}

	cout << endl;
}

void machineWordMultiplicity::printIntBits(unsigned long long int num, int minLenght, int maxLenght, int curLenght = 0)
{
	if ((num || curLenght < minLenght) && curLenght < maxLenght)
	{
		printIntBits(num >> 1, minLenght, maxLenght, ++curLenght);
		num &= 1;
		cout << num;
	}
}

void machineWordMultiplicity::SetRandom(int min, int max)
{
	char *possibleValues = (char *)malloc(53);

	for (int i = 0; i < 52; i++)
		possibleValues[i] = getCharByNum(i);
	possibleValues[52] = 0;

	values = 0;

	int rnd = Random(min, max);

	int maxPosChar = 51;
	for (int j = 0; j < rnd; j++)
	{
		int charPos = Random(0, maxPosChar);

		values |= 1ULL << getNumByChar(possibleValues[charPos]);
		char temp = possibleValues[charPos];
		possibleValues[charPos] = possibleValues[maxPosChar];
		possibleValues[maxPosChar] = temp;
		maxPosChar--;
	}



	free(possibleValues);
	possibleValues = 0;
}

machineWordMultiplicity machineWordMultiplicity::operator& (const machineWordMultiplicity& other)
{
#if defined (FunctTracking)
	cout << tag << " & " << other.tag << " calculation" << endl;
#endif // FunctTracking
	machineWordMultiplicity result = machineWordMultiplicity('&');
	result.values = values & other.values;

	return result;
}

machineWordMultiplicity machineWordMultiplicity::operator| (const machineWordMultiplicity& other)
{
#if defined (FunctTracking)
	cout << tag << " | " << other.tag << " calculation" << endl;
#endif // FunctTracking
	machineWordMultiplicity result = machineWordMultiplicity('|');
	result.values = values | other.values;

	return result;
}

machineWordMultiplicity machineWordMultiplicity::operator ~ ()
{
#if defined (FunctTracking)
	cout << "~" << tag << " calculation" << endl;
#endif // FunctTracking
	machineWordMultiplicity result = machineWordMultiplicity('~');
	result.values = ~values;

	return result;
}

machineWordMultiplicity& machineWordMultiplicity::operator = (const machineWordMultiplicity& other)
{
	values = other.values;

	return *this;
}