#pragma once

class machineWordMultiplicity
{
private:
	char tag;
	unsigned long long int values;

	void printIntBits(unsigned long long int num, int minLenght, int maxLenght, int curLenght);
public:
	machineWordMultiplicity();
	machineWordMultiplicity(char tag);
	~machineWordMultiplicity();

	machineWordMultiplicity(const machineWordMultiplicity& other);

	void SetRandom(int min, int max);
	void Print();

	machineWordMultiplicity operator | (const machineWordMultiplicity& other);
	machineWordMultiplicity operator ~ ();
	machineWordMultiplicity operator & (const machineWordMultiplicity& other);
	machineWordMultiplicity& operator = (const machineWordMultiplicity& other);
};