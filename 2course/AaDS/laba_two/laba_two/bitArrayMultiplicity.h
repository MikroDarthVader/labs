#pragma once

class bitArrayMultiplicity
{
private:
	char tag;
	bool *values;
	int ValuesSize;

public:
	bitArrayMultiplicity(char tag);
	~bitArrayMultiplicity();

	bitArrayMultiplicity(const bitArrayMultiplicity& other);

	void SetRandom(int min, int max);
	void Print();

	bitArrayMultiplicity operator | (const bitArrayMultiplicity& other);
	bitArrayMultiplicity operator ~ ();
	bitArrayMultiplicity operator & (const bitArrayMultiplicity& other);
	bitArrayMultiplicity& operator = (const bitArrayMultiplicity& other);
};