
#include "stdafx.h"

bitArrayMultiplicity::bitArrayMultiplicity(char tag)
{
	this->tag = tag;

	values = new bool[52];
	ValuesSize = 52;
}

bitArrayMultiplicity::~bitArrayMultiplicity()
{
	delete[] values;
}

bitArrayMultiplicity::bitArrayMultiplicity(const bitArrayMultiplicity& other)
{
	tag = other.tag;

	values = new bool[52];
	ValuesSize = 52;

	for (int i = 0; i < ValuesSize; i++)
	{
		values[i] = other.values[i];
	}
}

void bitArrayMultiplicity::SetRandom(int min, int max)
{
	char *possibleValues = (char *)malloc(53);

	for (int i = 0; i < ValuesSize; i++)
		possibleValues[i] = getCharByNum(i);
	possibleValues[ValuesSize] = 0;

	for (int j = 0; j < ValuesSize; j++)
	{
		values[j] = false;
	}

	int rnd = Random(min, max);

	int maxPosChar = 51;
	for (int j = 0; j < rnd; j++)
	{
		int charPos = Random(0, maxPosChar);

		values[getNumByChar(possibleValues[charPos])] = true;

		char temp = possibleValues[charPos];
		possibleValues[charPos] = possibleValues[maxPosChar];
		possibleValues[maxPosChar] = temp;
		maxPosChar--;
	}

	free(possibleValues);
	possibleValues = 0;
}

void bitArrayMultiplicity::Print()
{
	cout << tag << ":    ";
	for (int i = 0; i < 52; i++)
	{
		if (values[i])
			cout << getCharByNum(i);
	}

	cout << endl;
}

bitArrayMultiplicity bitArrayMultiplicity::operator&(const bitArrayMultiplicity& other)
{
	bitArrayMultiplicity result = bitArrayMultiplicity('&');

	for (int i = 0; i < ValuesSize; i++)
		result.values[i] = values[i] && other.values[i];

	return result;
}

bitArrayMultiplicity bitArrayMultiplicity::operator~()
{
	bitArrayMultiplicity result = bitArrayMultiplicity('~');

	for (int i = 0; i < ValuesSize; i++)
		result.values[i] = !values[i];

	return result;
}

bitArrayMultiplicity bitArrayMultiplicity::operator|(const bitArrayMultiplicity& other)
{
	bitArrayMultiplicity result = bitArrayMultiplicity('|');

	for (int i = 0; i < ValuesSize; i++)
		result.values[i] = values[i] || other.values[i];

	return result;
}

bitArrayMultiplicity& bitArrayMultiplicity::operator = (const bitArrayMultiplicity& other)
{
	for (int i = 0; i < ValuesSize; i++)
	{
		values[i] = other.values[i];
	}

	return *this;
}