
#include "stdafx.h"

ListMultiplicity::ListMultiplicity(char tag)
{
	this->tag = tag;
	values = EmptyPointerList;
}

ListMultiplicity::~ListMultiplicity()
{
	List.Free(values, free);
	values = EmptyPointerList;
}

ListMultiplicity::ListMultiplicity(const ListMultiplicity& other)
{
	tag = other.tag;
	values = List.Copy(other.values, copyChar);
}

bool ListMultiplicity::Contains(char c)
{
	for_each(values, ind, val)
	{
		if (*(char *)val == c)
			return true;
	}

	return false;
}

ListMultiplicity ListMultiplicity::operator&(const ListMultiplicity& other)
{
	ListMultiplicity result = ListMultiplicity('&');

	char *Value = 0;

	for_each(values, l_ind, l_val)
	{
		for_each(other.values, r_ind, r_val)
		{
			if (*(char *)l_val == *(char *)r_val)
				List.Add(&result.values, copyChar(l_val));
		}
	}

	return result;
}

ListMultiplicity ListMultiplicity::operator~()
{
	ListMultiplicity result = ListMultiplicity('~');

	bool finded = false;

	for (int j = 0; j < 52; j++)
	{
		finded = false;

		for_each(values, i, val)
		{
			if (getCharByNum(j) == *(char *)val)
				finded = true;
		}

		if (!finded)
		{
			char *newVal = (char *)malloc(1);
			*newVal = getCharByNum(j);
			List.Add(&result.values, newVal);
		}
	}

	return result;
}

ListMultiplicity ListMultiplicity::operator|(const ListMultiplicity& other)
{
	ListMultiplicity result = ListMultiplicity('|');

	{
		for_each(values, ind, val)
			List.Add(&result.values, copyChar(val));
	}
	//two for_each cycles can't work in the same access space
	{
		for_each(other.values, ind, val)
			if (!result.Contains(*(char *)val))
				List.Add(&result.values, copyChar(val));
	}

	return result;
}

ListMultiplicity& ListMultiplicity::operator = (const ListMultiplicity& other)
{
	List.Free(values, free);
	values = List.Copy(other.values, copyChar);

	return *this;
}

void ListMultiplicity::SetRandom(int min, int max)
{
	char* possibleValues = (char *)malloc(53);

	for (int i = 0; i < 52; i++)
		possibleValues[i] = getCharByNum(i);
	possibleValues[52] = 0;

	char *val;
	int rnd = Random(min, max);

	while (values.Count)
		List.Remove(&values, 0, free);

	int maxPosChar = 51;
	for (int i = 0; i < rnd; i++)
	{
		val = (char*)malloc(1);
		int charPos = Random(0, maxPosChar);
		*val = possibleValues[charPos];
		List.Add(&values, val);

		char temp = possibleValues[charPos];
		possibleValues[charPos] = possibleValues[maxPosChar];
		possibleValues[maxPosChar] = temp;
		maxPosChar--;
	}

	free(possibleValues);
	possibleValues = 0;
}

void ListMultiplicity::Print()
{
	cout << tag << ":    ";
	for_each(values, ind, val)
		cout << *(char *)val;
	cout << endl;
}