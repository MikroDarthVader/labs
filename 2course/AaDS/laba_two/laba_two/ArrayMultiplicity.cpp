#include "stdafx.h"


ArrayMultiplicity::ArrayMultiplicity()
{
	values = new char[53];
}

ArrayMultiplicity::ArrayMultiplicity(char tag)
{
	this->tag = tag;

	values = new char[53];

}

ArrayMultiplicity::ArrayMultiplicity(const ArrayMultiplicity& other)
{
	values = new char[53];

	tag = other.tag;

	int i;
	for (i = 0; other.values[i] != 0; i++)
		values[i] = other.values[i];

	values[i] = 0;
}

bool ArrayMultiplicity::Contains(char c)
{
	for (char *i = values; *i != 0; i++)
	{
		if (c == *i)
			return true;
	}

	return false;
}

ArrayMultiplicity::~ArrayMultiplicity()
{
	delete[] values;
}

ArrayMultiplicity ArrayMultiplicity::operator&(const ArrayMultiplicity& other)
{
	bool add = false;
	int size = 0;

	ArrayMultiplicity result = ArrayMultiplicity('&');

	for (char* i = values; *i != 0; i++)
	{
		add = false;
		for (char* j = other.values; *j != 0 && !add; j++)
			if (*i == *j)
				add = true;

		if (add)
		{
			size++;
			result.values[size] = 0;
			result.values[size - 1] = *i;
		}
	}

	return result;
}

ArrayMultiplicity ArrayMultiplicity::operator ~()
{
	ArrayMultiplicity result = ArrayMultiplicity('~');

	int k = 0;
	bool finded = false;

	for (int j = 0; j < 52; j++)
	{
		finded = false;

		for (char* i = values; *i != 0; i++)
		{
			if (getCharByNum(j) == *i)
				finded = true;
		}

		if (!finded)
		{
			k++;
			result.values[k] = 0;
			result.values[k - 1] = getCharByNum(j);
		}
	}

	return result;
}

ArrayMultiplicity ArrayMultiplicity:: operator|(const ArrayMultiplicity& other)
{
	ArrayMultiplicity result = ArrayMultiplicity('~');
	int k = 0;

	for (char* i = values; *i != 0; i++)
	{
		if (!result.Contains(*i))
		{
			k++;
			result.values[k - 1] = *i;
		}
	}

	for (char* i = other.values; *i != 0; i++)
	{
		if (!result.Contains(*i))
		{
			k++;
			result.values[k - 1] = *i;
		}
	}

	result.values[k] = 0;

	return result;
}

ArrayMultiplicity& ArrayMultiplicity::operator = (const ArrayMultiplicity& other)
{
	int i;
	for (i = 0; other.values[i] != 0; i++)
		values[i] = other.values[i];

	values[i] = 0;

	return *this;
}

void ArrayMultiplicity::SetRandom(int min, int max)
{
	char *possibleValues = (char *)malloc(53);

	for (int i = 0; i < 52; i++)
		possibleValues[i] = getCharByNum(i);
	possibleValues[52] = 0;

	int rnd = Random(min, max);

	int maxPosChar = 51;
	for (int i = 0; i < rnd; i++)
	{
		int charPos = Random(0, maxPosChar);
		values[i] = possibleValues[charPos];

		char temp = possibleValues[charPos];
		possibleValues[charPos] = possibleValues[maxPosChar];
		possibleValues[maxPosChar] = temp;
		maxPosChar--;
	}

	values[rnd] = 0;

	free(possibleValues);
	possibleValues = 0;
}

void ArrayMultiplicity::Print()
{
	cout << tag << ":                    " << values << endl;
}