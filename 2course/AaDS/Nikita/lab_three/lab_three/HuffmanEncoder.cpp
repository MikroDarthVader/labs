#include "pch.h"
#include "HuffmanEncoder.h"



HuffmanEncoder::HuffmanEncoder(string str)
{
	initStr = str;
	GenerateCodes();
}

string HuffmanEncoder::CodedString()
{
	string result = "";
	for (int i = initStr.length() - 1; i >= 0; i--)
	{
		string *out;
		codes.Find(initStr[i], &out);
		result.insert(0, *out);
	}

	return result;
}

string HuffmanEncoder::EncodedString(string codedString)
{
	string result;
	SinglyLinkedList<KeyValuePair<char, string>>* codesList = codes.GetAllRecords();
	for (int posInCodedStr = 0; posInCodedStr != codedString.length();)
	{
		for (int i = 0; i < codesList->length; i++)
		{
			KeyValuePair<char, string>* codePair = codesList->at(i);
			string sub = codedString.substr(posInCodedStr, codePair->value.length());
			if (sub == codePair->value)
			{
				result += codePair->key;
				posInCodedStr += codePair->value.length();
				break;
			}
		}
	}

	return result;
}

void HuffmanEncoder::GenerateCodes()
{
	Map<char, int> charFrequency = Map<char, int>();
	int *out; // generating char frequency
	for (unsigned int i = 0; i < initStr.length(); i++)
	{
		if (charFrequency.Find(initStr[i], &out))
			(*out)++;
		else
		{
			charFrequency.Insert(initStr[i], 1);
			codes.Insert(initStr[i], "");
		}
	}

	// huffman algorithm
	SinglyLinkedList<HuffmanTreeElement> huffmanTreeElements = SinglyLinkedList<HuffmanTreeElement>();

	SinglyLinkedList<KeyValuePair<char, int>>* charFrequencyList = charFrequency.GetAllRecords();

	for (int i = 0; i < charFrequencyList->length; i++)
	{
		KeyValuePair<char, int>* data = charFrequencyList->at(i);

		HuffmanTreeElement* element = new HuffmanTreeElement();
		element->charsInChild.push_back(new char(data->key));
		element->numOfReiteration = data->value;

		huffmanTreeElements.push_back(element);
	}
	cout << "char frequency list: " << *charFrequencyList << endl;
	delete charFrequencyList;


	SinglyLinkedList<HuffmanTreeElement>* minimalByCharsInChildLength = new SinglyLinkedList<HuffmanTreeElement>();
	SinglyLinkedList<HuffmanTreeElement>* nearToMinimalByCharsInChildLength = new SinglyLinkedList<HuffmanTreeElement>();
	while (huffmanTreeElements.length != 1)
	{
		int min = 0;
		int minWithUpperCharsInChildLength = -1;

		for (int i = 1; i < huffmanTreeElements.length; i++)
		{
			if (huffmanTreeElements.at(min)->charsInChild.length > huffmanTreeElements.at(i)->charsInChild.length)
				min = i;
		}
		for (int i = 0; i < huffmanTreeElements.length; i++)
		{
			if (minWithUpperCharsInChildLength == -1 && huffmanTreeElements.at(min)->charsInChild.length < huffmanTreeElements.at(i)->charsInChild.length ||
				minWithUpperCharsInChildLength != -1 && huffmanTreeElements.at(min)->charsInChild.length < huffmanTreeElements.at(i)->charsInChild.length &&
				huffmanTreeElements.at(i)->charsInChild.length < huffmanTreeElements.at(minWithUpperCharsInChildLength)->charsInChild.length)
				minWithUpperCharsInChildLength = i;
		}
		for (int i = 0; i < huffmanTreeElements.length; i++)
		{
			if (minWithUpperCharsInChildLength != -1)
				if (huffmanTreeElements.at(minWithUpperCharsInChildLength)->charsInChild.length == huffmanTreeElements.at(i)->charsInChild.length)
					nearToMinimalByCharsInChildLength->push_back(huffmanTreeElements.at(i));

			if (huffmanTreeElements.at(min)->charsInChild.length == huffmanTreeElements.at(i)->charsInChild.length)
				minimalByCharsInChildLength->push_back(huffmanTreeElements.at(i));
		}

		int firstNum = 0;
		int secondNum = 1;

		HuffmanTreeElement* first;
		HuffmanTreeElement* second;

		if (minimalByCharsInChildLength->length > 1)
		{
			for (int i = 1; i < minimalByCharsInChildLength->length; i++)
			{
				if (minimalByCharsInChildLength->at(firstNum)->numOfReiteration >= minimalByCharsInChildLength->at(i)->numOfReiteration)
				{
					secondNum = firstNum;
					firstNum = i;
				}
				else if (minimalByCharsInChildLength->at(secondNum)->numOfReiteration > minimalByCharsInChildLength->at(i)->numOfReiteration)
				{
					secondNum = i;
				}
			}

			first = minimalByCharsInChildLength->at(firstNum);
			second = minimalByCharsInChildLength->at(secondNum);
		}
		else
		{
			for (int i = 0; i < nearToMinimalByCharsInChildLength->length; i++)
			{
				if (nearToMinimalByCharsInChildLength->at(firstNum)->numOfReiteration > nearToMinimalByCharsInChildLength->at(i)->numOfReiteration)
				{
					firstNum = i;
				}
			}

			first = nearToMinimalByCharsInChildLength->at(firstNum);
			second = minimalByCharsInChildLength->at(0);
		}

		string* out;
		for (int i = 0; i < first->charsInChild.length; i++)
		{
			codes.Find(*first->charsInChild.at(i), &out);
			(*out).insert(0, "0");
		}

		for (int i = 0; i < second->charsInChild.length; i++)
		{
			codes.Find(*second->charsInChild.at(i), &out);
			(*out).insert(0, "1");
		}

		first->numOfReiteration += second->numOfReiteration;
		for (int i = 0; i < second->charsInChild.length; i++)
		{
			first->charsInChild.push_back(new char(*second->charsInChild.at(i)));
		}

		huffmanTreeElements.remove(second);

		minimalByCharsInChildLength->clear(false);
		nearToMinimalByCharsInChildLength->clear(false);
	}

	delete minimalByCharsInChildLength;
	delete nearToMinimalByCharsInChildLength;

	cout << "generatedCodes" << codes << endl;
}
