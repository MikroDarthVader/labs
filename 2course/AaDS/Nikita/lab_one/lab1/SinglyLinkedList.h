#pragma once

#include "stdafx.h"

template <class T> class List
{
	struct Node
	{
		Node* next;
		T *data;

		Node(T *data) : next(nullptr), data(data)
		{}
	};

	struct IType
	{
		Node* node;

		IType(Node* node) : node(node)
		{}

		bool operator!=(IType right)
		{
			return data != right.data;
		}

		T& operator*()
		{
			return *data->data;
		}

		void operator++()
		{
			data = data->next;
		}
	};

	Node * first;
	Node * last;
	unsigned int length;

	IType begin() { return IType(first); }
	IType end() { return IType(last); }

public:

	List();
	~List();

	unsigned int getLength();
	void push_back(T *data);
	void push_front(T *data);
	T *pop_back();
	T *pop_front();
	void insert(const unsigned int index, T *data);
	T *at(const unsigned  int index);
	unsigned int find(T* data);
	void remove(const unsigned int index);
	void remove(const T* data);
	void set(const unsigned int index, T *data);
	void clear(bool deleteData = true);
	bool isEmpty();

	friend ostream & operator << (ostream &out, const List<T> &list)
	{
		out << "[";
		for (List<T>::Node* currNode = list.first; currNode != nullptr; currNode = currNode->next)
		{
			out << *currNode->data;
			if (currNode->next != nullptr)
				out << ", ";
		}
		out << "]";

		return out;
	}

	friend bool operator==(const List& left, const List& right)
	{
		if (left.length != right.length)
			return false;

		Node* currLeft = left.first;
		Node* currRight = right.first;

		for (; currLeft != nullptr; currLeft = currLeft->next, currRight = currRight->next)
			if (*currLeft->data != *currRight->data)
				return false;

		return true;
	}

	List(const List &other)
	{
		if (length == 0)
			clear();

		Node* currNode;
		for (currNode = other.first; currNode != nullptr; currNode = currNode->next)
			push_back(new T(*currNode->data));
	}
};

template <class T>
List<T>::List()
{
	first = nullptr;
	last = nullptr;
	length = 0;
}

template<class T>
List<T>::~List()
{
	clear();
}

template<class T>
unsigned int List<T>::getLength()
{
	return length;
}

template<class T>
void List<T>::push_back(T *data)
{
	Node* newNode = new Node(data);

	if (!isEmpty())
	{
		last->next = newNode;
		last = newNode;
	}
	else
	{
		first = newNode;
		last = newNode;
	}

	length++;
}

template<class T>
void List<T>::push_front(T *data)
{
	Node* newNode = new Node(data);
	if (isEmpty())
		last = newNode;
	else
		newNode->next = first;

	first = newNode;

	length++;
}

template<class T>
T *List<T>::pop_back()
{
	if (isEmpty())
		throw out_of_range("List empty");

	T *deletedData;
	if (length > 1)
	{
		Node* prevLastNode;
		for (prevLastNode = first; prevLastNode->next->next != nullptr; prevLastNode = prevLastNode->next); // search for node that in front of last node
		deletedData = prevLastNode->next->data;
		delete prevLastNode->next;
		prevLastNode->next = nullptr;
		last = prevLastNode;
	}
	else
	{
		deletedData = first->data;
		delete first;
		first = nullptr;
		last = nullptr;
	}

	length--;

	return deletedData;
}

template<class T>
T *List<T>::pop_front()
{
	if (isEmpty())
		throw out_of_range("List empty");

	Node* firstNode = first;
	first = first->next;
	T *deletedData = firstNode->data;
	if (length > 1)
		delete firstNode;
	else
		last = nullptr;

	length--;

	return deletedData;
}

template<class T>
void List<T>::insert(const unsigned int index, T *data)
{
	if (index > length && index != 0) // if length of list equal zero we insert new node 
		throw out_of_range("Index is greater than list size");

	if (index == 0)
		push_front(data);
	else if (index == length)
		push_back(data);
	else
	{
		Node* newNode = new Node(data);

		Node* currNode = first;
		for (int i = 0; i + 1 != index; i++, currNode = currNode->next); // search for node that located on (index - 1) position in list

		newNode->next = currNode->next;
		currNode->next = newNode;

		length++;
	}
}

template<class T>
T *List<T>::at(const unsigned int index)
{
	if (index >= length)
		throw out_of_range("Index is greater than list size");

	Node* currNode = first;
	for (int i = 0; i != index; i++, currNode = currNode->next); // search for node that located on (index) position in list

	return currNode->data;
}

template<class T>
unsigned int List<T>::find(T * data)
{
	int i = 0;
	Node* currNode = first;
	for (; currNode != nullptr && *currNode->data != *data; currNode = currNode->next, i++);

	if (currNode == nullptr)
		throw invalid_argument("Same data doesn't exist in list");

	return i;
}

template<class T>
void List<T>::remove(const unsigned int index)
{
	if (index >= length)
		throw out_of_range("Index is greater than list size");

	if (index == 0)
		delete pop_front();
	else if (index == length - 1)
		delete pop_back();
	else
	{
		Node* currNode = first;
		for (int i = 0; i + 1 != index; i++, currNode = currNode->next); // search for node that located on (index - 1) position in list

		Node* nodeToDelete = currNode->next;
		currNode->next = nodeToDelete->next;
		delete nodeToDelete->data;
		delete nodeToDelete;

		length--;
	}
}

template<class T>
void List<T>::remove(const T* data)
{
	if (*first->data == *data)
	{
		delete pop_front();
		return;
	}
	else if (*last->data == *data)
	{
		delete pop_back();
		return;
	}

	Node* currNode = first;
	for (; currNode->next != nullptr && *currNode->next->data != *data; currNode = currNode->next);

	if (currNode->next == nullptr)
		return;

	Node* nodeToDelete = currNode->next;
	currNode->next = nodeToDelete->next;
	delete nodeToDelete->data;
	delete nodeToDelete;

	length--;
}

template<class T>
void List<T>::clear(bool deleteData)
{
	if (isEmpty())
		return;

	Node* currNode = first;
	do
	{
		Node *nextNode = currNode->next;
		if (deleteData)
			delete currNode->data;
		delete currNode;
		currNode = nextNode;
	} while (currNode != nullptr);

	first = nullptr;
	last = nullptr;

	length = 0;
}

template<class T>
void List<T>::set(const unsigned int index, T *data)
{
	if (index >= length)
		throw out_of_range("Index is greater than list size");

	Node* currNode = first;
	for (int i = 0; i != index; i++, currNode = currNode->next); // search for node that located on (index) position in list

	currNode->data = data;
}

template<class T>
bool List<T>::isEmpty()
{
	return length == 0;
}