#pragma once

#include "SinglyLinkedList.h"
#include "graph.h"

#define NotAvailable -1
struct CityConnection
{
	string cityA;
	string cityB;
	int fromAtoBPrice;
	int fromBtoAPrice;

	CityConnection(vector<string> fields)
	{
		cityA = fields[0];
		cityB = fields[1];
		if (fields[2] != "N/A")
			fromAtoBPrice = stoi(fields[2]);
		else
			fromAtoBPrice = -1;
		if (fields[3] != "N/A")
			fromBtoAPrice = stoi(fields[3]);
		else
			fromBtoAPrice = -1;
	}
};

class FlightCalculator
{
	Graph flights;
	List<string> citiesHash;


	List<CityConnection> GetConnectionsFromFile(string fileName);

public:

	FlightCalculator(string fileName);

	vector<string> GetAllCities();
	bool GetMinimalPriceFromTo(string from, string to, int *price);

	friend ostream & operator << (ostream &out, FlightCalculator &calc)
	{
		// cities print
		for (int i = 0; i < calc.citiesHash.getLength(); i++)
		{
			out.width(3);
			out.setf(ios::left);
			out << i << '|';
			out.setf(ios::left);
			out << *calc.citiesHash.at(i);
			out << endl;
		}
		out << endl;

		// edges print
		out << calc.flights;

		return out;
	}
};