#include "stdafx.h"
#include "flightCalculator.h"

List<CityConnection> FlightCalculator::GetConnectionsFromFile(string fileName)
{
	List<CityConnection> result = List<CityConnection>();

	ifstream file;

	file.open(fileName);
	if (!file)
		throw exception("Unable to open file datafile.txt");

	string line;

	vector<string> fields = vector<string>(4);
	while (getline(file, line))
	{
		int end;

		for (int i = 0; i < 3; i++)
		{
			end = line.find(';');

			fields[i] = line.substr(0, end);
			line = line.substr(end + 1, line.size() - end);
		}

		fields[3] = line;

		result.push_back(new CityConnection(fields));
	}

	file.close();

	return result;
}

FlightCalculator::FlightCalculator(string sourceFileName)
{
	List<CityConnection> cityConnections = GetConnectionsFromFile(sourceFileName);

	/*Generating citiesHash*/
	citiesHash = List<string>();

	for (int i = 0; i < cityConnections.getLength(); i++)
	{
		bool isExistCityA = false;
		bool isExistCityB = false;
		for (int j = 0; j < citiesHash.getLength(); j++)
		{
			if (*citiesHash.at(j) == cityConnections.at(i)->cityA)
				isExistCityA = true;

			if (*citiesHash.at(j) == cityConnections.at(i)->cityB)
				isExistCityB = true;
		}

		if (!isExistCityA)
			citiesHash.push_back(new string(cityConnections.at(i)->cityA));

		if (!isExistCityB)
			citiesHash.push_back(new string(cityConnections.at(i)->cityB));
	}
	/*Generating citiesHash*/

	/*Generating flights*/
	flights = Graph(citiesHash.getLength());

	for (int i = 0; i < cityConnections.getLength(); i++)
	{
		CityConnection curr = *cityConnections.at(i);

		if (curr.fromAtoBPrice != NotAvailable)
			flights.AddEdge(citiesHash.find(&curr.cityA), citiesHash.find(&curr.cityB), curr.fromAtoBPrice);

		if (curr.fromBtoAPrice != NotAvailable)
			flights.AddEdge(citiesHash.find(&curr.cityB), citiesHash.find(&curr.cityA), curr.fromBtoAPrice);
	}
	/*Generating flights*/
}

vector<string> FlightCalculator::GetAllCities()
{
	vector<string> result = vector<string>(citiesHash.getLength());

	for (int i = 0; i < result.size(); i++)
		result[i] = *citiesHash.at(i);

	return result;
}

bool FlightCalculator::GetMinimalPriceFromTo(string from, string to, int *price)
{
	int result = flights.NearestWayFromTo(citiesHash.find(&from), citiesHash.find(&to));

	if (result != INF)
	{
		*price = result;
		return true;
	}
	else
		return false;
}
