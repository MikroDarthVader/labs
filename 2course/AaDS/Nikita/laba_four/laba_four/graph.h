#pragma once
#include"stdafx.h"

#define INF (1 << 31) - 1
class Graph
{
	unsigned int vertsNum;
	vector<vector<int>> edges;
public:
	Graph(int numOfVertices = 0);

	int NearestWayFromTo(unsigned int fromVert, unsigned int toVert);
	void AddEdge(unsigned int startVert, unsigned int endVert, int weight);
	void DeleteEdge(unsigned int startVert, unsigned int endVert);
	int GetWeight(unsigned int startVert, unsigned int endVert);

	friend ostream & operator << (ostream &out, const Graph &graph)
	{
		out << "    ";
		for (int i = 0; i < graph.vertsNum; i++)
		{
			out.width(6);
			out.setf(ios::left);
			out << i;
		}
		out << endl;

		for (int i = 0; i < graph.vertsNum; i++)
		{
			out.width(3);
			out.setf(ios::left);
			out << i << '|';
			for (int j = 0; j < graph.vertsNum; j++)
			{
				out.width(5);
				out.setf(ios::left);
				if (graph.edges[i][j] == INF)
					out << "INF" << '|';
				else
					out << graph.edges[i][j] << '|';
			}
			if (i != graph.vertsNum);
			out << endl;
		}

		return out;
	}
};