#include "stdafx.h"
#include "graph.h"

Graph::Graph(int numOfVertices)
{
	vertsNum = numOfVertices;

	edges = vector<vector<int>>(vertsNum);

	for (int i = 0; i < edges.size(); i++)
	{
		edges[i] = vector<int>(vertsNum);

		for (int j = 0; j < edges[i].size(); j++)
			edges[i][j] = INF;
	}
}

int Graph::NearestWayFromTo(unsigned int fromVert, unsigned int toVert)
{
	if (fromVert >= vertsNum || toVert >= vertsNum)
		throw invalid_argument("Index of vert out of range exception");

	vector<int> distance = vector<int>(vertsNum);

	//Initialization
	for (int i = 0; i < (int)edges.size(); i++)
	{
		distance[i] = INF;
	}
	distance[fromVert] = 0;

	for (int iter = 0; iter < (int)edges.size(); iter++)
	{
		for (int v = 0; v < (int)edges.size(); v++)
		{
			for (int i = 0; i < (int)edges[v].size(); i++)
			{
				if (distance[v] != INF && edges[v][i] != INF && distance[i] > distance[v] + edges[v][i])
				{
					distance[i] = distance[v] + edges[v][i];
				}
			}
		}
	}

	return distance[toVert];
}

void Graph::AddEdge(unsigned int startVert, unsigned int endVert, int weight)
{
	if (startVert >= vertsNum || endVert >= vertsNum)
		throw invalid_argument("Index of vert out of range exception");

	if (edges[startVert][endVert] == INF)
		edges[startVert][endVert] = weight;
}

void Graph::DeleteEdge(unsigned int startVert, unsigned int endVert)
{
	if (startVert >= vertsNum || endVert >= vertsNum)
		throw invalid_argument("Index of vert out of range exception");

	edges[startVert][endVert] = INF;
}

int Graph::GetWeight(unsigned int startVert, unsigned int endVert)
{
	if (startVert >= vertsNum || endVert >= vertsNum)
		throw invalid_argument("Index of vert out of range exception");

	return edges[startVert][endVert];
}