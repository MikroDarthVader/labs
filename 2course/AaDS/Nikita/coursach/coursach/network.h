#pragma once
#include"stdafx.h"

#include "SinglyLinkedList.h"

struct NetworkNode;
struct NetworkEdge;

struct NetworkNode
{
	char index;
	List<NetworkEdge> *outEdges;

	struct BFS
	{
		NetworkEdge* edgeFromPrevToThis;
		NetworkNode* prevNode;
		bool isNodeVisited;

		BFS()
		{
			prevNode = nullptr;
			edgeFromPrevToThis = nullptr;
			isNodeVisited = false;
		}
	};

	BFS bfs;

	NetworkNode(char index) : index(index), outEdges(new List<NetworkEdge>()), bfs()
	{}
};

struct NetworkEdge
{
	unsigned int currCapacity;
	unsigned int maxCapacity;

	NetworkNode *endNode;

	NetworkEdge(unsigned int maxCapacity, NetworkNode *endNode = nullptr) : currCapacity(maxCapacity), maxCapacity(maxCapacity), endNode(endNode)
	{}
};

class Network
{
public:
	NetworkNode *source;
	NetworkNode *stock;

	NetworkNode *Find(char index);
	List<NetworkEdge>* GetNearWayFromSourceToStock();

	Network();
	~Network();
	Network(string fileName);

	friend ostream & operator << (ostream &out, const Network &network)
	{
		if (network.source == nullptr)
			return out;

		List<NetworkNode> processingNodes = List<NetworkNode>();
		processingNodes.push_back(network.source);

		List<NetworkNode> checkedNodes = List<NetworkNode>();

		NetworkNode* checkNode = nullptr;
		while (processingNodes.getLength() != 0)
		{
			checkNode = processingNodes.pop_front();
			checkedNodes.push_back(checkNode);

			for (NetworkEdge* edge : *checkNode->outEdges)
			{
				out << checkNode->index << " " << edge->endNode->index << " " << edge->currCapacity << "/" << edge->maxCapacity << endl;

				if (edge->endNode->bfs.isNodeVisited == false)
				{
					edge->endNode->bfs.isNodeVisited = true;
					processingNodes.push_back(edge->endNode);
				}
			}
		}

		for (NetworkNode* node : processingNodes)
			node->bfs.isNodeVisited = false;

		for (NetworkNode* node : checkedNodes)
			node->bfs.isNodeVisited = false;

		processingNodes.clear(false);
		checkedNodes.clear(false);
		return out;
	}

	void AddEdge(char startNodeInd, char endNodeInd, int edgeCapacity);
	int MaxFlow();
	void Clear();
};

NetworkNode * Network::Find(char index)
{
	NetworkNode* result = nullptr;

	List<NetworkNode> processingNodes = List<NetworkNode>();
	processingNodes.push_back(source);

	List<NetworkNode> checkedNodes = List<NetworkNode>();

	NetworkNode* checkNode = nullptr;
	while (processingNodes.getLength() != 0)
	{
		checkNode = processingNodes.pop_front();
		checkedNodes.push_back(checkNode);

		if (checkNode->index == index)
		{
			result = checkNode;
			break;
		}

		for (NetworkEdge* edge : *checkNode->outEdges)
		{
			if (edge->endNode->bfs.isNodeVisited == false)
			{
				edge->endNode->bfs.isNodeVisited = true;
				processingNodes.push_back(edge->endNode);
			}
		}
	}

	for (NetworkNode* node : processingNodes)
		node->bfs.isNodeVisited = false;

	for (NetworkNode* node : checkedNodes)
		node->bfs.isNodeVisited = false;

	processingNodes.clear(false);
	checkedNodes.clear(false);

	return result;
}

Network::Network()
{
	source = nullptr;
	stock = nullptr;
}

Network::~Network()
{
	Clear();
}

Network::Network(string fileName)
{
	source = nullptr;
	stock = nullptr;

	ifstream file;

	file.open(fileName);
	if (!file)
		throw exception(("Unable to open file '" + fileName + "'").c_str());

	string line;

	vector<string> fields = vector<string>(3);
	while (getline(file, line))
	{
		size_t end;

		for (int i = 0; i < 2; i++)
		{
			end = line.find(' ');

			fields[i] = line.substr(0, end);
			line = line.substr(end + 1, line.size() - end);
		}

		fields[2] = line;

		AddEdge(fields[0][0], fields[1][0], stoi(fields[2]));
	}

	file.close();
}

void Network::AddEdge(char startNodeInd, char endNodeInd, int edgeCapacity)
{
	NetworkNode *start, *end;
	if (source == nullptr)
	{
		source = start = new NetworkNode(startNodeInd);
		stock = end = new NetworkNode(endNodeInd);
	}
	else
	{
		start = Find(startNodeInd);

		if (start == nullptr)
			throw invalid_argument("start node doesn't exist in network");

		end = Find(endNodeInd);

		if (end == nullptr)
			end = new NetworkNode(endNodeInd);

		if (start == stock)
			stock = end;
	}

	start->outEdges->push_back(new NetworkEdge(edgeCapacity, end));
}

List<NetworkEdge>* Network::GetNearWayFromSourceToStock()
{
	if (source == nullptr)
		return nullptr;

	List<NetworkEdge>* result = nullptr;

	List<NetworkNode> processingNodes = List<NetworkNode>();
	processingNodes.push_back(source);
	source->bfs.isNodeVisited = true;

	List<NetworkNode> checkedNodes = List<NetworkNode>();

	NetworkNode* checkNode = nullptr;
	while (processingNodes.getLength() != 0)
	{
		checkNode = processingNodes.pop_front();
		checkedNodes.push_back(checkNode);

		for (NetworkEdge* edge : *checkNode->outEdges)
		{
			if (edge->endNode->bfs.isNodeVisited == false && edge->currCapacity != 0)
			{
				edge->endNode->bfs.isNodeVisited = true;
				edge->endNode->bfs.prevNode = checkNode;
				edge->endNode->bfs.edgeFromPrevToThis = edge;
				processingNodes.push_back(edge->endNode);
			}
		}

		if (checkNode == stock) // we find nearest way
		{
			result = new List<NetworkEdge>();


			for (NetworkNode* currNode = checkNode; currNode->bfs.prevNode != nullptr; currNode = currNode->bfs.prevNode)
				result->push_front(currNode->bfs.edgeFromPrevToThis);
		}
	}

	for (NetworkNode* node : processingNodes)
	{
		node->bfs.isNodeVisited = false;
		node->bfs.prevNode = nullptr;
		node->bfs.edgeFromPrevToThis = nullptr;
	}

	for (NetworkNode* node : checkedNodes)
	{
		node->bfs.isNodeVisited = false;
		node->bfs.prevNode = nullptr;
		node->bfs.edgeFromPrevToThis = nullptr;
	}

	processingNodes.clear(false);
	checkedNodes.clear(false);

	return result;
}

int Network::MaxFlow()
{
	int result = 0;
	source = source;
	List<NetworkEdge>* nearestWay;
	while ((nearestWay = GetNearWayFromSourceToStock()) != nullptr)
	{
		unsigned int minFlow = nearestWay->at(0)->currCapacity;
		for (NetworkEdge* edge : *nearestWay)
		{
			if (minFlow > edge->currCapacity)
				minFlow = edge->currCapacity;
		}

		for (NetworkEdge* edge : *nearestWay)
		{
			edge->currCapacity -= minFlow;
		}

		result += minFlow;

		cout << "residual network:" << endl << *this << endl;

		nearestWay->clear(false);
		delete nearestWay;
	}

	return result;
}

void Network::Clear()
{
	if (source == nullptr)
		return;

	List<NetworkNode> processingNodes = List<NetworkNode>();
	processingNodes.push_back(source);

	List<NetworkNode> checkedNodes = List<NetworkNode>();

	NetworkNode* checkNode = nullptr;
	while (processingNodes.getLength() != 0)
	{
		checkNode = processingNodes.pop_front();
		checkedNodes.push_back(checkNode);

		for (NetworkEdge* edge : *checkNode->outEdges)
		{
			if (edge->endNode->bfs.isNodeVisited == false)
			{
				edge->endNode->bfs.isNodeVisited = true;
				processingNodes.push_back(edge->endNode);
			}
		}

		checkNode->outEdges->clear();
	}
	checkedNodes.clear();

	source = nullptr;
	stock = nullptr;
}
