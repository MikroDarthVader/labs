#include "stdafx.h"
#include "CppUnitTest.h"
#include "../coursach/network.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{
	TEST_CLASS(UnitTests)
	{
	public:
		Network *net;

		//before each test
		TEST_METHOD_INITIALIZE(setUp)
		{
			net = new Network("D:/Downloads/stud/cbl/progalet/labs/2course/AaDS/Nikita/coursach/UnitTests/networkBase.txt");
		}
		// after each test
		TEST_METHOD_CLEANUP(cleanUp)
		{
			delete net;
		}

		TEST_METHOD(graphGeneratingTest)
		{
			List<NetworkNode> processingNodes = List<NetworkNode>();
			processingNodes.push_back(net->source);

			List<NetworkNode> checkedNodes = List<NetworkNode>();

			NetworkNode* checkNode = nullptr;
			while (processingNodes.getLength() != 0)
			{
				checkNode = processingNodes.pop_front();
				checkedNodes.push_back(checkNode);

				//Assert::IsTrue((checkNode == net->stock) == (checkNode->outEdges->getLength() == 0));

				for (NetworkEdge* edge : *checkNode->outEdges)
				{
					Assert::IsTrue(edge->endNode != net->source && edge->currCapacity >= 0 && edge->maxCapacity >= 0);

					if (edge->endNode->bfs.isNodeVisited == false)
					{
						edge->endNode->bfs.isNodeVisited = true;
						processingNodes.push_back(edge->endNode);
					}
				}
			}

			for (NetworkNode* node : processingNodes)
				node->bfs.isNodeVisited = false;

			for (NetworkNode* node : checkedNodes)
				node->bfs.isNodeVisited = false;

			processingNodes.clear(false);
			checkedNodes.clear(false);
		}

		TEST_METHOD(findTest)
		{
			NetworkNode* node = net->Find('Q');
			Assert::IsTrue(node != nullptr && node->index == 'Q');
		}

		TEST_METHOD(maxFlowTest)
		{
			Assert::IsTrue(net->MaxFlow() == 5);
		}
	};
}