#pragma once
#include "graph.h"
#include "Math.h"

class GraphDrawer
{
private:
	Graph* g;
	vector<vec2> coords;

	SDL_Renderer* ren;
	vec2 winSize;
	vec2 indent;
	int vertexSize;
public:
	GraphDrawer(Graph* _g, SDL_Renderer *_ren, vec2 _winSize, vec2 _indent, int _vertexSize);

	void DrawEdges(rgb col);
	void DrawEdgesWay(rgb col, vector<int>* vertices);
	void DrawVetices(rgb col, bool showSequience);
};