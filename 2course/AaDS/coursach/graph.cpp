#include"graph.h"
#include"Math.h"

void swap(int& v1, int& v2)
{
	v2 -= v1;
	v1 += v2;
	v2 = v1 - v2;
}

int getBitsetInd(int v1, int v2)
{
	assert(v1 != v2);

	if (v1 < v2)
		swap(v1, v2);

	return ((v1 - 1)*v1) / 2 + v2;
}


Graph::Graph(int verticesNum, int edgesNum)
{
	int maxEdges = (verticesNum * (verticesNum - 1)) / 2;
	assert(edgesNum <= maxEdges);

	this->verticesNum = verticesNum;

	edges = vector<bool>(maxEdges);

	vector<int> toSet = vector<int>(maxEdges);
	for (int i = 0; i < maxEdges; i++)
	{
		toSet[i] = i;
		edges[i] = false;
	}

	int toSetNum = maxEdges;
	int rnd;
	for (int i = 0; i < edgesNum; i++)
	{
		rnd = Rnd(0, toSetNum - 1);
		edges[toSet[rnd]] = true;

		toSetNum -= 1;
		swap(toSet[rnd], toSet[toSetNum]);
	}
}

bool Graph::hasEdge(int v1, int v2)
{
	return edges[getBitsetInd(v1, v2)];
}

void Graph::setEdge(int v1, int v2, bool val)
{
	edges[getBitsetInd(v1, v2)] = val;
}

void Graph::Print()
{
	cout << "   ";
	for (int i = 0; i < verticesNum; i++)
		printf("%-3d", i);
	cout << endl;

	for (int i = 0; i < verticesNum; i++)
	{
		printf("%-3d", i);
		for (int j = 0; j < i; j++)
			printf("%d  ", hasEdge(i, j) ? 1 : 0);
		cout << endl;
	}
}

int CompareVerts(void * c, const void * l, const void * r)
{
	int left = *(int *)l;
	int right = *(int *)r;
	int *context = (int*)c;

	if (context[left] < context[right]) return -1;
	if (context[left] > context[right]) return 1;

	return 0;
}

bool Graph::GamilWaySearchIter(int vertex, vector<int> &way, vector<bool> &vertices, int depth, int reachedEdgesNum)
{
	bool nextNodeRes = false;

	vertices[vertex] = true;
	reachedEdgesNum++;

	if (reachedEdgesNum == verticesNum)
	{
		nextNodeRes = true;
		way.resize(depth + 1);
	}

	for (int i = 0; i < verticesNum && !nextNodeRes; i++)
	{
		if (vertex != i && hasEdge(vertex, i) && !vertices[i])
			nextNodeRes = GamilWaySearchIter(i, way, vertices, depth + 1, reachedEdgesNum);
	}

	if (nextNodeRes)
		way[depth] = vertex;

	vertices[vertex] = false;
	reachedEdgesNum--;

	return nextNodeRes;
}

bool Graph::GetGamilWay(vector<int> &way)
{
	vector<bool> vertices = vector<bool>(verticesNum);
	for (int i = 0; i < vertices.size(); i++)
		vertices[i] = false;

	int* sortedVertices = new int[verticesNum];
	int* edgesNums = new int[verticesNum];

	/*generate num of edges array*/
	int edgesNum;
	for (int i = 0; i < verticesNum; i++)
	{
		sortedVertices[i] = i;

		edgesNum = 0;
		for (int j = 0; j < verticesNum; j++)
			if (i != j && hasEdge(i, j))
				edgesNum++;
		edgesNums[i] = edgesNum;
	}
	/*generate num of edges array*/

	qsort_s(sortedVertices, verticesNum, sizeof(int), CompareVerts, edgesNums);

	if ((verticesNum > 1 && edgesNums[sortedVertices[0]] == 0) ||
		(verticesNum > 3 && edgesNums[sortedVertices[3]] == 1)) //optimizing most of big graphs
		return false;

	int searchRes = false;
	for (int i = 0; i < verticesNum && !searchRes; i++)
	{
		searchRes = GamilWaySearchIter(i, way, vertices);
	}

	delete[] sortedVertices;
	delete[] edgesNums;
	return searchRes;
}
