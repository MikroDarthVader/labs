#include "pch.h"

#ifndef Rnd
#define Rnd( min, max) (rand()) % (max + 1 - min) + min;
#endif

struct vec2 {
	int x, y;
	vec2(int x = 0, int y = 0) { this->x = x; this->y = y; }
	vec2 operator/(int scalar) { return vec2(x / scalar, y / scalar); }
	vec2 operator*(int scalar) { return vec2(x * scalar, y * scalar); }
};

struct rgb {
	int r, g, b;
	void set(int _r, int _g, int _b) { r = _r, g = _g, b = _b; }
};