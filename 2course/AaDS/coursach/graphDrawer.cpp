#pragma once
#include "graphDrawer.h"

GraphDrawer::GraphDrawer(Graph* _g, SDL_Renderer *_ren, vec2 _winSize, vec2 _indent, int _vertexSize) :
	g(_g),
	ren(_ren),
	winSize(_winSize),
	indent(_indent),
	vertexSize(_vertexSize),
	coords(g->getVerticesNum())
{
	for (int i = 0; i < g->getVerticesNum(); i++)
	{
		coords[i].x = Rnd(indent.x, winSize.x - indent.x);
		coords[i].y = Rnd(indent.y, winSize.y - indent.y);
	}
}

void GraphDrawer::DrawEdges(rgb col)
{
	SDL_SetRenderDrawColor(ren, col.r, col.g, col.b, 255);
	for (int i = 0; i < g->getVerticesNum(); i++)
		for (int j = 0; j < i; j++)
			if (g->hasEdge(i, j))
				SDL_RenderDrawLine(ren,
					coords[i].x,
					coords[i].y,
					coords[j].x,
					coords[j].y);
}


void GraphDrawer::DrawEdgesWay(rgb col, vector<int>* vertices)
{
	SDL_SetRenderDrawColor(ren, col.r, col.g, col.b, 255);
	for (int i = 1; i < vertices->size(); i++)
		SDL_RenderDrawLine(ren,
			coords[(*vertices)[i]].x,
			coords[(*vertices)[i]].y,
			coords[(*vertices)[i - 1]].x,
			coords[(*vertices)[i - 1]].y);
}

void GraphDrawer::DrawVetices(rgb col, bool showSequience)
{
	SDL_Rect r;
	int vNum = g->getVerticesNum();
	int sizeInc;
	for (int i = 0; i < vNum; i++)
	{
		sizeInc = showSequience ? i : 0;
		SDL_SetRenderDrawColor(ren, col.r, col.g, col.b, 255);
		r.w = r.h = vertexSize + sizeInc;
		r.x = coords[i].x - (vertexSize + sizeInc) / 2;
		r.y = coords[i].y - (vertexSize + sizeInc) / 2;
		SDL_RenderDrawRect(ren, &r);
	}
}