#include "pch.h"
#include "graphDrawer.h"
#define PRINT
#define DRAW

void Render(Graph *g, vector<int>* gamilWay)
{
	SDL_Event event;

	SDL_Init(SDL_INIT_VIDEO);
	SDL_Window * window = SDL_CreateWindow("coursach",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 960, 680, 0);
	SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, 0);

	vec2 winSize;
	SDL_GetWindowSize(window, &(winSize.x), &(winSize.y));

	SDL_SetRenderDrawColor(renderer, 20, 20, 20, 255);
	SDL_RenderClear(renderer);

	/*render*/
	GraphDrawer gd(g, renderer, winSize, { 10 , 10 }, 4);
	rgb col = { 100, 0, 150 };
	gd.DrawEdges(col);

	col.set(20, 255, 20);
	if (gamilWay)
		gd.DrawEdgesWay(col, gamilWay);

	col.set(255, 0, 0);
	gd.DrawVetices(col, g->getVerticesNum() < 100);
	/*render*/

	SDL_RenderPresent(renderer);

	do
	{
		SDL_PollEvent(&event);
	} while (event.type != SDL_QUIT);

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

int main(int argc, char ** argv)
{
	srand(time(NULL));
	Graph g(5, 6);

	vector<int> way = vector<int>(10);
	bool gamilWayExist = g.GetGamilWay(way);

#if defined(PRINT)
	g.Print();
	cout << endl;

	if (gamilWayExist)
	{
		cout << "gamil way: ";
		for (int i = 0; i < way.size(); i++)
			cout << way[i] << " ";
	}
	else
		cout << "no gamil way";
#endif

#if defined(DRAW)
	Render(&g, gamilWayExist ? &way : nullptr);
#endif
	return 0;
}
