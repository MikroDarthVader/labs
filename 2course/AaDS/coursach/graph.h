#pragma once
#include "pch.h"

class Graph
{
private:
	vector<bool> edges;
	int verticesNum;

	bool GamilWaySearchIter(int vertex, vector<int> &way, vector<bool> &vertices, int depth = 0, int reachedEdgesNum = 0);
public:
	Graph(int verticesNum, int edgesNum);

	int getVerticesNum() { return verticesNum; }
	bool hasEdge(int v1, int v2);
	void setEdge(int v1, int v2, bool val = true);
	void Print();
	bool Graph::GetGamilWay(vector<int> &way);
};