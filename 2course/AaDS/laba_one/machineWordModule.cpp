
#include "stdafx.h"

void printValues_machineWordModule(unsigned long long int values)
{
	unsigned long long int mask = 1;
	for (int i = 0; i < 52; i++)
	{
		mask = values & (1 << i);
		if ((values & (1 << i)) != 0)
			cout << getCharByNum(i);
	}
}

void printIntBits(unsigned long long int num, int minLenght, int maxLenght, int curLenght)
{
	if ((num || curLenght < minLenght) && curLenght < maxLenght)
	{
		printIntBits(num >> 1, minLenght, maxLenght, ++curLenght);
		num &= 1;
		cout << num;
	}
}

unsigned long long int A = 0, B = 0, C = 0, D = 0;
unsigned long long int *vars[4] = { &A,&B,&C,&D };

char* possibleValues_machineWordModule = 0;
void setRandomValues_machineWordModule(int min, int max)
{
	if (!possibleValues_machineWordModule)
		possibleValues_machineWordModule = (char *)malloc(53);

	for (int i = 0; i < 52; i++)
		possibleValues_machineWordModule[i] = getCharByNum(i);
	possibleValues_machineWordModule[52] = 0;

	for (int i = 0; i < 4; i++)
	{
		*vars[i] = 0;

		int rnd = Random(min, max);

		int maxPosChar = 51;
		for (int j = 0; j < rnd; j++)
		{
			int charPos = Random(0, maxPosChar);

			*vars[i] |= 1 << getNumByChar(possibleValues_machineWordModule[charPos]);

			char temp = possibleValues_machineWordModule[charPos];
			possibleValues_machineWordModule[charPos] = possibleValues_machineWordModule[maxPosChar];
			possibleValues_machineWordModule[maxPosChar] = temp;
			maxPosChar--;
		}
	}

	free(possibleValues_machineWordModule);
	possibleValues_machineWordModule = 0;
}

void resetModule_machineWordModule()
{
}

void printGeneretedValues_machineWordModule()
{
	cout << "A:                    ";
	printValues_machineWordModule(A);
	cout << endl;

	cout << "B:                    ";
	printValues_machineWordModule(B);
	cout << endl;

	cout << "C:                    ";
	printValues_machineWordModule(C);
	cout << endl;

	cout << "D:                    ";
	printValues_machineWordModule(D);
	cout << endl;
}

unsigned long long int result;

void calculate_machineWordModule()
{
	result = A & ~B | C & D;
}

void printLastResult_machineWordModule()
{
	cout << "A & !B:               ";
	printValues_machineWordModule(A & ~B);
	cout << endl;

	cout << "C & D:                ";
	printValues_machineWordModule(C & D);
	cout << endl;

	cout << "A & !B | C & D:       ";
	printValues_machineWordModule(result);
	cout << endl;
}

void freeAllVariable_machineWordModule()
{

}

const struct Module machineWordModule =
{
	setRandomValues_machineWordModule,
	calculate_machineWordModule,
	printGeneretedValues_machineWordModule,
	printLastResult_machineWordModule,
	resetModule_machineWordModule,
	freeAllVariable_machineWordModule
};