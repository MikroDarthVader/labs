
#include "stdafx.h"

bool *A = (bool *)malloc(sizeof(bool) * ValuesSize),
*B = (bool *)malloc(sizeof(bool) * ValuesSize),
*C = (bool *)malloc(sizeof(bool) * ValuesSize),
*D = (bool *)malloc(sizeof(bool) * ValuesSize);
bool **vars[4] = { &A,&B,&C,&D };
bool *result = (bool *)malloc(sizeof(bool) * ValuesSize);

char* possibleValues_bitArrayModule = 0;
void setRandomValues_bitArrayModule(int min, int max)
{
	if (!possibleValues_bitArrayModule)
		possibleValues_bitArrayModule = (char *)malloc(53);

	for (int i = 0; i < ValuesSize; i++)
		possibleValues_bitArrayModule[i] = getCharByNum(i);
	possibleValues_bitArrayModule[ValuesSize] = 0;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < ValuesSize; j++)
		{
			(*vars[i])[j] = false;
		}

		int rnd = Random(min, max);

		int maxPosChar = 51;
		for (int j = 0; j < rnd; j++)
		{
			int charPos = Random(0, maxPosChar);

			(*vars[i])[getNumByChar(possibleValues_bitArrayModule[charPos])] = true;

			char temp = possibleValues_bitArrayModule[charPos];
			possibleValues_bitArrayModule[charPos] = possibleValues_bitArrayModule[maxPosChar];
			possibleValues_bitArrayModule[maxPosChar] = temp;
			maxPosChar--;
		}
	}

	free(possibleValues_bitArrayModule);
	possibleValues_bitArrayModule = 0;
}

void resetModule_bitArrayModule()
{
	for (int i = 0; i < ValuesSize; i++)
		result[i] = false;
}

void calculate_bitArrayModule()
{
	for (int i = 0; i < ValuesSize; i++)
		result[i] = A[i] && !B[i] || C[i] && D[i];
}

void printValues_bitArrayModule(bool *Values)
{
	for (int i = 0; i < ValuesSize; i++)
	{
		if (Values[i])
			cout << getCharByNum(i);
	}
}

void printGeneretedValues_bitArrayModule()
{
	cout << "A:                    ";
	printValues_bitArrayModule(A);
	cout << endl;

	cout << "B:                    ";
	printValues_bitArrayModule(B);
	cout << endl;

	cout << "C:                    ";
	printValues_bitArrayModule(C);
	cout << endl;

	cout << "D:                    ";
	printValues_bitArrayModule(D);
	cout << endl;
}

void LandR_BitArrayModule(bool *left, bool *right, bool **result) // O(n)
{
	for (int i = 0; i < ValuesSize; i++)
		(*result)[i] = left[i] && right[i];
}

void LandnotR_BitArrayModule(bool *left, bool *right, bool **result) // O(n)
{
	for (int i = 0; i < ValuesSize; i++)
		(*result)[i] = left[i] && !right[i];
}


void printLastResult_bitArrayModule()
{
	bool *tmp = (bool *)malloc(sizeof(bool) * ValuesSize);

	LandnotR_BitArrayModule(A, B, &tmp);
	cout << "A & !B:               ";
	printValues_bitArrayModule(tmp);
	cout << endl;

	LandR_BitArrayModule(C, D, &tmp);
	cout << "C & D:                ";
	printValues_bitArrayModule(tmp);
	cout << endl;

	cout << "A & !B | C & D:       ";
	printValues_bitArrayModule(result);
	cout << endl;
	free(tmp);
}

void freeAllVariable_bitArrayModule()
{
	for (int i = 0; i < 4; i++)
	{
		free(*(vars[i]));
	}

	free(result);
}

const struct Module bitArrayModule =
{
	setRandomValues_bitArrayModule,
	calculate_bitArrayModule,
	printGeneretedValues_bitArrayModule,
	printLastResult_bitArrayModule,
	resetModule_bitArrayModule,
	freeAllVariable_bitArrayModule
};