
#include "stdafx.h"

bool Contain_ArrayModule(char* str, char c)
{
	for (char* i = str; *i != 0; i++)
	{
		if (*i == c)
			return true;
	}
	return false;
}

void LandR_ArrayModule(char* left, char* right, char** result) // O(n^2)
{
	bool add = false;
	int size = 0;

	for (char* i = left; *i != 0; i++)
	{
		add = false;
		for (char* j = right; *j != 0 && !add; j++)
			if (*i == *j)
				add = true;

		if (add)
		{
			size++;
			(*result)[size] = 0;
			(*result)[size - 1] = *i;
		}
	}
}

void LandnotR_ArrayModule(char* left, char* right, char** result) //O(n^2)
{
	int pos = 0;

	for (char* i = left; *i != 0; i++)
	{
		if (!Contain_ArrayModule(right, *i))
		{
			pos++;
			(*result)[pos - 1] = *i;
		}
	}

	(*result)[pos] = 0;
}

void LorR_ArrayModule(char* left, char* right, char** result) //O(n^2)
{
	int pos = 0;

	for (char* i = left; *i != 0; i++)
	{
		pos++;
		(*result)[pos - 1] = *i;
		(*result)[pos] = 0;
	}

	for (char* i = right; *i != 0; i++)
	{
		if (!Contain_ArrayModule(*result, *i))
		{
			pos++;
			(*result)[pos - 1] = *i;
			(*result)[pos] = 0;
		}
	}
}

char *A = 0, *B = 0, *C = 0, *D = 0;
char **vars[4] = { &A,&B,&C,&D };

char* aandnotb = (char *)malloc(53);
char* candd = (char *)malloc(53);
char* result = (char *)malloc(53);

char* possibleValues_ArrayModule = 0;
void setRandomValues_ArrayModule(int min, int max)
{
	if (!possibleValues_ArrayModule)
		possibleValues_ArrayModule = (char *)malloc(53);

	for (int i = 0; i < 52; i++)
		possibleValues_ArrayModule[i] = getCharByNum(i);
	possibleValues_ArrayModule[52] = 0;

	for (int j = 0; j < 4; j++)
	{
		int rnd = Random(min, max);

		if (*(vars[j]))
			free(*(vars[j]));

		*(vars[j]) = (char*)malloc(rnd + 1);

		int maxPosChar = 51;
		for (int i = 0; i < rnd; i++)
		{
			int charPos = Random(0, maxPosChar);
			(*(vars[j]))[i] = possibleValues_ArrayModule[charPos];

			char temp = possibleValues_ArrayModule[charPos];
			possibleValues_ArrayModule[charPos] = possibleValues_ArrayModule[maxPosChar];
			possibleValues_ArrayModule[maxPosChar] = temp;
			maxPosChar--;
		}

		(*(vars[j]))[rnd] = 0;
	}

	free(possibleValues_ArrayModule);
	possibleValues_ArrayModule = 0;
}

void calculate_ArrayModule()
{
	LandnotR_ArrayModule(A, B, &aandnotb);
	LandR_ArrayModule(C, D, &candd);
	LorR_ArrayModule(aandnotb, candd, &result);
}

void resetModule_ArrayModule()
{
}

void freeAllVariable_ArrayModule()
{
	for (int i = 0; i < 4; i++)
	{
		if (*(vars[i]))
			free(*(vars[i]));
	}

	free(aandnotb);
	free(candd);
	free(result);
}

void printGeneretedValues_ArrayModule()
{
	cout << "A:                    " << A << endl
		<< "B:                    " << B << endl
		<< "C:                    " << C << endl
		<< "D:                    " << D << endl;
}

void printLastResult_ArrayModule()
{
	cout << "A & !B:               " << aandnotb << endl
		<< "C & D:                " << candd << endl
		<< "A & !B | C & D:       " << result << endl
		;
}

const struct Module ArrayModule =
{
	setRandomValues_ArrayModule,
	calculate_ArrayModule,
	printGeneretedValues_ArrayModule,
	printLastResult_ArrayModule,
	resetModule_ArrayModule,
	freeAllVariable_ArrayModule
};