
#include "stdafx.h"

void AddToList(PointerList *self, void *record)
{
	PointerListNode *node = self->first;
	if (!node)
	{
		node = (PointerListNode*)malloc(sizeof(PointerListNode));
		self->first = node;
		node->prev = NULL;
	}
	else
	{
		PointerListNode *prev = 0;
		for (; node; prev = node, node = node->next);

		node = (PointerListNode*)malloc(sizeof(PointerListNode));
		node->prev = prev;
		prev->next = node;
	}
	node->Value = record;
	node->next = NULL;
	self->last = node;
	self->Count++;
}

void* FindInList(PointerList self, void *record, Comparer cmp)
{
	for_each(self, ind, val)
		if (cmp(record, val) == 0)
			return val;
	return NULL;
}

bool RemoveFromList(PointerList *self, int index, void(*freeValue)(void *))
{
	if (index >= self->Count || index < 0)
		return false;

	PointerListNode *node;
	int i;
	for (i = 0, node = self->first; i != index; i++, node = node->next);

	if (node->prev)
		node->prev->next = node->next;
	else
		self->first = node->next;

	if (node->next)
		node->next->prev = node->prev;
	else
		self->last = node->prev;

	if (freeValue)
		freeValue(node->Value);
	free(node);

	self->Count--;

	return true;
}

void FreeList(PointerList self, void(*freeValue)(void *))
{
	PointerListNode *i = self.first;
	PointerListNode *prev;
	while (i)
	{
		if (freeValue)
			freeValue(i->Value);
		prev = i;
		i = prev->next;
		free(prev);
	}
	self.Count = 0;
}

void SortList(PointerList *self, Comparer cmp, int direction)
{
	if (!cmp)
		return;

	void *tmp;
	PointerListNode *i, *j;
	for (i = self->first; i != NULL; i = i->next)
		for (j = i->next; j != NULL; j = j->next)
			if (direction * cmp(i->Value, j->Value) < 0)
			{
				tmp = i->Value;
				i->Value = j->Value;
				j->Value = tmp;
			}
}

void *GetListItemByIndex(PointerList self, int index)
{
	for_each(self, ind, val)
		if (ind == index)
			return val;
	return NULL;
}

PointerList CopyList(PointerList self, void *(*CopyValue)(void*))
{
	PointerList result = EmptyPointerList;

	for_each(self, ind, val)
	{
		List.Add(&result, CopyValue ? CopyValue(val) : val);
	}

	return result;
}

const struct list List = {
	AddToList,
	FindInList,
	RemoveFromList,
	FreeList,
	SortList,
	GetListItemByIndex,
	CopyList
};