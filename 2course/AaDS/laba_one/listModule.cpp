
#include "stdafx.h"

void *CopyChar(void *c)
{
	char *res = (char *)malloc(1);
	*res = *(char *)c;
	return res;
}

bool Contain_ListModule(PointerList str, char c)
{
	for_each(str, ind, val)
	{
		if (*(char *)val == c)
			return true;
	}

	return false;
}

void LandR_ListModule(PointerList left, PointerList right, PointerList* result)
{
	char *Value = 0;

	for_each(left, l_ind, l_val)
	{
		for_each(right, r_ind, r_val)
		{
			if (*(char *)l_val == *(char *)r_val)
				List.Add(result, CopyChar(l_val));
		}
	}
}

void LandnotR_ListModule(PointerList left, PointerList right, PointerList *result)
{
	bool ValReserched = false;

	for_each(left, l_ind, l_val)
	{
		for_each(right, r_ind, r_val)
			if (*(char *)l_val == *(char *)r_val)
				ValReserched = true;

		if (!ValReserched)
			List.Add(result, CopyChar(l_val));
		else
			ValReserched = false;
	}
}

void LorR_ListModule(PointerList left, PointerList right, PointerList* result)
{
	{
		for_each(left, ind, val)
			List.Add(result, CopyChar(val));
	}
	//two for_each cycles can't work in the same access space
	{
		for_each(right, ind, val)
			if (!Contain_ListModule(*result, *(char *)val))
				List.Add(result, CopyChar(val));
	}
}

PointerList A = EmptyPointerList;
PointerList B = EmptyPointerList;
PointerList C = EmptyPointerList;
PointerList D = EmptyPointerList;
PointerList *vars[4] = { &A,&B,&C,&D };

char* possibleValues_ListModule = 0;
void setRandomValues_ListModule(int min, int max)
{
	if (!possibleValues_ListModule)
		possibleValues_ListModule = (char *)malloc(53);

	for (int i = 0; i < 52; i++)
		possibleValues_ListModule[i] = getCharByNum(i);
	possibleValues_ListModule[52] = 0;

	char *val;
	for (int j = 0; j < 4; j++)
	{
		int rnd = Random(min, max);

		while ((*vars[j]).Count)
			List.Remove(vars[j], 0, free);

		int maxPosChar = 51;
		for (int i = 0; i < rnd; i++)
		{
			val = (char*)malloc(1);
			int charPos = Random(0, maxPosChar);
			*val = possibleValues_ListModule[charPos];
			List.Add(vars[j], val);

			char temp = possibleValues_ListModule[charPos];
			possibleValues_ListModule[charPos] = possibleValues_ListModule[maxPosChar];
			possibleValues_ListModule[maxPosChar] = temp;
			maxPosChar--;
		}
	}

	free(possibleValues_ListModule);
	possibleValues_ListModule = 0;
}

PointerList Result = EmptyPointerList;
PointerList dandc = EmptyPointerList;
PointerList aandnotb = EmptyPointerList;

void calculate_ListModule()
{
	LandnotR_ListModule(A, B, &aandnotb);
	LandR_ListModule(D, C, &dandc);
	LorR_ListModule(aandnotb, dandc, &Result);
}

void resetModule__ListModule()
{
	List.Free(aandnotb, free);
	aandnotb = EmptyPointerList;

	List.Free(dandc, free);
	dandc = EmptyPointerList;

	List.Free(Result, free);
	Result = EmptyPointerList;
}

void printList(PointerList list)
{
	for_each(list, ind, val)
		cout << *(char *)val;
	cout << endl;
}

void printGeneretedValues_ListModule()
{
	cout << "A:                    ";
	printList(A);

	cout << "B:                    ";
	printList(B);

	cout << "C:                    ";
	printList(C);

	cout << "D:                    ";
	printList(D);
}

void printLastResult_ListModule()
{
	cout << "A & !B:               ";
	printList(aandnotb);

	cout << "C & D:                ";
	printList(dandc);

	cout << "A & !B + C & D:       ";
	printList(Result);
}

void freeAllVariable_ListModule()
{
	for (int i = 0; i < 4; i++)
	{
		List.Free(*(vars[i]), free);
		*(vars[i]) = EmptyPointerList;
	}

	resetModule__ListModule();
}

const struct Module ListModule =
{
	setRandomValues_ListModule,
	calculate_ListModule,
	printGeneretedValues_ListModule,
	printLastResult_ListModule,
	resetModule__ListModule,
	freeAllVariable_ListModule
};