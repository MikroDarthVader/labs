// ConsoleApplication1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

#define Array
//#define List
//#define BitArray
//#define MachineWord

#define TEST_ITERATIONS 1000000
#define MAX_SET_POWER 52
#define MIN_SET_POWER 10

uint64_t getTimeNanoSec()
{
	chrono::high_resolution_clock m_clock;
	return chrono::duration_cast<std::chrono::nanoseconds>(m_clock.now().time_since_epoch()).count();
}

long long int Random(long long int min, long long int max)
{
	uint64_t nanosec = getTimeNanoSec();
	return nanosec % (max + 1 - min) + min;
}

char getCharByNum(int num)
{
	return 'A' + num + (num > 25 ? 6 : 0);
}

int getNumByChar(char c)
{
	return c - 'A' - (c >= 'a' ? 6 : 0);
}

char getRandChar()
{
	int rnd = Random(0, 51);
	return getCharByNum(rnd);
}

int main()
{
	/*int activeModule;

#if defined(Array)
	activeModule = 0;
#elif defined(List)
	activeModule = 1;
#elif defined(BitArray)
	activeModule = 2;
#elif defined(MachineWord)
	activeModule = 3;
#else
	return;
#endif

	struct Module modules[4] =
	{
		ArrayModule,
		ListModule,
		bitArrayModule,
		machineWordModule
	};

	modules[activeModule].resetModule();
	modules[activeModule].setRandomValues(MIN_SET_POWER, MAX_SET_POWER); // 52 - max size
	modules[activeModule].printGeneratedValues();
	uint64_t sumTime = getTimeNanoSec();
	for (int i = 0; i < TEST_ITERATIONS; i++)
	{
		modules[activeModule].resetModule();
		modules[activeModule].calculate();
	}
	sumTime = getTimeNanoSec() - sumTime;

	modules[activeModule].printLastResult();

	cout << "Average elapsed time: " << (sumTime / TEST_ITERATIONS) / 1000000000.f << " sec (" << (sumTime / TEST_ITERATIONS) << " nanosec)" << endl << endl;

	modules[activeModule].freeModule();

	getchar();*/

	bool x, y, z;
	x = false;
	do
	{
		y = false;
		do
		{
			z = false;
			do
			{
				cout << x << " " << y << " " << z << " " << (!x && y && !z || !y && x || z) /*(z && (x == y))*/ << endl;
				z = !z;
			} while (z);
			y = !y;
		} while (y);
		x = !x;
	} while (x);

	system("pause");
}