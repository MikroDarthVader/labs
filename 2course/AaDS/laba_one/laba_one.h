#pragma once

long long int Random(long long int min, long long int max);
char getCharByNum(int num);
int getNumByChar(char c);
char getRandChar();

struct Module
{
	void(*setRandomValues)(int min, int max);
	void(*calculate)();
	void(*printGeneratedValues)();
	void(*printLastResult)();
	void(*resetModule)();
	void(*freeModule)();
};
