// laba_three.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include "Tree.h"

int main()
{
	Tree t(3);
	cout << "Tree generation code: ";
	t.RndFill(1, 2);
	cout << endl << endl;
	t.Print();
	cout << endl << "Depth-first search: ";
	int numOfNodes = t.GetNumOfNodes(3);
	cout << endl << "Number of tops with three all lower descendant: " << numOfNodes << endl;
	getchar();
	return 0;
}

