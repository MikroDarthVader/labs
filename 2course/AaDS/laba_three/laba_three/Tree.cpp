#include "stdafx.h"
#include "Tree.h"


uint64_t getTimeNanoSec()
{
	chrono::high_resolution_clock m_clock;
	return chrono::duration_cast<std::chrono::nanoseconds>(m_clock.now().time_since_epoch()).count();
}

long long int Random(long long int min, long long int max)
{
	uint64_t nanosec = getTimeNanoSec();
	return nanosec % (max + 1 - min) + min;
}

char getLetter(int ind)
{
	ind %= 52;
	ind += 65;
	if (ind > 90)
		ind += 6;
	return ind;
}

Tree::Tree(int maxDescsCount)
{
	this->maxDescsCount = maxDescsCount;
	NodeNum = 0;
	Root = nullptr;
}

Tree::~Tree()
{
}

void Tree::RndFill(int minDepth, int maxDepth)
{
	int tagNum = 0;
	depth = 0;
	Root = CreateRndNode(0, minDepth, maxDepth, &tagNum);
}

Tree::Node* Tree::CreateRndNode(int depth, int minDepth, int maxDepth, int *tagNum)
{
	Node* node = nullptr;

	int rand = Random(minDepth, maxDepth);

	if (depth > rand)
		cout << 0;
	else if (depth <= rand)
	{
		cout << 1;
		if (this->depth < depth)
			this->depth = depth;

		node = new Node(maxDescsCount);

		if (depth != maxDepth)
			for (int i = 0; i < maxDescsCount; i++)
				node->descendants[i] = CreateRndNode(depth + 1, minDepth, maxDepth, tagNum);

		node->tag = getLetter(*tagNum);
		(*tagNum)++;
	}

	return node;
}

void Tree::Print()
{
	int maxStrLen = 1;
	for (int i = 0; i < depth; i++)
		maxStrLen *= maxDescsCount;

	maxStrLen = maxStrLen * 2 + 1;

	char** output = new char*[depth + 1];

	int curDescsPow = 1;
	for (int d = 0; d <= depth; d++, curDescsPow *= maxDescsCount)
	{
		output[d] = new char[maxStrLen + 1];
		output[d][maxStrLen] = 0;
		int sectionSize = (maxStrLen - 1) / curDescsPow;

		for (int p = 0; p < maxStrLen; p++)
			if (p % sectionSize == 0)
				output[d][p] = '|';
			else
				output[d][p] = ' ';
	}

	insertTags(Root, 0, 1, 0, 0, output, maxStrLen);

	for (int i = 0; i <= depth; i++)
	{
		cout << output[i] << endl;
		delete[] output[i];
	}
}

void Tree::insertTags(Node* node, int curDepth, int curDescsPow, int ind, int parPos, char** output, int maxStrLen)
{
	int sectionSize = (maxStrLen - 1) / curDescsPow;
	int startPos = parPos + ind * sectionSize;
	output[curDepth][startPos + sectionSize / 2] = node->tag;

	for (int i = 0; i < maxDescsCount; i++)
		if (node->descendants[i])
			insertTags(node->descendants[i], curDepth + 1, curDescsPow * maxDescsCount, i, startPos, output, maxStrLen);
}

int Tree::GetNodeDescendants(Node* node, int* resultNumber, int numberOfDescendant)
{
	cout << node->tag << '_';

	int r = 0;
	for (int i = 0; i < node->descsCount; i++)
		r += (node->descendants[i] ? 1 + GetNodeDescendants(node->descendants[i], resultNumber, numberOfDescendant) : 0);

	if (r <= numberOfDescendant)
		(*resultNumber)++;

	return r;
}

int Tree::GetNumOfNodes(int numberOfDescendant)
{
	int result = 0;
	GetNodeDescendants(Root, &result, numberOfDescendant);

	return result;
}