#pragma once

class Tree
{
	struct Node
	{
		char tag;
		Node **descendants;
		int descsCount;

		Node(int descsCount)
		{
			this->descsCount = descsCount;
			descendants = new Node*[descsCount];
			for (int i = 0; i < descsCount; i++)
				descendants[i] = nullptr;
		}

		~Node()
		{
			for (int i = 0; i < descsCount; i++)
				if (descendants + i)
					delete descendants[i];
			delete[] descendants;
		}
	};

	Node *Root;
	int NodeNum;
	int maxDescsCount;
	int depth;

	Node* CreateRndNode(int depth, int minDepth, int maxDepth, int *tagNum);
	int GetNodeDescendants(Node * node, int * resultNumber, int numberOfDescendant);
	void insertTags(Node* node, int curDepth, int curDescsPow, int ind, int parPos, char** output, int maxStrLen);
	Tree(const Tree &);
	Tree(Tree &&);
	Tree operator = (const Tree &) = delete;
	Tree operator = (Tree &&) = delete;

public:
	Tree(int maxDescsCount);
	~Tree();
	void RndFill(int minDepth, int maxDepth);
	void Print();
	int GetNumOfNodes(int numberOfDescendant);
};

